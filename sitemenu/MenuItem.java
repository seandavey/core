package sitemenu;

import app.Stringify;
import db.object.DBField;

@Stringify
public class MenuItem {
	@DBField
	public String name;

	//--------------------------------------------------------------------------

	@Override
	public boolean
	equals(Object object) {
		return name.equals(((MenuItem)object).name);
	}
}
