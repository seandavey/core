package sitemenu;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import app.ClassTask;
import app.Module;
import app.Person;
import app.Request;
import app.Site;
import db.DBConnection;
import db.DeleteHook;
import db.NameValuePairs;
import db.Reorderable;
import db.SQL;
import db.Select;
import db.SelectRenderer;
import db.UpdateHook;
import db.ViewDef;
import db.column.Column;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.DBObjects;
import db.object.JSONField;
import pages.Page;
import pages.Pages;
import ui.NavBar;

public class SiteMenu extends Module implements DeleteHook, UpdateHook {
	@JSONField
	boolean					m_allow_users_to_customize = true;
	private Page			m_home_page;
	@JSONField
	private String			m_home_page_url = "/Home";
	DBObjects<MenuItem>		m_menu;

	//--------------------------------------------------------------------

	public
	SiteMenu() {
		m_required = true;
	}

	//--------------------------------------------------------------------

	@Override
	protected void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Other", "Other"), db);
	}

	//--------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("menu")
				.add(new JDBCColumn("name", Types.VARCHAR))
				.add(new JDBCColumn("_order_", Types.INTEGER));
			JDBCTable.adjustTable(table_def, true, false, db);
			table_def = new JDBCTable("menu_items")
				.add(new JDBCColumn("people"))
				.add(new JDBCColumn("name", Types.VARCHAR))
				.add(new JDBCColumn("_order_", Types.INTEGER));
			JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		String old_name = (String)previous_values.get("name");
		String new_name = nvp.getString("name");
		if (!old_name.equals(new_name)) {
			NameValuePairs nvp2 = new NameValuePairs();
			nvp2.set("name", new_name);
			r.db.update("menu", nvp2, "name=" + SQL.string(old_name));
			r.db.update("menu_items", nvp2, "name=" + SQL.string(old_name));
		}
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeDelete(String where, Request r) {
		String name = r.db.lookupString(new Select("name").from("pages").where(where));
		if (r.db.countRows("pages", "name=" + SQL.string(name)) == 1) {
			r.db.delete("menu", "name", name, false);
			r.db.delete("menu_items", "name", name, false);
		}
		m_menu.clear();
		r.setSessionAttribute("menu_items", null);
		return null;
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		String name = r.db.lookupString(new Select("name").from("pages").whereIdEquals(id));
		previous_values.put("name", name);
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public void
	clean(DBConnection db) {
		List<String[]> rows = db.readRows(new Select("id,people_id,_order_").from("menu_items").orderBy("people_id,_order_"));
		String previous = null;
		int o = 1;
		NameValuePairs nvp = new NameValuePairs();
		for (String[] row : rows) {
			if (!row[1].equals(previous)) {
				o = 1;
				previous = row[1];
			}
			if (Integer.parseInt(row[2]) != o) {
				nvp.set("_order_", o);
				db.update("menu_items", nvp, Integer.parseInt(row[0]));
			}
			o++;
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch(path_segments[0]) {
		case "menu":
			r.w.h2("Site Menu");
			writeSettingsForm(null, false, true, r);
			Site.site.newView("menu", r).writeComponent();
			r.close();
			return true;
		case "navbar":
			writeMenu(r.getPathSegment(2), r);
			r.close();
			return true;
		case "pages":
			Site.site.newView("pages", r).writeComponent();
			r.close();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if ("reset".equals(segment_one)) {
			r.db.delete("menu_items", "people_id", r.getUser().getId(), false);
			r.removeSessionAttribute("menu_items");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks(Request r) {
		return new String[][] { { "Pages", "pages" }, { "Site Menu", "menu" } };
	}

	//--------------------------------------------------------------------------

	public final DBObjects<MenuItem>
	getMenu() {
		m_menu.getObjects(); // ensure it's loaded
		return m_menu;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	DBObjects<MenuItem>
	getUserPages(Request r) {
		if (!m_allow_users_to_customize)
			return null;
		DBObjects<MenuItem> user_pages = (DBObjects<MenuItem>)r.getSessionAttribute("menu_items");
		Person user = r.getUser();
		if (user_pages == null && user != null) {
			user_pages = new DBObjects<>(new Select("name").from("menu_items").where("people_id=" + user.getId()).orderBy("_order_"), MenuItem.class);
			r.setSessionAttribute("menu_items", user_pages);
		}
		if (user != null && m_menu.getObjects().equals(user_pages.getObjects())) {
			r.db.delete("menu_items", "people_id=" + user.getId(), false);
			r.setSessionAttribute("menu_items", null);
			user_pages = null;
		}
		return user_pages;
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		m_menu = new DBObjects<>(new Select("*").from("menu").orderBy("_order_"), MenuItem.class);
		Site.site.addObjects(m_menu); // so that the UpdateHook calls get called on reorder
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	init2(DBConnection db) {
		Site.site.getViewDef("pages", db).addDeleteHook(this).addUpdateHook(this);
	}

	//--------------------------------------------------------------------------

	public final SiteMenuColumn
	newColumn(String name) {
		return new SiteMenuColumn(name, this);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("menu"))
			return new ViewDef(name) {
					@Override
					public void
					afterInsert(int id, NameValuePairs nvp, Request r) {
						String n = nvp.getString("name");
						NameValuePairs nvp2 = new NameValuePairs();
						nvp2.set("name", n);
						n = SQL.string(n);
						List<Integer> ids = r.db.readValuesInt(new Select("people_id").distinct().from("menu_items"));
						for (int i : ids)
							if (!r.db.exists(new Select().from("menu_items").whereEquals("people_id", i).andWhere("name=" + n))) {
								nvp2.set("people_id", i);
								nvp2.set("_order_", "NEXT:people_id=" + i);
								r.db.insert("menu_items", nvp2);
							}
						r.removeSessionAttribute("menu_items");
					}
				}
				.setDefaultOrderBy("_order_")
				.setOnSuccess("app.refresh_top_menu()")
				.setReorderable(new Reorderable().setOnUpdate("app.refresh_top_menu()"))
				.setRecordName("Menu Item", true)
				.setColumn(new Column("name").setInputRenderer(new SelectRenderer(Site.pages.getPages())));
		if (name.equals("menu_items"))
			return new MenuItemsViewDef(Site.pages.getPages());
		return null;
	}

	//--------------------------------------------------------------------------

	public final void
	write(String current_page, Request r) {
		r.w.setAttribute("data-url", apiURL("navbar"))
			.tagOpen("div");
		writeMenu(current_page, r);
		r.w.tagClose();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		super.updateSettings(r);
		m_home_page = m_home_page_url == null ? null : Site.pages.getPageByURL(m_home_page_url);
	}

	//--------------------------------------------------------------------------

	private void
	writeMenu(String current_page, Request r) {
		NavBar nav_bar = r.w.ui.navBar("top_menu", null, 0, r).setActiveItem(current_page);
		List<MenuItem> menu_items = null;
		DBObjects<MenuItem> user_pages = getUserPages(r);
		if (user_pages != null)
			menu_items = user_pages.getObjects();
		if (menu_items == null || menu_items.size() == 0)
			menu_items = m_menu.getObjects();

		if (m_home_page == null)
			m_home_page = m_home_page_url == null ? null : Site.pages.getPageByURL(m_home_page_url);
		if (m_home_page != null && m_home_page.canView(r))
			nav_bar.setBrand(m_home_page.getName(), m_home_page.getURL());
		nav_bar.open();
		Pages pages = Site.pages;
		for (MenuItem menu_item : menu_items) {
			Page page = pages.getPageByName(menu_item.name);
			if (page == null)
				continue;
			if (!"Other".equals(page.getURL())) {
				if (page.canView(r))
					page.a(nav_bar, r);
				continue;
			}
			boolean first = true;
			for (Page p : pages.getPages()) {
				if (p == m_home_page || !p.showInOther())
					continue;
				boolean contains = false;
				for (MenuItem mi : menu_items)
					if (mi.name.equals(p.getName())) {
						contains = true;
						break;
					}
				if (!contains && p.canView(r)) {
					if (first) {
						nav_bar.dropdownOpen(page.getName(), false);
						first = false;
					}
					p.a(nav_bar, r);
				}
			}
			if (!first)
				nav_bar.dropdownClose();
		}
		nav_bar.ulOpen(true);
		Site.site.writeUserDropdown(nav_bar, r);
		nav_bar.close();
	}
}
