package sitemenu;

import app.Request;
import app.Site;
import db.Form;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import db.object.DBObjects;
import web.HTMLWriter;

public class SiteMenuColumn extends ColumnBase<SiteMenuColumn> {
	private final SiteMenu m_site_menu;

	//--------------------------------------------------------------------------

	public
	SiteMenuColumn(String name, SiteMenu site_menu) {
		super(name);
		m_site_menu = site_menu;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isHidden(Request r) {
		return !m_site_menu.m_allow_users_to_customize || !r.userHasRole(Site.site.getDefaultRole());
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		DBObjects<MenuItem> user_menu_items = m_site_menu.getUserPages(r);
		boolean has_items = user_menu_items != null && user_menu_items.getObjects().size() != 0;
		HTMLWriter w = r.w;
		if (has_items)
			w.addStyle("display:none");
		w.ui.buttonOnClick("Customize", "this.style.display='none';this.nextSibling.style.display='';net.replace(this.nextSibling,context+'/Views/menu_items/component')");
		if (!has_items)
			w.addStyle("display:none");
		w.addClass("bg-light p-1")
			.tagOpen("div");
		if (has_items) {
			r.w.h5("Menu");
			Site.site.newView("menu_items", r).setMode(Mode.LIST).writeComponent();
		}
		w.tagClose()
			.js("app.subscribe('menu_items', app.refresh_top_menu)");
	}
}
