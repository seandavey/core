package util;

import java.util.ArrayList;
import java.util.TreeMap;

public class MultiMap<T> extends TreeMap<String,ArrayList<T>> {
	private static final long serialVersionUID = 1L;

	//--------------------------------------------------------------------------

	public final void
	put(String key, T value) {
		ArrayList<T> a = get(key);
		if (a == null) {
			a = new ArrayList<>();
			super.put(key, a);
		}
		a.add(value);
	}
}
