package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import app.Site;
import jakarta.servlet.http.HttpServletResponse;

public class Zip {
	private static void
	addFile(File file, ZipOutputStream zos, int base_length) {
		byte[] buffer = new byte[1024];
		String file_path = file.getAbsolutePath();
		FileInputStream in = null;
        try {
			ZipEntry zip_entry = new ZipEntry(file_path.substring(base_length));
			zip_entry.setTime(0);
			zos.putNextEntry(zip_entry);
			try {
				in = new FileInputStream(file_path);
				int len;
				while ((len = in.read(buffer)) > 0)
					zos.write(buffer, 0, len);
			} finally {
			   in.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	private static void
	addFiles(File node, ZipOutputStream zos, int base_length, String[] skip) {
		if (node.isFile())
			addFile(node, zos, base_length);
		if (node.isDirectory()) {
			if (skip != null && Array.indexOf(skip, node.getName()) != -1)
				return;
			File[] files = node.listFiles();
			if (files != null)
				for (File file : files)
					addFiles(file, zos, base_length, skip);
		}
	}

	//--------------------------------------------------------------------------

	public static boolean
	sendDirectoryAsZip(String directory, String[] skip, String filename, HttpServletResponse http_response) {
		http_response.setContentType("application/zip");
		if (filename == null) {
			filename = directory;
			int index = filename.lastIndexOf('/');
			if (index != -1)
				filename = filename.substring(index + 1);
		}
		http_response.setHeader("Content-disposition", "attachment; filename=\"" + filename + ".zip\"");
		String base_path = Site.site.getPath(directory).toString();
		try {
			ZipOutputStream zos = new ZipOutputStream(http_response.getOutputStream());
			addFiles(new File(base_path), zos, base_path.length() + 1, skip);
			zos.close();
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		return true; // always true, useful for switch expressions
	}
}
