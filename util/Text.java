package util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Text {
	public static final String				add = "Add";
	public static final String				all = "All";
	public static final String				delete = "Delete";
	public static final String				done = "Done";
	public static final String				edit = "Edit";
	public static final String				join = "Join";
	public static final String				leave = "Leave";
	public static final String				none = "None";
	public static final String				remove = "Remove";
	public static final String				remove_all = "Remove All";
	public static final String				search = "Search";
	public static final String				send = "Send";
	public static final String				settings = "Settings";
	public static final String				submit = "Submit";
	public static final String				subscribe = "Subscribe";
	public static final String				unsubscribe = "Unsubscribe";
	public static final String				view = "View";

	public static final NumberFormat		currency = NumberFormat.getCurrencyInstance(Locale.CANADA);
	public static final NumberFormat		two_digits = NumberFormat.getInstance();
	public static final NumberFormat		two_digits_max = NumberFormat.getInstance();

	private static final Map<String,String>	s_plurals = Map.of("person", "people", "child", "children", "dry", "drys");
	private static final Set<String>		s_same = Set.of("faculty", "info", "insurance", "mosaic", "participating", "series", "species", "work");
	private static final Map<String,String>	s_singulars = Map.of("people", "person", "children", "child");

	//--------------------------------------------------------------------------

	static {
		two_digits.setMaximumFractionDigits(2);
		two_digits.setMinimumFractionDigits(2);
		two_digits_max.setMaximumFractionDigits(2);
	}

	//--------------------------------------------------------------------------

	public static boolean
	blackText(String color) {
		if (color == null)
			return true;
		try {
			double a = 1 - ( 0.299 * Integer.valueOf(color.substring(1, 3), 16) + 0.587 * Integer.valueOf(color.substring(3, 5), 16) + 0.114 * Integer.valueOf(color.substring(5, 7), 16))/255;
			return a < 0.5;
		} catch (Exception e) {
			return true;
		}
	}

	//--------------------------------------------------------------------------

	public static String
	capitalize(String s) {
		if (s == null)
			return null;
		StringBuilder sb = new StringBuilder(s);
		for (int i=0; i<s.length(); i++)
			if (i == 0 || sb.charAt(i - 1) == ' ')
				sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	deleteAll(String string, char c) {
		int index = string.indexOf(c);
		if (index == -1)
			return string;
		StringBuilder s = new StringBuilder(string);
		s.deleteCharAt(index);
		for (int i=index; i<s.length(); i++)
			if (s.charAt(i) == c)
				s.deleteCharAt(i--);
		return s.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	escapeDoubleQuotes(String s) {
		if (s == null)
			return null;
		StringBuilder buf = new StringBuilder(s);
		for (int i=0; i<buf.length(); i++) {
			char c = buf.charAt(i);
			if (c == '"' || c == '\\') {
				buf.insert(i, '\\');
				i++;
			}
		}
		return buf.toString();
	}

	//--------------------------------------------------------------------------

//	public static String
//	join(String delimeter, Object[] objects) {
//		if (objects == null)
//			return null;
//		StringBuilder sb = new StringBuilder();
//		for (Object o : objects)
//			if (o != null) {
//				if (delimeter != null && sb.length() > 0)
//					sb.append(delimeter);
//				sb.append(o.toString());
//			}
//		return sb.toString();
//	}

	//--------------------------------------------------------------------------

	public static String
	join(String delimeter, String... strings) {
		if (strings == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (String string : strings)
			if (string != null) {
				if (delimeter != null && sb.length() > 0)
					sb.append(delimeter);
				sb.append(string);
			}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	join(String delimeter, Collection<String> strings) {
		if (strings == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (String string : strings) {
			if (delimeter != null && sb.length() > 0)
				sb.append(delimeter);
			if (string != null)
				sb.append(string);
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	join(String delimeter, int... ints) {
		if (ints == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (int i : ints) {
			if (delimeter != null && sb.length() > 0)
				sb.append(delimeter);
			sb.append(Integer.toString(i));
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	join(String delimeter, List<Integer> ints) {
		if (ints == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (Integer i : ints) {
			if (delimeter != null && sb.length() > 0)
				sb.append(delimeter);
			sb.append(i);
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	private static String
	matchCapital(String s, String to_match) {
		if (Character.isUpperCase(to_match.charAt(0)))
			return capitalize(s);
		return s;
	}

	//--------------------------------------------------------------------------

	public static String
	md5(String s) {
		try {
			return String.format("%032x", new BigInteger(1, MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8"))));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public static String
	pluralize(String s) {
		if (s == null)
			return null;
		char last = s.charAt(s.length() - 1);
		if (last == 's')
			return s;
		String lc = s.toLowerCase();
		if (s_same.contains(lc))
			return s;
		String plural = s_plurals.get(lc);
		if (plural != null)
			return matchCapital(plural, s);
		if (last == 'x' || s.endsWith("ch") || s.endsWith("sh"))
			return s + "es";
		if (last == 'y') {
			char c = s.charAt(s.length() - 2);
			if (!(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'))
				return s.substring(0, s.length() - 1) + "ies";
		}
		return s + "s";
	}

	//--------------------------------------------------------------------------

	public static String
	pluralize(String s, int counter) {
		if (counter == 1)
			return s;
		return pluralize(s);
	}

	//--------------------------------------------------------------------------

	public static String
	pluralize(String s, double counter) {
		if (counter == 1.0)
			return s;
		return pluralize(s);
	}

	//--------------------------------------------------------------------------

	public static String
	scramble(String s, int key) {
		key &= 127;
		byte[] bytes = s.getBytes();
		for (int i=0; i<bytes.length; i++)
			bytes[i] ^= key;
		return new String(bytes);
	}

	//--------------------------------------------------------------------------

	public static String
	singularize(String s) {
		if (s == null)
			return null;
		String lc = s.toLowerCase();
		String singular = s_singulars.get(lc);
		if (singular != null)
			return matchCapital(singular, s);
		if (s_same.contains(lc))
			return s;
		if (s.endsWith("ies"))
			return s.substring(0, s.length() - 3) + "y";
		if (s.endsWith("xes"))
			return s.substring(0, s.length() - 2);
		if (s.charAt(s.length() - 1) == 's')
			return s.substring(0, s.length() - 1);
		return s;
	}

	//--------------------------------------------------------------------------

	public static String
	singularize(String s, int counter) {
		if (counter != 1)
			return s;
		return singularize(s);
	}

	//--------------------------------------------------------------------------

//	public static String
//	singularize(String s, double counter) {
//		if (counter != 1.0)
//			return s;
//		return singularize(s);
//	}

	//--------------------------------------------------------------------------
	
	public static String
	uuid() {
		return "u" + UUID.randomUUID().toString().replace("-", "");
	}
}