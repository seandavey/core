package util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import app.Person;
import app.Request;

public class Time {
	public static TimeFormatter	formatter;
	public static ZoneId		zone_id;

	//--------------------------------------------------------------------------

	public static int
	compareTo(LocalTime a, LocalTime b) {
		if (a == null) {
			if (b == null)
				return 0;
			return -1;
		} else if (b == null)
			return 1;
		return a.compareTo(b);
	}

	//--------------------------------------------------------------------------

	public static ZoneId
	getUserTimezone(Request r) {
		Person user = r.getUser();
		if (user == null)
			return null;
		String timezone = user.getTimezone();
		if (timezone == null)
			return null;
		if (zone_id == null)
			return null;
		ZoneId user_zone_id = ZoneId.of(timezone);
		if (zone_id.equals(user_zone_id))
			return null;
		return user_zone_id;
	}

	//--------------------------------------------------------------------------

	public static LocalDate
	newDate() {
		return zone_id != null ? LocalDate.now(zone_id) : LocalDate.now();
	}

	//--------------------------------------------------------------------------

	public static LocalDateTime
	newDateTime() {
		return zone_id != null ? LocalDateTime.now(zone_id) : LocalDateTime.now();
	}

	//--------------------------------------------------------------------------

	public static LocalTime
	newTime() {
		return zone_id != null ? LocalTime.now(zone_id) : LocalTime.now();
	}

	//--------------------------------------------------------------------------

	public static LocalDate
	toSiteZone(LocalDate date, LocalTime time, Request r) {
		if (date == null || time == null)
			return date;
		ZoneId user_zone_id = Time.getUserTimezone(r);
		if (user_zone_id == null)
			return date;
		ZonedDateTime zoned_date_time = ZonedDateTime.of(date, time, user_zone_id);
		zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
		return zoned_date_time.toLocalDate();
	}

	//--------------------------------------------------------------------------
	// if date is null, compute for today's date

	public static LocalTime
	toSiteZone(LocalTime time, LocalDate date,  Request r) {
		if (time == null)
			return null;
		ZoneId user_zone_id = Time.getUserTimezone(r);
		if (user_zone_id == null)
			return time;
		if (date == null)
			date = newDate();
		ZonedDateTime zoned_date_time = ZonedDateTime.of(date, time, user_zone_id);
		zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
		return zoned_date_time.toLocalTime();
	}

	//--------------------------------------------------------------------------

	public static LocalDate
	toUserZone(LocalDate date, LocalTime time, Request r) {
		if (date == null || time == null)
			return date;
		ZoneId user_zone_id = Time.getUserTimezone(r);
		if (user_zone_id == null)
			return date;
		ZonedDateTime zoned_date_time = ZonedDateTime.of(date, time, zone_id);
		zoned_date_time = zoned_date_time.withZoneSameInstant(user_zone_id);
		return zoned_date_time.toLocalDate();
	}

	//--------------------------------------------------------------------------
	// if date is null, compute for today's date

	public static LocalTime
	toUserZone(LocalTime time, LocalDate date,  Request r) {
		if (time == null)
			return null;
		ZoneId user_zone_id = Time.getUserTimezone(r);
		if (user_zone_id == null)
			return time;
		if (date == null)
			date = newDate();
		ZonedDateTime zoned_date_time = ZonedDateTime.of(date, time, zone_id);
		zoned_date_time = zoned_date_time.withZoneSameInstant(user_zone_id);
		return zoned_date_time.toLocalTime();
	}
}
