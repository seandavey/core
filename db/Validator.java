package db;

import app.Request;

public interface Validator {
	public String validate(String table, String value, Request r);
}
