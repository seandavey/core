package db.jdbc;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import db.DBConnection;
import db.SQL;
import db.object.JSONField;

public class JDBCColumn implements Comparable<JDBCColumn> {
	public static final String[] types = { "BOOLEAN", "BYTEA", "CHAR", "DATE", "DOUBLE PRECISION", "INTEGER", "JSON", "JSONB", "REAL", "TIME", "TIMESTAMP", "TSVECTOR", "VARCHAR" };

	@JSONField
	public String		name;
	@JSONField
	public int			size;

	@JSONField
	private String		m_default_value;
	@JSONField
	private boolean		m_is_nullable = true;
	@JSONField
	private boolean		m_on_delete_set_null;
	@JSONField
	protected String	m_primary_table;
	@JSONField
	private int			m_type;
	@JSONField
	private boolean		m_unique;

	//--------------------------------------------------------------------------

	public
	JDBCColumn() {
		// only used for loading as a DBObject
	}

	//--------------------------------------------------------------------------

	public
	JDBCColumn(String name, int type) {
		this(name, type, 0);
	}

	//--------------------------------------------------------------------------

	public
	JDBCColumn(String name, int type, int size) {
		this.name = name.toLowerCase();
		m_type = type;
		this.size = size > 1000 ? -1 : size;
		if (type == Types.BOOLEAN || type == Types.BIT) {
			m_type = Types.BOOLEAN;
			m_default_value = "false";
		} else if (type == Types.DOUBLE)
			this.size = 20;
		else if (type == Types.INTEGER)
			this.size = 10;
		else if (type == Types.REAL)
			this.size = 10;
		else if (type == Types.TIMESTAMP)
			this.size = 0; //29;
	}

	//--------------------------------------------------------------------------

	public
	JDBCColumn(String primary_table) {
		this(primary_table + "_id", Types.INTEGER, 0);
		m_primary_table = primary_table;
	}

	//--------------------------------------------------------------------------

	public
	JDBCColumn(String name, String primary_table) {
		this(name, Types.INTEGER, 0);
		m_primary_table = primary_table;
	}

	//--------------------------------------------------------------------------

	JDBCColumn(String name, ResultSet rs) throws SQLException {
		this(name, rs.getInt("DATA_TYPE"), rs.getInt("COLUMN_SIZE"));
		m_default_value = rs.getString("COLUMN_DEF");
		if (m_default_value != null) {
			if (m_default_value.charAt(0) == '\'')
				m_default_value = m_default_value.substring(1, m_default_value.indexOf("'::"));
			if (m_type == Types.BOOLEAN && "true".equals(m_default_value))
				m_default_value = "true";
		}
		m_is_nullable = rs.getInt("NULLABLE") == DatabaseMetaData.attributeNullable;
	}

	//--------------------------------------------------------------------------

	final void
	alterType(String table_name, DBConnection db) {
		db.alterColumnType(table_name, name, getSQLType(), size == 0 ? null : Integer.toString(size));
	}

	//--------------------------------------------------------------------------

	void
	appendCode(StringBuilder code) {
		code.append(".add(new JDBCColumn(\"").append(name).append("\", ");
		if (m_primary_table != null) {
			code.append('"');
			code.append(m_primary_table);
			code.append("\")");
		} else {
			code.append("Types.");
			if (m_type == Types.BINARY)
				code.append("BINARY");
			else if (m_type == Types.BOOLEAN)
				code.append("BOOLEAN");
			else if (m_type == Types.CHAR) {
				code.append("CHAR");
				if (size > 0) {
					code.append(", ");
					code.append(size);
				}
			} else if (m_type == Types.DATE)
				code.append("DATE");
			else if (m_type == Types.DOUBLE)
				code.append("DOUBLE");
			else if (m_type == Types.INTEGER)
				code.append("INTEGER");
			else if (m_type == Types.REAL)
				code.append("REAL");
			else if (m_type == Types.TIME)
				code.append("TIME");
			else if (m_type == Types.TIMESTAMP)
				code.append("TIMESTAMP");
			else if (m_type == Types.VARCHAR) {
				code.append("VARCHAR");
				if (size > 0) {
					code.append(", ");
					code.append(size);
				}
			} else
				throw new RuntimeException("unknown type");
			code.append(')');
			if (m_default_value != null) {
				code.append(".setDefaultValue(");
				if (isString()) {
					code.append('"');
					code.append(m_default_value);
					code.append('"');
				} else
					code.append(m_default_value);
				code.append(')');
			}
			if (!m_is_nullable)
				code.append(".setIsNullable(true)");
			if (m_unique)
				code.append(".setUnique(true)");
		}
		code.append(')');
	}

	//--------------------------------------------------------------------------

	public final void
	appendSQL(StringBuilder sql) {
		sql.append(SQL.columnName(name));
		sql.append(" ");
		sql.append(getSQLType());
		if (name.equals("id"))
			return;
		if (m_default_value != null) {
			sql.append(" DEFAULT ");
			if (isString())
				sql.append(SQL.string(m_default_value));
			else
				sql.append(m_default_value);
		} else if (m_type == Types.BOOLEAN)
			sql.append(" DEFAULT false");
		if (!m_is_nullable)
			sql.append(" NOT NULL");
		if (m_primary_table != null) {
			sql.append(" REFERENCES ");
			sql.append(m_primary_table);
			sql.append("(id) ON DELETE ");
			sql.append(m_on_delete_set_null ? "SET NULL" : "CASCADE");
		}
		if (m_unique)
			sql.append(" UNIQUE");
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(JDBCColumn column) {
		return name.compareTo(column.name);
	}

	//--------------------------------------------------------------------------

	final void
	create(String table_name, DBConnection db) {
		db.addColumn(table_name, this);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	equals(Object object) {
		if (!(object instanceof JDBCColumn))
			return false;
		return name.equals(((JDBCColumn)object).name);
	}

	//--------------------------------------------------------------------------

	public final String
	getDefaultValue() {
		return m_default_value;
	}

	//--------------------------------------------------------------------------

	public final String
	getPrimaryTable() {
		return m_primary_table;
	}

	//--------------------------------------------------------------------------

	public String
	getSQLType() {
		if (name.equals("id"))
			return "SERIAL PRIMARY KEY";
		if (m_type == Types.BINARY)
			return types[1];
		if (m_type == Types.BOOLEAN)
			return types[0];
		if (m_type == Types.CHAR)
			return size > 0 ? types[2] + "(" + size + ")" : types[2];
		if (m_type == Types.DATE)
			return types[3];
		if (m_type == Types.DOUBLE)
			return types[4];
		if (m_type == Types.INTEGER)
			return types[5];
		if (m_type == Types.OTHER)
			return "Other";
		if (m_type == Types.REAL)
			return types[8];
		if (m_type == Types.TIME)
			return types[9];
		if (m_type == Types.TIMESTAMP)
			return types[10];
		if (m_type == Types.VARCHAR)
			return size > 0 ? types[12] + "(" + size + ")" : types[12];
		throw new RuntimeException("unknown type");
	}

	//--------------------------------------------------------------------------

	public final boolean
	isBinary() {
		return m_type == Types.BINARY;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isBoolean() {
		return m_type == Types.BOOLEAN;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isDate() {
		return m_type == Types.DATE;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isDouble() {
		return m_type == Types.DOUBLE;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isInteger() {
		return m_type == Types.INTEGER;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isNullable() {
		return m_is_nullable;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isNumber() {
		return m_type == Types.INTEGER || m_type == Types.REAL || m_type == Types.DOUBLE;
	}

	//--------------------------------------------------------------------------

	public boolean
	isOther() {
		return m_type == Types.OTHER;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isReal() {
		return m_type == Types.REAL;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isString() {
		return m_type == Types.VARCHAR || m_type == Types.CHAR || m_type == Types.LONGVARCHAR;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isText() {
		return m_type == Types.VARCHAR && (size <= 0 || size > 1000);
	}

	//--------------------------------------------------------------------------

	public final boolean
	isTime() {
		return m_type == Types.TIME;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isTimestamp() {
		return m_type == Types.TIMESTAMP;
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	setDefaultToCurrentTimestamp() {
		m_default_value = "now()";
		return this;
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	setDefaultValue(String default_value) {
		m_default_value = default_value;
		return this;
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	setDefaultValue(boolean default_value) {
		m_default_value = default_value ? "true" : "false";
		return this;
	}

	//--------------------------------------------------------------------------

//	public final JDBCColumn
//	setIsNullable(boolean is_nullable) {
//		m_is_nullable = is_nullable;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	setOnDeleteSetNull(boolean on_delete_set_null) {
		m_on_delete_set_null = on_delete_set_null;
		return this;
	}

	//--------------------------------------------------------------------------

	final JDBCColumn
	setPrimaryTable(String primary_table) {
		m_primary_table = primary_table;
		return this;
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	setUnique(boolean unique) {
		m_unique = unique;
		return this;
	}
}
