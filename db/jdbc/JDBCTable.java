package db.jdbc;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import db.DBConnection;
import db.Mods;

public class JDBCTable implements Iterable<JDBCColumn> {
	private String[]				m_column_names;
	private String[]				m_column_names_all;
	private ArrayList<JDBCColumn>	m_columns = new ArrayList<>();
	private String					m_name;

	//--------------------------------------------------------------------------

	public
	JDBCTable(String name) {
		m_name = name;
	}

	//--------------------------------------------------------------------------

	public
	JDBCTable(String name, DatabaseMetaData db_meta_data) {
		m_name = name;
		try {
			ResultSet rs = db_meta_data.getColumns(null, "public", m_name, null);
			ArrayList<String> column_names = new ArrayList<>();
			ArrayList<String> column_names_all = new ArrayList<>();

			while (rs.next()) {
				String column_name = rs.getString("COLUMN_NAME");
				if (column_name.charAt(0) != '.') {
					if (!column_name.equals("id") && !column_name.startsWith("_"))
						column_names.add(column_name);
					column_names_all.add(column_name);
					String type = rs.getString("TYPE_NAME");
					m_columns.add("jsonb".equals(type) ? new JSONB(column_name) : "tsvector".equals(type) ? new TSVector() : new JDBCColumn(column_name, rs));
				}
			}
			rs.close();
			rs = db_meta_data.getImportedKeys(null, "public", m_name);
			while (rs.next())
				getColumn(rs.getString("FKCOLUMN_NAME")).setPrimaryTable(rs.getString("PKTABLE_NAME"));
			rs.close();
			m_column_names = column_names.toArray(new String[column_names.size()]);
			m_column_names_all = column_names_all.toArray(new String[column_names_all.size()]);
			Arrays.sort(m_column_names_all);
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	//--------------------------------------------------------------------------

	public final JDBCTable
	add(JDBCColumn jdbc_column) {
		m_columns.add(jdbc_column);
		return this;
	}

	//--------------------------------------------------------------------------
	// read in current columns (t) and adjust table with differences from table_def

	public static void
	adjustTable(JDBCTable table_def, boolean create_missing, boolean drop_extra, DBConnection db) {
		String table = table_def.m_name;
		if (!db.tableExists(table))
			db.createTable(table, null);
		Mods.adjustTable(table, db);
		JDBCTable t = db.newJDBCTable(table);
		if (create_missing)
			// create columns missing from t
			for (JDBCColumn column : table_def.m_columns) {
				JDBCColumn c = t.getColumn(column.name);
				if (c == null)
					column.create(table, db);
				else if (!c.getSQLType().equals(column.getSQLType()))
					column.alterType(table, db);
			}
		if (drop_extra)
			// drop columns in t that are not in this
			for (JDBCColumn column : t.m_columns)
				if (!column.name.equals("id") && table_def.getColumn(column.name) == null)
					db.dropColumn(table, column.name);
	}

	//--------------------------------------------------------------------------

	public final String
	getCode() {
		StringBuilder code = new StringBuilder("JDBCTable table_def = new JDBCTable()");
		for (JDBCColumn column : m_columns) {
			if (column.name.equals("id"))
				continue;
			code.append("<br />");
			column.appendCode(code);
		}
		code.append(";<br />db.getTable(\"");
		code.append(m_name);
		code.append("\", true).adjustTable(table_def, true, true, db);");
		return code.toString();
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	getColumn(String column_name) {
		column_name = column_name.toLowerCase();
		for (JDBCColumn column : m_columns)
			if (column.name.equals(column_name))
				return column;
		return null;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getColumnNames() {
		return m_column_names;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getColumnNamesAll() {
		return m_column_names_all;
	}

	//--------------------------------------------------------------------------

	public final String
	getSQL() {
		StringBuilder sql = new StringBuilder("CREATE TABLE ");
		sql.append(m_name);
		sql.append('(');
		for (int i=0; i<m_column_names_all.length; i++) {
			if (i > 0)
				sql.append(',');
			getColumn(m_column_names_all[i]).appendSQL(sql);
		}
		sql.append(")");
		return sql.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public Iterator<JDBCColumn>
	iterator() {
		return m_columns.iterator();
	}
}
