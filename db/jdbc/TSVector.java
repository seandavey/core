package db.jdbc;

public class TSVector extends JDBCColumn {
	public
	TSVector() {
		name = "tsvector";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	appendCode(StringBuilder code) {
		code.append(".add(new TSVector())");
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getSQLType() {
		return "TSVECTOR";
	}
}
