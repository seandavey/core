package db;

import app.Request;

public interface Filter {
	public boolean accept(Rows rows, Request r);
}