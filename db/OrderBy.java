package db;

import app.Request;
import app.Stringify;
import db.column.ColumnBase;
import db.column.LookupColumn;

@Stringify
public class OrderBy {
	private String[]	m_column_names;
	private boolean[]	m_descending;
	private boolean[]	m_nulls_first;

	// --------------------------------------------------------------------------

	public
	OrderBy() {
		m_column_names = new String[0];
		m_descending = new boolean[0];
		m_nulls_first = new boolean[0];
	}

	// --------------------------------------------------------------------------

	public
	OrderBy(String column_name) {
		this(column_name, false, false);
	}

	// --------------------------------------------------------------------------

	public
	OrderBy(String column_name, boolean descending, boolean nulls_first) {
		m_column_names = new String[] { column_name };
		m_descending = new boolean[] { descending };
		m_nulls_first = new boolean[] { nulls_first };
	}

	// --------------------------------------------------------------------------

	private void
	append(StringBuilder sb, int i) {
		sb.append(m_column_names[i]);
		if (m_nulls_first[i])
			sb.append(" NULLS FIRST");
		if (m_descending[i])
			sb.append(" DESC");
	}

	// --------------------------------------------------------------------------

	public static void
	appendDisplayName(StringBuilder sb, String column_name, boolean descending, String context, View v) {
		ColumnBase<?> column = v.getColumn(column_name);
		sb.append(column == null ? ColumnBase.getDisplayName(column_name, true) : column.getDisplayName(true));
		sb.append("&nbsp;");
		sb.append(descending ? "↓" : "↑");
	}

	// --------------------------------------------------------------------------

	public final String
	columnName(int index) {
		return m_column_names[index];
	}

	// --------------------------------------------------------------------------

	public final boolean
	descending(int index) {
		return m_descending[index];
	}

	// --------------------------------------------------------------------------

	public final void
	set(String order_by) {
		m_column_names = order_by.split(";");
		m_descending = new boolean[m_column_names.length];
		m_nulls_first = new boolean[m_column_names.length];
		for (int i=0; i<m_column_names.length; i++) {
			if (m_column_names[i].endsWith(" DESC")) {
				m_descending[i] = true;
				m_column_names[i] = m_column_names[i].substring(0, m_column_names[i].length() - 5);
			}
			if (m_column_names[i].endsWith(" NULLS FIRST")) {
				m_nulls_first[i] = true;
				m_column_names[i] = m_column_names[i].substring(0, m_column_names[i].length() - 12);
			}
		}
	}

	// --------------------------------------------------------------------------

	public final int
	size() {
		return m_column_names.length;
	}

	// --------------------------------------------------------------------------

	public final String
	toQueryString(View v, Request r) {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<m_column_names.length; i++) {
			if (i > 0)
				sb.append(',');
			ColumnBase<?> column = v.getColumn(m_column_names[i]);
			if (column != null && column instanceof LookupColumn)
				sb.append(((LookupColumn)column).getOrderBy(v.getFrom(), m_descending[i], m_nulls_first[i]));
//				query.addColumns(((LookupColumn)column).getOrderBy(v.getFrom(), false));
			else if (column != null && m_descending[i])
				if (column.getOrderByDesc() != null)
					sb.append(column.getOrderByDesc());
				else if (column.getOrderBy() != null)
					sb.append(column.getOrderBy()).append(" DESC");
				else
					append(sb, i);
			else if (column != null && !m_descending[i] && column.getOrderBy() != null)
				sb.append(column.getOrderBy());
			else
				append(sb, i);
		}
		return sb.toString();
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	toString() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<m_column_names.length; i++) {
			if (i > 0)
				sb.append(',');
			append(sb, i);
		}
		return sb.toString();
	}
}
