package db;

import db.object.DBField;
import ui.SelectOption;

public class Option implements Comparable<Option>, SelectOption {
	@DBField
	private String m_id;
	@DBField
	private String m_text;

	//--------------------------------------------------------------------------

	public
	Option() {
	}

	//--------------------------------------------------------------------------

	public
	Option(String text, String value) {
		m_text = text;
		m_id = value;
//		if (value != null)
//			m_id = URLBuilder.encode(value);
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(Option o) {
		return m_text.compareTo(o.m_text);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getText() {
		return m_text;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getValue() {
		return m_id;
	}
}
