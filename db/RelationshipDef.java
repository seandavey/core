package db;

import app.Request;
import db.View.Mode;

public abstract class RelationshipDef<T> {
	private Mode[]		m_hide_modes;
	protected String	m_many_table_column;
	private String		m_record_name;
	private boolean		m_show_add_button_on_read_only_form;
	private String		m_view_role;

	public String		many_view_def_name;

	//--------------------------------------------------------------------------

	public String
	getManyTableColumn() {
		return m_many_table_column;
	}

	//--------------------------------------------------------------------------

	public String
	getRecordName() {
		return m_record_name;
	}

	//--------------------------------------------------------------------------

	public String
	getViewRole() {
		return m_view_role;
	}

	//--------------------------------------------------------------------------

	abstract public void
	leftJoin(Select query, String one_table, Request r);

	//--------------------------------------------------------------------------

	public final RelationshipDef<T>
	setHideOnForms(Mode ...hide_modes) {
		m_hide_modes = hide_modes;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RelationshipDef<T>
	setManyTableColumn(String many_table_column) {
		m_many_table_column = many_table_column;
		return this;
	}

	// --------------------------------------------------------------------------

	public final RelationshipDef<T>
	setRecordName(String record_name) {
		m_record_name = record_name;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RelationshipDef<T>
	setShowAddButtonOnReadOnlyForm(boolean show_add_button_on_read_only_form) {
		m_show_add_button_on_read_only_form = show_add_button_on_read_only_form;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RelationshipDef<T>
	setViewRole(String view_role) {
		m_view_role = view_role;
		return this;
	}

	//--------------------------------------------------------------------------

	final boolean
	showAddButtonOnReadOnlyForm() {
		return m_show_add_button_on_read_only_form;
	}

	//--------------------------------------------------------------------------

	protected boolean
	showOnForm(Mode mode, Rows data) {
		if (m_hide_modes == null)
			return true;
		for (Mode m : m_hide_modes)
			if (m == mode)
				return false;
		return true;
	}
}
