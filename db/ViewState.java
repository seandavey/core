package db;

import app.Request;
import app.Site;
import app.Stringify;
import util.Array;

@Stringify
public class ViewState {
	private String				m_base_filter;
//	private String[]			m_check_boxes_columns;
//	private String				m_check_boxes_name;
	private String[]			m_column_names_all;
	private String[]			m_column_names_table;
	private String				m_filter;
	private int					m_key_value;
	private OrderBy				m_order_by = new OrderBy();
	private String				m_quicksearch;
	private String[]			m_quicksearch_columns;
	private boolean				m_quicksearch_is_tsvector;
	private boolean[]			m_sections_active;
	private int					m_row_window_size;
	private int					m_row_window_start;

	//--------------------------------------------------------------------------

	public
	ViewState(ViewDef view_def, Request r) {
		String base_filter = view_def.getBaseFilter();
		if (base_filter != null) {
			int index = base_filter.indexOf("$(@user id)");
			if (index != -1)
				base_filter = base_filter.substring(0, index) + r.getUser().getId() + base_filter.substring(index + 11);
			m_base_filter = base_filter;
		}
		String order_by = view_def.getDefaultOrderBy();
		if (order_by != null)
			m_order_by.set(order_by);
		m_row_window_size = view_def.getRowWindowSize();
	}

	//--------------------------------------------------------------------------

	public final void
	clearFilter() {
//		m_filter = null;
		m_quicksearch = null;
	}

	// --------------------------------------------------------------------------

	public static void
	doPost(Request r) {
		String view_def_name = r.getPathSegment(1);
		String segment_two = r.getPathSegment(2);
		if (segment_two != null) {
			switch(segment_two) {
 			case "toggle_column":
				toggleColumn(view_def_name, r.getParameter("db_column"), r);
				return;
			case "tsvectorsearch":
				String value = r.getParameter("value");
				ViewState.setTSVectorSearch(view_def_name, value != null && value.length() > 0 ? value : null, r);
				return;
			}
			return;
		}
		String value;
		if ((value = r.getParameter("base_filter")) != null)
			ViewState.setBaseFilter(view_def_name, value.length() == 0 ? Site.site.getViewDef(view_def_name, r.db).getBaseFilter() : value, r);
		if ((value = r.getParameter("db_key_value")) != null)
			ViewState.setKeyValue(view_def_name, r.getInt("db_key_value", 0), r);
		if ((value = r.getParameter("db_orderby")) != null)
			ViewState.setOrderBy(view_def_name, value, r);
		if ((value = r.getParameter("filter")) != null)
			//			if (value.length() == 0)
//				value = null;
			ViewState.setFilter(view_def_name, value, r);
		if ((value = r.getParameter("row_window_size")) != null)
			try {
				ViewState.setRowWindowSize(view_def_name, value.equals("all") ? 0 : Integer.parseInt(value), r);
			} catch (Exception e) {
			}
		if ((value = r.getParameter("row_window_start")) != null)
			ViewState.setRowWindowStart(view_def_name, Integer.parseInt(value), r);
	}

	//--------------------------------------------------------------------------

	public final String
	getBaseFilter() {
		return m_base_filter;
	}

	//--------------------------------------------------------------------------

//	public final String[]
//	getCheckBoxesColumns() {
//		return m_check_boxes_columns;
//	}

	//--------------------------------------------------------------------------

//	public final String
//	getCheckBoxesName() {
//		return m_check_boxes_name;
//	}

	//--------------------------------------------------------------------------

	public final String[]
	getColumnNamesAll(View v) {
		if (m_column_names_all == null && v != null)
			m_column_names_all = v.getColumnNamesAll();
		return m_column_names_all;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getColumnNamesTable(View v) {
		if (m_column_names_table == null && v != null)
			m_column_names_table = v.getColumnNamesTable();
		return m_column_names_table;
	}

	//--------------------------------------------------------------------------

	public final String
	getFilter() {
		if (m_quicksearch == null)
			return m_filter;
		if (m_quicksearch_is_tsvector)
			return "tsvector @@ plainto_tsquery(" + SQL.string(m_quicksearch) + ")";
		StringBuilder sb = new StringBuilder();
		for (String column : m_quicksearch_columns) {
			if (sb.length() > 0)
				sb.append(" OR ");
			sb.append(column).append(" ILIKE '%").append(m_quicksearch).append("%'");
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public final int
	getKeyValue() {
		if (m_key_value == 0)
			Site.site.log("m_key_value 0 in ViewState.getKeyValue()", false);
		return m_key_value;
	}

	//--------------------------------------------------------------------------

	public final OrderBy
	getOrderBy() {
		return m_order_by;
	}

	//--------------------------------------------------------------------------

	public final String
	getQuicksearch() {
		return m_quicksearch;
	}

	//--------------------------------------------------------------------------

	public final int
	getRowWindowSize() {
		return m_row_window_size;
	}

	//--------------------------------------------------------------------------

	public final int
	getRowWindowStart() {
		return m_row_window_start;
	}

	//--------------------------------------------------------------------------

	public final void
	processRequest(Request r) {
		int id = r.getInt("db_key_value", 0);
		if (id != 0)
			m_key_value = id;
		String orderby = r.getParameter("db_orderby");
		if (orderby != null && !Site.areEqual(m_order_by.toString(), orderby)) {
			m_row_window_start = 0;
			m_order_by.set(orderby);
		}
		int row_window_start = r.getInt("row_window_start", -1);
		if (row_window_start != -1)
			m_row_window_start = row_window_start;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isSectionActive(int i) {
		if (m_sections_active == null)
			return true;
		return m_sections_active[i];
	}

	//--------------------------------------------------------------------------

	public final void
	setBaseFilter(String base_filter) {
		m_base_filter = base_filter == null || base_filter.length() == 0 ? null : base_filter;
	}

	//--------------------------------------------------------------------------

	public static void
	setBaseFilter(String view_def_name, String base_filter, Request r) {
		((ViewState)r.getOrCreateState(ViewState.class, view_def_name)).setBaseFilter(base_filter);
	}

	//--------------------------------------------------------------------------

	public final void
	setFilter(String filter) {
		m_filter = filter;
		if (m_filter != null && m_filter.length() == 0)
			m_filter = null;
		m_row_window_start = 0;
	}

	//--------------------------------------------------------------------------

	public static void
	setFilter(String view_def_name, String filter, Request r) {
		ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
		if (!Site.areEqual(filter, view_state.m_filter))
			view_state.setFilter(filter);
	}

	//--------------------------------------------------------------------------

	public final void
	setKeyValue(int key_value) {
		m_key_value = key_value;
	}

	//--------------------------------------------------------------------------

	public static void
	setKeyValue(String view_def_name, int key_value, Request r) {
		((ViewState)r.getOrCreateState(ViewState.class, view_def_name)).m_key_value = key_value;
	}

	//--------------------------------------------------------------------------

	private static void
	setOrderBy(String view_def_name, String order_by, Request r) {
		ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
		if (!Site.areEqual(order_by, view_state.m_order_by.toString())) {
			view_state.m_row_window_start = 0;
			view_state.m_order_by.set(order_by);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	setQuicksearch(String view_def_name, String quicksearch, String[] columns, Request r) {
		ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
		if (!Site.areEqual(quicksearch, view_state.m_quicksearch)) {
			view_state.m_quicksearch = quicksearch;
			view_state.m_quicksearch_is_tsvector = false;
			view_state.m_row_window_start = 0;
			view_state.m_quicksearch_columns = columns;
		}
	}

	//--------------------------------------------------------------------------

	public static void
	setRowWindowSize(String view_def_name, int row_window_size, Request r) {
		if (row_window_size >= 0) {
			ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
			view_state.m_row_window_size = row_window_size;
			if (row_window_size == 0)
				view_state.m_row_window_start = 0;

		}
	}

	//--------------------------------------------------------------------------

	public static void
	setRowWindowStart(String view_def_name, int row_window_start, Request r) {
		if (row_window_start < 0)
			row_window_start = 0;
		((ViewState)r.getOrCreateState(ViewState.class, view_def_name)).m_row_window_start = row_window_start;
	}
//
//	// --------------------------------------------------------------------------
//
//	public static void
//	setShowCheckBoxes(String view_def_name, String check_boxes_name, String check_boxes_columns, Request r) {
//		ViewState view_state = (ViewState)request.getOrCreateState(ViewState.class, view_def_name);
//		view_state.m_check_boxes_name = check_boxes_name;
//		view_state.m_check_boxes_columns = check_boxes_columns.split(",");
//	}

	//--------------------------------------------------------------------------

	public static void
	setTSVectorSearch(String view_def_name, String value, Request r) {
		ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
		if (!Site.areEqual(value, view_state.m_quicksearch)) {
			view_state.m_quicksearch = value;
			view_state.m_quicksearch_is_tsvector = true;
			view_state.m_row_window_start = 0;
		}
	}

	//--------------------------------------------------------------------------

	private static void
	toggleColumn(String view_def_name, String column, Request r) {
		View v = null;
		ViewState view_state = (ViewState)r.getOrCreateState(ViewState.class, view_def_name);
		if (view_state.m_column_names_all == null) {
			v = Site.site.newView(view_def_name, r);
			view_state.m_column_names_all = v.getColumnNamesAll();
		}
		if (view_state.m_column_names_table == null) {
			if (v == null)
				v = Site.site.newView(view_def_name, r);
			view_state.m_column_names_table = v.getColumnNamesTable();
		}
		view_state.toggleColumn(column);
	}

	//--------------------------------------------------------------------------

	private void
	toggleColumn(String column) {
		int index = Array.indexOf(m_column_names_table, column);
		if (index != -1)
			if (m_column_names_table.length == 1)
				m_column_names_table = null;
			else
				m_column_names_table = Array.remove(m_column_names_table, index);
		else {
			int i = 0;
			int j = 0;
			while (true) {
				if (m_column_names_all[i].equals(column)) {
					m_column_names_table = Array.insert(m_column_names_table, column, j);
					return;
				} else if (j < m_column_names_table.length && m_column_names_all[i].equals(m_column_names_table[j]))
					++j;
				++i;
			}
		}
	}
}
