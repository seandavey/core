package db;

import app.Request;
import app.Site;
import db.column.Column;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import db.column.ColumnValueRenderer;
import db.object.DBObjects;
import web.JS;

public class Options extends DBObjects<Option> implements ColumnInputRenderer, ColumnValueRenderer {
//	private boolean	m_allow_adding;
	private boolean	m_allow_editing;
	private boolean m_allow_no_selection;
	private boolean m_text_is_value;

	//--------------------------------------------------------------------------

	public
	Options(Select query) {
		super(query, Option.class);
	}

	//--------------------------------------------------------------------------

	public Option
	getOptionByValue(String option_value) {
		for (Option option : getObjects())
			if (option_value.equals(option.getValue()))
				return option;
		return null;
	}

	//--------------------------------------------------------------------------

	public Column
	newColumn(String name) {
		return new Column(name).setInputRenderer(this).setValueRenderer(this, false);
	}

	//--------------------------------------------------------------------------

//	public Options
//	setAllowAdding(boolean allow_adding) {
//		m_allow_adding = allow_adding;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public Options
	setAllowEditing(boolean allow_editing) {
		m_allow_editing = allow_editing;
		return this;
	}

	//--------------------------------------------------------------------------

	public Options
	setAllowNoSelection(boolean allow_no_selection) {
		m_allow_no_selection = allow_no_selection;
		return this;
	}

	//--------------------------------------------------------------------------

	public Options
	setTextIsValue(boolean text_is_value) {
		m_text_is_value = text_is_value;
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	writeEditButton(String column_name, View v, Request r) {
		if (!m_allow_editing && !r.userIsAdmin())
			return;
		StringBuilder options = new StringBuilder("column:'").append(column_name).append("',db_view_def:").append(JS.string(v.getViewDef().getName()));
		if (v.getMode() == View.Mode.EDIT_FORM)
			options.append(",db_key_value:").append(v.data.getString("id"));
		String title = "Edit&nbsp;" + Site.site.getViewDef(m_query.getFirstTable(), r.db).getRecordNamePlural();
		r.w.addStyle("position:absolute;right:0;top:0")
			.setAttribute("title", title)
			.ui.buttonIconOnClick("pencil", "new Dialog({title:" + JS.string(title) + ",url:context+'/Views/" + m_query.getFirstTable() + "/component'," + options.toString() + "})");
	}

	//--------------------------------------------------------------------------
	// ColumnInputRenderer

	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r) {
		SelectRenderer s = new SelectRenderer(getObjects());
		s.setAllowNoSelection(!column.isRequired() || m_allow_no_selection).setTextIsValue(m_text_is_value);
//		if (m_allow_adding)
//			s.setLastOption(new ui.Option("Add New " + column.getDisplayName(false) + "..."));
		s.writeInput(v, f, column, false, r);
		writeEditButton(column.getName(), v, r);
	}

	//--------------------------------------------------------------------------
	// ColumnValueRenderer

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		String value = v.data.getString(column.getName());
		if (value == null)
			return;
		if (m_text_is_value)
			r.w.write(value);
		else
			for (Option option : getObjects())
				if (value.equals(option.getValue() == null ? option.getText() : option.getValue())) {
					r.w.write(option.getText());
					return;
				}
	}
}
