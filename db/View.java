package db;

import java.net.URLDecoder;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import app.Request;
import app.Site;
import db.access.AccessPolicy;
import db.column.ColumnBase;
import db.column.FileColumn;
import db.column.PasswordColumn;
import db.column.UsernameColumn;
import db.feature.Feature;
import db.feature.Feature.Location;
import db.feature.QuickFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Section;
import db.section.Tabs;
import ui.Dropdown;
import ui.Table;
import util.Array;
import util.Text;
import web.HTMLWriter;
import web.Head;
import web.JS;
import web.JSONBuilder;
import web.URLBuilder;

public class View {
	public enum Mode { ADD_FORM, EDIT_FORM, LIST, READ_ONLY_FORM, READ_ONLY_LIST, SELECT_LIST }

	private final boolean				m_add_one_columns = true; // TODO: look into making this false by default
	private final Map<String,Object>	m_column_data = new HashMap<>();
	private Map<String,ColumnBase<?>>	m_columns;
	private String[]					m_column_names_form;
	private String[]					m_column_names_form_table;
	private String[]					m_column_names_table;
	protected double[]					m_grand_totals;
	protected boolean					m_has_buttons_column;
	protected String					m_id;
	protected Mode						m_mode = Mode.LIST;
	private int							m_num_cols = -1;
	protected int						m_num_rows = -1;
	protected int						m_num_totals_written;
	protected final Pager				m_pager;
	private String[]					m_previous_section_values;
	private boolean						m_printer_friendly;
	protected Select					m_query;
	protected final Request				m_r;
	protected boolean					m_request_processed;
	protected Relationship				m_relationship;
	private final List<Relationship>	m_relationships = new ArrayList<>();
	private int							m_reorderable_id;
	private int							m_rwc_id;
	private String						m_select_columns;
	protected final ViewState			m_state;
	protected final ViewDef				m_view_def;
	protected int						m_total_rows;
	private double[]					m_totals;
	private String						m_where;
	protected final HTMLWriter			m_w;

	public Rows							data;

	// --------------------------------------------------------------------------

	public
	View(ViewDef view_def, Request r) {
		m_view_def = view_def;
		m_r = r;
		m_select_columns = view_def.getSelectColumns();
		m_state = (ViewState)r.getOrCreateState(ViewState.class, view_def.getName());
		m_w = r.w;
		List<RelationshipDef<?>> relationship_defs = view_def.getRelationshipDefs();
		if (relationship_defs != null)
			for (RelationshipDef<?> relationship_def : relationship_defs)
				m_relationships.add(new Relationship(relationship_def, this));
		m_pager = new Pager(m_state, m_view_def, m_w);
	}

	// --------------------------------------------------------------------------

	protected boolean
	beforeRow() {
		List<Section> sections = m_view_def.getSections();
		if (sections == null)
			return true;
		for (int i=0; i<sections.size(); i++)
			if (m_state.isSectionActive(i)) {
				m_previous_section_values[i] = sections.get(i).beforeRow(this, m_previous_section_values[i], m_r);
				if ("".equals(m_previous_section_values[i])) // signal to finish table
					return false;
			}
		return true;
	}

	// --------------------------------------------------------------------------

	private boolean
	canDeleteRow() {
		AccessPolicy access_policy = m_view_def.getAccessPolicy();
		return access_policy == null || access_policy.showDeleteButtonForRow(data, m_r);
	}

	// --------------------------------------------------------------------------

	private boolean
	canViewColumn(ColumnBase<?> column) {
		if (column == null)
			return true;
		if (column instanceof UsernameColumn || column instanceof PasswordColumn || column.isHidden(m_r) || !column.userCanView(m_mode, data, m_r))
			return false;
		return true;
	}

	// --------------------------------------------------------------------------

	public String
	componentOpen() {
		m_state.clearFilter();
		URLBuilder url = new URLBuilder(Site.context).append("/Views/").append(getRoot().m_view_def.getName());
		if (m_relationship != null)
			url.set("db_relationship", m_relationship.def.many_view_def_name);
		url.set("db_mode", m_mode.toString());
		m_w.setAttribute("data-view", m_view_def.getName());
		m_id = m_w.componentOpen(url.toString());
		return m_id;
	}

	//--------------------------------------------------------------------------

	public static void
	doDelete(Request r) {
		String view_def_name = r.getPathSegment(1);
		ViewDef	view_def = Site.site.getViewDef(view_def_name, r.db);
		if (view_def == null)
			return;
		String one_table = r.getParameter("one_table"); // exists if we're unlinking a OneToManyLink
		if (one_table != null)
			r.db.update(view_def.getFrom(), one_table + "_id=NULL", r.getInt("id", 0));
		else {
			String table = r.getParameter("table");
			view_def.delete(table != null ? table : view_def.getFrom(), r.getInt("id", 0), r.getParameter("column"), r.getInt("id2", 0), r.getParameter("column2"), r);
		}
		r.response.publish(view_def_name, "delete");
	}

	// --------------------------------------------------------------------------

	public static void
	doGet(Request r) {
		String view_def_name = r.getPathSegment(1);
		View v = Site.site.newView(view_def_name, r);
		if (v == null)
			return;
		String segment_two = r.getPathSegment(2);
		if (segment_two == null) {
			v.write();
			return;
		}
		switch(segment_two) {
		case "add":
			v.newAddForm().write();
			return;
		case "component":
			v.writeComponent();
			return;
		case "input":
			int key_value = r.getInt("db_key_value", 0);
			Form f = key_value == 0 ? v.newAddForm() : v.newEditForm();
			if (f != null) {
				String column_name = r.getParameter("column");
				ColumnBase<?> column = v.getColumn(column_name);
				f.writeLabel(column_name);
				f.writeColumnInput(column_name, column, false);
			}
			return;
//		case "json":
//			r.response.setContentType("application/javascript");
//			v.writeJSON();
//			return;
		case "many_table_row":
			int one_id = r.getPathSegmentInt(3);
			v.selectByID(one_id);
			String many = r.getPathSegment(4);
			Relationship relationship = v.getRelationship(many);
			if (relationship != null)
				relationship.writeManyTableRow(one_id, r);
			else
				r.log("relationship " + many + " not found", false);
			return;
		case "page":
			Site.site.pageOpen(view_def_name, true, r);
			v.writeComponent();
			r.close();
			return;
		case "print":
			new Head(Site.site.getDisplayName(), r)
				.script("site-min")
				.styleSheet("report")
				.close();
			r.w.write("<div class=\"report\">");
			v.setPrinterFriendly(true).writeComponent();
			r.w.write("</div>");
			r.close();
			return;
		case "value":
			v.selectByID(r.getInt("db_key_value", -1));
			r.w.write(v.data.getString(r.getParameter("column")));
			return;
		}
	}

	// --------------------------------------------------------------------------

	public static void
	doPost(Request r) {
		String view_def_name = r.getPathSegment(1);
		ViewDef	view_def = Site.site.getViewDef(view_def_name, r.db);

		String segment_two = r.getPathSegment(2);
		if (segment_two == null)
			return;
		switch(segment_two) {
		case "insert":
			String db_view_def = r.getParameter("db_view_def");
			if (db_view_def != null)
				view_def = Site.site.getViewDef(db_view_def, r.db);
			Result result = view_def.insert(r);
			if (result != null && result.error != null)
				r.response.addError(result.error);
			return;
		case "link":
			String one_name = r.getParameter("db_one_name");
			r.db.update(view_def.getFrom(), Site.site.getViewDef(one_name, r.db).getFrom() + "_id=" + ((ViewState)r.getOrCreateState(ViewState.class, one_name)).getKeyValue(), r.getInt("many_id", -1));
			r.response.add("relationship", new JsonObject().add("one", one_name).add("many", view_def_name));
			r.response.js(view_def.getOnSuccess(r));
			return;
		case "reorder":
			view_def.getReorderable(r).reorder(r);
			return;
		case "actions":
			String action = r.getPathSegment(3);
			if (action != null)
				view_def.getActions().get(action).perform(r.getPathSegment(4), r);
			return;
		case "update":
			String error = view_def.update(r);
			if (error != null)
				r.response.addError(error);
			return;
		}
	}

	// --------------------------------------------------------------------------

	public final ColumnBase<?>
	getColumn(String name) {
		ColumnBase<?> column = null;

		if (m_columns != null)
			column = m_columns.get(name);
		if (column == null)
			column = m_view_def.getColumn(name);
		if (column == null) {
			int index = name.indexOf('.');
			if (index != -1)
				return getColumn(name.substring(index + 1));
		}

		return column;
	}

	// --------------------------------------------------------------------------

	public final String
	getColumnHTML(String column) {
		m_w.captureStart();
		writeColumnHTML(column);
		return m_w.captureEnd();
	}

	// --------------------------------------------------------------------------

	public final Map<String,Object>
	getColumnData() {
		return m_column_data;
	}

	// --------------------------------------------------------------------------

	protected final String[]
	getColumnNamesAll() {
		ArrayList<String> column_names = new ArrayList<>();
		String[] column_names_all = m_view_def.getColumnNamesAll();
		if (column_names_all == null)
			column_names_all = getColumnNamesTable();
		for (String column_name : column_names_all)
			if (canViewColumn(getColumn(column_name)))
				column_names.add(column_name);
		return column_names.toArray(new String[column_names.size()]);
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNamesForm() {
		if (m_column_names_form == null)
			m_column_names_form = m_view_def.getColumnNamesForm();
		if (m_column_names_form == null) {
			JDBCTable table = m_r.db.getJDBCTable(m_view_def.getFrom());
			if (table != null)
				if (m_view_def.getName().startsWith("admin__"))
					m_column_names_form = table.getColumnNamesAll();
				else
					m_column_names_form = table.getColumnNames();
		}
		return m_column_names_form;
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNamesFormTable() {
		if (m_column_names_form_table == null)
			m_column_names_form_table = m_view_def.getColumnNamesFormTable();
		if (m_column_names_form_table == null)
			if (m_view_def.getName().startsWith("admin__"))
				m_column_names_form_table = m_r.db.getJDBCTable(m_view_def.getFrom()).getColumnNamesAll();
			else if (data != null)
				m_column_names_form_table = data.getColumnNames();
			else
				m_column_names_form_table = m_r.db.getJDBCTable(m_view_def.getFrom()).getColumnNames();
		return m_column_names_form_table;
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNamesTable() {
		String[] column_names_table = m_state.getColumnNamesTable(null);
		if (column_names_table != null)
			return column_names_table;
		if (m_column_names_table == null)
			m_column_names_table = m_view_def.getColumnNamesTable();
		if (m_column_names_table == null)
			if (!"*".equals(m_select_columns)) {
				m_column_names_table = m_select_columns.split(",");
				for (int i=0; i<m_column_names_table.length; i++) {
					int index = m_column_names_table[i].indexOf(" as ");
					if (index != -1)
						m_column_names_table[i] = m_column_names_table[i].substring(index + 4);
					else {
						index = m_column_names_table[i].indexOf('.');
						if (index != -1)
							m_column_names_table[i] = m_column_names_table[i].substring(index + 1);
					}
				}
			} else if (m_view_def.getName().startsWith("admin__"))
				m_column_names_table = m_r.db.getJDBCTable(m_view_def.getFrom()).getColumnNamesAll();
			else if (data != null)
				m_column_names_table = data.getColumnNames();
			else {
				JDBCTable table = m_r.db.getJDBCTable(m_view_def.getFrom());
				if (table == null)
					m_r.abort("table " + m_view_def.getFrom() + " not found in View.getColumnNamesTable()");
				m_column_names_table = table.getColumnNames();
			}
		return m_column_names_table;
	}

	// --------------------------------------------------------------------------

	public final String
	getFrom() {
		return m_view_def.getFrom();
	}

	// --------------------------------------------------------------------------

	public final int
	getID() {
		if (!m_request_processed) {
			String db_relationship = Validation.getValidIdentifier("db_relationship", m_r);
			if (db_relationship == null || m_relationship != null && db_relationship.equals(m_relationship.def.many_view_def_name))
				m_state.setKeyValue(m_r.getInt("db_key_value", 0));
		}
		return m_state.getKeyValue();
	}

	// --------------------------------------------------------------------------

	public final Mode
	getMode() {
		if (!m_request_processed) {
			String db_relationship = Validation.getValidIdentifier("db_relationship", m_r);
			if (db_relationship == null || m_relationship != null && db_relationship.equals(m_relationship.def.many_view_def_name)) {
				String db_mode = Validation.getValidIdentifier("db_mode", m_r);
				if (db_mode != null)
					setMode(Mode.valueOf(db_mode));
			}
		}
		return m_mode;
	}

	// --------------------------------------------------------------------------

	public final int
	getNumCols() {
		if (m_num_cols == -1) {
			m_num_cols = 0;
			for (String column : getColumnNamesTable()) {
				ColumnBase<?> c = getColumn(column);
				if (c != null)
					if (c.isHidden(m_r) || !c.userCanView(m_mode, data, m_r))
						continue;
				++m_num_cols;
			}
			if (showButtons())
				++m_num_cols;
		}
		return m_num_cols;
	}

	// --------------------------------------------------------------------------

	public final int
	getNumRows() {
		return m_num_rows;
	}

	// --------------------------------------------------------------------------

	public final int
	getOneId(Request r) {
		int one_id = r.getInt("one_id", 0);
		if (one_id != 0)
			return one_id;
		return m_relationship.one.getState().getKeyValue();
	}

	// --------------------------------------------------------------------------

	public final String
	getOnClickForRow(int id, Mode mode) {
		URLBuilder url = getRowURL(id, mode);
		List<Section> sections = m_view_def.getSections();
		if (sections != null)
			for (Section section : sections)
				section.setOnClickForRowParams(url, mode, data);

		url.append("/component");
		if (mode == View.Mode.ADD_FORM)
			return m_view_def.getAddButtonOnClick(url.toString());
		if (mode == View.Mode.EDIT_FORM)
			return "_.table.dialog_edit(this,'" + m_view_def.getName() + "'," + id + ",'" + url.toString() + "')";
//		if (mode == View.Mode.READ_ONLY_FORM) {
		StringBuilder s = new StringBuilder();
		s.append("_.table.dialog_view(this,'").append(m_view_def.getName()).append("','").append(url.toString()).append("'");
		String view_link_column = m_view_def.getViewLinkColumn();
		if (view_link_column != null)
			s.append(",").append(JS.string(data.getString(view_link_column)));
		s.append(")");
		return s.toString();
	}

	// --------------------------------------------------------------------------

	public final Pager
	getPager() {
		return m_pager;
	}

	// --------------------------------------------------------------------------

	public final Relationship
	getRelationship() {
		return m_relationship;
	}

	// --------------------------------------------------------------------------

	public final Relationship
	getRelationship(String many_table_view_def_name) {
		return getRelationship(many_table_view_def_name, new ArrayList<>());
	}

	// --------------------------------------------------------------------------

	private Relationship
	getRelationship(String many_table_view_def_name, List<String> stack) {
		if (stack.indexOf(m_view_def.getName()) != -1)
			return null;

		for (Relationship relationship : m_relationships)
			if (relationship.def.many_view_def_name.equals(many_table_view_def_name))
				return relationship;

		// note: is this still needed?
		m_r.log("deeper relationship", true);
		stack.add(m_view_def.getName());
		for (Relationship relationship : m_relationships) {
			Relationship r = relationship.getMany(m_r).getRelationship(many_table_view_def_name, stack);
			if (r != null)
				return r;
		}
		stack.remove(stack.size() - 1);

		return null;
	}

	// --------------------------------------------------------------------------

	public final List<Relationship>
	getRelationships() {
		return m_relationships;
	}

	// --------------------------------------------------------------------------

	protected final View
	getRoot() {
		if (m_relationship != null)
			return m_relationship.one.getRoot();
		return this;
	}

	// --------------------------------------------------------------------------

	private URLBuilder
	getRowURL(int id, Mode mode) {
		URLBuilder url = new URLBuilder(Site.context).append("/Views/").append(getRoot().m_view_def.getName());
		if (m_relationship != null) {
			url.set("db_relationship", m_relationship.def.many_view_def_name);
			url.set("one_id", m_relationship.one.data.getInt("id"));
		}
		if (mode != Mode.ADD_FORM)
			url.set("db_key_value", id);
		url.set("db_mode", mode);
		return url;
	}

	// --------------------------------------------------------------------------

	private String
	getSelectFrom() {
		String select_from = m_view_def.getSelectFrom();
		if (select_from == null)
			return getFrom();
		int index = select_from.indexOf("$(@user id)");
		if (index != -1)
			select_from = select_from.substring(0, index) + m_r.getUser().getId() + select_from.substring(index + 11);
		return select_from;
	}

	// --------------------------------------------------------------------------

	public final ViewState
	getState() {
		return m_state;
	}

	// --------------------------------------------------------------------------

	private URLBuilder
	getURL(Mode mode) {
		URLBuilder url = new URLBuilder(Site.context);
		url.append("/Views/").append(getRoot().m_view_def.getName());
		if (m_relationship != null)
			url.set("db_relationship", m_relationship.def.many_view_def_name);
		if (mode != null)
			url.set("db_mode", mode);
		return url;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	getViewDef() {
		return m_view_def;
	}

	// --------------------------------------------------------------------------

	public final String
	getWhere() {
		return m_where;
	}

	// --------------------------------------------------------------------------

	public final boolean
	hasFileColumn() {
		if (m_view_def.getFileColumns(m_r) != null)
			return true;
		if (m_columns == null)
			return false;
		for (ColumnBase<?> column : m_columns.values())
			if (column instanceof FileColumn && !column.isHidden(m_r))
				return true;
		return false;
	}

	// --------------------------------------------------------------------------

	private void
	initSections() {
		List<Section> sections = m_view_def.getSections();
		if (sections == null)
			return;
		m_previous_section_values = new String[sections.size()];
		for (int i=0; i<sections.size(); i++)
			m_previous_section_values[i] = sections.get(i).beforeTable(this, m_r);
	}

	// --------------------------------------------------------------------------

	final void
	initTotals(String[] columns) {
		for (String column_name : columns) {
			ColumnBase<?> column = getColumn(column_name);
			if (column != null && column.total()) {
				m_totals = new double[columns.length];
				m_grand_totals = new double[columns.length];
				m_num_totals_written = 0;
				break;
			}
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	isPrinterFriendly() {
		return m_printer_friendly;
	}

	// --------------------------------------------------------------------------

	protected final boolean
	isRowWindowLast() {
		if (m_state.getRowWindowSize() == 0)
			return false;
		if (m_num_rows == m_state.getRowWindowSize() && (m_view_def.getSections() == null || !m_state.isSectionActive(0) || !(m_view_def.getSections().get(0) instanceof Tabs))) {
			m_total_rows = m_state.getRowWindowStart() + m_num_rows;
			while (next())
				++m_total_rows;
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	public final Form
	newAddForm() {
		m_mode = Mode.ADD_FORM;
		return newEditForm();
	}

	// --------------------------------------------------------------------------

	public Form
	newEditForm() {
		if (!m_request_processed)
			return processRequest(this, m_r).newEditForm();

		if (m_mode == Mode.LIST)
			m_mode = Mode.EDIT_FORM;

		if (m_mode == Mode.ADD_FORM)
			data = null;
		else if (m_mode == Mode.EDIT_FORM || m_mode == Mode.READ_ONLY_FORM) {
			if (data == null) {
				if (m_state.getKeyValue() == 0) {
					m_r.log("m_state.key_value is zero in View.newEditForm", false);
					return null;
				}
				selectByID(m_state.getKeyValue());
				if (!data.isFirst()) {
					m_r.response.addError(m_view_def.getRecordName() + " with id " + m_state.getKeyValue() + " not found");
					return null;
				}
			} else
				m_state.setKeyValue(data.getInt("id"));
			if (m_view_def.getAccessPolicy() != null && !m_r.userIsAdmin() && !m_view_def.getAccessPolicy().canUpdateRow(m_view_def.getFrom(), m_state.getKeyValue(), m_r))
				m_mode = Mode.READ_ONLY_FORM;
		}
		return m_view_def.newForm(data == null ? 0 : data.getInt("id"), this, m_r);
	}

	// --------------------------------------------------------------------------

	public final boolean
	next() {
		if (data == null || !data.next())
			return false;
		if (data.getColumnIndex("id") > 0)
			m_state.setKeyValue(data.getInt("id"));
		return true;
	}

	// --------------------------------------------------------------------------

	public static View
	processRequest(View v, Request r) {
		String db_relationship = r.getParameter("db_relationship");
		if (db_relationship != null && !db_relationship.equals(v.m_view_def.getName())) { // second check is for circular many-to-many relationships
			Relationship relationship = v.getRelationship(db_relationship);
			if (relationship != null) {
				int one_id = r.getInt("one_id", 0);
				if (one_id != 0)
					v.selectByID(one_id);
				v = relationship.getMany(r);
			} else
				r.log("null relationship " + db_relationship + " for view " + v.m_view_def.getName(), false);
		}

		String mode = r.getParameter("db_mode");
		if (mode != null && v.m_mode == Mode.LIST)
			v.m_mode = r.getEnum("db_mode", Mode.class);
		String where = r.getParameter("where");
		if (where != null) // ever true?
			if (v.m_mode == Mode.ADD_FORM && v.m_relationship != null) {
				v.m_relationship.one.setWhere(where);
				v.m_relationship.one.select();
				v.m_relationship.one.next();
			} else
				v.setWhere(where);

		v.m_state.processRequest(r);

		v.m_request_processed = true;

		return v;
	}

	// --------------------------------------------------------------------------

	public View
	select() {
		m_num_rows = 0;
		Select query = m_view_def.getSelect();
		if (query == null)
			query = new Select(m_select_columns).from(getSelectFrom()).where(m_where);

		// columns
		if (!m_select_columns.equals("*")) {
			if (m_view_def.getAccessPolicy() != null)
				m_view_def.getAccessPolicy().adjustQuery(query);
			if (m_view_def.timestampRecords())
				query.addColumns("_timestamp_");
		}
		String[] column_names = getColumnNamesTable();
		for (String column_name : column_names) {
			ColumnBase<?> column = getColumn(column_name);
			if (column != null)
				column.adjustQuery(query, m_mode);
		}
		if (m_add_one_columns)
			for (Relationship relationship : m_relationships)
				if (!(relationship.def instanceof OneToManyLink) && relationship.addOneColumns(query, m_r)) // only allow one
					break;

		// from
		if (m_relationship != null && m_relationship.def instanceof ManyToMany)
			query.addFrom(((ManyToMany)m_relationship.def).getManyManyTable());

		// where
		String base_filter = m_state.getBaseFilter();
		if (base_filter != null)
			query.andWhere(base_filter);
		if (m_state.getFilter() != null)
			query.andWhere(m_state.getFilter());

		// order by
		if (m_state.getOrderBy() != null) {
			List<Section> sections = m_view_def.getSections();
			if (sections != null)
				for (int i=sections.size() - 1; i>=0; i--)
					sections.get(i).insertOrderBy(query, this, m_state, m_r);
			query.addOrderBy(m_state.getOrderBy().toQueryString(this, m_r));
		}

		return select(query);
	}

	// --------------------------------------------------------------------------

	public final View
	select(Select query) {
		m_query = query;
		data = new Rows(query, m_r.db);
		m_previous_section_values = null;
		return this;
	}

	// --------------------------------------------------------------------------

	public final boolean
	selectByID(int id) {
		Select query = new Select(m_select_columns).from(getFrom()).whereIdEquals(id);

		if (!m_select_columns.equals("*")) {
			if (m_view_def.getAccessPolicy() != null)
				m_view_def.getAccessPolicy().adjustQuery(query);
			if (m_view_def.timestampRecords())
				query.addColumns("_timestamp_");
		}

		select(query);
		return next();
	}

	// --------------------------------------------------------------------------

	public final View
	setColumn(ColumnBase<?> column) {
		if (m_columns == null)
			m_columns = new HashMap<>();
		m_columns.put(column.getName(), column);
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setColumnNamesForm(String... column_names) {
		m_column_names_form = column_names;
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setColumnNamesForm(List<String> column_names_form) {
		m_column_names_form = column_names_form.toArray(new String[column_names_form.size()]);
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setColumnNamesFormTable(String... columns) {
		m_column_names_form_table = columns;
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setColumnNamesTable(String... columns) {
		m_column_names_table = columns;
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setColumns(ColumnBase<?>... columns) {
		for (ColumnBase<?>column : columns)
			setColumn(column);
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setMode(Mode mode) {
		m_mode = mode;
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setPrinterFriendly(boolean printer_friendly) {
		m_printer_friendly = printer_friendly;
		return this;
	}

	// --------------------------------------------------------------------------

	public final View
	setWhere(String where) {
		if (where != null)
			try {
				where = URLDecoder.decode(where, "UTF-8");
			} catch (Exception e) {
			}

		m_where = where;
		data = null;
		return this;
	}

	// --------------------------------------------------------------------------

	final boolean
	showAddButton() {
		if (m_printer_friendly || m_mode == Mode.SELECT_LIST || m_relationship != null && m_relationship.one.m_mode == Mode.READ_ONLY_FORM && !m_relationship.def.showAddButtonOnReadOnlyForm())
			return false;
		return m_view_def.getAccessPolicy() == null || m_view_def.getAccessPolicy().showAddButton(this, m_r);
	}

	// --------------------------------------------------------------------------

	private boolean
	showButtons() {
		if (m_mode == Mode.READ_ONLY_LIST || m_mode == Mode.SELECT_LIST || m_printer_friendly) // || m_state.getCheckBoxesName() != null)
			return false;
		AccessPolicy access_policy = m_view_def.getAccessPolicy();
		return access_policy == null ||
			access_policy.showViewButtons(m_r) ||
			access_policy.showDeleteButtons(data, m_r) ||
			access_policy.showEditButtons(data, m_r);
	}

	// --------------------------------------------------------------------------

	protected final boolean
	skip() {
		m_num_rows = 0;
		for (int i=0; i<m_state.getRowWindowStart(); i++)
			//			if (m_view_def.getAccessPolicy() != null && !m_view_def.getAccessPolicy().showRow(this, m_r))
//				--i;
			if (!next())
				return false;
		return true;
	}

	// --------------------------------------------------------------------------

	protected void
	tableClose(Table table, String[] columns) {
		table.close();
		if (m_view_def.allowQuickEdit())
			writeJavascriptQuickEdit(columns);
		if (m_reorderable_id != 0 && m_num_rows > 1)
			m_view_def.getReorderable(m_r).write(m_reorderable_id, m_view_def, m_w);
	}

	// --------------------------------------------------------------------------

	protected Table
	tableOpen() {
		Table table = m_w.ui.table().addDefaultClasses();
		table.addAttribute("data-view", m_view_def.getName());
		String o = Section.getOrderBys(m_view_def.getSections());
		if (o == null)
			o = "_order_";
		else
			o += ",_order_";
		boolean reorderable = m_view_def.getReorderable(m_r) != null && (!m_view_def.allowSorting() || m_query != null && o.equals(m_query.getOrderBy()));
		if (reorderable) {
			m_reorderable_id = m_w.newID();
			table.setId("reorderable" + m_reorderable_id);
		}
//		if (m_view_def.allowQuickEdit())
//			m_w.setAttribute("onclick", "quick_edit(event,'" + m_view_def.getName() + "');return true");
//		m_w.setId(m_view_def.getName());
		if (m_r.userIsAdministrator())
			table.addAttribute("onclick", "JS.get('admin-min',function(){admin.table_menu(this,'" + m_view_def.getName() + "')}.bind(event))");
		return table.open();
	}

	// --------------------------------------------------------------------------

	public void
	write() {
		if (!m_request_processed) {
			processRequest(this, m_r).write();
			return;
		}

		if (m_mode == Mode.LIST || m_mode == Mode.READ_ONLY_LIST || m_mode == Mode.SELECT_LIST) {
			writeViewJSON();
			writeFilters();
			if (m_view_def.getFeatures() != null) {
				m_w.write("<table style=\"margin-bottom:5px;white-space:nowrap;width:100%;\"><tr><td onmousedown=\"if(event.detail>1)event.preventDefault()\">");
				writeFeatures(Location.TOP_LEFT);
				m_w.write("</td><td onmousedown=\"if(event.detail>1)event.preventDefault()\" style=\"text-align:right\">");
				if (m_view_def.getPrintButtonLocation() == Location.TOP_RIGHT && !m_printer_friendly && m_relationship == null)
					writePrintButton();
				writeFeatures(Location.TOP_RIGHT);
				m_w.write("</td></tr></table>");
			}
			List<Section> sections = m_view_def.getSections();
			if (sections != null && m_state.isSectionActive(0))
				sections.get(0).writeList(this, m_r);
			else
				writeList();
			if (m_mode == Mode.LIST)
				for (Relationship r : m_relationships) {
					ViewDef v = Site.site.getViewDef(r.def.many_view_def_name, m_r.db);
					if (v.showAddButtonOnOneList())
						writeViewJSON(v, r);
				}
			else if (m_mode == Mode.SELECT_LIST)
				m_w.write("<p><small><strong>Click a row to link that " + m_view_def.getRecordName().toLowerCase() + "</strong></small></p>");
			int id = m_r.getInt("edit", 0);
			if (id != 0)
				m_w.js("window.addEventListener('load',function(){" + getOnClickForRow(id, Mode.EDIT_FORM) + "})");
		} else {
			Form edit_form = newEditForm();
			if (edit_form != null)
				edit_form.write();
		}
	}

	// --------------------------------------------------------------------------

//	public void
//	writeAddForm() {
//		m_mode = Mode.ADD_FORM;
//		writeEditForm();
//	}

	// --------------------------------------------------------------------------

	private void
	writeButtonDelete() {
		String button_text = m_view_def.getDeleteButtonText();
		String params;
		if (m_relationship == null || m_relationship.def instanceof OneToMany) {
			if (!m_r.db.hasColumn(m_view_def.getFrom(), "id")) { // many many table, probably in admin)
				String[] columns = m_r.db.getJDBCTable(m_view_def.getFrom()).getColumnNames();
				params = "id=" + data.getInt(columns[0]) + "&column=" + columns[0] + "&id2=" + data.getInt(columns[1]) + "&column2=" + columns[1];
			} else
				params = "id=" + data.getInt("id");
			if (button_text == null)
				button_text = Text.delete;
		} else {
			String one_table = m_relationship.one.getFrom();
			if (m_relationship.def instanceof OneToManyLink)
				params = "one_table=" + one_table + "&id=" + m_state.getKeyValue();
			else { // ManyToMany
				ViewState one_state = (ViewState)m_r.getOrCreateState(ViewState.class, m_relationship.one.getViewDef().getName());
				params = "id=" + one_state.getKeyValue() + "&column=" + one_table + "_id&id2=" + data.getInt("id") + "&column2=" + m_view_def.getFrom() + "_id";
			}
			if (button_text == null)
				button_text = Text.remove;
		}
		m_w.ui.buttonOutlineOnClick(button_text, "_.db.delete(this,'" + Text.capitalize(button_text) + "','" + m_view_def.getName() + "','" + params + "',false)");
	}

	// --------------------------------------------------------------------------

	protected final void
	writeButtons() {
		boolean first = true;
		if (m_view_def.getAccessPolicy() != null && m_view_def.getAccessPolicy().showViewButtonForRow(this, m_r)) {
			m_w.ui.buttonOutlineOnClick(Text.view, getOnClickForRow(data.getInt("id"), Mode.READ_ONLY_FORM));
			first = false;
		}
		boolean write_button_edit = (m_relationship == null || !(m_relationship.def instanceof OneToManyLink)) && (m_view_def.getAccessPolicy() == null || m_view_def.getAccessPolicy().showEditButtonForRow(data, m_r));
		if (write_button_edit) {
			if (first)
				first = false;
			else
				m_w.space();
			m_w.ui.buttonOutlineOnClick(m_view_def.getEditButtonText(this, m_r), getOnClickForRow(data.getInt("id"), Mode.EDIT_FORM));
		}
		if (canDeleteRow()) {
			if (first)
				first = false;
			else
				m_w.space();
			writeButtonDelete();
		}
		for (Relationship r : m_relationships) {
			ViewDef many_view_def = Site.site.getViewDef(r.def.many_view_def_name, m_r.db);
			if (many_view_def.showAddButtonOnOneList()) {
				if (first)
					first = false;
				else
					m_w.space();
				m_w.ui.buttonOutlineOnClick(many_view_def.getAddButtonText(), many_view_def.getAddButtonOnClick(r, true));
			}
		}
	}

	// --------------------------------------------------------------------------

	protected void
	writeCells(String[] columns) {
		for (int i=0; i<columns.length; i++) {
			ColumnBase<?> column = getColumn(columns[i]);
			if (!canViewColumn(column))
				continue;
			if (column != null) {
				String text_align = column.getTextAlign();
				if (text_align != null)
					m_w.addStyle("text-align:" + text_align);
				if (column.total())
					m_totals[i] += column.getDouble(this, m_r);
			}

			m_w.tagOpen("td");
			if (!writeColumnHTML(columns[i]))
				m_w.nbsp();
			m_w.tagClose();
		}
	}

	// --------------------------------------------------------------------------

	private void
	writeColumnHead(String column_name, boolean include_dropdown) {
		ColumnBase<?> column = getColumn(column_name);
		if (column != null) {
			String text_align = column.getTextAlign();
			if ("center".equals(text_align))
				m_w.addStyle("text-align:" + text_align);
		}
		String column_head = column == null ? ColumnBase.getDisplayName(column_name, false) : column.getColumnHead();
		boolean sortable = column == null || column.isSortable() || m_view_def.getRowWindowSize() == 0;
		if (sortable)
			if (column != null && column.getSortType() != null)
				m_w.setAttribute("data-sort", column.getSortType());
			else if (data != null && data.hasColumn(column_name)) {
				int type = data.getColumnType(column_name);
				if (type == Types.DATE || type == Types.TIMESTAMP)
					m_w.setAttribute("data-sort", "date");
			}
		if (!include_dropdown) {
			m_w.tag("th", column_head);
			return;
		}
		m_w.addStyle("position:relative");
		boolean show_menu = column == null || column.isFilterable() || column.isSortable() || m_view_def.getRowWindowSize() == 0;
		Dropdown dropdown = null;
		if (show_menu) {
			dropdown = m_w.ui.dropdown();
			m_w.addStyle("cursor:pointer")
				.setAttribute("onclick", dropdown.getMenuJS())
				.setAttribute("onmouseenter", "this.classList.add('table-active')")
				.setAttribute("onmouseleave", "this.classList.remove('table-active')");
		}
		m_w.setAttribute("data-column", column_name)
			.tagOpen("th");
		m_w.write(m_view_def.columnHeadsCanWrap() ? "<div style=\"white-space:normal\">" : "<div>")
			.write(column_head);
		if (show_menu) {
			if (m_view_def.showHead() && (column == null || column.isFilterable()))
				dropdown.aOnClick("Add filter...", "_.table.add_filter(" + JS.string(m_view_def.getName()) + "," + JS.string(column_head) + ",this.closest('table'))");
			if (sortable) {
				writeSortMenuItem(dropdown, column_name, false);
				writeSortMenuItem(dropdown, column_name, true);
			}
			dropdown.close();
		}
		m_w.write("</div>")
			.tagClose();
	}

	// --------------------------------------------------------------------------

	public void
	writeColumnHeadsRow(String[] columns, boolean include_popup_menu) {
		int num_columns = 0;
		for (String column : columns)
			if (canViewColumn(getColumn(column)))
				num_columns++;
		if (num_columns < 2)
			return;
		m_w.write("<thead class=\"table-primary\"><tr class=\"column_heads\" style=\"inset-block-start:0;position:sticky;white-space:nowrap\">");
//		if (m_state.getCheckBoxesName() != null) {
//			m_w.write("<th>");
//			m_w.ui.buttonOutlineOnClick("Select All", "_.form.set_all_cb(this.closest('form'),'db_check',this.value!=='select all');this.value=this.value=='select all'?'deselect all':'select all'");
//			m_w.write("</th>");
//		}
		for (String column : columns)
			if (canViewColumn(getColumn(column)))
				writeColumnHead(column, include_popup_menu);
		if (showButtons())
			m_w.addClass("db_button_cell")
				.addStyle("padding-right:0 !important;position:relative;white-space:nowrap")
				.tag("th", "&nbsp;");
		m_w.write("</tr></thead>");
	}

	// --------------------------------------------------------------------------

	public final boolean
	writeColumnHTML(String column_name) {
		boolean something_written;
		boolean view_link = column_name.equals(m_view_def.getViewLinkColumn());
		if (view_link)
			m_w.captureStart();
		ColumnBase<?> column = getColumn(column_name);
		if (column != null)
			something_written = column.writeValue(this, m_column_data, m_r);
		else
			something_written = ColumnBase.writeValue(column_name, m_mode == Mode.LIST || m_mode == Mode.READ_ONLY_LIST ? 100 : 0, this, m_r);
		if (view_link) {
			m_w.addStyle("text-decoration:none")
				.aOnClick(m_w.captureEnd(), getOnClickForRow(data.getInt("id"), Mode.READ_ONLY_FORM));
			something_written = true;
		}
		return something_written;
	}

	// --------------------------------------------------------------------------

	private void
	writeColumnsPopup() {
		String[] column_names_all = m_state.getColumnNamesAll(this);
		String[] column_names_table = m_state.getColumnNamesTable(this);
		Dropdown dropdown = m_w.ui.dropdown().setDropdownMenuRight(true);
		m_w.addStyle("display:inline-block;position:relative")
			.setOnClick(dropdown.getMenuJS())
			.tagOpen("div");
		m_w.addClass("dropdown-toggle")
			.addStyle("border:0;cursor:pointer")
			.setAttribute("title", "toggle columns on and off")
			.ui.buttonIcon("layout-three-column");
		for (String column_name : column_names_all) {
			ColumnBase<?> column = getColumn(column_name);
			if (!canViewColumn(column))
				continue;
			String text = column == null ? ColumnBase.getDisplayName(column_name, true) : column.getDisplayName(true);
			if (column_names_table == null || Array.indexOf(column_names_table, column_name) != -1)
				text = "<img src=\"" + Site.context + "/images/check.png\"> " + text;
			else
				text = "<img src=\"" + Site.context + "/images/check.png\" style=\"visibility:hidden;\"> " + text;
			dropdown.aOnClick(text, "net.post(context+'/ViewStates/" + m_view_def.getName() + "/toggle_column',{db_column:'" + column_name + "'},function(){net.replace(_.c(this))}.bind(this))");
		}
		dropdown.close();
		m_w.tagClose();
	}

	// --------------------------------------------------------------------------

	public void
	writeComponent() {
		if (!m_request_processed) {
			processRequest(this, m_r).writeComponent();
			return;
		}
		List<Feature> features = m_view_def.getFeatures();
		if (features != null && m_state.getFilter() == null) {
			StringBuilder s = new StringBuilder();
			for (Feature feature : features)
				if (feature instanceof QuickFilter)
					((QuickFilter)feature).appendDefaultValue(s, this, m_r);
			if (s.length() > 0)
				m_state.setFilter(s.toString());
		}
		componentOpen();
		write();
		m_w.tagClose();
	}

	// --------------------------------------------------------------------------

	public final void
	writeEditForm() {
		if (!m_request_processed) {
			processRequest(this, m_r).writeEditForm();
			return;
		}

		Form f = newEditForm();
		if (f != null) {
			m_w.componentOpen(Site.context + "/Views/" + m_view_def.getName() + "?db_mode=" + m_mode.toString());
			f.write();
			m_w.tagClose();
		}
	}

	// --------------------------------------------------------------------------

	private void
	writeFeatures(Location location) {
		List<Feature> features = m_view_def.getFeatures();
		boolean first = true;
		boolean has_quick_filter = false;
		for (Feature feature : features) {
			if (feature.getLocation() != location)
				continue;
			if (feature instanceof QuickFilter) {
				if (first)
					m_w.setId("quickfilter");
				has_quick_filter |= feature.write(location, this, m_r);
				if (first && has_quick_filter)
					first = false;
			} else
				feature.write(location, this, m_r);
			m_w.space();
		}
		if (has_quick_filter)
			m_w.js("new QuickFilters(" + JS.string(m_view_def.getName()) + ");");
	}

	// --------------------------------------------------------------------------

	private void
	writeFilterButton() {
		for (String column_name : getColumnNamesTable()) {
			ColumnBase<?> column = getColumn(column_name);
			if (canViewColumn(column) && (column == null || column.isFilterable())) {
				m_w.addStyle("border:none").ui.buttonOutlineOnClick("Filter", "new FiltersDialog(" + JS.string(m_view_def.getName()) + ",this.closest('table').nextSibling).open()").nbsp();
				return;
			}
		}
	}

	// --------------------------------------------------------------------------

	private void
	writeFilters() {
		String[] filters = m_view_def.getFilters();
		if (filters != null) {
			ArrayList<Option> options = new ArrayList<>();
			String base_filter = m_state.getBaseFilter();
			String selected_option = null;

			for (String filter : filters) {
				int index = filter.indexOf('|');
				String where = filter.substring(index + 1);
				if (m_r.db.exists(m_view_def.getFrom(), where)) {
					String text = filter.substring(0, index);
					options.add(new Option(text, where));
					if (where.equals(base_filter))
						selected_option = text;
				}
			}
			if (options.size() > 1)
				new ui.Select(null, options)
					.setInline(true)
					.setSelectedOption(selected_option, base_filter)
					.setOnChange("var s=this;var f=s.options[s.selectedIndex].value;var i=f.indexOf('|');net.post(context+'/ViewStates/" + m_view_def.getName() + "',{base_filter:f.substring(i+1)},function(){net.replace(_.c(s))});")
					.write(m_r.w);
		}
	}

	// --------------------------------------------------------------------------

	private void
	writeJavascriptQuickEdit(String[] column_names) {
		StringBuilder js = new StringBuilder("_.qe.add('").append(m_view_def.getName()).append("',[");
		int num_columns = 0;
		for (String column_name : column_names) {
			JDBCColumn jdbc_column = m_r.db.getColumn(m_view_def.getFrom(), column_name);
			ColumnBase<?> column = getColumn(column_name);
			if (num_columns > 0)
				js.append(',');
			if (jdbc_column == null || !canViewColumn(column))
				js.append("null,null");
			else {
				js.append('\'').append(column_name).append("',");
				if (column != null && column.isReadOnly(this, m_r))
					js.append("null");
				else if (jdbc_column.isBoolean())
					js.append("'b'");
				else if (!column_name.endsWith("_id"))
					js.append("'s'");
				else
					js.append("null");
			}
			++num_columns;
		}
		js.append("]);");
		m_w.js(js.toString());
	}

	// --------------------------------------------------------------------------

//	private void
//	writeJSON() {
//		if (data == null) {
//			select();
//			if (!next())
//				return;
//		}
//
//		if (!skip())
//			return;
//		if (0 == data.getRow())
//			return;
//
//		m_view_def.getAccessPolicy();
//		String[] columns = getColumnNamesTable();
//
//		initSections();
//		initTotals(columns);
//
//		JSONBuilder json = new JSONBuilder(m_w.getWriter());
//		json.startObject();
//		json.array("columns", getColumnNamesTable());
//		json.startArray("rows");
//
////		if (m_view_def.showColumnHeads())
//			writeColumnHeadsRow(false);
//		do {
////			if (access_policy != null && !access_policy.showRow(this, m_r))
////				continue;
//			if (!beforeRow())
//				break;
//			json.startArray();
//			json.string(data.getString("id"));
//			for (int i=0; i<columns.length; i++) {
//				ColumnBase<?> column = getColumn(columns[i]);
//				if (!canViewColumn(column))
//					continue;
//				if (column != null && column.total())
//					m_totals[i] += column.getDouble(this, m_r);
//
//				json.string(getColumnHTML(columns[i]));
//			}
//			json.endArray();
//			++m_num_rows;
//			if (isRowWindowLast())
//				break;
//		} while (!data.isAfterLast());
//		if (m_totals != null) {
//			writeTotalsRow("Total", m_totals);
//			if (m_num_totals_written > 1 && m_view_def.showGrandTotals())
//				writeTotalsRow("Grand Total", m_grand_totals);
//		}
//		json.endArray();
//		json.endObject();
//	}

	// --------------------------------------------------------------------------

	public final void
	writeList() {
		m_view_def.beforeList(this, m_r);
		if (m_view_def.showHead())
			writeListHead();
		writeTable(m_view_def.allowSorting(), true);
		writeRowWindowControls(m_rwc_id);
		m_w.write("<div style=\"margin-top:10px;\">");
		List<Section> sections = m_view_def.getSections();
		if (sections == null || sections.get(0).writeAfterList())
			m_view_def.afterList(this, m_r);
		m_w.write("</div>");
	}

	// --------------------------------------------------------------------------

	private void
	writeListHead() {
		boolean show_add_button = showAddButton();
		if (m_mode != Mode.READ_ONLY_LIST && !m_view_def.showDoneLink() && !m_view_def.showNumRecords() && !m_view_def.showTableColumnPicker() && !show_add_button && !m_r.userIsAdministrator())
			return;
		m_w.addClass("list_head")
			.addStyle("margin:5px auto;width:100%;clear:both")
			.tagOpen("table");
		m_w.write("<tr><td>");
		if (m_mode == Mode.READ_ONLY_LIST) {
			List<Section> sections = m_view_def.getSections();
			if (sections == null || !(sections.get(0) instanceof Tabs))
				m_w.addStyle("font-size:15.6px;font-weight:bold")
					.tag("span", m_view_def.getRecordNamePlural());
			AccessPolicy access_policy = m_view_def.getAccessPolicy();
			if (m_r.userIsAdministrator() || access_policy == null || access_policy.showAddButton(this, m_r) || access_policy.showEditButtons(data, m_r) || access_policy.showDeleteButtons(data, m_r))
				m_w.space()
					.addStyle("float:right;margin-left:5px;vertical-align:baseline")
					.ui.buttonIconOnClick("pencil", "net.replace(_.c(this),'" + getURL(Mode.LIST).toString() + "')");
		} else if (m_view_def.showDoneLink())
			m_w.space().ui.buttonOutlineOnClick(Text.done, "net.replace(_.c(this),'" + getURL(Mode.READ_ONLY_LIST).toString() + "')");
		else if (m_view_def.showNumRecords())
			m_rwc_id = writeRowWindowControlsSpan();
		if (m_view_def.getFeatures() != null) {
			m_w.write("<div style=\"display:inline-block;padding-left:20px;\">");
			writeFeatures(Location.LIST_HEAD);
			m_w.write("</div>");
		}
		m_w.write("</td>");
		if (m_mode != Mode.READ_ONLY_LIST) {
			m_w.write("<td style=\"text-align:right;white-space:nowrap;padding:2px 0 2px 6px;\"> ");
			writeFilterButton();
			if (m_view_def.getPrintButtonLocation() == Location.LIST_HEAD && !m_printer_friendly && m_relationship == null) {
				writePrintButton();
				m_w.nbsp();
			}
			if ((m_view_def.showTableColumnPicker() || m_r.userIsAdministrator()) && getColumnNamesAll().length > 1) {
				writeColumnsPopup();
				m_w.nbsp();
			}
			if (show_add_button && !m_printer_friendly && m_r.getUser() != null)
				m_w.ui.buttonOutlineOnClick(m_view_def.getAddButtonText(), m_relationship != null ? m_view_def.getAddButtonOnClick(m_relationship, false) : getOnClickForRow(0, Mode.ADD_FORM));
			m_w.write("</td>");
		}
		m_w.write("</tr>")
			.tagClose();
	}

	// --------------------------------------------------------------------------

	private void
	writePrintButton() {
		m_w.setAttribute("title", "open print view in new tab")
			.addStyle("border:0")
			.ui.buttonIconOnClick("printer", m_view_def.getPrintFunction() == null ? "_.open_print_window(_.c(this))" : "_.open_print_window(_.c(this)," + m_view_def.getPrintFunction() + ")");
	}

	// --------------------------------------------------------------------------

	protected void
	writeRow(String[] columns) {
		if (m_reorderable_id != 0)
			m_w.setId("id" + data.getString("id"))
				.addStyle("cursor:pointer");
		else if (m_mode == Mode.SELECT_LIST) {
			boolean validate = m_mode == View.Mode.ADD_FORM || m_mode == View.Mode.EDIT_FORM;
			m_w.setAttribute("onclick", "_.table.select_list_click('" + data.getString("id") + "'," + (validate ? "true" : "false") + ")")
				.addStyle("cursor:pointer");
		}
		if (data.hasColumn("id"))
			m_w.setAttribute("data-id", data.getInt("id"));
		m_w.tagOpen("tr");
		writeCells(columns);
		if (showButtons()) {
			m_has_buttons_column = true;
			m_w.addClass("db_button_cell")
				.addStyle("padding-right:0 !important;text-align:right;white-space:nowrap;width:10px")
				.tagOpen("td");
			writeButtons();
			m_w.tagClose();
		}
		m_w.tagClose();
		++m_num_rows;
	}

	// --------------------------------------------------------------------------

//	private void
//	writeRowCheckboxes() {
//		String check_boxes_name = m_state.getCheckBoxesName();
//		if (check_boxes_name != null) {
//			StringBuilder values = new StringBuilder();
//			String[] check_boxes_columns = m_state.getCheckBoxesColumns();
//			for (int i=0; i<check_boxes_columns.length; i++) {
//				if (i > 0)
//					values.append('|');
//				values.append(data.getString(check_boxes_columns[i]));
//			}
//			String value = values.toString();
//
//			m_w.write("<td><input type=\"checkbox\" id=\"cb").write(data.getString("id")).write("\" class=\"db_check\" name=\"").write(check_boxes_name).write("\" value=\"").write(value).write("\" /></td>");
//		}
//	}

	// --------------------------------------------------------------------------

	protected void
	writeRows(String[] columns) {
		do {
			if (!beforeRow())
				break;
			writeRow(columns);
			if (isRowWindowLast())
				break;
			next();
		} while (!data.isAfterLast());
	}

	// --------------------------------------------------------------------------

	public final void
	writeRowWindowControls(int id) {
		if (id == 0)
			return;
		m_w.setId("rwc" + id)
			.addStyle("display:none")
			.tagOpen("div");
		if (m_printer_friendly || m_relationship != null)
			m_pager.setWriteDropdown(false);
		m_pager.write(m_num_rows, m_total_rows);
		m_w.tagClose()
			.js("_.table.write_controls(" + id + "," + JS.string(m_view_def.getRecordName()) + "," + JS.string(m_view_def.getRecordNamePlural()) + ");");
	}

	// --------------------------------------------------------------------------

	final int
	writeRowWindowControlsSpan() {
		int id = m_w.newID();
		m_w.setId("rwcs" + id);
		m_w.tag("span", null);
		return id;
	}

	// --------------------------------------------------------------------------

	private void
	writeSortMenuItem(Dropdown dropdown, String column_name, boolean descending) {
		StringBuilder item_text = new StringBuilder("sort by ");
		OrderBy.appendDisplayName(item_text, column_name, descending, Site.context, this);
		dropdown.aOnClick(item_text.toString(), m_view_def.getRowWindowSize() == 0
			? "event.stopPropagation();app.hide_popups();_.sorter.sort(this.closest('table'),this," + (descending ? "true)" : "false)")
			: "net.post(context+'/ViewStates/" + m_view_def.getName() + "',{db_orderby:'" + (descending ? column_name + " DESC" : column_name) + "'},function(){net.replace(_.c(this))}.bind(this));");
	}

	// --------------------------------------------------------------------------

	public int
	writeSpanRowOpen(String tr_class) {
		if (tr_class != null)
			m_w.addClass(tr_class);
		int mark = m_w.tagOpen("tr");
		m_w.setAttribute("colspan", getNumCols());
		m_w.tagOpen("td");
		return mark;
	}

	// --------------------------------------------------------------------------

	private void
	writeTable(boolean sort_links, boolean show_find) {
		if (data == null) {
			select();
			if (!next())
				return;
		}
		if (!skip() || 0 == data.getRow())
			return;
		initSections();
		String[] columns = getColumnNamesTable();
		initTotals(columns);

		Table table = tableOpen();
		if (m_printer_friendly)
			m_w.setAttribute("border", "1");
//		if (m_view_def.showColumnHeads())
			writeColumnHeadsRow(getColumnNamesTable(), sort_links);
		writeRows(columns);
		if (m_totals != null) {
			table.tfoot();
			writeTotalsRow("Total", m_totals);
			if (m_num_totals_written > 1 && m_view_def.showGrandTotals())
				writeTotalsRow("Grand Total", m_grand_totals);
		}
		tableClose(table, columns);
	}

	// --------------------------------------------------------------------------

	public final void
	writeTotalsRow(String label) {
		if (m_totals != null)
			writeTotalsRow(label, m_totals);
	}

	// --------------------------------------------------------------------------

	protected void
	writeTotalsRow(String label, double[] totals) {
		if (totals == null || m_num_rows == 0)
			return;
		String[] columns = getColumnNamesTable();
		m_w.write("<tr class=\"bg-secondary\"><td style=\"font-weight:bold;text-align:right;\">").write(label).write("</td>");
		for (int i=1; i<columns.length; i++)
			if (m_view_def.getSections() == null || !Section.contains(m_view_def.getSections(), columns[i])) {
				m_w.write("<td class=\"num\">");
				ColumnBase<?> column = getColumn(columns[i]);
				if (column != null && column.total()) {
					column.writeTotal(totals[i], m_w);
					m_grand_totals[i] += totals[i];
					totals[i] = 0;
				}
				m_w.write("</td>");
			}
		if (m_has_buttons_column)
			m_w.tag("td", null);
		m_w.write("</tr>");
		++m_num_totals_written;
	}

	// --------------------------------------------------------------------------

	public final void
	writeViewJSON() {
		writeViewJSON(m_view_def, m_relationship);
	}

	// --------------------------------------------------------------------------

	final void
	writeViewJSON(ViewDef view_def, Relationship relationship) {
		String name = view_def.getName();
		if (m_r.getData(name) != null)
			return;
		m_r.setData(name, Boolean.TRUE);
		JSONBuilder json = new JSONBuilder().startObject();
		json.string("add_text", m_view_def.getAddButtonText());
		String delete_text = view_def.getDeleteText();
		if (delete_text != null)
			json.string("delete_text", delete_text);
		if (m_mode == Mode.EDIT_FORM || m_mode == Mode.READ_ONLY_FORM) {
			if (data == null)
				json.number("id", m_state.getKeyValue());
			else if (data.getRow() > 0 && data.findColumn("id") != -1)
				json.number("id", data.getInt("id"));
		} else if (m_mode == Mode.LIST || m_mode == Mode.READ_ONLY_LIST) {
			json.startArray("columns");
			int index = 0;
			for (String column_name : getColumnNamesTable()) {
				ColumnBase<?> column = getColumn(column_name);
				if (!canViewColumn(column))
					continue;
				if (column == null || column.isFilterable())
					json.startObject()
						.bool("filterable", true)
						.number("index", index)
						.string("name", column == null ? column_name : column.getDisplayName(false))
						.string("type", "string")
						.endObject();
				++index;
			}
			json.endArray();
		}
		json.string("record_name", view_def.getRecordName());
		json.string("record_name_plural", view_def.getRecordNamePlural());
		if (relationship != null) {
			json.string("one_name", relationship.one.getViewDef().getName());
			if (relationship.def instanceof ManyToMany)
				json.string("many_many_table", ((ManyToMany)relationship.def).getManyManyTable());
		}
		view_def.addToViewJSON(json);
		m_w.js("app.merge('view:" + JS.escape(name) + "'," + json.endObject().toString() + ");");
	}
}
