package db;

import java.io.IOException;

import app.Request;
import app.Site;

public class CSVWriter {
	private int					m_column;
	private final Appendable	m_w;

	//--------------------------------------------------------------------------

	public
	CSVWriter(Appendable w) {
		m_w = w;
	}

	//--------------------------------------------------------------------------

	public
	CSVWriter(String filename, Request r) {
		m_w = r.w.getWriter();
		r.response.setContentCSV(filename);
	}

	//--------------------------------------------------------------------------

	public final CSVWriter
	cell(String string) {
		if (m_column > 0)
			write(',');
		m_column++;
		if (string != null)
			write('"').write(string.replaceAll("\"", "\"\"")).write('"');
		return this;
	}

	//--------------------------------------------------------------------------

//	public CSVWriter
//	cell(double d) {
//		if (m_column > 0)
//			write(',');
//		m_column++;
//		write(Double.toString(d));
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final CSVWriter
	row() {
		if (m_column > 0)
			write("\n");
		m_column = 0;
		return this;
	}

	//--------------------------------------------------------------------------

	private CSVWriter
	write(char c) {
		try {
			m_w.append(c);
		} catch (IOException e) {
			Site.site.log(e);
		}
		return this;
	}

	//--------------------------------------------------------------------------

	private CSVWriter
	write(String s) {
		try {
			m_w.append(s);
		} catch (IOException e) {
			Site.site.log(e);
		}
		return this;
	}
}
