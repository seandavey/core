package db.section;

import java.util.List;

import app.Request;
import db.DBConnection;
import db.NameValuePairs;
import db.OrderBy;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewState;
import util.Time;
import web.URLBuilder;

public abstract class Section {
	protected final String	m_column_name;
	protected boolean 		m_format_date;
//	protected boolean 		m_format_month_name_long;
	protected boolean 		m_format_month_name_short;
	protected boolean 		m_format_year;
	protected String		m_null_value_label = "other";
	private final OrderBy	m_order_by;

	// --------------------------------------------------------------------------

	public
	Section(String column_name, OrderBy order_by) {
		m_column_name = column_name;
		m_order_by = order_by;
	}

	// --------------------------------------------------------------------------

	public static void
	addOrderBys(Select query, List<Section> sections) {
		if (sections == null)
			return;
		for (Section section : sections)
			query.addOrderBy(section.m_column_name);
	}

	// --------------------------------------------------------------------------

	abstract public String
	beforeRow(View v, String previous_section_value, Request r);

	// --------------------------------------------------------------------------

	public String
	beforeTable(View v, Request r) {
		return null;
	}

	// --------------------------------------------------------------------------

	public static boolean
	contains(List<Section> sections, String column_name) {
		if (sections == null)
			return false;
		for (Section section : sections)
			if (column_name.equals(section.m_column_name))
				return true;
		return false;
	}

	// --------------------------------------------------------------------------

	public String
	getColumnDefaultValue(String column_name, Request r) {
		return null;
	}

	// --------------------------------------------------------------------------

	public static String
	getOrderBys(List<Section> sections) {
		if (sections == null)
			return null;
		StringBuilder order_bys = new StringBuilder();
		for (Section section : sections) {
			if (order_bys.length() > 0)
				order_bys.append(',');
			order_bys.append(section.m_column_name);
		}
		return order_bys.toString();
	}

	// --------------------------------------------------------------------------

	protected String
	getValue(View v, Request r) {
		String value = v.getColumnHTML(m_column_name);
		if (value.length() == 0)
			return m_null_value_label;
		if (m_format_date)
			return Time.formatter.getWeekdayMonthDate(v.data.getDate(m_column_name));
//		if (m_format_month_name_long)
//			return Time.formatter.getMonthNameLong(v.data.getDate(m_column_name));
		if (m_format_month_name_short)
			return Time.formatter.getMonthNameShort(v.data.getDate(m_column_name));
		if (m_format_year)
			return Integer.toString(v.data.getDate(m_column_name).getYear());
		return value;
	}

	// --------------------------------------------------------------------------
	// only called by Admin

	public final String
	getValue(Rows rows, Request r) {
		String value = rows.getString(m_column_name);
		if (value == null)
			return null;
		if (m_format_date)
			return rows.getDate(m_column_name).toString();
		if (/*m_format_month_name_long ||*/ m_format_month_name_short)
			return Integer.toString(rows.getDate(m_column_name).getMonthValue());
		if (m_format_year)
			return Integer.toString(rows.getDate(m_column_name).getYear());
		return value;
	}

	// --------------------------------------------------------------------------

	public static String
	getWhere(List<Section> sections, String from, String where, DBConnection db) {
		StringBuilder sb = new StringBuilder();
		for (Section section : sections) {
			if (sb.length() > 0)
				sb.append(" AND ");
			sb.append(section.m_column_name).append("=").append(SQL.string(db.lookupString(section.m_column_name, from, where)));
		}
		return sb.toString();
	}

	// --------------------------------------------------------------------------

	public static String
	getWhere(List<Section> sections, NameValuePairs nvp) {
		StringBuilder sb = new StringBuilder();
		for (Section section : sections) {
			if (sb.length() > 0)
				sb.append(" AND ");
			sb.append(section.m_column_name).append("=").append(SQL.string(nvp.getString(section.m_column_name)));
		}
		return sb.toString();
	}

	// --------------------------------------------------------------------------

	public final void
	insertOrderBy(Select query, View v, ViewState state, Request r) {
		if (m_order_by == null)
			return;
		if (m_format_month_name_short) {
			query.insertOrderBy("EXTRACT(year FROM " + m_column_name + "),EXTRACT(month FROM " + m_column_name + ")");
			if (state.getOrderBy().size() == 0) {
				String from = query.getFrom();
				int index = from.indexOf(' ');
				if (index != -1)
					from = from.substring(0, index);
				query.addOrderBy(m_column_name + "," + from + ".id");
			}
		} else if (m_format_year) {
			query.insertOrderBy("EXTRACT(year FROM " + m_column_name + ")");
			if (state.getOrderBy().size() == 0) {
				String from = query.getFrom();
				int index = from.indexOf(' ');
				if (index != -1)
					from = from.substring(0, index);
				query.addOrderBy(m_column_name + "," + from + ".id");
			}
		} else
			query.insertOrderBy(m_order_by.toQueryString(v, r));
	}

	// --------------------------------------------------------------------------

	public Section
	setOnClickForRowParams(URLBuilder url, Mode mode, Rows data) {
		return this;
	}

	// --------------------------------------------------------------------------

	public final Section
	setFormatDate(boolean format_date) {
		m_format_date = format_date;
		return this;
	}
//
//	// --------------------------------------------------------------------------
//
//	public Section
//	setFormatMonthNameLong(boolean format_month_name_long) {
//		m_format_month_name_long = format_month_name_long;
//		return this;
//	}

	// --------------------------------------------------------------------------

	public final Section
	setFormatMonthNameShort(boolean format_month_name_short) {
		m_format_month_name_short = format_month_name_short;
		return this;
	}

	// --------------------------------------------------------------------------

	public final Section
	setFormatYear(boolean format_year) {
		m_format_year = format_year;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Section
	setNullValueLabel(String null_value_label) {
		m_null_value_label = null_value_label;
		return this;
	}

	// --------------------------------------------------------------------------

	public boolean
	writeAfterList() {
		return true;
	}

	// --------------------------------------------------------------------------

	public void
	writeList(View v, Request r) {
		v.writeList();
	}
}
