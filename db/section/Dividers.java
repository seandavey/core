package db.section;

import app.Request;
import app.Site;
import db.OrderBy;
import db.View;

public class Dividers extends Section {
	private boolean m_collapse_rows;

	// --------------------------------------------------------------------------

	public
	Dividers(String column_name, OrderBy order_by) {
		super(column_name, order_by);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	beforeRow(View v, String previous_section_value, Request r) {
		String s = getValue(v, r);
		if (!Site.areEqual(s, previous_section_value)) {
			if (previous_section_value != null)
				v.writeTotalsRow("Total");
			r.w.write("</tbody><tbody>");
			if (s != null) {
				previous_section_value = s;
				int mark = v.writeSpanRowOpen(r.w.ui.section_head);
				r.w.addStyle("color:inherit")
					.ui.iconToggle(s, !m_collapse_rows)
					.tagsCloseTo(mark);
			}
		}
		if (m_collapse_rows && previous_section_value != null)
			r.w.addStyle("display:none");
		return previous_section_value;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	beforeTable(View v, Request r) {
		return null;
	}

	// --------------------------------------------------------------------------

	public Dividers
	setCollapseRows(boolean collapse_rows) {
		m_collapse_rows = collapse_rows;
		return this;
	}
}
