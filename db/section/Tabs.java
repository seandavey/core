package db.section;

import java.util.List;

import app.Request;
import app.Site;
import db.OrderBy;
import db.Rows;
import db.View;
import db.View.Mode;
import db.ViewDef;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;
import web.URLBuilder;

public class Tabs extends Section {
	private boolean m_pluralize = true;

	// --------------------------------------------------------------------------

	public
	Tabs(String column_name) {
		super(column_name, new OrderBy(column_name));
	}

	// --------------------------------------------------------------------------

	public
	Tabs(String column_name, OrderBy order_by) {
		super(column_name, order_by);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	beforeRow(View v, String previous_section_value, Request r) {
		if (!Site.areEqual(getValue(v, r), previous_section_value))
			return ""; // signal to finish table
		return previous_section_value;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	beforeTable(View v, Request r) {
		return getValue(v, r);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getColumnDefaultValue(String column_name, Request r) {
		if (column_name.equals(m_column_name)) {
			if (m_format_date || /*m_format_month_name_long ||*/ m_format_month_name_short || m_format_year)
				return null;
			return r.getParameter("db_tab_" + m_column_name);
		}
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public Section
	setOnClickForRowParams(URLBuilder url, Mode mode, Rows data) {
		if (mode == View.Mode.ADD_FORM && data.getRow() != 0) {
			String value = data.getString(m_column_name);
			if (value == null)
				value = "";
			url.set("db_tab_" + m_column_name, JS.escape(value));
		}
		return this;
	}


	// --------------------------------------------------------------------------

	public Tabs
	setPluralize(boolean pluralize) {
		m_pluralize = pluralize;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	writeAfterList() {
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeList(View v, Request r) {
		v.select();
		if (!v.next()) {
			v.writeList();
			return;
		}

		ViewDef view_def = v.getViewDef();
		HTMLWriter w = r.w;
		ui.Tabs tabs = w.ui.tabs(view_def.getName());
		if (m_format_month_name_short)
			tabs.setStartTab(Time.formatter.getMonthNameShort(Time.newDate().getMonthValue())).setStartWithLast(true);
		else if (m_format_year)
			tabs.setStartTab(Integer.toString(Time.newDate().getYear())).setStartWithLast(true);
		tabs.open();
		while (!v.data.isAfterLast()) {
			String value = getValue(v, r);
			if (m_format_date || /*m_format_month_name_long ||*/ m_format_month_name_short || m_format_year)
				tabs.pane(value);
			else if (m_pluralize && !value.equals(m_null_value_label))
				tabs.pane(Text.pluralize(value));
			else
				tabs.pane(value);
			v.writeList();
		}
		List<String> labels = tabs.getLabels();
		if (labels.size() == 1 && labels.get(0).equals(m_pluralize && !"other".equals(m_null_value_label) ? Text.pluralize(m_null_value_label) : m_null_value_label))
			tabs.clearLabels();
		tabs.close();
		view_def.afterList(v, r);
	}
}
