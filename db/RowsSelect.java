package db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import app.Request;
import ui.SelectOption;
import web.HTMLWriter;
import web.URLBuilder;

public class RowsSelect extends ui.Select {
	private Filter			m_filter;
	private int				m_num_options = -1;
	private final Request	m_r;
	private final Select	m_query;
	private final String	m_text_column;
	private final String	m_value_column;

	//--------------------------------------------------------------------------

	public
	RowsSelect(String name, Select query, String text_column, String value_column, Request r) {
		super(name, (Collection<? extends SelectOption>)null);
		m_query = query;
		m_text_column = text_column;
		m_value_column = value_column;
		m_r = r;
	}

	//--------------------------------------------------------------------------

	private void
	readOptions() {
		List<String> 	options = new ArrayList<>();
		List<String> 	option_values = new ArrayList<>();

		m_num_options = 0;

		Rows			rows = new Rows(m_query, m_r.db);
		int	 			num_columns = rows.getColumnCount();
		StringBuilder	option = new StringBuilder();
		int[]			option_column_indexes = null;
		String[]		option_columns = null;
		int				value_column_num = -1;
		boolean			view_value_column = false;

		if (m_text_column != null) {
			option_columns = m_text_column.split(",");
			option_column_indexes = new int[option_columns.length];
			for (int i=0; i<option_column_indexes.length; i++) {
				option_column_indexes[i] = rows.findColumn(option_columns[i]);
				if (option_columns[i].equals(m_value_column))
					view_value_column = true;
			}
		}
		if (m_value_column != null)
			value_column_num = rows.findColumn(m_value_column);
		while (rows.next()) {
			if (m_filter != null && !m_filter.accept(rows, m_r))
				continue;
			option.setLength(0);
			if (option_column_indexes != null)
				for (int option_column_index : option_column_indexes) {
					String col = rows.getString(option_column_index);
					if (col != null) {
						if (option.length() > 0)
							option.append(' ');
						option.append(col.trim());
					}
				}
			else
				for (int i=1; i<=num_columns; i++) {
					if (i == value_column_num && !view_value_column)
						continue;
					String col = rows.getString(i);
					if (col != null) {
						if (option.length() > 0)
							option.append(' ');
						option.append(col.trim());
					}
				}
			if (option.length() > 0) {
				String option_value = null;
				if (m_value_column != null)
					option_value = rows.getString(value_column_num).trim();
				++m_num_options;
				options.add(option.toString());
				if (m_value_column != null && option_value != null)
					option_values.add(URLBuilder.encode(option_value));
				else
					option_values.add(URLBuilder.encode(option.toString()));
			}
		}
		rows.close();

		if (m_num_options > 0) {
			ArrayList<SelectOption> list = new ArrayList<>(options.size());
			for (int i=0; i<options.size(); i++)
				list.add(new Option(options.get(i), option_values.get(i)));
			m_options = list;
		}
	}

    //--------------------------------------------------------------------------

	public final RowsSelect
	setFilter(Filter filter) {
		m_filter = filter;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	size() {
		if (m_num_options == -1)
			readOptions();
		return m_num_options;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write(HTMLWriter w) {
		if (m_num_options == -1)
			readOptions();
		super.write(w);
	}
}