package db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import app.Site;
import db.column.Column;
import db.column.ColumnBase;
import db.column.FileColumn;
import db.column.OptionsColumn;
import db.section.Tabs;
import web.JSON;

public class Mods {
	public static void
	adjustTable(String table, DBConnection db) {
		JsonArray a = Site.settings.getArray("mods", "tables", table, "columns");
		if (a != null) {
			Iterator<JsonValue> i = a.iterator();
			while (i.hasNext()) {
				JsonObject o = i.next().asObject();
				if (o != null) {
					String name = o.getString("name", null);
					if (!db.hasColumn(table, name)) {
						db.addColumn(table, name, o.getString("type", null), null, null);
						JsonValue v = o.get("default");
						if (v != null)
							if (v.isBoolean())
								db.setColumnDefault(table, name, v.asBoolean() ? "true" : "false");
							else
								db.setColumnDefault(table, name, v.asString());
					}
				}
			}
		}
	}

	// --------------------------------------------------------------------------

	public static void
	hideColumn(String view_def_name, String columns_list, String column, DBConnection db) {
		JsonValue v = Site.settings.get("mods");
		JsonObject mods = v == null ? new JsonObject() : v.asObject();
		JsonObject view_def = JSON.getObject(mods, "views", view_def_name);
		JSON.getArray(view_def, columns_list).add(new JsonObject().set(column, "hide"));
		Site.settings.set("mods", mods, db);
		Site.site.removeViewDef(view_def_name);
	}

	// --------------------------------------------------------------------------

	public static void
	mod(ViewDef view_def) {
		JsonObject o = Site.settings.getObject("mods", "views", view_def.m_name);
		if (o == null)
			return;
		String default_order_by = o.getString("default order by", null);
		if (default_order_by != null)
			view_def.m_default_order_by = default_order_by ;
		String record_name = o.getString("record name", null);
		if (record_name != null)
			view_def.m_record_name = record_name ;
		String column = o.getString("section", null);
		if (column != null)
			view_def.addSection(new Tabs(column, new OrderBy("lower(" + column + ")")));
		JsonValue v = o.get("columns");
		if (v != null)
			modColumns(view_def.m_columns, v.asArray());
	}

	// --------------------------------------------------------------------------

	static void
	modColumns(Map<String, ColumnBase<?>> columns, JsonArray a) {
		Iterator<JsonValue> i = a.iterator();
		while (i.hasNext()) {
			JsonObject o = i.next().asObject();
			String name = o.getString("name", null);
			ColumnBase<?> c = columns.get(name);
			if (c == null) {
				JsonValue options = o.get("options");
				if (options != null)
					c = new OptionsColumn(name, JSON.toArray(options.asArray()));
				else {
					String type = o.getString("type", null);
					if (type != null) {
						if (type.equals("file"))
							c = new FileColumn(name, o.getString("table", null), o.getString("directory", null))
								.setDirColumn(o.getString("dir column", null));
					} else
						c = new Column(name);
				}
				columns.put(name, c);
			}
			c.mod(o);
		}
	}

	// --------------------------------------------------------------------------

	static void
	modColumnNames(String view_def_name, ArrayList<String> column_names, String columns_list) {
		JsonArray a = Site.settings.getArray("mods", "views", view_def_name, columns_list + " columns");
		if (a != null) {
			column_names.clear();
			for (int i=0; i<a.size(); i++)
				column_names.add(a.get(i).asString());
		}
		JsonObject o = Site.settings.getObject("mods", "views", view_def_name, columns_list);
		if (o != null) 
			for (String name : o.names()) {
				String v = o.getString(name, null);
				if (v.startsWith("after "))
					column_names.add(column_names.indexOf(v.substring(6)) + 1, name);
				else if (v.startsWith("before "))
					column_names.add(column_names.indexOf(v.substring(7)), name);
				else
					column_names.add(name);
			}
	}
}
