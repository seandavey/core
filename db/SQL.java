package db;

import java.time.LocalDate;

public class SQL {
	public static String
	columnName(String column_name) {
		return '"' + column_name.toLowerCase() + '"';
	}

	//--------------------------------------------------------------------------

	public static String
	decodeBinary(String s) {
		if (s == null)
			return null;

		StringBuilder sb = new StringBuilder();
		if (s.startsWith("\\x"))
			for (int i=2; i<s.length(); i+=2)
				sb.append((char)Integer.parseInt(s.substring(i, i + 2), 16));
		else
			for (int i=0; i<s.length(); i++) {
				char c = s.charAt(i);
				if (c == '\\') {
					sb.append((char)Integer.parseInt(s.substring(i + 1, i + 4), 8));
					i += 3;
				} else
					sb.append(c);
			}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	encodeBinary(String s) {
		if (s == null)
			return null;

		StringBuilder buf = new StringBuilder();
		for (int i=0; i<s.length(); i++) {
			char c = s.charAt(i);
			if (c < 32 || c == 39 || c == 92 || c >= 127) {
				buf.append("\\\\");
				String octal = Integer.toOctalString(c);
				if (octal.length() == 1)
					buf.append("00");
				else if (octal.length() == 2)
					buf.append('0');
				buf.append(octal);
			} else
				buf.append(c);
		}
		return buf.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	escape(String s) {
		if (s == null)
			return null;

		StringBuilder sb = new StringBuilder(s);
		for (int i=0; i<sb.length(); i++) {
			char c = sb.charAt(i);
			if (c == '\'') {
				sb.insert(i, '\'');
				i++;
			}
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	firstDayOfMonth(LocalDate date) {
		return date.getYear() + "-" + date.getMonthValue() + "-01";
	}

	//--------------------------------------------------------------------------

	public static String
	lastDayOfMonth(LocalDate date) {
		date = date.withDayOfMonth(1).plusMonths(1).minusDays(1);
		return date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth();
	}

	//--------------------------------------------------------------------------

	public static String
	string(String s) {
		if (s == null || s.length() == 0)
			return "NULL";
		return '\'' + escape(s) + '\'';
	}

}
