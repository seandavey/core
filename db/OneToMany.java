package db;

import app.Request;
import app.Site;

public class OneToMany extends RelationshipDef<OneToMany> {
	public
	OneToMany(String many_view_def_name) {
		this.many_view_def_name = many_view_def_name;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	leftJoin(Select query, String one_table, Request r) {
		query.leftJoin(one_table, Site.site.getViewDef(many_view_def_name, r.db).getFrom());
	}
}
