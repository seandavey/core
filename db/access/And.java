package db.access;

import app.Request;
import db.Rows;
import db.View;

public class And extends AccessPolicy {
	private final AccessPolicy m_a;
	private final AccessPolicy m_b;

	//--------------------------------------------------------------------------

	public
	And(AccessPolicy a, AccessPolicy b) {
		m_a = a;
		m_b = b;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request r) {
		return m_a.canDeleteRow(from, id, r) && m_b.canDeleteRow(from, id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		return m_a.canUpdateRow(from, id, r) && m_b.canUpdateRow(from, id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View v, Request r) {
		return m_a.showAddButton(v, r) && m_b.showAddButton(v, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		return m_a.showDeleteButtonForRow(data, r) && m_b.showDeleteButtonForRow(data, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(Rows data, Request r) {
		return m_a.showDeleteButtons(data, r) && m_b.showDeleteButtons(data, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		return m_a.showEditButtonForRow(data, r) && m_b.showEditButtonForRow(data, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(Rows data, Request r) {
		return m_a.showEditButtons(data, r) && m_b.showEditButtons(data, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showViewButtons(Request r) {
		return m_a.showViewButtons(r) && m_b.showViewButtons(r);
	}
}
