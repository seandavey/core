package db.access;

import app.Request;
import db.Rows;
import db.Select;
import db.View;

public class AccessPolicy {
	protected boolean	m_add;
	protected boolean	m_delete;
	protected boolean	m_edit;
	protected boolean	m_view;

	//--------------------------------------------------------------------------

	public final AccessPolicy
	add() {
		m_add = true;
		return this;
	}

	//--------------------------------------------------------------------------
	// called before select is performed

	public void
	adjustQuery(Select query) {
	}

	//--------------------------------------------------------------------------
	// called before honoring request to delete a row

	public boolean
	canDeleteRow(String from, int id, Request r) {
		if (r.userIsGuest())
			return false;
		return r.userIsAdmin() || m_delete;
	}

	//--------------------------------------------------------------------------
	// called to determine if a row can be updated

	public boolean
	canUpdateRow(String from, int id, Request r) {
		if (r.userIsGuest())
			return false;
		return r.userIsAdmin() || m_edit;
	}

	//--------------------------------------------------------------------------

	public final AccessPolicy
	delete() {
		m_delete = true;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AccessPolicy
	edit() {
		m_edit = true;
		return this;
	}

	//--------------------------------------------------------------------------
	// whether or not to show add button for entire view

	public boolean
	showAddButton(View v, Request r) {
		if (r.userIsGuest())
			return false;
		return m_add;
	}

	//--------------------------------------------------------------------------
	// called to determine whether or not to show delete button for each row

	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		return showDeleteButtons(data, r);
	}

	//--------------------------------------------------------------------------
	// whether or not to show delete buttons in general for view

	public boolean
	showDeleteButtons(Rows data, Request r) {
		if (r.userIsGuest())
			return false;
		return m_delete;
	}

	//--------------------------------------------------------------------------
	// called to determine whether or not to show edit button for each row

	public boolean
	showEditButtonForRow(Rows data, Request r) {
		return showEditButtons(data, r);
	}

	//--------------------------------------------------------------------------
	// whether or not to show edit buttons for whole view

	public boolean
	showEditButtons(Rows data, Request r) {
		if (r.userIsGuest())
			return false;
		return m_edit;
	}

	//--------------------------------------------------------------------------

	public final boolean
	showViewButtonForRow(View v, Request r) {
		return showViewButtons(r);
	}

	//--------------------------------------------------------------------------
	// whether or not to show view buttons in general for view

	public boolean
	showViewButtons(Request r) {
		return m_view;
	}

	//--------------------------------------------------------------------------

	public final AccessPolicy
	view() {
		m_view = true;
		return this;
	}
}