package db.access;

import app.Request;
import app.Stringify;
import db.Rows;
import db.View;

@Stringify
public class RoleAccessPolicy extends AccessPolicy {
	private final String m_role;

	//--------------------------------------------------------------------------

	public
	RoleAccessPolicy(String role) {
		m_role = role;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request r) {
		return r.userHasRole(m_role);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		return r.userHasRole(m_role);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View v, Request r) {
		return m_add && r.userHasRole(m_role);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(Rows data, Request r) {
		return m_delete && r.userHasRole(m_role);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(Rows data, Request r) {
		return m_edit && r.userHasRole(m_role);
	}
}