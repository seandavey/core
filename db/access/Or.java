package db.access;

import app.Request;
import app.Stringify;
import db.Rows;
import db.View;

@Stringify
public class Or extends AccessPolicy {
	private final AccessPolicy[] m_policies;

	//--------------------------------------------------------------------------

	public
	Or(AccessPolicy ...policies) {
		m_policies = policies;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.canDeleteRow(from, id, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.canUpdateRow(from, id, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View v, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showAddButton(v, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showDeleteButtonForRow(data, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(Rows data, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showDeleteButtons(data, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showEditButtonForRow(data, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(Rows data, Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showEditButtons(data, r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showViewButtons(Request r) {
		for (AccessPolicy p : m_policies)
			if (p.showViewButtons(r))
				return true;
		return false;
	}
}
