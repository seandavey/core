package db;

import app.Request;

public interface InsertHook {
	/**
	 * called after insert has completed unless insert was overridden (see insert() method)
	 * @param id
	 * @param nvp
	 * @param request
	 */
	default void
	afterInsert(int id, NameValuePairs nvp, Request r) {
	}

	/**
	 * called before insert unless insert was overridden (see insert() method)
	 * @param nvp
	 * @param request
	 * @return error message or null
	 */
	default String
	beforeInsert(NameValuePairs nvp, Request r) {
		return null;
	}
}
