package db;

import java.time.LocalDate;

import app.Request;
import db.jdbc.JDBCColumn;
import util.Text;
import web.JS;

public class Validation {
	public static String
	getValidIdentifier(String parameter_name, Request r) {
		String identifier = r.getParameter(parameter_name);
		if (identifier == null)
			return null;
		for (int i = 0, n = identifier.length(); i < n; i++) {
			char c = identifier.charAt(i);
			if (!Character.isLetterOrDigit(c) && c != ' ' && c != '_' && c != '-' && c != ',' && c != '&' && c != '/')
				throw new RuntimeException("invalid identifier " + parameter_name + ": " + identifier);
		}
		return identifier;
	}

	// --------------------------------------------------------------------------

//	public static String
//	getValidIntegerString(String parameter_name, Request r) {
//		String integer = request.getParameter(parameter_name);
//		if (integer == null || integer.length() == 0)
//			//			System.out.println("getValidIntegerString: invalid " + parameter_name + ": " + integer);
//			throw new RuntimeException("invalid value for " + parameter_name);
//		integer = integer.trim();
//		for (int i = 0, n = integer.length(); i < n; i++) {
//			char c = integer.charAt(i);
//			if (!Character.isDigit(c) && c != ',')
//				//				System.out.println("getValidIntegerString: invalid " + parameter_name + ": " + integer);
//				throw new RuntimeException("invalid value for " + parameter_name);
//		}
//		return integer;
//	}

	// --------------------------------------------------------------------------

//	public static String
//	getValidKeyValue(String key_value, boolean null_ok) {
//		if (key_value != null)
//			for (int i = 0, n = key_value.length(); i < n; i++) {
//				char c = key_value.charAt(i);
//				if (!Character.isDigit(c) && c != ',') {
//					System.out.println("getValidKeyValue: invalid " + key_value);
//					throw new RuntimeException("invalid key value " + key_value);
//				}
//			}
//		else if (!null_ok)
//			throw new RuntimeException("invalid null key value");
//		return key_value;
//	}

	// --------------------------------------------------------------------------

//	public static String
//	getValidToken(String parameter_name, Request r) {
//		String token = request.getParameter(parameter_name);
//		if (token == null || token.length() == 0)
//			return null;
//		token = token.trim();
//		if (token.indexOf(' ') != -1)
//			throw new RuntimeException("invalid value for " + parameter_name);
//		return token;
//	}

	// --------------------------------------------------------------------------

	public static String
	validate(JDBCColumn column, String name, String value, Request r) {
		if (column.isDate())
			try {
				LocalDate.parse(value);
			} catch (Exception e) {
				return name + " value \"" + JS.escape(value) + "\" is not a valid date";
			}
		else if (column.isDouble())
			try {
				Double.parseDouble(Text.deleteAll(Text.deleteAll(value, ','), '$'));
			} catch (NumberFormatException e) {
				return name + " value \"" + JS.escape(value) + "\" is not a valid number";
			}
		else if (column.isInteger())
			try {
				Integer.parseInt(Text.deleteAll(Text.deleteAll(value, ','), '$'));
			} catch (NumberFormatException e) {
				return name + " value \"" + JS.escape(value) + "\" is not a valid integer";
			}
		else if (column.isReal())
			try {
				Float.parseFloat(Text.deleteAll(Text.deleteAll(value, ','), '$'));
			} catch (NumberFormatException e) {
				return name + " value \"" + JS.escape(value) + "\" is not a valid number";
			}
		return null;
	}
}
