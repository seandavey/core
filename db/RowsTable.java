package db;

import ui.Table;
import web.HTMLWriter;

public class RowsTable {
	public static void
	write(Rows rows, HTMLWriter w) {
		if (!rows.isBeforeFirst())
			return;
		Table table = w.ui.table().addDefaultClasses().open();
		while (rows.next()) {
			table.tr();
			for (int i=1; i<=rows.getColumnCount(); i++)
				table.td(rows.getString(i));
		}
		rows.close();
		table.close();
	}
}
