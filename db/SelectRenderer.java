package db;

import java.util.Collection;
import java.util.Iterator;

import app.Request;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import db.column.ColumnValueRenderer;
import ui.Select;
import ui.SelectOption;
import web.HTMLWriter;

public class SelectRenderer implements ColumnInputRenderer, ColumnValueRenderer {
	private boolean										m_allow_no_selection;
	private Filter										m_filter;
//	private ui.Option									m_first_option;
	private boolean										m_inline;
	private boolean										m_is_required;
//	private ui.Option									m_last_option;
	private String										m_on_change;
	private final String								m_option_column;
	private final String[]								m_option_texts;
	private final String[]								m_option_values;
	private final Collection<? extends SelectOption>	m_options;
	private final db.Select								m_query;
	private QueryAdjustor								m_query_adjustor;
	private boolean										m_text_is_value;
	private final String								m_value_column;

	//--------------------------------------------------------------------------
	/**
	 * constructor for a column that uses a Select populated by a Collection for its input renderer
	 * @param table
	 * @param option_column
	 */

	public
	SelectRenderer(Collection<? extends SelectOption> options) {
		m_options = options;
		m_option_column = null;
		m_option_texts = null;
		m_option_values = null;
		m_query = null;
		m_value_column = null;
	}

	//--------------------------------------------------------------------------
	/**
	 * constructor for a column that uses a RowsSelect for its input renderer
	 * @param query
	 * @param option_column
	 * @param value_column
	 */

	public
	SelectRenderer(db.Select query, String option_column, String value_column) {
		m_query = query;
		m_option_column = option_column;
		m_value_column = value_column;
		m_option_texts = null;
		m_option_values = null;
		m_options = null;
	}

	//--------------------------------------------------------------------------
	/**
	 * constructor for a column that uses a plain Select for its input renderer
	 * @param options
	 * @param option_values
	 */

	public
	SelectRenderer(String[] options, String[] option_values) {
		m_option_texts = options;
		m_option_values = option_values;
		m_option_column = null;
		m_query = null;
		m_value_column = null;
		m_options = null;
	}

	//--------------------------------------------------------------------------

	public final boolean
	hasOptions(DBConnection db) {
		if (m_options != null)
			return m_options.iterator().hasNext();
		if (m_option_texts != null)
			return m_option_texts.length > 0;
		if (m_query != null)
			return db.exists(m_query.getFrom(), m_query.getWhere());
		return false;
	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setAllowNoSelection(boolean allow_no_selection) {
		m_allow_no_selection = allow_no_selection;
		return this;
	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setFilter(Filter filter) {
		m_filter = filter;
		return this;
	}

	//--------------------------------------------------------------------------

//	public final SelectRenderer
//	setFirstOption(ui.Option option) {
//		m_first_option = option;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setInline(boolean inline) {
		m_inline = inline;
		return this;
	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setIsRequired(boolean is_required) {
		m_is_required = is_required;
		return this;
	}

	//--------------------------------------------------------------------------

//	public final SelectRenderer
//	setLastOption(ui.Option option) {
//		m_last_option = option;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setOnChange(String on_change) {
		m_on_change = on_change;
		return this;
	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setQueryAdjustor(QueryAdjustor query_adjustor) {
		m_query_adjustor = query_adjustor;
		return this;
	}

	//--------------------------------------------------------------------------

	private void
	setSelectedOption(Select select, View v) {
		String name = select.getName();
		if (m_options != null) {
			Iterator<? extends SelectOption> i = m_options.iterator();
			if (i.hasNext())
				if (m_text_is_value || i.next().getValue() == null)
					select.setSelectedOption(v.data.getString(name), null);
				else
					select.setSelectedOption(null, v.data.getString(name));
		} else if (m_query != null)
			select.setSelectedOption(null, v.data.getString(name));
		else if (m_text_is_value || m_option_values == null)
			select.setSelectedOption(v.data.getString(name), null);
		else
			select.setSelectedOption(null, v.data.getString(name));
	}

	//--------------------------------------------------------------------------

	public final SelectRenderer
	setTextIsValue(boolean text_is_value) {
		m_text_is_value = text_is_value;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r) {
		String name = column.getName();
		Select select;
		if (m_options != null)
			select = new Select(name, m_options).setTextIsValue(m_text_is_value);
		else if (m_query != null) {
			db.Select query = m_query;
			if (m_query_adjustor != null) {
				query = query.clone();
				m_query_adjustor.adjust(query, v, r);
			}
			select = new RowsSelect(name, query, m_option_column, m_value_column, r).setFilter(m_filter).setTextIsValue(m_text_is_value);
		} else
			select = new Select(name, m_option_texts, m_option_values).setTextIsValue(m_text_is_value);
		if (v == null || v.getMode() == View.Mode.ADD_FORM)
			select.setSelectedOption(null, column.getDefaultValue(v, r));
		else
			setSelectedOption(select, v);
		HTMLWriter w = r.w;
		if (m_is_required) {
			select.setIsRequired(true);
			w.setAttribute("title", column.getDisplayName(false));
		}
//		if (m_first_option != null)
//			select.addFirstOption(m_first_option);
//		if (m_last_option != null)
//			select.addLastOption(m_last_option);
		if (m_allow_no_selection)
			select.setAllowNoSelection(true);
		if (m_on_change != null)
			select.setOnChange(m_on_change);
		if (m_inline)
			select.setInline(true);
		select.write(r.w);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		String value = ColumnBase.getValue(column.getName(), v, r);
		if (value == null || value.length() == 0)
			return;
		if (m_options != null)
			for (SelectOption option : m_options)
				if (value.equals(option.getValue())) {
					r.w.write(option.getText());
					return;
				}
		if (m_option_values != null)
			for (int i=0; i<m_option_values.length; i++)
				if (value.equals(m_option_values[i])) {
					r.w.write(m_option_texts[i]);
					return;
				}
		if (m_query != null) {
			String s = r.db.lookupString(m_option_column, m_query.getFrom(), m_value_column + "=" + SQL.string(value));
			if (s != null)
				r.w.write(s);
			return;
		}
		r.w.write(value);
	}
}
