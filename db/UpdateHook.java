package db;

import java.util.Map;

import app.Request;

public interface UpdateHook {
	/**
	 * called after update has completed unless update was overridden (see update() method)
	 * @param id
	 * @param nvp
	 * @param previous_values can be used to send values from beforeUpdate when they are changed by the update
	 * @param request
	 */
	default void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
	}

	/**
	 * called before update unless update was overridden (see update() method)
	 * @param id
	 * @param nvp
	 * @param previous_values can be used to send values to afterUpdate when they are changed by the update
	 * @param request
	 * @return error message or null
	 */
	default String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		return null;
	}
}
