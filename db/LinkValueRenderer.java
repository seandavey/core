package db;

import app.Files;
import app.Request;
import app.Site;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import web.JS;

public class LinkValueRenderer implements ColumnValueRenderer {
	private String			m_dialog_ok_text;
	private String			m_dialog_title;
	private String			m_target;
	private final String	m_text;
	private final String	m_url;
	private boolean			m_use_dialog;

	//--------------------------------------------------------------------------

	public
	LinkValueRenderer(String url, String text) {
		m_url = url;
		m_text = text;
	}

	//--------------------------------------------------------------------------

	private static String
	getTemplateString(String template, View v) {
		if (template == null)
			return null;
		int start = template.indexOf("$(");
		if (start == -1)
			return template;
		StringBuilder sb = new StringBuilder();
		int end = -1;
		while (start != -1) {
			sb.append(template.substring(end + 1, start));
			end = template.indexOf(")", start + 2);
			sb.append(v.data.getString(template.substring(start + 2, end)));
			start = template.indexOf("$(", end + 1);
		}
		sb.append(template.substring(end + 1));
		if (sb.length() == 0)
			return null;
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public final LinkValueRenderer
	setTarget(String target) {
		m_target = target;
		return this;
	}

	//--------------------------------------------------------------------------

	public final LinkValueRenderer
	setUseDialog(String title, String ok_text) {
		m_use_dialog = true;
		m_dialog_title = title;
		m_dialog_ok_text = ok_text;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		String text = getTemplateString(m_text, v);
		if (v.isPrinterFriendly()) {
			r.w.write(text);
			return;
		}
		String href = getTemplateString(m_url, v);
		if (href == null)
			return;
		if (text == null || text.length() == 0)
			text = "link";
		boolean is_absolute = href.indexOf("://") > 0;
		if (m_use_dialog || !is_absolute && Files.isViewable(href)) {
			StringBuilder s = new StringBuilder("new Dialog({url:'").append(href).append("',title:").append(JS.string(getTemplateString(m_dialog_title == null ? text : m_dialog_title, v)));
			if (m_dialog_ok_text != null)
				s.append(",cancel:true,ok:{text:'").append(m_dialog_ok_text).append("'}");
			s.append("})");
			r.w.aOnClick(text, s.toString());
		} else {
			if (m_target != null)
				r.w.setAttribute("target", m_target).setAttribute("rel", "noreferrer");
			if (!is_absolute)
				href = Site.context + "/" + href;
			r.w.a(text, href);
		}
	}
}
