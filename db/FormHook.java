package db;

import app.Request;

public interface FormHook {
	public void
	afterForm(int id, Rows data, ViewDef view_def, View.Mode mode, boolean printer_friendly, Request r);
}
