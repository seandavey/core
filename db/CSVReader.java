package db;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
	private BufferedReader	m_br;
//	private String[]		m_columns;
//	private DBConnection	m_db;
//	private int[]			m_ignore_columns;
	private int				m_num_columns;
//	private String			m_table;

    //--------------------------------------------------------------------------

	public
	CSVReader(Reader reader, int num_columns) {
		m_br = new BufferedReader(reader);
		m_num_columns = num_columns;
	}

    //--------------------------------------------------------------------------

	public
	CSVReader(String file, int num_columns) {
		try {
			m_br = new BufferedReader(new FileReader(file));
			m_num_columns = num_columns;
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

    //--------------------------------------------------------------------------

//	public
//	CSVReader(String table, Reader reader, DBConnection db) {
//		m_table = table;
//		m_br = new BufferedReader(reader);
//		if (db.tableExists(table)) {
//			m_columns = db.getTable(table).getColumnNames();
//			m_num_columns = m_columns.length;
//		}
//		m_db = db;
//	}

    //--------------------------------------------------------------------------

	public final void
	close() {
		try {
			m_br.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

    //--------------------------------------------------------------------------

	public final List<String>
	readLine() {
		String l;
		try {
			l = m_br.readLine();
			if (l == null)
				return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		int num_commas = 0;
		boolean quote = false;
		ArrayList<String> values = new ArrayList<>();
		StringBuilder value = new StringBuilder();

		for (int i=0; i<l.length(); i++) {
			char c = l.charAt(i);
			if (c == '\\')
				value.append(l.charAt(++i));
			else if (c == '"')
				quote ^= true;
			else if (c == ',' && !quote) {
//				if (m_ignore_columns == null || Array.indexOf(m_ignore_columns, num_commas) == -1) {
					values.add(value.toString().trim());
					if (values.size() == m_num_columns)
						break;
//				}
				++num_commas;
				value.setLength(0);
			} else
				value.append(c);
		}
		if (m_num_columns == 0)
			m_num_columns = num_commas + 1;
		if (values.size()< m_num_columns)
			values.add(value.toString().trim());

		return values;
	}

    //--------------------------------------------------------------------------

//	public String[][]
//	readLines() {
//		ArrayList<String[]> lines = new ArrayList<>();
//		String[] line = readLine();
//		while (line != null) {
//			lines.add(line);
//			line = readLine();
//		}
//		return lines.toArray(new String[lines.size()][]);
//	}

    //--------------------------------------------------------------------------

//	public void
//	readRows() throws IOException {
//		NameValuePairs nvp = new NameValuePairs();
//		String[] values = readLine();
//		while (values != null) {
//			nvp.clear();
//			boolean not_empty = false;
//			for (int i=0; i<m_columns.length; i++) {
//				if (values[i].length() > 0)
//					not_empty = true;
//				nvp.set(m_columns[i], values[i]);
//			}
//			if (not_empty)
//				m_db.insert(m_table, nvp);
//			values = readLine();
//		}
//		m_br.close();
//	}

    //--------------------------------------------------------------------------

//	public CSVReader
//	setColumns(String[] columns) {
//		m_columns = columns;
//		if (!m_db.tableExists(m_table)) {
//			StringBuilder c = new StringBuilder();
//			for (int i=0; i<columns.length; i++) {
//				if (i > 0)
//					c.append(',');
//				c.append(columns[i]);
//				c.append(" VARCHAR");
//			}
//			m_db.createTable(m_table, c.toString());
//		}
//		return this;
//	}

    //--------------------------------------------------------------------------

//	public CSVReader
//	setIgnoreColumns(int[] ignore_columns) {
//		m_ignore_columns = ignore_columns;
//		return this;
//	}

    //--------------------------------------------------------------------------

//	public void
//	skipLine() {
//		try {
//			m_br.readLine();
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//	}
}
