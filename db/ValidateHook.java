package db;

import app.Request;

public interface ValidateHook {
	/**
	 * called before an insert or an update
	 * @param id
	 * @param request
	 * @return null for acceptance else error string
	 */
	String
	validate(int id, Request r);
}
