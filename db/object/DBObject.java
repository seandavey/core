package db.object;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import app.Request;
import app.Stringify;
import db.DBConnection;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import ui.Tags;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JSON;
import web.JSONBuilder;
import web.WriterBase;

public class DBObject {
	public static void
	addColumns(JDBCTable jdbc_table, Class<?> object_class) {
		Map<String,Field> fields = getFields(object_class, DBField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
			if (name.startsWith("m_"))
				name = name.substring(2);
			try {
				Class<?> type = field.getType();
				if (type == boolean.class)
					jdbc_table.add(new JDBCColumn(name, Types.BOOLEAN));
				else if (type == int.class)
					jdbc_table.add(new JDBCColumn(name, Types.INTEGER));
				else if (type == float.class)
					jdbc_table.add(new JDBCColumn(name, Types.REAL));
				else if (type == double.class)
					jdbc_table.add(new JDBCColumn(name, Types.DOUBLE));
				else if (type.isEnum() || type == String.class || type == String[].class || type == List.class)
					jdbc_table.add(new JDBCColumn(name, Types.VARCHAR));
				else
					throw new RuntimeException("unforseen type in DBObject.init");
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			}
		}
		fields = getFields(object_class, JSONField.class);
		if (fields.size() > 0)
			jdbc_table.add(new JDBCColumn("json", Types.VARCHAR));
	}

	//--------------------------------------------------------------------------

	public static void
	fromJSON(Object object, String json) {
		set(object, Json.parse(json).asObject());
	}

	//--------------------------------------------------------------------------

	public static void
	gatherNames(Class<?> object_class, String[] names, Map<String, String> label_names, Request r) {
		Map<String,Field> fields = DBObject.getFields(object_class, JSONField.class);
		Iterable<String> i = names == null || r.userIsAdministrator() ? fields.keySet() : Arrays.asList(names);
		for (String name : i) {
			Field field = fields.get(name);
			if (field == null)
				continue;
			if (hasJSONFields(field.getType()))
				gatherNames(field.getType(), names, label_names, r);
			else
				label_names.put(DBObject.getFieldLabel(field), name);
		}
	}

	//--------------------------------------------------------------------------

	public static Map<String,Field>
	getFields(Class<?> object_class, Class<? extends Annotation> annotation_class) {
		Map<String,Field> map = new TreeMap<>();
		while (object_class != Object.class) {
			Field[] fields = object_class.getDeclaredFields();
			AccessibleObject.setAccessible(fields, true);
			for (Field field : fields)
				if (annotation_class == null || field.getAnnotation(annotation_class) != null)
					map.put(field.getName(), field);
			object_class = object_class.getSuperclass();
		}
		return map;
	}

	//--------------------------------------------------------------------------

	public static String
	getFieldLabel(Field field) {
		String label = field.getAnnotation(JSONField.class).label();
		if (label.length() > 0)
			return label;
		String name = field.getName();
		if (name.startsWith("m_"))
			name = name.substring(2);
		return name.replace('_', ' ');
	}

	//--------------------------------------------------------------------------

	public static ArrayList<String>
	getFieldNames(Class<?> object_class) {
		ArrayList<String> names = new ArrayList<>();
		Map<String,Field> fields = getFields(object_class, DBField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
			if (name.startsWith("m_"))
				name = name.substring(2);
			names.add(name);
		}
		return names;
	}

	//--------------------------------------------------------------------------

	public static boolean
	hasJSONFields(Class<?> object_class) {
		Field[] fields = object_class.getDeclaredFields();
		for (Field field : fields)
			if (field.getAnnotation(JSONField.class) != null)
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public static void
	init(String table, Class<?> object_class, DBConnection db) {
		JDBCTable table_def = new JDBCTable(table);
		addColumns(table_def, object_class);
		JDBCTable.adjustTable(table_def, true, true, db);
	}

	//--------------------------------------------------------------------------

	public static boolean
	isSubsetting(Field field, Map<String,Field> fields) {
		String name = field.getName();
		for (Field f : fields.values())
			for (String subfield : f.getAnnotation(JSONField.class).fields())
				if (name.equals(subfield))
					return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public static boolean
	load(Object object, String from, int id, DBConnection db) {
		return load(object, from, "id=" + id, db);
	}

	//--------------------------------------------------------------------------

	public static boolean
	load(Object object, String from, String where, DBConnection db) {
		Rows rows = new Rows(new Select("*").from(from).where(where), db);
		if (rows.next()) {
			load(object, rows);
			rows.close();
			return true;
		}
		rows.close();
		return false;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void
	load(Object object, Rows rows) {
		Map<String,Field> fields = getFields(object.getClass(), DBField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
			if (name.startsWith("m_"))
				name = name.substring(2);
			if (rows.getColumnIndex(name) == -1) {
				System.out.println("DBObject.load: column " + name + " not found for " + object.getClass());
				Thread.dumpStack();
				continue;
			}
			try {
				Class<?> type = field.getType();
				if (type.isEnum()) {
					String value = rows.getString(name);
					int index = value.lastIndexOf('.');
					try {
						field.set(object, Enum.valueOf((Class<Enum>)Class.forName(value.substring(0, index)), value.substring(index + 1)));
					} catch (ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
				} else if (type == boolean.class)
					field.setBoolean(object, rows.getBoolean(name));
				else if (type == double.class)
					field.setDouble(object, rows.getDouble(name));
				else if (type == float.class)
					field.setFloat(object, rows.getFloat(name));
				else if (type == int.class)
					field.setInt(object, rows.getInt(name));
				else if (type == JsonObject.class) {
					String string = rows.getString(name);
					if (string != null)
						field.set(object, Json.parse(string).asObject());
				} else if (type == String.class)
					field.set(object, rows.getString(name));
				else if (type == String[].class) {
					String value = rows.getString(name);
					if (value != null)
						if (value.charAt(0) == '[')
							field.set(object, JSON.toArray(Json.parse(value).asArray()));
						else {
							String[] strings = value.split(",", -1);
							field.set(object, strings != null && strings.length > 0 ? strings : null);
						}
				} else if (List.class.isAssignableFrom(type)) {
					String value = rows.getString(name);
					if (value != null)
						if (value.charAt(0) == '[')
							field.set(object, new ArrayList<String>(Arrays.asList(JSON.toArray(Json.parse(value).asArray()))));
						else {
							ArrayList<String> list = new ArrayList<>();
							for (String s : value.split(",", -1))
								list.add(s);
							field.set(object, list.size() > 0 ? list : null);
						}
				} else
					load(object, rows);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		if (rows.getColumnIndex("json") != -1) {
			String json = rows.getString("json");
			if (json != null)
				fromJSON(object, json);
		}
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void
	set(Object object, Field field, String value) {
		Class<?> type = field.getType();
		if (value == null && type == boolean.class)
			value = "false";
		try {
			if (type == String.class)
				field.set(object, value == null || value.length() == 0 ? null : value);
			else if (value == null && (type == LocalDate.class || type == LocalTime.class || type.isArray()))
				field.set(object, null);
			else if (value != null)
				if (type.isArray())
					if (value.length() == 0 || value.equals("[]"))
						set(object, field, null);
					else {
						if (value.charAt(0) != '[') // hack for numeric arrays
							value = '[' + value + ']';
						setArray(object, field, Json.parse(value).asArray());
					}
				else if (type == boolean.class)
					field.setBoolean(object, "on".equals(value) || "true".equals(value) || "yes".equals(value));
				else if (type == double.class)
					if (value.length() > 0)
						field.setDouble(object, Double.parseDouble(value));
					else
						field.setDouble(object, 0);
				else if (type.isEnum())
					field.set(object, value.length() > 0 ? Enum.valueOf((Class<? extends Enum>)type, value) : null);
				else if (type == float.class)
					if (value.length() > 0)
						field.setFloat(object, Float.parseFloat(value));
					else
						field.setFloat(object, 0);
				else if (type == int.class) {
					if (value.length() > 0)
						field.setInt(object, Integer.parseInt(value));
					else
						field.setInt(object, 0);
				} else if (type == LocalDate.class) {
					if (value.length() > 0) {
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern(value.indexOf('/') != -1 ? "M/d/y" : "M-d-y");
						field.set(object, dtf.parse(value, LocalDate::from));
					} else
						field.set(object, null);
				} else if (type == LocalTime.class)
					field.set(object, Time.formatter.parseTime(value));
				else if (type == JsonObject.class)
					field.set(object, Json.parse(value));
				else {
					field.setAccessible(true);
					Object o = field.get(object);
					Field f = o.getClass().getDeclaredField(field.getName());
					f.setAccessible(true);
					set(o, f, value);
				}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void
	set(Object object, JsonObject json_object) {
		Map<String,Field> fields = getFields(object.getClass(), JSONField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
//			if (name.startsWith("m_"))
//				name = name.substring(2);
			JsonValue value = json_object == null ? null : json_object.get(name);
			if (value == null)
				continue;
			Class<?> type = field.getType();
			try {
				if (type == boolean.class) {
					field.setBoolean(object, value.isNull() ? false : value.asBoolean());
					continue;
				}
				if (type == double.class) {
					field.setDouble(object, value.isNull() ? 0 : value.asDouble());
					continue;
				}
				if (type.isEnum()) {
					field.set(object, value.isNull() ? null : Enum.valueOf((Class<? extends Enum>)type, value.asString()));
					continue;
				}
				if (type == float.class) {
					field.setFloat(object, value.isNull() ? 0 : value.asFloat());
					continue;
				}
				if (type == int.class) {
					field.setInt(object, value.isNull() ? 0 : value.asInt());
					continue;
				}
				if (type == LocalDate.class) {
					field.set(object, value.isNull() ? null : LocalDate.parse(value.asString()));
					continue;
				}
				if (type == LocalTime.class) {
					field.set(object, value.isNull() ? null : LocalTime.parse(value.asString()));
					continue;
				}
				if (value.isArray()) {
					if (value.isNull())
						field.set(object, null);
					else if (field.getType().isArray())
						setArray(object, field, value.asArray());
					continue;
				}
				if (value.isString()) {
					if (value.isNull())
						field.set(object, null);
					else if (value.asString().length() > 0)
						set(object, field, value.asString());
					else
						field.set(object, null);
					continue;
				}
				Object o = field.get(object);
//				if (o == null) {
//					o = type.getDeclaredConstructor().newInstance();
//					field.set(object, o);
//				}
				if (o != null)
					set(o, value.isNull() ? null : value.asObject());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	//--------------------------------------------------------------------------

	public static void
	set(Object object, Request r) {
		Map<String,Field> fields = getFields(object.getClass(), JSONField.class);
		Enumeration<String> names = r.request.getParameterNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			Field field = fields.get("m_" + name);
			if (field != null)
				set(object, field, r.getParameter(name));
		}
	}

	//--------------------------------------------------------------------------

	private static void
	setArray(Object object, Field field, JsonArray json_array) {
			try {
				if (json_array == null || json_array.size() == 0) {
					field.set(object, null);
					return;
				}
				Class<?> array_type = null;
				boolean is_array_list = field.getType() == ArrayList.class;
				if (is_array_list) {
					String class_name = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0].getTypeName();
					try {
						array_type = Class.forName(class_name);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				} else
					array_type = field.getType().getComponentType();
				if (array_type == boolean.class) {
					if (is_array_list) {
						ArrayList<Boolean> array = new ArrayList<>();
						for (int i=0; i<json_array.size(); i++)
							array.add(json_array.get(i).asBoolean());
						field.set(object, array);
					} else {
						boolean[] array = new boolean[json_array.size()];
						for (int i=0; i<array.length; i++)
							array[i] = json_array.get(i).asBoolean();
						field.set(object, array);
					}
					return;
				}
				if (array_type == double.class) {
					if (is_array_list) {
						ArrayList<Double> array = new ArrayList<>();
						for (int i=0; i<json_array.size(); i++)
							array.add(json_array.get(i).asDouble());
						field.set(object, array);
					} else {
						double[] array = new double[json_array.size()];
						for (int i=0; i<array.length; i++)
							array[i] = json_array.get(i).asDouble();
						field.set(object, array);
					}
					return;
				}
				if (array_type == float.class) {
					if (is_array_list) {
						ArrayList<Float> array = new ArrayList<>();
						for (int i=0; i<json_array.size(); i++)
							array.add(json_array.get(i).asFloat());
						field.set(object, array);
					} else {
						float[] array = new float[json_array.size()];
						for (int i=0; i<array.length; i++)
							array[i] = json_array.get(i).asFloat();
						field.set(object, array);
					}
					return;
				}
				if (array_type == int.class) {
					if (is_array_list) {
						ArrayList<Integer> array = new ArrayList<>();
						for (int i=0; i<json_array.size(); i++)
							array.add(json_array.get(i).asInt());
						field.set(object, array);
					} else {
						int[] array = new int[json_array.size()];
						for (int i=0; i<array.length; i++) {
							JsonValue v = json_array.get(i);
							if (v.isNumber())
								array[i] = v.asInt();
							else // value from Tags input
								array[i] = Integer.parseInt(v.asObject().getString("id", null));
						}
						field.set(object, array);
					}
					return;
				}
				if (array_type == String.class) {
					if (is_array_list) {
						ArrayList<String> array = new ArrayList<>();
						for (int i=0; i<json_array.size(); i++)
							if (json_array.get(i).isObject()) // tagify
								array.add(json_array.get(i).asObject().get("value").asString());
							else
								array.add(json_array.get(i).asString());
						field.set(object, array);
					} else
						field.set(object, JSON.toArray(json_array));
					return;
				}
				if (is_array_list) {
					ArrayList<Object> array = new ArrayList<>();
					for (int i=0; i<json_array.size(); i++) {
						Object o = array_type.getDeclaredConstructor().newInstance();
						set(o, json_array.get(i).asObject());
						array.add(o);
					}
					field.set(object, array);
				} else {
					Object[] array = new Object[json_array.size()];
					for (int i=0; i<array.length; i++) {
						Object o = array_type.getDeclaredConstructor().newInstance();
						set(o, json_array.get(i).asObject());
						array[i] = o;
					}
					field.set(object, array);
				}
			} catch (IllegalArgumentException | IllegalAccessException | InstantiationException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				throw new RuntimeException(e);
			}
	}

	//--------------------------------------------------------------------------

	public static int
	store(Object object, String table, int id, DBConnection db) {
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("id", id);
		return store(object, table, nvp, db);
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public static int
	store(Object object, String table, NameValuePairs nvp, DBConnection db) {
		String where = nvp.getWhereString(table, db);
		Map<String,Field> fields = getFields(object.getClass(), DBField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
			if (name.startsWith("m_"))
				name = name.substring(2);
			if (name.equals("id"))
				continue;
			if (!db.hasColumn(table, name))
				continue;
			Class<?> type = field.getType();
			try {
				if (type == boolean.class)
					nvp.set(name, field.getBoolean(object));
				else if (type == int.class)
					nvp.set(name, field.getInt(object));
				else if (type == float.class)
					nvp.set(name, field.getFloat(object));
				else if (type == double.class)
					nvp.set(name, field.getDouble(object));
				else if (type == String.class)
					nvp.set(name, (String)field.get(object));
				else if (type == String[].class) {
					String[] a = (String[])field.get(object);
					nvp.set(name, a == null ? null : Json.array(a).toString());
				} else if (type == JsonObject.class) {
					Object o = field.get(object);
					nvp.set(name, o == null ? null : o.toString());
				} else if (List.class.isAssignableFrom(type)) {
					List<String> l = (List<String>)field.get(object);
					nvp.set(name, l == null ? null : Json.array(l.toArray(new String[l.size()])).toString());
				} else
					throw new RuntimeException("unforseen type in DBObject.store");
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		String json = DBObject.toJSON(object, null);
		if (json.length() > 2)
			nvp.set("json", json);
		return db.updateOrInsert(table, nvp, where).id;
	}

	//--------------------------------------------------------------------------

	private static String
	toJSON(Object object, ArrayList<Object> stack) {
		if (object == null)
			return "null";
		Class<? extends Object> object_class = object.getClass();

		if (stack != null)
			for (Object o : stack)
				if (object == o)
					return JSON.string(object_class.toString());

		if (object instanceof Boolean || object instanceof Double || object instanceof Float || object instanceof Integer || object instanceof Long || object instanceof Short)
			return object.toString();
		if (object instanceof Class)
			return JSON.string(object.toString());
		if (object instanceof String)
			return JSON.string((String)object);
		if (object instanceof LocalDate)
			return '"' + DateTimeFormatter.ofPattern("MM/dd/yyyy").format((LocalDate)object) + '"';
		if (object instanceof LocalTime)
			return JSON.string(object.toString());
		if (object instanceof JsonObject)
			return JSON.string(object.toString());

		if (object_class.isArray()) {
			Class<?> array_type = object_class.getComponentType();
			StringBuilder json = new StringBuilder("[");
			for (int i=0,n=Array.getLength(object); i<n; i++) {
				if (i > 0)
					json.append(',');
				if (array_type == boolean.class)
					json.append(Array.getBoolean(object, i) ? "true" : "false");
				else if (array_type == double.class)
					json.append(Array.getDouble(object, i));
				else if (array_type == float.class)
					json.append(Array.getFloat(object, i));
				else if (array_type == int.class)
					json.append(Array.getInt(object, i));
				else if (array_type == long.class)
					json.append(Array.getLong(object, i));
				else if (array_type == short.class)
					json.append(Array.getShort(object, i));
				else {
					if (stack != null)
						stack.add(object);
					json.append(toJSON(Array.get(object, i), stack));
					if (stack != null)
						stack.remove(stack.size() - 1);
				}
			}
			json.append(']');
			return json.toString();
		}
		if (object_class.isEnum())
			return JSON.string(object.toString());
		if (object instanceof List) {
			StringBuilder json = new StringBuilder("[");
			@SuppressWarnings("rawtypes")
			List list = (List)object;
			for (int i=0; i<list.size(); i++) {
				if (i > 0)
					json.append(',');
				if (stack != null)
					stack.add(object);
				json.append(toJSON(list.get(i), stack));
				if (stack != null)
					stack.remove(stack.size() - 1);
			}
			json.append(']');
			return json.toString();
		}
		if (object instanceof Map) { // keys must be strings
			JSONBuilder json = new JSONBuilder();
			json.startObject();
			@SuppressWarnings({ "unchecked" })
			Map<String,Object> map = (Map<String,Object>)object;
			for (String key : map.keySet()) {
				json.name(key);
				if (stack != null)
					stack.add(object);
				json.json(toJSON(map.get(key), stack));
				if (stack != null)
					stack.remove(stack.size() - 1);
			}
			json.endObject();
			return json.toString();
		}

		if (stack != null && !object_class.isAnnotationPresent(Stringify.class))
			return JSON.string(object_class.toString());

		StringBuilder json = new StringBuilder("{");
		Map<String,Field> fields = getFields(object_class, stack != null ? null : JSONField.class);
		for (Field field : fields.values()) {
			String name = field.getName();
			if (name.equals("$assertionsDisabled"))
				continue;
			if (json.length() > 1)
				json.append(',');
			json.append('"').append(name).append("\":");
			try {
				if (stack != null)
					stack.add(object);
				json.append(toJSON(field.get(object), stack));
				if (stack != null)
					stack.remove(stack.size() - 1);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				System.out.println(e);
			}
		}
		json.append('}');
		return json.toString();
	}

	//--------------------------------------------------------------------------

	public static void
	write(Object object, HTMLWriter w) {
		if (object instanceof Boolean || object instanceof Double || object instanceof Float || object instanceof Integer || object instanceof Long || object instanceof Short || object instanceof String)
			w.write(object.toString());
		else {
			String id = Text.uuid();
			w.aShowHideOpen("show", "hide", "div", id);
			String json = toJSON(object, new ArrayList<>());
			w.js("$pre({parent:_.$('#" + id + "')},JSON.stringify(" + SQL.escape(WriterBase.encode(json)) + ",undefined,4))")
				.tagClose();
		}
	}

	//--------------------------------------------------------------------------

	public static void
	writeInput(Object object, Field field, Request r) {
		String name = field.getName();
		if (name.startsWith("m_"))
			name = name.substring(2);
		Class<?> type = field.getType();
		HTMLWriter w = r.w;
		try {
			if (field.getAnnotation(JSONField.class).required())
				w.setAttribute("required", "true");
			if (type.isArray()) {
				Object array = object == null ? null : field.get(object);
				Class<?> array_type = type.getComponentType();
				if (array_type == String.class)
					new Tags(name, (String[])array, field.getAnnotation(JSONField.class).choices())
						.setAllowDragging(true)
						.setOnEditUpdated(field.getAnnotation(JSONField.class).on_edit_updated())
						.setRestrictToChoices(true)
						.write(w);
				else if (array == null)
					w.textAreaExpanding(name, null, null);
				else if (array_type == String.class)
					w.textAreaExpanding(name, null, new JSONBuilder().array((String[])array).toString());
				else {
					StringBuilder sb = new StringBuilder();
					for (int i=0,n=Array.getLength(array); i<n; i++) {
						if (i > 0)
							sb.append(',');
						sb.append(Array.get(array, i).toString());
					}
					w.textAreaExpanding(name, null, sb.toString());
				}
			} else if (type.isEnum()) {
				Enum<?>[] constants = (Enum<?>[]) type.getEnumConstants();
				String[] names = new String[constants.length];
				for (int i=0; i<constants.length; i++)
					names[i] = constants[i].name();
				ui.Select s = new ui.Select(name, names).setAllowNoSelection(true);
				Enum<?> e = (Enum<?>)field.get(object);
				if (e != null)
					s.setSelectedOption(e.name(), null);
				s.write(w);
			} else if (type == boolean.class) {
				String[] choices = field.getAnnotation(JSONField.class).choices();
				if (choices.length > 0)
					w.ui.radioButtons(name, choices, new String[] { "0", "1" }, field.getBoolean(object) ? "1" : "0");
				else
					w.ui.checkbox(name, getFieldLabel(field), null, field.getBoolean(object), false, true);
			} else if (type == LocalDate.class)
				w.dateInput(name, (LocalDate)field.get(object));
			else if (type == LocalTime.class)
				w.timeInput(name, (LocalTime)field.get(object), 5, false, false, true);
			else if (type == double.class)
				w.textInput(name, 0, Text.two_digits.format(field.getDouble(object)));
			else if (type == float.class)
				w.textInput(name, 0, Text.two_digits.format(field.getFloat(object)));
			else if (type == int.class) {
				String[] choices = field.getAnnotation(JSONField.class).choices();
				if (choices.length > 0)
					w.ui.radioButtons(name, choices, choices, Integer.toString(field.getInt(object)));
				else
					w.textInput(name, 0, Integer.toString(field.getInt(object)));
			} else if (type == String.class) {
				String type2 = field.getAnnotation(JSONField.class).type();
				if ("color".equals(type2))
					w.colorInput(name, (String)field.get(object));
				else if ("html".equals(type2)) {
					w.addStyle("width:100%")
						.textAreaOpen(name, null, null);
					String value = (String)field.get(object);
					if (value != null)
						w.write(WriterBase.encode(value));
					w.tagClose();
					w.js("_.rich_text.create('#" + name + "',{toolbar:'" + (r.userIsAdmin() ? "admin" : "full") + "'})");

				} else if ("time".equals(type2))
					w.timeInput(name, Time.formatter.parseTime((String)field.get(object)), 5, false, false, true);
				else {
					String[] choices = field.getAnnotation(JSONField.class).choices();
					if (choices.length > 0)
						w.ui.radioButtons(name, choices, choices, (String)field.get(object));
					else
						w.textAreaExpanding(name, null, (String)field.get(object));
				}
			} else if (type == JsonObject.class) {
				String value = null;
				JsonObject o = (JsonObject)field.get(object);
				if (o != null)
					value = o.toString();
				w.textAreaExpanding(name, null, value);
			} else {
				Object o = field.get(object);
				try {
					Field f = type.getDeclaredField(field.getName());
					f.setAccessible(true);
					writeInput(o, f, r);
				} catch (NoSuchFieldException | SecurityException e) {
					System.out.println(e);
				}
			}
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		String after = field.getAnnotation(JSONField.class).after();
		if (after.length() > 0)
			w.ui.helpText(after);
	}
}
