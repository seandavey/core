package db.object;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import app.Site;
import app.Site.Message;
import app.Stringify;
import app.Subscriber;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.ViewDef;

@Stringify
public class DBObjects<T> implements Collection<T>, Comparable<DBObjects<T>>, Subscriber {
	protected final String			m_name;
	private Class<? extends Object>	m_object_class;
	protected ArrayList<T>			m_objects;
	protected final Select			m_query;

	//--------------------------------------------------------------------------

	public
	DBObjects(Select query, Class<? extends Object> object_class) {
		m_name = query.getFirstTable();
		m_query = query;
		m_object_class = object_class;
		Site.site.subscribe(m_name, this);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	add(T e) {
		return getObjects().add(e);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	addAll(Collection<? extends T> c) {
		return getObjects().addAll(c);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	clear() {
		m_objects = null;
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(DBObjects<T> objects) {
		return m_name.compareTo(objects.m_name);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	contains(Object o) {
		return getObjects().contains(o);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	containsAll(Collection<?> c) {
		return getObjects().containsAll(c);
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final void
	copyObjects(DBObjects<T> objects) {
		ArrayList<T> o = (ArrayList<T>) objects.getObjects();
		m_objects = (ArrayList<T>)o.clone();
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final synchronized List<T>
	getObjects() {
		if (m_objects != null)
			return m_objects;
		m_objects = new ArrayList<>();
		try {
			DBConnection db = new DBConnection();
			Rows rows = new Rows(m_query, db);
			while (rows.next()) {
				T object = (T)m_object_class.getDeclaredConstructor().newInstance();
				DBObject.load(object, rows);
				m_objects.add(object);
			}
			rows.close();
			db.close();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		}
		return m_objects;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isEmpty() {
		return getObjects().isEmpty();
	}

	//--------------------------------------------------------------------------

	@Override
	public Iterator<T>
	iterator() {
		return getObjects().iterator();
	}

	//--------------------------------------------------------------------------

	public final ViewDef
	newViewDef() {
		return new ViewDef(m_name)
			.setDefaultOrderBy(m_query.getOrderBy());
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		m_objects = null;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	remove(Object o) {
		return getObjects().remove(o);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	removeAll(Collection<?> c) {
		return getObjects().removeAll(c);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	retainAll(Collection<?> c) {
		return getObjects().retainAll(c);
	}

	//--------------------------------------------------------------------------

	public final void
	setObjectClass(Class<? extends Object> object_class) {
		m_object_class = object_class;
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	size() {
		return getObjects().size();
	}

	//--------------------------------------------------------------------------

	@Override
	public Object[]
	toArray() {
		return getObjects().toArray();
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("hiding")
	@Override
	public <T> T[]
	toArray(T[] a) {
		return getObjects().toArray(a);
	}
}
