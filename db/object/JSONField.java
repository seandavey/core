package db.object;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JSONField {
	boolean		admin_only()		default false;
	String		after()				default "";
	String[]	choices()			default {};
	String[]	fields()			default {};
	String		label()				default "";
	String		on_edit_updated()	default "";
	boolean		required()			default false;
	String		type()				default "";
}
