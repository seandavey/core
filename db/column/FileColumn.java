package db.column;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;

import app.ClassTask;
import app.Files;
import app.ImageUploads;
import app.Request;
import app.Site;
import db.Form;
import db.NameValuePairs;
import db.Relationship;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import jakarta.servlet.http.Part;
import ui.Table;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;

public class FileColumn extends ColumnBase<FileColumn> {
	protected String		m_accept;
	private boolean			m_allow_editing;
	private boolean			m_allow_urls;
	private String			m_badge_column;
	private String			m_dir_column;
	protected final String	m_directory;
//	protected boolean		m_generate_file_names;
	protected String		m_kind = "file";
	protected boolean		m_multiple;
	private String			m_print_template;
	protected final String	m_table;
	protected String		m_title_column;
	private boolean			m_use_git;

	//--------------------------------------------------------------------------

	public
	FileColumn(String name, String table, String directory) {
		super(name);
		m_table = table;
		m_directory = directory;
	}

	//--------------------------------------------------------------------------

	public void
	appendTokens(NameValuePairs nvp, StringBuilder tsvector) {
		if ("l".equals(nvp.getString("kind")))
			return;
		Path path = getFilePath(nvp);
		if (path == null)
			return;
		String t = Site.site.loadTokens(path);
		if (t != null && t.length() > 0)
			tsvector.append(t).append("\n");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	beforeDelete(ViewDef view_def, String where, Request r) {
		String filename = r.db.lookupString(m_name, m_table, where);
		if (filename == null)
			return;
		String dir_column_value = null;
		if (m_dir_column != null)
			dir_column_value = r.db.lookupString(m_dir_column, m_table, where);
//			if (dir_column_value == null)
//				System.out.println(m_dir_column + " is null for " + m_table + ":" + where);
		File f = getDirectory(dir_column_value, r).resolve(filename).toFile();
		if (f.exists()) {
			if (filename.endsWith(".html"))
				ImageUploads.deleteFiles(f);
			f.delete();
		}
	}

	//--------------------------------------------------------------------------

	private void
	checkDirChange(int id, Request r) {
		if (m_dir_column == null || "id".equals(m_dir_column))
			return;
		if (r.getParameter(m_dir_column) == null)
			return;
		int new_dir = r.getInt(m_dir_column, 0);
		int old_dir = r.db.lookupInt(new Select(m_dir_column).from(m_table).whereIdEquals(id), 0);
		if (new_dir != old_dir) {
			String filename = r.db.lookupString(new Select(m_name).from(m_table).whereIdEquals(id));
			Path old_path = getBasePath();
			if (old_dir != 0)
				old_path = old_path.resolve(Integer.toString(old_dir));
			old_path = old_path.resolve(filename);
			Path new_path = getBasePath();
			if (new_dir != 0)
				new_path = new_path.resolve(Integer.toString(new_dir));
			new_path.toFile().mkdirs();
			new_path = new_path.resolve(filename);
			Site.site.log("moving " + old_path.toString() + " to " + new_path.toString(), "file.log");
			old_path.toFile().renameTo(new_path.toFile());
			r.db.update(m_table, m_dir_column + "=" + (new_dir == 0 ? "NULL" : new_dir), id);
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask({"table","column","show only problems"})
	public static void
	checkFilesInDatabase(String table, String column, boolean show_only_problems, Request r) {
		HTMLWriter w = r.w;
		FileColumn file_column = (FileColumn)Site.site.getViewDef(table, r.db).getColumn(column);
		String dir_column = file_column.getDirColumn();
		Select query = new Select(column).from(table).orderBy(column).where("kind IS NULL");
		if (dir_column != null && r.db.hasColumn(table, dir_column))
			query.addColumn(dir_column).insertOrderBy(dir_column);
		Rows rows = new Rows(query, r.db);
		w.write("<table class=\"table table-condensed table-bordered\" style=\"width:auto;\"><tr><th>filename</th>");
		if (dir_column != null)
			w.write("<th>dir column</th>");
		w.write("<th>status</th></tr>");
		while (rows.next()) {
			String dir_column_value = null;
			String filename = rows.getString(1);
			String problem = null;
			if (filename == null)
				problem = column + " is null";
			else {
				if (dir_column != null)
					dir_column_value = rows.getString(2);
				Path file_path = file_column.getDirectory(dir_column_value, r).resolve(filename);
				File file = file_path.toFile();
				if (!file.exists())
					if (dir_column != null) {
						// check if file is in root and move it. remove this if bug ever fixed
						Path file_path2 = file_column.getDirectory((String)null, r).resolve(filename);
						File file2 = file_path2.toFile();
						if (file2.exists()) {
							file2.renameTo(file);
							problem = "file was in parent directory";
						} else
							problem = "missing";
					} else
						problem = "missing";
			}
			if (problem != null || !show_only_problems) {
				w.write("<tr><td>");
				if (filename != null)
					w.write(filename);
				if (dir_column != null) {
					w.write("</td><td>");
					if (dir_column_value != null)
						w.write(dir_column_value);
				}
				w.write("</td><td>").write(problem == null ? "ok" : problem).write("</td></tr>");
			}
		}
		w.write("</table>");
		rows.close();
	}

	//--------------------------------------------------------------------------

	@ClassTask({"table","column"})
	public static void
	checkFilesNotInDatabase(final String table, final String column, final Request r) {
		FileColumn file_column = (FileColumn)Site.site.getViewDef(table, r.db).getColumn(column);
		String dir_column = file_column.getDirColumn();
		HTMLWriter w = r.w;
		Table t = r.w.ui.table().open();

		Path dir = file_column.getBasePath();
		w.addClass(w.ui.bg_secondary);
		t.tr().td();
		r.w.write("checking ").write(dir);
		dir.toFile().listFiles(new FileFilter(){
			@Override
			public boolean
			accept(File file) {
				if (file.isFile() && !r.db.rowExists(table, dir_column != null ? dir_column + " IS NULL AND " + column + "=" + SQL.string(file.getName()) : column + "=" + SQL.string(file.getName()))) {
					t.tr().td();
					w.write(file.getName());
					t.td();
					w.ui.buttonOnClick(Text.delete, "net.delete(context+'/admin/file_delete?file=" + HTMLWriter.URLEncode(file.getPath()) + "',function(){this.closest('tr').remove()}.bind(this))");
				}
				return false;
			}
		});
		if (dir_column != null)
			dir.toFile().listFiles(new FileFilter(){
				@Override
				public boolean
				accept(File d) {
					if (d.isDirectory()) {
						w.addClass(w.ui.bg_secondary);
						t.tr().td();
						r.w.write("checking ").write(d.getName());
						new File(d.getPath()).listFiles(new FilenameFilter(){
							@Override
							public boolean
							accept(File file, String filename) {
								if (!r.db.rowExists(table, dir_column + "=" + d.getName() + " AND " + column + "=" + SQL.string(filename))) {
									t.tr().td();
									w.write(filename);
									t.td();
									w.ui.buttonOnClick(Text.delete, "net.delete(context+'/admin/file_delete?file=" + HTMLWriter.URLEncode(file.getPath()) + "',function(){this.closest('tr').remove()}.bind(this))");
								}
								return false;
							}
						});
					}
					return false;
				}
			});
		t.close();
	}

	//--------------------------------------------------------------------------

	public final void
	deleteFile(int id, Request r) {
		String old_filename = r.db.lookupString(new Select(m_name).from(m_table).whereIdEquals(id));
		if (old_filename != null) {
			File file = getDirectory(id, r).resolve(old_filename).toFile();
			if (file.exists())
				file.delete();
			r.db.update(m_table, m_name + "=NULL", id);
		}
	}

	//--------------------------------------------------------------------------

	private Path
	getBasePath() {
		Path base_path = Site.site.getPath();
		if (m_directory != null)
			base_path = base_path.resolve(m_directory);
		return base_path;
	}

	//--------------------------------------------------------------------------

	public final String
	getDirColumn() {
		return m_dir_column;
	}

	//--------------------------------------------------------------------------

	private String
	getDirColumnValue(View v) {
		if (m_dir_column == null)
			return null;
		if (v.getMode() == View.Mode.ADD_FORM) {
			Relationship relation = v.getRelationship();
			if (relation == null)
				return null;
			if (relation.one.data == null)
				relation.one.selectByID(relation.one.getID());
			return relation.one.data.getString("id");
		}
		return v.data.getString(m_dir_column);
	}

	//--------------------------------------------------------------------------

	private Path
	getDirectory(String dir_column_value, Request r) {
		Path dir = getBasePath();
		if (m_dir_column != null )
			if (dir_column_value != null)
				dir = dir.resolve(dir_column_value);
			else if ("id".equals(m_dir_column))
				dir = dir.resolve(Integer.toString(r.getUser().getId()));
//			else
//				r.log("dir_column_value is null for " + m_dir_column, true);
		return dir;
	}

	//--------------------------------------------------------------------------

	protected final Path
	getDirectory(int id, Request r) {
		Path dir = getBasePath();
		if (m_dir_column != null)
			if ("id".equals(m_dir_column))
				dir = dir.resolve(Integer.toString(id));
			else {
				String dir_column_value = r.db.lookupString(new Select(m_dir_column).from(m_table).whereIdEquals(id));
				if (dir_column_value != null)
					dir = dir.resolve(dir_column_value);
			}
		return dir;
	}

	//--------------------------------------------------------------------------

	public final Path
	getFilePath(NameValuePairs nvp) {
		String filename = nvp.getString(m_name);
		if (filename == null)
			return null;
		Path path = getBasePath();
		if (m_dir_column != null) {
			String dir_column_value = nvp.getString(m_dir_column);
			if (dir_column_value != null && dir_column_value.length() > 0)
				path = path.resolve(dir_column_value);
		}
		return path.resolve(filename);
	}

	//--------------------------------------------------------------------------

	public final Path
	getFilePath(Rows data) {
		String filename = data.getString(m_name);
		if (filename == null)
			return null;
		Path path = getBasePath();
		if (m_dir_column != null) {
			String dir_column_value = data.getString(m_dir_column);
			if (dir_column_value != null && dir_column_value.length() > 0)
				path = path.resolve(dir_column_value);
		}
		return path.resolve(filename);
	}

	//--------------------------------------------------------------------------

	private static String
	getIcon(String filename) {
		int i = filename.lastIndexOf('.');
		if (i == -1)
			return null;
		String ext = filename.substring(i + 1).toLowerCase();
		String img;
		switch (ext) {
		case "gif":
		case "jpg":
		case "jpeg":
		case "png":
			img = "document-image.png";
			break;
		case "pdf":
			img = "document-pdf.png";
			break;
		case "doc":
		case "docx":
		case "dot":
			img = "document-word.png";
			break;
		case "ods":
			img = "document-table.png";
			break;
		case "odt":
		case "pages":
			img = "document-text-image.png";
			break;
		case "odp":
		case "ppt":
		case "pptx":
			img = "document-powerpoint.png";
			break;
		case "xls":
		case "xlsx":
			img = "document-excel.png";
			break;
		default:
			img = "document.png";
		}
		return "<img src=\"" + Site.site.relativeURL("images", img) + "\" style=\"vertical-align:baseline;\" />";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getLabel(View v, Request r) {
		if (isLink(v, r))
			return "link";
		String display_name = getDisplayName(false);
		if (display_name != null)
			return display_name;
		if ("id".equals(m_dir_column))
			return super.getLabel(v, r);
		if (isRichText(v, r))
			return "document";
		return "file";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderBy() {
		return "lower(" + (m_title_column == null ? m_name : m_title_column) + ")";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderByDesc() {
		return "lower(" + (m_title_column == null ? m_name : m_title_column) + ") DESC";
	}

	//--------------------------------------------------------------------------

	private String
	getURLPath(String dir) {
		StringBuilder url_path = new StringBuilder(Site.context);
		if (m_directory != null) {
			url_path.append('/');
			url_path.append(m_directory);
		}
		if (dir != null) {
			url_path.append('/');
			url_path.append(dir);
		}
		url_path.append('/');
		return url_path.toString();
	}

	//--------------------------------------------------------------------------

	protected final String
	getURLPath(Rows data) {
		return getURLPath(m_dir_column != null ? data.getString(m_dir_column) : null);
	}

	//--------------------------------------------------------------------------
	// record will exist when this is called so view_def.update is called even if this is new record

	public Path
	handleFile(Part part, InputStream is, String is_filename, ViewDef view_def, boolean insert, int id, Request r) {
		if (m_allow_urls && "l".equals(r.db.lookupString(new Select("kind").from(m_table).whereIdEquals(id))))
			return null;
		Path dir = getDirectory(id, r);
		File d = dir.toFile();
		if (!d.exists())
			if (!d.mkdirs())
				r.abort("problem with directory " + d.getAbsolutePath());
		String filename;
		Path path = null;
		String html = r.getParameter("db_" + m_name + "_text");
		if (html != null) {
			if (insert)
				filename = Text.uuid() + ".html";
			else {
				filename = r.db.lookupString(new Select(m_name).from(m_table).whereIdEquals(id));
				checkDirChange(id, r);
//				String path = dir.toString().substring(Site.site.getBasePath().length());
//				request.response.js("Cache.delete('" + Site.context + path + "/" + filename + "')");
			}
			path = dir.resolve(filename);
			Files.write(path, html);
		} else if (is != null) {
			filename = Files.getNonexistantFilename(Files.getSafeFilename(is_filename), dir);
			path = dir.resolve(filename);
			Site.site.log("writing " + path.toString(), "file.log");
			try {
				Files.write(path, is);
			} catch (RuntimeException e) {
				r.abort(e);
			}
			if (m_title_column != null)
				r.db.update(m_table, m_title_column + "=" + SQL.string(filename), id);
		} else {
			if (part == null || part.getSize() <= 0) {
				if (r.getBoolean(m_name + "_delete"))
					deleteFile(id, r);
				else if (!insert)
					checkDirChange(id, r);
				return null;
			}
			if (!insert)
				deleteFile(id, r);
			filename = part.getSubmittedFileName();
			if (filename == null || filename.length() == 0)
				filename = null;
			else {
	//			if (m_generate_file_names) {
	//				String extension = filename.substring(filename.lastIndexOf('.')).toLowerCase();
	//				filename = Text.uuid() + extension;
	//			} else
					filename = Files.getNonexistantFilename(Files.getSafeFilename(filename), dir);
				path = dir.resolve(filename);
				Site.site.log("writing " + path.toString(), "file.log");
				try {
					part.write(path.toString());
				} catch (IOException e) {
					r.abort(e);
				}
			}
		}
		r.db.update(m_table, m_name + "=" + SQL.string(filename), id);
		if (m_use_git && path != null)
			try {
				new ProcessBuilder("git", "add", ".").directory(d).start().waitFor();
				new ProcessBuilder("git", "commit", "-am", Integer.toString(id)).directory(d).start();
			} catch (IOException | InterruptedException e) {
				r.log(e);
			}
		return path;
	}

	//--------------------------------------------------------------------------

	private boolean
	isLink(View v, Request r) {
		if (v.getMode() == Mode.ADD_FORM)
			return "link".equals(r.getParameter("kind"));
		return m_allow_urls && "l".equals(v.data.getString("kind"));
	}

	//--------------------------------------------------------------------------

	private boolean
	isRichText(View v, Request r) {
		if (v.getMode() == Mode.ADD_FORM)
			return "richtext".equals(r.getParameter("kind"));
		return m_allow_editing && app.Files.isViewable(v.data.getString(m_name));
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setAllowEditing(boolean allow_editing) {
		m_allow_editing = allow_editing;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setAllowURLs(boolean allow_urls) {
		m_allow_urls = allow_urls;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setBadgeColumn(String badge_column) {
		m_badge_column = badge_column;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setDirColumn(String dir_column) {
		m_dir_column = dir_column;
		return this;
	}

	//--------------------------------------------------------------------------

//	public final FileColumn
//	setGenerateFileNames(boolean generate_file_names) {
//		m_generate_file_names = generate_file_names;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setMultiple(boolean multiple) {
		m_multiple = multiple;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setPrintTemplate(String print_template) {
		m_print_template = print_template;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setTitleColumn(String title_column) {
		m_title_column = title_column;
		return this;
	}

	//--------------------------------------------------------------------------

	public final FileColumn
	setUseGit(boolean use_git) {
		m_use_git = use_git;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (m_allow_urls && ("link".equals(r.getParameter("db_" + m_name + "_kind")) || "l".equals(r.db.lookupString(new Select("kind").from(m_table).whereIdEquals(id)))))
			nvp.set(m_name, value)
				.set("kind", "l");
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	validate(ViewDef view_def, String value, int id, Request r) {
		return null;
	}

	//--------------------------------------------------------------------------

	protected void
	writeEditLinks(View v, HTMLWriter w) {
		StringBuilder onclick = new StringBuilder("_.file_column.replace_clicked(this,'");
		onclick.append(m_name)
			.append("'");
		if (m_is_required)
			onclick.append(",true");
		onclick.append(")");
		w.space()
			.addStyle("font-size:9pt")
			.setAttribute("onclick",  onclick.toString())
			.addStyle("cursor:pointer")
			.setAttribute("title", "replace " + m_kind)
			.img("images", "delete.png");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (v.getMode() == Mode.ADD_FORM)
			r.w.hiddenInput("db_" + m_name + "_kind", r.getParameter("kind"));
		if (isLink(v, r))
			writeInputLink(v, r);
		else if (isRichText(v, r))
			writeInputRichText(v, r);
		else
			writeInputUpload(v, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeInputLink(View v, Request r) {
		if (m_is_required)
			r.w.setAttribute("required", "yes");
		r.w.textInput(m_name, 0, getValue(m_name, v, r))
			.ui.helpText("a URL or web address, e.g. http://www.mylink.com");
	}

	//--------------------------------------------------------------------------

	private void
	writeInputRichText(View v, Request r) {
		String filename = getValue(m_name, v, r);
		HTMLWriter w = r.w;
		if (m_is_required)
			w.setAttribute("data-required-msg", "Please enter some text for the document");
		w.setId("db_" + m_name + "_text")
			.addStyle("display:none;height:200px")
			.textAreaOpen("db_" + m_name + "_text", "5", "80");
		if (filename != null)
			w.writeFile(getDirectory(getDirColumnValue(v), r).resolve(filename), false);
		w.tagClose();
		w.hiddenInput(m_name, filename);
		w.js("_.rich_text.create('#db_" + m_name + "_text',{toolbar:'" + (r.userIsAdmin() ? "admin" : "full") + "'})");
	}

	// --------------------------------------------------------------------------

	private void
	writeInputUpload(View v, Request r) {
		String filename = getValue(m_name, v, r);
		HTMLWriter w = r.w;
		if (v.getMode() == Mode.ADD_FORM || filename == null) {
			if (m_is_required)
				w.setAttribute("required", "yes");
			w.setAttribute("onchange", "_.file_column.changed(this)");
			w.fileInput(m_name, m_accept, m_multiple);
		} else {
			String href = getURLPath(v.data) + HTMLWriter.URLEncode(filename).replace("+", "%20");
			writeLink(filename, href, m_allow_urls && "l".equals(v.data.getString("kind")), m_print_template, v.data.getInt("id"), r.w);
			writeEditLinks(v, w);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	writeLink(String text, String url, boolean is_url, String print_url, int id, HTMLWriter w) {
		w.addStyle("text-decoration:none");
		if (is_url) {
			if (url.startsWith("http"))
				w.aBlank(text, url);
			else
				w.a(text, url);
		} else if (app.Files.isViewable(url)) {
			StringBuilder on_click = new StringBuilder("new Dialog({max_width:'doc',url:")
				.append(JS.string(url + "?d=" + Time.newDateTime().toString()))
				.append(",title:")
				.append(JS.string(text));
			if (print_url != null)
				on_click.append(",print_on_click:function(){window.open(context+'" + print_url.replace("{id}", Integer.toString(id)) + "')}");
			on_click.append(",on_close:function(){history.back()}});history.replaceState({dialog:true},'');history.pushState(null,'','" + url + "')");
			w.aOnClick(text, on_click.toString());
		} else {
			String icon = getIcon(url);
			if (icon != null)
				text += " " + icon;
			String lc = url.toLowerCase();
			if (lc.endsWith(".gif") || lc.endsWith(".jpg") || lc.endsWith(".jpeg") || lc.endsWith(".png") || lc.endsWith(".pdf"))
				w.aBlank(text, url);
			else
				w.setAttribute("download", null).a(text, url);
		}
	}

	//--------------------------------------------------------------------------

	public void
	writeValue(Rows data, HTMLWriter w) {
		String filename = data.getString(m_name);
		if (filename == null || filename.length() == 0)
			return;
		String href;
		if (m_allow_urls && "l".equals(data.getString("kind")))
			href = filename;
		else
			href = getURLPath(data) + HTMLWriter.URLEncode(filename).replace("+", "%20");
		if (m_title_column == null)
			w.a(filename, href);
		else
			writeLink(data.getString(m_title_column), href, m_allow_urls && "l".equals(data.getString("kind")), m_print_template, data.getInt("id"), w);
		if (m_badge_column != null) {
			String badge = data.getString(m_badge_column);
			w.write("<span class=\"badge ms-3 bg-secondary\">")
				.write(badge)
				.write("</span>");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		writeValue(v.data, r.w);
		return true;
	}
}
