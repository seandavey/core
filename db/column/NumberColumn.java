package db.column;

import app.Request;
import db.Form;
import db.View;

public class NumberColumn extends ColumnBase<NumberColumn> {
	private String	m_min;
	private String	m_step;

	//--------------------------------------------------------------------------

	public
	NumberColumn(String name) {
		super(name);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		r.w.numberInput(m_name, m_min, null, m_step, getValue(v, r), false);
	}

	// --------------------------------------------------------------------------

	public NumberColumn
	setMin(String min) {
		m_min = min;
		return this;
	}

	// --------------------------------------------------------------------------

	public NumberColumn
	setStep(String step) {
		m_step = step;
		return this;
	}
}
