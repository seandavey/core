package db.column;

import java.time.LocalDate;
import java.util.Map;

import app.Request;
import db.Form;
import db.View;

public class TimestampColumn extends ColumnBase<TimestampColumn> {
	private boolean m_just_date;

	//--------------------------------------------------------------------------

	public
	TimestampColumn(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	public TimestampColumn
	setJustDate(boolean just_date) {
		m_just_date = just_date;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (m_just_date) {
			r.w.dateInput(m_name, getValueDate(m_name, v, r));
			return;
		}
		super.writeInput(v, f, inline, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		if (m_just_date) {
			LocalDate date = v.data.getDate("_timestamp_");
			if (date != null)
				r.w.writeDate(date);
			return true;
		}
		return super.writeValue(v, data, r);
	}
}
