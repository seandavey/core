package db.column;

import java.util.ArrayList;
import java.util.Map;

import app.Request;
import app.Site;
import db.Form;
import db.View;
import db.ViewDef;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import web.WriterBase;

public class EmailColumn extends Column {
	private EmailAddressProvider	m_address_provider;

	//--------------------------------------------------------------------------

	public
	EmailColumn(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	public EmailColumn
	setAddressProvider(EmailAddressProvider address_provider) {
		m_address_provider = address_provider;
		setHelpText("choose from popup or enter a valid email address");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	validate(ViewDef view_def, String value, int id, Request r) {
		if (m_address_provider != null && m_address_provider.isValidAddress(value))
			return null;
		String error = super.validate(view_def, value, id, r);
		if (error != null)
			return error;
		if (value != null && value.length() > 0)
			try {
				InternetAddress[] addresses = InternetAddress.parse(value, false);
				for (InternetAddress address : addresses)
					address.validate();
			} catch (AddressException e) {
				return "Invalid Address: \"" + WriterBase.encode(value) + "\" is not a valid email address";
			}
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (m_address_provider != null) {
			ArrayList<String> a = new ArrayList<>();
			m_address_provider.addAddresses(a);
			a.sort(String.CASE_INSENSITIVE_ORDER);
			Site.site.addAddresses(a, r.db);
			if (!a.isEmpty()) {
				writeInputWithDropdown(a, v, r);
				return;
			}
		}
		super.writeInput(v, f, inline, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		if (v.data == null)
			return false;
		if (v.data.hasColumn("hide_email_on_site") && v.data.getBoolean("hide_email_on_site"))
			return false;
		String value = v.data.getString(m_name);
		if (value != null && value.length() > 0)
			if (v.isPrinterFriendly())
				r.w.write(value);
			else if (value.indexOf('@') != -1)
				r.w.write("<a href=\"mailto:").write(value).write("\">").write(value).write("</a>");
			else
				r.w.write(value);
		return true;
	}
}
