package db.column;

import app.Request;
import db.Form;
import db.View;
import web.HTMLWriter;

public class MultiColumnRenderer implements ColumnValueRenderer, ColumnInputRenderer {
	private final String[] 	m_columns;
	private final boolean	m_show_labels_for_inputs;
	private final boolean	m_show_labels_for_values;

	//--------------------------------------------------------------------------

	public
	MultiColumnRenderer(String[] columns, boolean show_labels_for_inputs, boolean show_labels_for_values) {
		m_columns = columns;
		m_show_labels_for_inputs = show_labels_for_inputs;
		m_show_labels_for_values = show_labels_for_values;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r) {
		HTMLWriter w = r.w;
		w.write("<div style=\"vertical-align:baseline;\">");
		for (int i=0; i<m_columns.length; i++) {
			String column_name = m_columns[i];
			ColumnBase<?> c = v.getColumn(column_name);
			if (i > 0)
				w.space();
			if (m_show_labels_for_inputs) {
				if (c != null) {
					String label = c.getLabel(v, r);
					if (label != null)
						w.write(label);
				} else if (!r.db.getColumn(v.getViewDef().getFrom(), column_name).isBoolean())
					w.write(column_name.replace('_', ' '));
				w.write(": ");
			}
			if (c != null)
				f.writeColumnInput(column_name, true);
			else
				ColumnBase.writeInput(column_name, v, true, r);
		}
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		HTMLWriter w = r.w;
		for (int i=0; i<m_columns.length; i++) {
			ColumnBase<?> c = v.getColumn(m_columns[i]);
			if (i > 0)
				w.space();
			if (m_show_labels_for_values) {
				if (c != null) {
					String label = c.getLabel(v, r);
					if (label != null)
						w.write(label);
				} else
					w.write(m_columns[i].replace('_', ' '));
				w.write(": ");
			}
			v.writeColumnHTML(m_columns[i]);
		}
	}
}