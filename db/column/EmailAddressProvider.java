package db.column;

import java.util.ArrayList;

public interface EmailAddressProvider {
	public void		addAddresses(ArrayList<String> a);
	public boolean	isValidAddress(String a);
}
