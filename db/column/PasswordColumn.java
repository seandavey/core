package db.column;

import app.Request;
import app.Site;
import db.Form;
import db.Rows;
import db.View;
import db.View.Mode;

public class PasswordColumn extends ColumnBase<PasswordColumn> {
	public
	PasswordColumn(String name) {
		super(name);
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	userCanView(View.Mode mode, Rows data, Request r) {
		if (mode == View.Mode.READ_ONLY_FORM)
			return false;
		if (r.userHasRole("people"))
			return true;
		if (mode == View.Mode.EDIT_FORM && data.getInt("id") != r.getUser().getId())
			return false;
		return super.userCanView(mode, data, r);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		Mode mode = v.getMode();
		if (mode == View.Mode.ADD_FORM)
			r.w.passwordInput(m_name, null, true);
		else if (mode == View.Mode.EDIT_FORM)
			if (m_name.equals("password")) {
				int people_id = v.data.getInt("id");
				String reset_id = v.data.getString("reset_id");
				if (reset_id == null)
					reset_id = Site.credentials.reset(people_id, r.db);
				r.w.write("<div>")
					.ui.buttonOnClick("Change",
						"var p=prompt('Enter new password');if(p){" +
							"let fd=new FormData();" +
							"fd.set('password',p);" +
							"fd.set('id'," + people_id + ");" +
							"fd.set('r','" + reset_id + "');" +
							"net.post(context+'/Credentials/set',fd)" +
						"}");
				if (r.userHasRole("people"))
					r.w.addStyle("margin-left:10px")
						.ui.buttonOnClick("Send reset email", "net.post(context+'/Credentials/send email',{id:'" + v.data.getInt("id") + "'},function(){Dialog.alert('','A credentials reset email has been sent.')})");
				r.w.write("</div>");
				if (r.userIsAdministrator())
					r.w.ui.helpText(Site.site.absoluteURL("Credentials", "activate_account.jsp").set("r", reset_id).toString());
			} else
				r.w.passwordInput(m_name, v.data.getString(m_name), true);
	}
}
