package db.column;

import java.io.InputStream;
import java.nio.file.Path;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import jakarta.servlet.http.Part;
import web.HTMLWriter;

public class DocumentColumn extends FileColumn {
	private boolean			m_allow_folders;

	//--------------------------------------------------------------------------

	public
	DocumentColumn(String name, String table, String directory) {
		super(name, table, directory);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	appendTokens(NameValuePairs nvp, StringBuilder tsvector) {
		if ("f".equals(nvp.getString("kind")))
			return;
		super.appendTokens(nvp, tsvector);
	}

	// --------------------------------------------------------------------------

	@Override
	public final String
	getLabel(View v, Request r) {
		if (isFolder(v, r))
			return "folder name";
		return super.getLabel(v, r);
	}

	//--------------------------------------------------------------------------
	// record will exist when this is called so view_def.update is called even if this is new record

	@Override
	public final Path
	handleFile(Part part, InputStream is, String is_filename, ViewDef view_def, boolean insert, int id, Request r) {
		if (m_allow_folders && "f".equals(r.db.lookupString(new Select("kind").from(m_table).whereIdEquals(id))))
			return null;
		return super.handleFile(part, is, is_filename, view_def, insert, id, r);
	}

	//--------------------------------------------------------------------------

	private boolean
	isFolder(View v, Request r) {
		if (v.getMode() == Mode.ADD_FORM)
			return "folder".equals(r.getParameter("kind"));
		return m_allow_folders && "f".equals(v.data.getString("kind"));
	}

	//--------------------------------------------------------------------------

	public final DocumentColumn
	setAllowFolders(boolean allow_folders) {
		m_allow_folders = allow_folders;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (m_allow_folders && ("folder".equals(r.getParameter("db_" + m_name + "_kind")) || "f".equals(r.db.lookupString(new Select("kind").from(m_table).whereIdEquals(id)))))
			nvp.set("title", value)
				.set("kind", "f");
		else
			super.setValue(value, nvp, view_def, id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (isFolder(v, r)) {
			if (v.getMode() == Mode.ADD_FORM)
				r.w.hiddenInput("db_" + m_name + "_kind", r.getParameter("kind"));
			writeInputFolder(v, r);
		} else
			super.writeInput(v, f, inline, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeInputFolder(View v, Request r) {
		if (m_is_required)
			r.w.setAttribute("required", "yes");
		r.w.textInput(m_name, 0, getValue(m_title_column == null ? m_name : m_title_column, v, r));
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	writeValue(Rows data, HTMLWriter w) {
		if (m_allow_folders && "f".equals(data.getString("kind"))) {
			w.addStyle("text-decoration:none")
				.aOnClickOpen("documents.toggle_folder(this)");
			w.addStyle("margin-right:10px")
				.tagOpen("span");
			w.ui.icon("folder")
				.tagClose()
				.write(data.getString(m_title_column == null ? m_name : m_title_column))
				.tagClose();
		} else
			super.writeValue(data, w);
	}
}
