package db.column;

import java.time.LocalDate;
import java.time.LocalTime;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.ViewDef;
import util.Time;

public class DateColumn extends ColumnBase<DateColumn> {
	private boolean	m_check_not_same;
	private String	m_time_column;

	// --------------------------------------------------------------------------

	public
	DateColumn(String name) {
		super(name);
	}

	// --------------------------------------------------------------------------

	public final DateColumn
	setCheckNotSame(boolean check_not_same) {
		m_check_not_same = check_not_same;
		return this;
	}

	// --------------------------------------------------------------------------

	public final DateColumn
	setTimeColumn(String time_column) {
		m_time_column = time_column;
		return this;
	}

	// --------------------------------------------------------------------------
	// id != 0 on update, 0 on insert

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (value != null && m_time_column != null) {
			LocalDate date = LocalDate.parse(value);
			LocalTime time = Time.formatter.parseTime(r.getParameter(m_time_column));
			value = Time.toSiteZone(date, time, r).toString();
		}
		nvp.set(m_name, value);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		LocalDate date = getValueDate(m_name, v, r);
		if (m_time_column != null)
			date = Time.toUserZone(date, getValueTime(m_time_column, v, r), r);
		r.w.dateInput(m_name, date, null, m_check_not_same, false);
	}
}
