package db.column;

import java.sql.Types;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import app.Request;
import db.Form;
import db.Rows;
import db.View;
import web.HTMLWriter;

public class CurrencyColumn extends ColumnBase<CurrencyColumn> {
	private double m_min;

	// --------------------------------------------------------------------------

	public
	CurrencyColumn(String name) {
		super(name);
		m_text_align = "right";
	}

	// --------------------------------------------------------------------------

	public final ColumnBase<CurrencyColumn>
	setMin(double min) {
		m_min = min;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		r.w.addStyle("display:inline");
		if (m_min != 0)
			r.w.setAttribute("min", Double.toString(m_min));
		if (v.getMode() == View.Mode.ADD_FORM)
			r.w.textInput(m_name, inline ? 10 : 0, getDefaultValue(v, r));
		else {
			NumberFormat nf = NumberFormat.getInstance(Locale.US);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			r.w.textInput(m_name, inline ? 10 : 0, nf.format(v.data.getDouble(m_name)));
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeTotal(double total, HTMLWriter w) {
		w.writeCurrency(total);
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		if (m_is_computed) {
			r.w.writeCurrency(getDouble(v, r));
			return true;
		}
		Rows d = v.data;
		if (d == null)
			return false;
		int column_index = d.getColumnIndex(m_name);
		if (column_index == -1)
			r.log("column " + m_name + " not found in CurrencyColumn.writeValue", true);
		else {
			int type = d.getColumnType(column_index);
			switch(type) {
			case Types.DOUBLE:
			case Types.NUMERIC:
				double dbl = getDouble(v, r);
				if (!d.wasNull())
					r.w.writeCurrency(dbl);
				return true;
			case Types.BIGINT:
			case Types.INTEGER:
				int i = d.getInt(column_index);
				if (!d.wasNull())
					r.w.writeCurrency(i);
				return true;
			case Types.REAL:
				float f = d.getFloat(column_index);
				if (!d.wasNull())
					r.w.writeCurrency(f);
				return true;
			}
		}
		return writeValue(m_name, 0, v, r);
	}
}
