package db.column;

import app.Request;
import db.View;

public interface ColumnValueRenderer {
	public void
	writeValue(View v, ColumnBase<?> column, Request r);
}
