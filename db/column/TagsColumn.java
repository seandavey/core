package db.column;

import java.util.Map;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.ViewDef;
import ui.Tags;
import web.JSON;

public class TagsColumn extends ColumnBase<TagsColumn> {
	protected String[] m_choices;

	//--------------------------------------------------------------------------

	public
	TagsColumn(String name, String[] choices) {
		super(name);
		m_choices = choices;
	}

	//--------------------------------------------------------------------------

	public final TagsColumn
	setDefaultValue(String[] default_value) {
		setDefaultValue(Json.array(default_value).toString());
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (value == null || value.length() < 2) {
			nvp.set(m_name, null);
			return;
		}
		JsonArray a = Json.parse(value).asArray();
		for (int i=0; i<a.size(); i++)
			a.set(i, a.get(i).asObject().get("value"));
		nvp.set(m_name, a.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		String value = getValue(v, r);
		new Tags(m_name, value == null ? null : JSON.toArray(Json.parse(value).asArray()), m_choices)
			.setRestrictToChoices(true)
			.setAllowDragging(true)
			.write(r.w);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		String value = v.data.getString(m_name);
		if (value != null) {
			value = value.substring(1, value.length() - 1).replaceAll("\"", "").replaceAll(",", ", ");
			r.w.write(value);
			return value.length() > 0;
		}
		return false;
	}
}
