package db.column;

import java.util.Map;

import app.ImageUploads;
import app.Request;
import db.Form;
import db.View;
import db.ViewDef;
import ui.RichText;
import util.Text;

public class TextAreaColumn extends ColumnBase<TextAreaColumn> {
	private boolean	m_is_rich_text;
	private boolean	m_small_toolbar;

	//--------------------------------------------------------------------------

	public
	TextAreaColumn(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	beforeDelete(ViewDef view_def, String where, Request r) {
		ImageUploads.deleteFiles(r.db.lookupString(m_name, view_def.getFrom(), where));
	}

	//--------------------------------------------------------------------------

	public TextAreaColumn
	setIsRichText(boolean is_rich_text, boolean small_toolbar) {
		m_is_rich_text = is_rich_text;
		m_small_toolbar = small_toolbar;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		String value = getValue(v, r);
		if (m_encryption_column != null && v.getMode() == View.Mode.EDIT_FORM)
			value = Text.scramble(value, v.data.getInt(m_encryption_column));
		if (m_is_rich_text) {
			if (value != null)
				value = value.replaceAll("\n", "<br>");
			new RichText()
				.setEncode(true)
				.write(m_name, value, "textarea", true, m_small_toolbar ? "small" : r.userIsAdministrator() ? "admin" : "full", r.w);
		} else
			r.w.textArea(m_name, value);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		if (!m_is_rich_text)
			return super.writeValue(v, data, r);
		String value = getValue(v, r);
		if (value != null && value.length() > 0) {
			if (m_encryption_column != null)
				value = Text.scramble(value, v.data.getInt(m_encryption_column));
			if (!m_small_toolbar)
				r.w.addClass("ck-content").tagOpen("div");
			r.w.writeWithLinks(value);
			if (!m_small_toolbar)
				r.w.tagClose();
			return true;
		}
		return false;
	}
}
