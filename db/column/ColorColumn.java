package db.column;

import java.util.Map;

import app.Request;
import db.Form;
import db.View;

public class ColorColumn extends ColumnBase<ColorColumn> {
	public
	ColorColumn(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		r.w.colorInput(m_name, getValue(v, r));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		String value = v.data.getString(m_name);
		if (value == null)
			return false;
		r.w.write("<div style=\"background:").write(value).write(";height:20px;width:20px\"></div>");
		return true;
	}
}
