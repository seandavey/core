package db.column;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import app.Person;
import app.Request;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.Relationship;
import db.Rows;
import db.SQL;
import db.Select;
import db.Validation;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.feature.QuickFilter;
import db.jdbc.JDBCColumn;
import db.object.DBObject;
import db.section.Section;
import ui.Dropdown;
import ui.InputGroup;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.WriterBase;

public class ColumnBase<T> implements Cloneable {
	private enum DefaultValue {
		BASE_FILTER, PREVIOUS, REQUEST_PARAMETER, SESSION_ATTRIBUTE, SHORT_DATE, SQL, USER_EMAIL, USER_ID
	}

	private String					m_column_head;
	protected DefaultValue			m_compute_default_value;
	private Select					m_default_select;
	protected String				m_default_value;
	protected String				m_display_name;
	private String					m_edit_role;
	protected String				m_encryption_column;
	private QuickFilter				m_filter;
	private String					m_help_text;
	private Mode[]					m_hide_modes;
	protected ColumnInputRenderer	m_input_renderer;
	private JDBCColumn				m_jdbc_column;
	protected boolean				m_is_computed;
	protected boolean				m_is_filterable = true;
	protected boolean				m_is_hidden;
	protected boolean				m_is_read_only;
	protected boolean				m_is_required;
	protected boolean				m_is_sortable = true;
	protected boolean				m_is_unique;
//	protected ColumnLabelRenderer	m_label_renderer;
	protected final String			m_name;
	private boolean					m_nospaces;
	private String					m_on_change;
	private String					m_on_input;
	private String					m_order_by;
//	private String					m_order_by_desc;
	protected String				m_post_text;
	private String					m_pre_text;
	private boolean					m_show_previous_values_dropdown;
	private String					m_sort_type;
	private String[]				m_sub_columns;
	private boolean					m_sub_columns_toggle;
	protected String				m_text_align;
	private String					m_title;
	private boolean					m_total;
	private boolean					m_total_is_int;
//	private List<Validator>			m_validators;
	private ColumnValueRenderer		m_value_renderer;
	private String					m_view_role;

	// --------------------------------------------------------------------------

	public
	ColumnBase(String name) {
		m_name = name;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	addValidator(Validator validator) {
//		if (m_validators == null)
//			m_validators = new ArrayList<>();
//		m_validators.add(validator);
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	public void
	adjustQuery(Select query, Mode mode) {
	}

	// --------------------------------------------------------------------------

	public void
	afterDelete(ViewDef view_def, String where, Request r) {
	}

	// --------------------------------------------------------------------------

	public void
	beforeDelete(ViewDef view_def, String where, Request r) {
	}

	// --------------------------------------------------------------------------

	public final boolean
	canEdit(View v, Request r) {
		if (isReadOnly(v, r))
			return false;
		return m_edit_role == null || r.userHasRole(m_edit_role);
	}

	// --------------------------------------------------------------------------

	@Override
	public Object
	clone() throws CloneNotSupportedException {
		return super.clone();
	}

	// --------------------------------------------------------------------------

	public final boolean
	defaultIsPrevious() {
		return m_compute_default_value == DefaultValue.PREVIOUS;
	}

	// --------------------------------------------------------------------------

	public final String
	getColumnHead() {
		if (m_column_head != null)
			return m_column_head;
		return getDisplayName(false);
	}

	// --------------------------------------------------------------------------

	public String
	getDefaultValue(ViewState view_state, Request r) {
		String default_value = r.getParameter(m_name);
		if (default_value != null)
			return default_value;

		if (m_compute_default_value != null)
			switch (m_compute_default_value) {
			case BASE_FILTER:
				String base_filter = view_state.getBaseFilter();
				return base_filter.substring(base_filter.lastIndexOf('=') + 1);
			case PREVIOUS:
				return (String) r.getSessionAttribute("default:" + m_name);
			case REQUEST_PARAMETER:
				return r.getParameter(m_default_value);
			case SESSION_ATTRIBUTE:
				String attribute_name = m_default_value != null ? m_default_value : m_name;
				Object attribute = r.getSessionAttribute(attribute_name);
				return attribute == null ? null : attribute.toString();
			case SHORT_DATE:
				return Time.newDate().toString();
			case SQL:
				return r.db.lookupString(m_default_select);
			case USER_EMAIL:
				Person user = r.getUser();
				if (user == null)
					return null;
				return user.getEmail();
			case USER_ID:
				user = r.getUser();
				if (user != null)
					return Integer.toString(user.getId());
				return "0";
			}

		return m_default_value;
	}

	// --------------------------------------------------------------------------

	public final String
	getDefaultValue(View v, Request r) {
		return getDefaultValue(m_name, v, r);
	}

	// --------------------------------------------------------------------------

	public static String
	getDefaultValue(String name, View v, Request r) {
		if (v == null || v.getMode() != Mode.ADD_FORM)
			return null;
		String default_value = r.getParameter(name);
		if (default_value == null) {
			Relationship relationship = v.getRelationship();
			if (relationship != null && name.equals(relationship.one.getFrom() + "_id"))
				default_value = Integer.toString(relationship.one.getID());
		}
		if (default_value == null) {
			ColumnBase<?> column = v.getColumn(name);
			if (column != null)
				default_value = column.getDefaultValue(v.getState(), r);
		}
		if (default_value == null) {
			List<Section> sections = v.getViewDef().getSections();
			if (sections != null)
				for (Section section : sections) {
					default_value = section.getColumnDefaultValue(name, r);
					if (default_value != null)
						break;
				}
		}
		if (default_value == null) {
			JDBCColumn jdbc_column = r.db.getColumn(v.getViewDef().getFrom(), name);
			if (jdbc_column != null)
				default_value = jdbc_column.getDefaultValue();
		}
		return default_value;
	}

	// --------------------------------------------------------------------------

	public String
	getDisplayName(boolean use_non_breaking_spaces) {
		if (m_display_name != null)
			return getDisplayName(m_display_name, use_non_breaking_spaces);
		String name = m_name;
		int index = name.indexOf('.');
		if (index != -1)
			name = name.substring(index + 1);
		if (name.endsWith("_id"))
			name = Text.singularize(name.substring(0, name.length() - 3));
		return getDisplayName(name, use_non_breaking_spaces);
	}

	// --------------------------------------------------------------------------

	public static String
	getDisplayName(String name, boolean use_non_breaking_spaces) {
		StringBuilder display_name = new StringBuilder(name);
		for (int i = 0; i < display_name.length(); i++) {
			char c = display_name.charAt(i);
			if (c == '_' || use_non_breaking_spaces && c == ' ')
				if (use_non_breaking_spaces) {
					display_name.replace(i, i + 1, "&nbsp;");
					i += 5;
				} else
					display_name.setCharAt(i, ' ');
		}
		return display_name.toString();
	}

	// --------------------------------------------------------------------------

	public double
	getDouble(View v, Request r) {
		return v.data.getDouble(m_name);
	}

	// --------------------------------------------------------------------------

	public final QuickFilter
	getFilter() {
		return m_filter;
	}

	// --------------------------------------------------------------------------

	protected final JDBCColumn
	getJDBCColumn(String table_name, DBConnection db) {
		if (m_jdbc_column == null)
			m_jdbc_column = db.getColumn(table_name, m_name);
		return m_jdbc_column;
	}

	// --------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	// --------------------------------------------------------------------------

	public String
	getOrderBy() {
		return m_order_by;
	}

	// --------------------------------------------------------------------------

	public String
	getOrderByDesc() {
		return null;
//		return m_order_by_desc;
	}

	// --------------------------------------------------------------------------

	public final String
	getSortType() {
		return m_sort_type;
	}

	// --------------------------------------------------------------------------

	public final String
	getTextAlign() {
		return m_text_align;
	}

	// --------------------------------------------------------------------------

	public final String
	getValue(View v, Request r) {
		return getValue(m_name, v, r);
	}

	// --------------------------------------------------------------------------

	public static String
	getValue(String name, View v, Request r) {
		if (v.getMode() == View.Mode.ADD_FORM)
			return getDefaultValue(name, v, r);
		JDBCColumn jdbc_column = r.db.getColumn(v.getViewDef().getFrom(), name);
		if (jdbc_column == null) {
			r.log("ColumnBase.getValue(): column " + name + " not found in " + v.getViewDef().getFrom(), false);
			return null;
		}
		if (jdbc_column.isBinary())
			return SQL.decodeBinary(v.data.getString(name));
		if (jdbc_column.isTimestamp()) {
			LocalDateTime timestamp = v.data.getTimestamp(name);
			if (timestamp != null)
				return Time.formatter.getDateTime(timestamp);
		}
		return v.data.getString(name);
	}

	// --------------------------------------------------------------------------

	protected static boolean
	getValueBoolean(String name, View v, Request r) {
		if (v.getMode() == View.Mode.ADD_FORM) {
			String value = getDefaultValue(name, v, r);
			return value != null && (value.equals("yes") || value.equals("t") || value.equals("true"));
		}
		return v.data.getBoolean(name);
	}

	// --------------------------------------------------------------------------

	protected static LocalDate
	getValueDate(String name, View v, Request r) {
		if (v.getMode() == View.Mode.ADD_FORM) {
			String default_value = getDefaultValue(name, v, r);
			if (default_value != null)
				return LocalDate.parse(default_value);
			return null;
		}
		return v.data.getDate(name);
	}

	// --------------------------------------------------------------------------

	protected static LocalTime
	getValueTime(String name, View v, Request r) {
		if (v.getMode() == View.Mode.ADD_FORM)
			return Time.formatter.parseTime(getDefaultValue(name, v, r));
		return v.data.getTime(name);
	}

	// --------------------------------------------------------------------------

	public final ColumnValueRenderer
	getValueRenderer() {
		return m_value_renderer;
	}

	// --------------------------------------------------------------------------

	public final String
	getViewRole() {
		return m_view_role;
	}

	// --------------------------------------------------------------------------

	public final boolean
	isComputed() {
		return m_is_computed;
	}

	// --------------------------------------------------------------------------

	public final boolean
	isFilterable() {
		return m_is_filterable;
	}

	// --------------------------------------------------------------------------

	public boolean
	isHidden(Request r) {
		return m_is_hidden;
	}

	// --------------------------------------------------------------------------

	public boolean
	isReadOnly(View v, Request r) {
		if (r.userIsAdmin())
			return false;
		return m_is_read_only;
	}

	// --------------------------------------------------------------------------

	public final boolean
	isRequired() {
		return m_is_required;
	}

	// --------------------------------------------------------------------------

	public final boolean
	isSortable() {
		return (m_is_sortable || m_order_by != null) && m_encryption_column == null;
	}

	// --------------------------------------------------------------------------

	public final void
	mod(JsonObject o) {
		m_display_name = o.getString("display name", null);
		m_help_text = o.getString("help text", null);
		m_post_text = o.getString("post text", null);
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	setColumnHead(String column_head) {
//		m_column_head = column_head;
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToBaseFilter() {
		m_compute_default_value = DefaultValue.BASE_FILTER;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToPrevious() {
		m_compute_default_value = DefaultValue.PREVIOUS;
		return (T) this;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	setDefaultToRequestParameter(String parameter_name) {
//		m_compute_default_value = DefaultValue.REQUEST_PARAMETER;
//		m_default_value = parameter_name;
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToSessionAttribute() {
		m_compute_default_value = DefaultValue.SESSION_ATTRIBUTE;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToSessionAttribute(String attribute_name) {
		m_compute_default_value = DefaultValue.SESSION_ATTRIBUTE;
		m_default_value = attribute_name;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToDateNow() {
		m_compute_default_value = DefaultValue.SHORT_DATE;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToSQL(Select select) {
		m_compute_default_value = DefaultValue.SQL;
		m_default_select = select;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToUserEmail() {
		m_compute_default_value = DefaultValue.USER_EMAIL;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultToUserId() {
		m_compute_default_value = DefaultValue.USER_ID;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultValue(boolean default_value) {
		m_default_value = default_value ? "yes" : "no";
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultValue(int default_value) {
		m_default_value = Integer.toString(default_value);
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDefaultValue(String default_value) {
		m_default_value = default_value;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setDisplayName(String display_name) {
		m_display_name = display_name;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setEditRole(String edit_role) {
		m_edit_role = edit_role;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setEncryptionColumn(String encryption_column) {
		m_encryption_column = encryption_column;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setQuickFilter(QuickFilter filter) {
		m_filter = filter;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setHelpText(String help_text) {
		m_help_text = help_text;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setHideOnForms(Mode... hide_modes) {
		m_hide_modes = hide_modes;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setInputRenderer(ColumnInputRenderer input_renderer) {
		m_input_renderer = input_renderer;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setInputAndValueRenderers(Object input_and_value_renderer, boolean sortable) {
		m_input_renderer = (ColumnInputRenderer) input_and_value_renderer;
		m_value_renderer = (ColumnValueRenderer) input_and_value_renderer;
		m_is_sortable = sortable;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsComputed(boolean is_computed) {
		m_is_computed = is_computed;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsFilterable(boolean is_filterable) {
		m_is_filterable = is_filterable;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsHidden(boolean is_hidden) {
		m_is_hidden = is_hidden;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsReadOnly(boolean is_read_only) {
		m_is_read_only = is_read_only;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	setIsRequired(boolean is_required) {
		m_is_required = is_required;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsSortable(boolean is_sortable) {
		m_is_sortable = is_sortable;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setIsUnique(boolean is_unique) {
		m_is_unique = is_unique;
		return (T) this;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	setLabelRenderer(ColumnLabelRenderer label_renderer) {
//		m_label_renderer = label_renderer;
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setNoSpaces(boolean nospaces) {
		m_nospaces = nospaces;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	setOnChange(String on_change) {
		m_on_change = on_change;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	setOnInput(String on_input) {
		m_on_input = on_input;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setOrderBy(String order_by) {
		m_order_by = order_by;
		return (T) this;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	setOrderByDesc(String order_by_desc) {
//		m_order_by_desc = order_by_desc;
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setPostText(String post_text) {
		m_post_text = post_text;
		return (T) this;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public final T
//	setPreText(String pre_text) {
//		m_pre_text = pre_text;
//		return (T) this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setShowPreviousValuesDropdown(boolean show_previous_values_dropdown) {
		m_show_previous_values_dropdown = show_previous_values_dropdown;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setSortType(String sort_type) {
		m_sort_type = sort_type;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setSubColumns(boolean toggle, String... sub_columns) {
		m_sub_columns_toggle = toggle;
		m_sub_columns = sub_columns;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setTextAlign(String text_align) {
		m_text_align = text_align;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setTitle(String title) {
		m_title = title;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setTotal(boolean total) {
		m_total = total;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setTotalIsInt(boolean total_is_int) {
		m_total_is_int = total_is_int;
		return (T) this;
	}

	// --------------------------------------------------------------------------
	// id != 0 on update, 0 on insert

	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (m_encryption_column != null)
			if (id != 0)
				nvp.set(m_name, Text.scramble(value, r.db.lookupInt(new Select(m_encryption_column).from(view_def.getFrom()).whereIdEquals(id), 0)));
			else
				nvp.set(m_name, Text.scramble(value, r.getInt(m_encryption_column, 0)));
		else
			nvp.set(m_name, value);
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setValueRenderer(ColumnValueRenderer value_renderer, boolean sortable) {
		m_value_renderer = value_renderer;
		m_is_sortable = sortable;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setViewRole(String view_role) {
		m_view_role = view_role;
		return (T) this;
	}

	// --------------------------------------------------------------------------

	public boolean
	showOnForm(View.Mode mode, Rows data, Request r) {
		if (m_hide_modes == null)
			return true;
		for (Mode m : m_hide_modes)
			if (m == mode)
				return false;
		return true;
	}

	// --------------------------------------------------------------------------

	public final boolean
	total() {
		return m_total;
	}

	// --------------------------------------------------------------------------

	public boolean
	userCanView(View.Mode mode, Rows data, Request r) {
		return m_view_role == null || r.userHasRole(m_view_role);
	}

	// --------------------------------------------------------------------------

	public final boolean
	userHasViewRole(Request r) {
		return m_view_role != null && r.userHasRole(m_view_role);
	}

	// --------------------------------------------------------------------------

	public String
	validate(ViewDef view_def, String value, int id, Request r) {
		if (m_is_required && value != null && value.length() == 0)
			return "Required Field: Please enter a value for " + getDisplayName(false);
		if (m_is_unique) {
			NameValuePairs nvp = new NameValuePairs();
			nvp.set(m_name, value);
			boolean different = false;
			if (id != 0) {
				String current_value = r.db.lookupString(m_name, view_def.getFrom(), id);
				if (current_value == null)
					different = value != null && value.length() > 0;
				else
					different = !current_value.equals(value);
			}
			if (id == 0 || different)
				if (r.db.exists(view_def.getFrom(), nvp.getWhereString(view_def.getFrom(), r.db)))
					return "There is already a " + view_def.getRecordName() + " with " + getDisplayName(false) + " equal to " + value + ". Please enter a different value for " + getDisplayName(false) + ".";
		}
//		if (m_validators != null)
//			for (Validator validator : m_validators) {
//				String error = validator.validate(view_def.getFrom(), value, r);
//				if (error != null)
//					return error;
//			}
		if (value == null || value.length() == 0)
			return null;
		JDBCColumn jdbc_column = getJDBCColumn(view_def.getFrom(), r.db);
		if (jdbc_column == null)
			return null;
		return Validation.validate(jdbc_column, getDisplayName(false), value, r);
	}

	// --------------------------------------------------------------------------

	public final void
	writeAdministratorInfo(Request r) {
		if (isHidden(r) && r.userIsAdministrator())
			r.w.write(" <span class=\"text-success\" style=\"font-size:small\">hidden</span>");
		String view_role = getViewRole();
		if (view_role != null && r.userIsAdministrator())
			r.w.write(" <span class=\"text-success\" style=\"font-size:small\">").write(view_role).write("</span>");
	}

	// --------------------------------------------------------------------------

	public final void
	writeInput(View v, Form f, boolean inline, boolean printer_friendly, Request r) {
		Mode mode = v.getMode();
		HTMLWriter w = r.w;

		if (m_pre_text != null) {
			w.write(m_pre_text);
			w.space();
		}
		if (printer_friendly || isReadOnly(v, r) || mode == View.Mode.EDIT_FORM && !canEdit(v, r)) {
			if (mode == View.Mode.ADD_FORM) {
				String default_value = getDefaultValue(v, r);
				if (default_value != null) {
					w.hiddenInput(m_name, default_value);
					if (m_value_renderer != null)
						m_value_renderer.writeValue(v, this, r);
					else
						w.write(default_value);
				}
			} else
				v.writeColumnHTML(m_name);
		} else {
			if (m_is_required) {
				w.setAttribute("required", "yes");
				w.setAttribute("data-title", getDisplayName(false));
			}
			if (m_nospaces)
				w.setAttribute("nospaces", "true");
			if (m_on_change != null)
				w.setAttribute("onchange", m_on_change);
			if (m_on_input != null)
				w.setAttribute("oninput", m_on_input);
			if (m_show_previous_values_dropdown) {
				writeInputWithPreviousDropdown(v, r);
				return;
			}
			if (m_sub_columns_toggle)
				w.setAttribute("onchange", "var d=this.closest('div').nextElementSibling;d.style.display=this.checked?'block':'none';if(this.checked){d.querySelectorAll('textarea').forEach(function(i){_.form.resize_textarea(i)})}");
			writeInput(v, f, inline, r);
			if (m_sub_columns != null) {
				w.addClass("mt-3");
				if (m_sub_columns_toggle && !getValueBoolean(m_name, v, r))
					w.addStyle("display:none");
				w.tagOpen("div");
				f.subrowsStart();
				for (String sub_column : m_sub_columns) {
					w.addClass("mb-3").addStyle("padding-left:20px").tagOpen("div");
					f.writeColumnRow(sub_column, v.getColumn(sub_column));
					w.tagClose();
				}
				f.subrowsEnd();
				w.tagClose();
			}
		}
		writeInputPostText(v, r);
		if (m_help_text != null)
			w.ui.helpText(m_help_text);
	}

	// --------------------------------------------------------------------------

	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (m_input_renderer != null) {
			m_input_renderer.writeInput(v, f, this, inline, r);
			return;
		}
		if (m_title != null)
			r.w.setAttribute("title", m_title);
		JDBCColumn jdbc_column = getJDBCColumn(v.getViewDef().getFrom(), r.db);
		if (jdbc_column == null)
			throw new RuntimeException("column \"" + m_name + "\" not found in table " + v.getViewDef().getFrom());
		if (jdbc_column.isBoolean())
			r.w.ui.checkbox(m_name, getDisplayName(false), null, getValueBoolean(m_name, v, r), false, true);
		else
			writeInput(m_name, v, inline, r);
	}

	// --------------------------------------------------------------------------
	// returns true if input was written, false if column not found in table

	public static void
	writeInput(String name, View v, boolean inline, Request r) {
		JDBCColumn jdbc_column = r.db.getColumn(v.getViewDef().getFrom(), name);
		if (jdbc_column == null)
			throw new RuntimeException("column \"" + name + "\" not found in table " + v.getViewDef().getFrom());
		if (jdbc_column.isBinary())
			r.w.textArea(name, getValue(name, v, r));
		else if (jdbc_column.isBoolean())
			r.w.ui.checkbox(name, name.replace('_', ' '), null, getValueBoolean(name, v, r), false, true);
		else if (jdbc_column.isDate())
			r.w.dateInput(name, getValueDate(name, v, r));
		else if (jdbc_column.isInteger())
			r.w.numberInput(name, null, null, null, getValue(name, v, r), inline);
		else if (jdbc_column.isText() || jdbc_column.isOther())
			r.w.textAreaExpanding(name, "min-width:100px", getValue(name, v, r));
		else if (jdbc_column.isTime())
			r.w.timeInput(name, getValueTime(name, v, r), 5, true, false, inline);
		else if (jdbc_column.isTimestamp())
			r.w.write(getValue(name, v, r));
		else
			r.w.textInput(name, 0/*jdbc_column.size*/, getValue(name, v, r));
	}

	// --------------------------------------------------------------------------

	private void
	writeInputPostText(View v, Request r) {
		if (m_post_text != null)
			r.w.space().write(m_post_text);
	}

	// --------------------------------------------------------------------------

	protected final void
	writeInputWithDropdown(List<String> values, View v, Request r) {
		JDBCColumn jdbc_column = getJDBCColumn(v.getViewDef().getFrom(), r.db);
		HTMLWriter w = r.w;
		InputGroup ig = w.ui.inputGroup().open();
		String id = Text.uuid();
		w.setId(id);
		if (values.size() > 0)
			w.setAttribute("onkeyup", "_.form.fill_choice(this,this.nextElementSibling.querySelector('.dropdown-menu'))"); // won't work with different UI classes
		w.ui.textInput(m_name, jdbc_column.size, getValue(m_name, v, r));
		if (values.size() > 0) {
			Dropdown dropdown = ig.dropdownOpen();
			for (String value : values)
				dropdown.aOnClick(value, "this.closest('.input-group').firstChild.value=innerText"); // won't work with different UI classes
			dropdown.close();
		}
		ig.close();
	}

	// --------------------------------------------------------------------------

	private void
	writeInputWithPreviousDropdown(View v, Request r) {
		writeInputWithDropdown(r.db.readValues(new Select(m_name).distinct().from(v.getViewDef().getFrom()).where(m_name + " IS NOT NULL").orderBy(m_name)), v, r);
	}

	// --------------------------------------------------------------------------

	public String
	getLabel(View v, Request r) {
		if (!v.isPrinterFriendly()) {
			JDBCColumn jdbc_column = getJDBCColumn(v.getViewDef().getFrom(), r.db);
			if (jdbc_column != null && jdbc_column.isBoolean())
				return null;
		}
//		if (m_label_renderer != null)
//			return m_label_renderer.getLabel(this);
		return getDisplayName(false);
	}

	// --------------------------------------------------------------------------

	private static boolean
	writeString(String value, View v, int display_length, boolean binary, int key, HTMLWriter w) {
		if (value == null || value.length() == 0)
			return false;
		if (binary)
			value = SQL.decodeBinary(value);
		if (key != 0)
			value = Text.scramble(value, key);
		else
			value = value.trim();
		if (display_length > 0 && value.length() > display_length && !v.isPrinterFriendly()) {
			w.write(WriterBase.encode(value.substring(0, display_length)));
			w.write("<span class=\"text-more-link\"> <a onclick=\"var p=this.parentNode;p.nextElementSibling.style.display='';p.remove();return false;\" style=\"text-decoration:underline;cursor:pointer;\">more...</a></span><span class=\"text-more-text\" style=\"display:none\">");
			w.write(WriterBase.encode(value.substring(display_length)));
			w.write("</span>");
		} else if (v.getMode() != View.Mode.LIST && v.getMode() != View.Mode.READ_ONLY_LIST)
			w.writeWithLinks(value);
		else
			w.write(value);
		return true;
	}

	// --------------------------------------------------------------------------

	public void
	writeTotal(double total, HTMLWriter w) {
		if (m_total_is_int)
			w.write((int)total);
		else
			w.write(Text.two_digits.format(total));
	}

	// --------------------------------------------------------------------------

	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		if (m_value_renderer != null) {
			m_value_renderer.writeValue(v, this, r);
			return true;
		}
		Rows d = v.data;
		if (d == null)
			return false;
		if (m_encryption_column != null)
			return writeString(d.getString(m_name), v, 0, d.getColumnType(m_name) == Types.BINARY, d.getInt(m_encryption_column), r.w);
		return writeValue(m_name, 0, v, r);
	}

	// --------------------------------------------------------------------------

	public static boolean
	writeValue(String name, int display_length, View v, Request r) {
		Rows data = v.data;
		if (data == null)
			return false;

		int index = name.lastIndexOf(" AS ");
		if (index != -1)
			name = name.substring(index + 4);
		index = name.indexOf('.');
		if (index != -1)
			name = name.substring(index + 1);
		int column_index = data.getColumnIndex(name);
		if (column_index == -1) {
			r.log("Column \"" + name + "\" not found in ColumnBase.writeValue", true);
			return false;
		}
		int type = data.getColumnType(column_index);

		if (type == Types.BOOLEAN || type == Types.BIT) {
			if (v.getViewDef().allowQuickEdit()) {
				StringBuilder on_click = new StringBuilder();
				on_click.append("_.qe.send_b('").append(v.getViewDef().getName()).append("',this");
				if (v.getViewDef().replaceOnQuickEdit())
					on_click.append(",true");
				on_click.append(")");
				r.w.setAttribute("onclick", on_click.toString());
				r.w.ui.checkbox(null, null, null, data.getBoolean(column_index), true, true);
			} else {
				Mode mode = v.getMode();
				if (!v.isPrinterFriendly() && (mode == Mode.EDIT_FORM || mode == Mode.READ_ONLY_FORM))
					r.w.write(getDisplayName(name, true)).write(": ");
				r.w.write(data.getBoolean(column_index) ? "yes" : "no");
			}
			return true;
		}
		if (type == Types.DATE) {
			LocalDate date = data.getDate(column_index);
			if (date != null) {
				r.w.writeDate(date);
				return true;
			}
			return false;
		}
		if (type == Types.DOUBLE) {
			r.w.write(data.getDouble(column_index));
			return true;
		}
		if (type == Types.INTEGER) {
			r.w.write(data.getInt(column_index));
			return true;
		}
		if (type == Types.REAL) {
			r.w.write(data.getFloat(column_index));
			return true;
		}
		if (type == Types.TIME) {
			LocalTime time = data.getTime(column_index);
			if (time != null) {
				r.w.writeTime(time);
				return true;
			}
			return false;
		}
		if (type == Types.TIMESTAMP) {
			LocalDateTime timestamp = data.getTimestamp(column_index);
			if (timestamp != null) {
				r.w.write(Time.formatter.getDateTime(timestamp));
				return true;
			}
			return false;
		}
		return writeString(data.getString(column_index), v, display_length, type == Types.BINARY, 0, r.w);
	}

	// --------------------------------------------------------------------------

	public static ColumnBase<?>
	_load_(Rows rows) {
		ColumnBase<?> column = null;
		try {
			String type_name = rows.getString("type");
			if (type_name == null)
				column = new Column(rows.getString("name"));
			else {
				Class<?> c = Class.forName(type_name);
				String type_args = rows.getString("type_args");
				String[] tokens;
				if (type_args != null)
					tokens = type_args.split("\t");
				else
					tokens = new String[0];
				for (Constructor<?> constructor : c.getConstructors()) {
					Class<?>[] parameter_types = constructor.getParameterTypes();
					if (tokens.length == parameter_types.length - 1) {
						ArrayList<Object> arguments = new ArrayList<>();
						arguments.add(rows.getString("name"));
						for (int i = 1; i < parameter_types.length; i++)
							if (parameter_types[i] == Integer.class)
								arguments.add(Integer.valueOf(tokens[i - 1]));
							else if (parameter_types[i] == String.class)
								arguments.add(tokens[i - 1]);
						column = (ColumnBase<?>) constructor.newInstance(arguments.toArray());
						break;
					}
				}
			}
			if (column != null)
				DBObject.load(column, rows);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		return column;
	}
}
