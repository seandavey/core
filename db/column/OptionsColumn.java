package db.column;
import java.util.Collection;

import app.Request;
import db.Form;
import db.View;
import ui.Select;
import ui.Select.Type;
import ui.SelectOption;

public class OptionsColumn extends ColumnBase<OptionsColumn> {
	private boolean						m_allow_no_selection;
	private Collection<SelectOption>	m_options;
	protected String[]					m_texts;
	private Type						m_type = Type.SELECT;

	//--------------------------------------------------------------------------

	public
	OptionsColumn(String name, String... texts) {
		super(name);
		if (texts.length > 0)
			m_texts = texts;
	}

	//--------------------------------------------------------------------------

	public
	OptionsColumn(String name, Collection<String> texts) {
		super(name);
		if (texts.size() > 0)
			m_texts = texts.toArray(new String[texts.size()]);
	}

	//--------------------------------------------------------------------------

	public final OptionsColumn
	setAllowNoSelection(boolean allow_no_selection) {
		m_allow_no_selection = allow_no_selection;
		return this;
	}

	//--------------------------------------------------------------------------

	public final ColumnBase<OptionsColumn>
	setOptions(Collection<SelectOption> options) {
		m_options = options;
		return this;
	}

	//--------------------------------------------------------------------------

	public final ColumnBase<OptionsColumn>
	setType(Type type) {
		m_type = type;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		Select select;
		if (m_texts != null)
			select= new Select(m_name, m_texts).setSelectedOption(getValue(v, r), null);
		else
			select = new Select(m_name, m_options).setSelectedOption(null, getValue(v, r));
		select.setType(m_type);
		if (m_is_required) {
			select.setIsRequired(true);
			r.w.setAttribute("title", getDisplayName(false));
		}
		if (m_allow_no_selection)
			select.setAllowNoSelection(true);
		select.write(r.w);
	}
}
