package db;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import app.Request;
import app.Site;
import db.jdbc.JDBCColumn;
import util.Text;
import util.Time;

public class NameValuePairs {
	private final Map<String,String> m_values = new HashMap<>();

	//--------------------------------------------------------------------------

	private void
	appendValue(StringBuilder sb, String name, JDBCColumn jdbc_column, DBConnection db, String from) {
		String value = m_values.get(name);
		if (jdbc_column.isBoolean()) {
			if (value != null && booleanValue(value))
				sb.append("TRUE");
			else
				sb.append("FALSE");
		} else if (value == null || value.length() == 0 || jdbc_column.getPrimaryTable() != null && "0".equals(value)) {
			if (name.equals("_timestamp_"))
				System.out.println("setting _timestamp_ to NULL");
			sb.append("NULL");
		} else if ("tsvector".equals(name))
			sb.append("to_tsvector(" + SQL.string(value) + ")");
		else if (jdbc_column.isDate() || jdbc_column.isTimestamp())
			sb.append('\'').append(value.replaceAll("-", "/")).append('\'');
		else if (jdbc_column.isTime())
			sb.append('\'').append(fixTime(value)).append('\'');
		else if (jdbc_column.isString() || jdbc_column.isOther()) {
			if (value.startsWith("md5('"))
				sb.append(value);
			else
				sb.append(SQL.string(value.trim()));
		} else if (jdbc_column.isBinary())
			sb.append("E'").append(SQL.encodeBinary(value)).append("'");
		else if (jdbc_column.isNumber())
			if (value.startsWith("NEXT")) {
				StringBuilder v = new StringBuilder("(SELECT COALESCE(MAX(").append(name).append("),0)+1 FROM ").append(from);
				if (value.length() > 5)
					v.append(" WHERE ").append(value.substring(5));
				v.append(')');
				sb.append(v.toString());
			} else
				sb.append(Text.deleteAll(Text.deleteAll(value, ','), '$'));
		else
			sb.append(value);
	}

	//--------------------------------------------------------------------------

	private boolean
	booleanValue(String value) {
		return "on".equals(value) || "t".equals(value) || "true".equals(value) || "yes".equals(value);
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	clear() {
		m_values.clear();
		return this;
	}

	//--------------------------------------------------------------------------

	public final boolean
	containsName(String name) {
		return m_values.containsKey(name);
	}

	//--------------------------------------------------------------------------

	private String
	fixTime(String time) {
		StringBuilder sb = new StringBuilder(time);
		String valid_chars = "0123456789:pPaAmM";

		int i = 0;
		while (i < sb.length())
			if (valid_chars.indexOf(sb.charAt(i)) == -1)
				sb.deleteCharAt(i);
			else
				++i;

		if (sb.length() == 0 || -1 != sb.indexOf(":"))
			return sb.toString();

		for (i=0; i<time.length(); i++)
			if (sb.charAt(i) < '0' || sb.charAt(i) > '9')
				break;
		sb.insert(i, ":00");

		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public final boolean
	getBoolean(String name) {
		String value = m_values.get(name);
		return value != null && booleanValue(value);
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	getDate(String name) {
		String value = m_values.get(name);
		if (value == null || value.length() == 0)
			return null;
		return LocalDate.parse(value);
	}

	//--------------------------------------------------------------------------

	public final LocalDateTime
	getDateTime(String name) {
		String value = m_values.get(name);
		if (value == null || value.length() == 0)
			return null;
		if (value.indexOf(':') == -1)
			return LocalDate.parse(value).atStartOfDay();
		return LocalDateTime.parse(value);
	}

	//--------------------------------------------------------------------------

	public final double
	getDouble(String name, int default_value) {
		String value = m_values.get(name);
		if (value == null || value.length() == 0)
			return default_value;
		value = value.replaceAll(Matcher.quoteReplacement("$"), "");
		return Double.parseDouble(value);
	}

	//--------------------------------------------------------------------------

	public final int
	getInt(String name, int default_value) {
		String value = m_values.get(name);
		if (value == null || value.length() == 0)
			return default_value;
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return default_value;
		}
	}

	//--------------------------------------------------------------------------

	public final Set<String>
	getNames() {
		return m_values.keySet();
	}

	//--------------------------------------------------------------------------
	// returns a comma delemited string of quoted names for an SQL insert

	String
	getNamesString(String from, DBConnection db) {
		StringBuilder s = new StringBuilder();

		for (String name : m_values.keySet()) {
			JDBCColumn jdbc_column = db.getColumn(from, name);
			if (jdbc_column == null) {
				Site.site.log("getNamesString: no jdbc column for " + name, true);
				continue;
			}
			if (s.length() > 0)
				s.append(',');
			s.append('"')
				.append(name)
				.append('"');
		}

		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final String
	getString(String name) {
		return m_values.get(name);
	}

	//--------------------------------------------------------------------------
	// returns string for an SQL update

	String
	getUpdateString(String from, DBConnection db) {
		int num_names = 0;
		StringBuilder sb = new StringBuilder();

		for (String name : m_values.keySet()) {
			JDBCColumn jdbc_column = db.getColumn(from, name);
			if (jdbc_column == null)
				continue;
			if (num_names > 0)
				sb.append(',');
			sb.append('"')
				.append(name)
				.append("\"=");
			appendValue(sb, name, jdbc_column, db, from);
			++num_names;
		}

		if (sb.length() == 0)
			Site.site.log("empty update string for " + from + ": " + Text.join(", ", m_values.keySet()), true);
		return sb.toString();
	}

	//--------------------------------------------------------------------------
	// returns string for an SQL insert

	String
	getValuesString(String from, DBConnection db) {
		int num_values = 0;
		StringBuilder values = new StringBuilder();

		for (String name : m_values.keySet()) {
			JDBCColumn jdbc_column = db.getColumn(from, name);
			if (jdbc_column == null) {
				Site.site.log("getValuesString: no jdbc column for " + name, true);
				continue;
			}
			if (num_values > 0)
				values.append(',');
			appendValue(values, name, jdbc_column, db, from);
			++num_values;
		}

		return values.toString();
	}

	//--------------------------------------------------------------------------
	// returns string for an SQL select

	public final String
	getWhereString(String from, DBConnection db) {
		boolean first = true;
		StringBuilder s = new StringBuilder();

		for (String name : m_values.keySet()) {
			JDBCColumn jdbc_column = db.getColumn(from, name);
			if (jdbc_column == null)
				continue;
			if (first)
				first = false;
			else
				s.append(" AND ");
			s.append('"')
				.append(name)
				.append("\"=");
			appendValue(s, name, jdbc_column, db, from);
		}

		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	remove(String name) {
		m_values.remove(name);
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	set(String name, String value) {
		m_values.put(name.toLowerCase(), value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	set(String name, boolean value) {
		m_values.put(name.toLowerCase(), value ? "true" : "false");
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	set(String name, int value) {
		m_values.put(name.toLowerCase(), Integer.toString(value));
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	set(String name, float value) {
		m_values.put(name.toLowerCase(), Float.toString(value));
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	set(String name, double value) {
		m_values.put(name.toLowerCase(), Double.toString(value));
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setDate(String name, LocalDate value) {
		set(name, value.toString());
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setFromParameters(String[] names, Request r) {
		for (String name : names) {
			String value = r.getParameter(name);
			if (value != null && value.length() > 0)
				m_values.put(name.toLowerCase(), value);
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setFromParameters(Request r) {
		Enumeration<String> parameters = r.request.getParameterNames();
		while (parameters.hasMoreElements()) {
			String p = parameters.nextElement();
			if (!p.startsWith("db_"))
				set(p, r.getParameter(p));
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setId(String name, int value) {
		m_values.put(name.toLowerCase(), value < 1 ? null : Integer.toString(value));
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setIfNotNull(String name, String value) {
		if (value != null)
			m_values.put(name.toLowerCase(), value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final NameValuePairs
	setTimestamp(String name) {
		set(name, Time.newDateTime().toString());
		return this;
	}

	//--------------------------------------------------------------------------

	public final int
	size() {
		return m_values.size();
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	toString() {
		StringBuilder s = new StringBuilder();
		int num_values = 0;

		for (String name : m_values.keySet()) {
			if (num_values > 0)
				s.append(',');
			s.append(name)
				.append('=')
				.append(m_values.get(name));
			++num_values;
		}

		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final boolean
	valueChanged(String name, String table, int id, DBConnection db) {
		if (!m_values.containsKey(name))
			return false;
		String new_value = m_values.get(name);
		String old_value = db.lookupString(new Select(name).from(table).whereIdEquals(id));
		JDBCColumn column = db.getColumn(table, name);
		if (column != null && column.isBoolean())
			return booleanValue(new_value) != booleanValue(old_value);
		return !Site.areEqual(new_value, old_value);
	}
}
