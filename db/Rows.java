package db;

import java.io.Closeable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import app.Site;

public class Rows implements Closeable {
	private final DBConnection	m_db;
	private final Select		m_query;
	private int					m_row_count;
	private final ResultSet		m_rs;
	private ResultSetMetaData	m_rsmd;

	// --------------------------------------------------------------------------

	public
	Rows(Select query, DBConnection db) {
		m_query = query;
		m_db = db;
		m_rs = m_db.select(query.toString());
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	close() {
		try {
			m_rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	findColumn(String name) {
		try {
			return m_rs.findColumn(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	getBoolean(String name) {
		try {
			return m_rs.getBoolean(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	getBoolean(int name) {
		try {
			return m_rs.getBoolean(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getColumnCount() {
		try {
			if (m_rsmd == null)
				m_rsmd = m_rs.getMetaData();
			return m_rsmd.getColumnCount();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getColumnIndex(String column) {
		try {
			return m_rs.findColumn(column);
		} catch (SQLException e) {
			return -1;
		}
	}

	// --------------------------------------------------------------------------

	public final String
	getColumnName(int column) {
		try {
			if (m_rsmd == null)
				m_rsmd = m_rs.getMetaData();
			return m_rsmd.getColumnName(column);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNames() {
		return m_db.getColumnNames(m_rs);
	}

	// --------------------------------------------------------------------------

	public final int
	getColumnType(String column) {
		try {
			return m_rs.getMetaData().getColumnType(m_rs.findColumn(column));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getColumnType(int column) {
		try {
			return m_rs.getMetaData().getColumnType(column);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalDate
	getDate(String column) {
		try {
			Date date = m_rs.getDate(column);
			if (date == null)
				return null;
			return date.toLocalDate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalDate
	getDate(int column) {
		try {
			Date date = m_rs.getDate(column);
			if (date == null)
				return null;
			return date.toLocalDate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final DBConnection
	getDB() {
		return m_db;
	}

	// --------------------------------------------------------------------------

	public final double
	getDouble(String column) {
		try {
			return m_rs.getDouble(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final double
	getDouble(int column) {
		try {
			return m_rs.getDouble(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final float
	getFloat(String column) {
		try {
			return m_rs.getFloat(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final float
	getFloat(int column) {
		try {
			return m_rs.getFloat(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getInt(String column) {
		try {
			return m_rs.getInt(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getInt(int column) {
		try {
			return m_rs.getInt(column);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

//	public final long
//	getLong(String column) {
//		try {
//			return m_rs.getLong(column);
//		} catch (SQLException e) {
//			System.out.println(m_query.toString());
//			throw new RuntimeException(e);
//		}
//	}

	// --------------------------------------------------------------------------

//	public final long
//	getLong(int column) {
//		try {
//			return m_rs.getLong(column);
//		} catch (SQLException e) {
//			System.out.println(m_query.toString());
//			throw new RuntimeException(e);
//		}
//	}

	// --------------------------------------------------------------------------

	public final int
	getRow() {
		try {
			return m_rs.getRow();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final int
	getRowCount() {
		return m_row_count;
	}

	// --------------------------------------------------------------------------

	public final String
	getString(int index) {
		if (index <= 0)
			return null;
		try {
			int type = m_rs.getMetaData().getColumnType(index);
			if (type == Types.BOOLEAN)
				return m_rs.getBoolean(index) ? "yes" : "no";
			if (type == Types.DATE) {
				Date date = m_rs.getDate(index);
				if (date == null)
					return null;
				return util.Time.formatter.getDateShort(date.toLocalDate());
			}
			return m_rs.getString(index);
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final String
	getString(String column) {
		try {
			return getString(m_rs.findColumn(column));
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalTime
	getTime(String column) {
		try {
			Time time = m_rs.getTime(column);
			if (time == null)
				return null;
			return time.toLocalTime();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalTime
	getTime(int column) {
		try {
			Time time = m_rs.getTime(column);
			if (time == null)
				return null;
			return time.toLocalTime();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalDateTime
	getTimestamp(String column) {
		try {
			Timestamp timestamp = m_rs.getTimestamp(column);
			if (timestamp != null)
				return timestamp.toLocalDateTime();
			return null;
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final LocalDateTime
	getTimestamp(int column) {
		try {
			Timestamp timestamp = m_rs.getTimestamp(column);
			if (timestamp != null)
				return timestamp.toLocalDateTime();
			return null;
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	hasColumn(String column) {
		try {
			m_rs.findColumn(column);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	isAfterLast() {
		try {
			return m_rs.isAfterLast();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	isBeforeFirst() {
		try {
			return m_rs.isBeforeFirst();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

//	public final boolean
//	isColumnValueNull(String column) {
//		try {
//			m_rs.getString(column);
//			return m_rs.wasNull();
//		} catch (SQLException e) {
//			System.out.println(m_query.toString());
//			throw new RuntimeException(e);
//		}
//	}

	// --------------------------------------------------------------------------

	public final boolean
	isFirst() {
		try {
			return m_rs.isFirst();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	isLast() {
		try {
			return m_rs.isLast();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	next() {
		try {
			boolean next = m_rs.next();
			if (next)
				++m_row_count;
			return next;
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

//	public final List<Object[]>
//	readRowsObjects() {
//		try {
//			ArrayList<Object[]> rows = new ArrayList<>();
//			if (m_rs.isBeforeFirst())
//				m_rs.next();
//			while (!m_rs.isAfterLast())
//				rows.add(DBConnection.readRowObjects(m_rs));
//			return rows;
//		} catch (SQLException e) {
//			System.out.println(m_query.toString());
//			Site.site.log(e);
//		}
//		return null;
//	}

	// --------------------------------------------------------------------------

	public final boolean
	wasNull() {
		try {
			return m_rs.wasNull();
		} catch (SQLException e) {
			System.out.println(m_query.toString());
			Site.site.log(e.toString(), false);
			throw new RuntimeException(e);
		}
	}
}
