package db.feature;

import app.Request;
import db.Options;
import db.View;
import ui.Option;
import ui.Select;
import util.Text;

public class OptionsFilter extends QuickFilter {
	private boolean			m_can_be_null = true;
	private final String	m_column;
	private final Options	m_db_options;
	private final String[]	m_options;
	private final String	m_type;

	//--------------------------------------------------------------------------

	public
	OptionsFilter(String column, String[] options, String type, Location location) {
		super(location);
		m_column = column;
		m_options = options;
		m_db_options = null;
		m_type = type;
	}

	//--------------------------------------------------------------------------

	public
	OptionsFilter(String column, Options options, String type, Location location) {
		super(location);
		m_column = column;
		m_db_options = options;
		m_options = null;
		m_type = type;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return m_column;
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		int id =  nvp.getInt(m_column, 0);
//		return id == 0 ? null : m_column + "='" + id + "'";
//	}

	//--------------------------------------------------------------------------

	public final OptionsFilter
	setCanBeNull(boolean can_be_null) {
		m_can_be_null = can_be_null;
		return this;
	}
	//--------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		Select select = m_options != null ? new Select(null, m_options) : new Select(null, m_db_options.getObjects());
		select.addFirstOption(new Option("All " + Text.pluralize(m_type), "all"));
		if (m_can_be_null)
			select.addFirstOption(new Option("No " + m_type, "null"));
		String filter = v.getState().getFilter();
		if (filter != null) {
			String[] filters = filter.split(" AND ");
			boolean found = false;
			for (String f : filters) {
				int index = f.indexOf('=');
				if (index == -1)
					index = f.indexOf(' ');
				if (index == m_column.length() && f.startsWith(m_column)) {
					if (f.charAt(index) == '=')
						select.setSelectedOption(null, f.substring(index + 2, f.length() - 1));
					else
						select.setSelectedOption(null, "null");
					found = true;
					break;
				}
			}
			if (!found)
				select.setSelectedOption(null, "all");
		}
		r.w.setAttribute("data-filter", getFilter());
		select.setInline(true).write(r.w);
		return true;
	}
}
