package db.feature;

import app.Request;
import db.View;

public class Button extends Feature {
	private final String m_on_click;
	private final String m_text;

	// --------------------------------------------------------------------------

	public
	Button(Location location, String text, String on_click) {
		super(location);
		m_text = text;
		m_on_click = on_click;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		if (location == m_location) {
			r.w.ui.buttonOnClick(m_text, m_on_click);
			return true;
		}
		return false;
	}
}
