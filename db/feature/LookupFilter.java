package db.feature;

import app.Request;
import db.RowsSelect;
import db.Select;
import db.View;
import ui.Option;

public class LookupFilter extends QuickFilter {
	private final String	m_column;
	private final String	m_label;
	private String			m_none_label;
	private final String	m_table;
	private final String	m_where;

	// --------------------------------------------------------------------------

	public
	LookupFilter(String column, String table, String where, String label, Location location) {
		super(location);
		m_column = column;
		m_table = table;
		m_where = where;
		m_label = label;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return m_table + "_id";
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		String value = nvp.getString(m_column);
//		if (value == null || value.length() == 0)
//			return null;
//		return getFilter() + "=" + SQL.string(value);
//	}

	// --------------------------------------------------------------------------

	public LookupFilter
	setNoneLabel(String none_label) {
		m_none_label = none_label;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		String filter = v.getState().getFilter();
		if (filter != null)
			if (filter.indexOf(m_table + "_id IS NULL") != -1)
				filter = "null";
			else {
				int start = filter.indexOf(m_table + "_id='");
				if (start != -1) {
					start += + m_table.length() + 5;
					int end = filter.indexOf("'", start);
					filter = filter.substring(start, end);
				} else
					filter = null;
			}
		r.w.setAttribute("data-filter", getFilter());
		Select query = new Select(m_table + ".id," + m_column).distinctOn("lower(" + m_column + ")").from(v.getFrom()).joinOn("groups", m_table + "_id=" + m_table + ".id")
			.where(m_where).andWhere(v.getWhere()).andWhere(v.getState().getBaseFilter()).orderByLower(m_column);
//		Select query = new Select("id," + m_column).from(m_table).where(m_where).andWhere("EXISTS(SELECT 1 FROM " + v.getFrom() + " WHERE " + m_table + "_id=" + m_table + ".id)").orderBy("2");
		ui.Select rs = new RowsSelect(null, query, m_column, "id", r);
		if (rs.size() > 1) {
			if (m_label != null)
				r.w.write(m_label).space();
			rs.addFirstOption(new Option("All", "all"))
				.addFirstOption(new Option(m_none_label == null ? "None" : m_none_label, "null"))
				.addFirstOption(new Option("-", null))
				.setInline(true)
				.setSelectedOption(null, filter == null ? "all" : filter)
				.write(r.w);
			return true;
		}
		return false;
	}
}
