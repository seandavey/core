package db.feature;

import java.time.LocalDate;

import app.Request;
import db.DBConnection;
import db.Select;
import db.View;

public class MonthYearFilter extends QuickFilter {
	private final MonthFilter	m_month_filter;
	private final String		m_table;
	private final YearFilter	m_year_filter;

	// --------------------------------------------------------------------------

	public
	MonthYearFilter(String table, Location location) {
		super(location);
		m_table = table;
		m_month_filter = new MonthFilter(table, location).setUseDataView(true);
		m_year_filter = new YearFilter(table, location);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
		m_month_filter.appendDefaultValue(s, v, r);
		m_year_filter.appendDefaultValue(s, v, r);
	}

	// --------------------------------------------------------------------------

	public final LocalDate
	getDefaultDate(View v, DBConnection db) {
		return db.lookupDate(new Select("max(date)").from(m_table).where(v.getState().getBaseFilter()));
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return null;
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		return null;
//	}

	// --------------------------------------------------------------------------

//	public MonthYearFilter
//	setColumn(String column) {
//		m_month_filter.setColumn(column);
//		m_year_filter.setColumn(column);
//		return this;
//	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		m_month_filter.write(null, v, r);
		r.w.space();
		m_year_filter.write(null, v, r);
		r.w.space().addStyle("cursor:pointer").ui.icon("three-dots");
		return true;
	}
}
