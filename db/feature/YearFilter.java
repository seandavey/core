package db.feature;

import app.Request;
import db.View;
import db.ViewState;
import util.Time;

public class YearFilter extends QuickFilter {
	private String			m_column = "date";
	private final String	m_table;

	// --------------------------------------------------------------------------

	public
	YearFilter(String table, Location location) {
		super(location);
		m_table = table;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
		int latest_year = r.db.lookupInt("extract(year from max(" + m_column + "))", m_table, v.getState().getBaseFilter(), Time.newDate().getYear());
		if (latest_year != 0) {
			if (s.length() > 0)
				s.append(" AND ");
			s.append(getFilter()).append("='").append(latest_year).append("'");
		}
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return "extract(year from " + m_column + ")";
	}

	// --------------------------------------------------------------------------

	private int
	getYear(String filter) {
		if (filter != null) {
			int start = filter.indexOf("extract(year from " + m_column + ")='");
			if (start != -1) {
				start += 21 + m_column.length();
				int end = filter.indexOf('\'', start);
				if (end != -1)
					return Integer.parseInt(filter.substring(start, end));
			}
		}
		return 0;
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		LocalDate date = nvp.getDate(m_column);
//		if (date == null)
//			return null;
//		return getFilter() + "='" + date.getYear() + "'";
//	}

	// --------------------------------------------------------------------------

	public final YearFilter
	setColumn(String column) {
		m_column = column;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		ViewState state = v.getState();
		if (state.getQuicksearch() != null)
			return false;
		int earliest_year = r.db.lookupInt("extract(year from min(" + m_column + "))", m_table, state.getBaseFilter(), 0);
		if (earliest_year == 0)
			return false;
		int latest_year = r.db.lookupInt("extract(year from max(" + m_column + "))", m_table, state.getBaseFilter(), 0);
//		String filter = state.getFilter();
//		if (filter == null)
//			filter = "extract(year from " + m_column + ")='" + latest_year + "'";
//		else {
//			int start = filter.indexOf("extract(year from " + m_column + ")='");
//			if (start != -1) {
//				start += 21 + m_column.length();
//				int end = filter.indexOf('\'', start);
//				filter = filter.substring(0, start) + latest_year + filter.substring(end);
//			} else
//				filter += " AND extract(year from " + m_column + ")='" + latest_year + "'";
//		}
//		state.setFilter(filter);
		r.w.setAttribute("data-filter", getFilter())
			.setAttribute("data-type", "year")
			.ui.yearSelect(earliest_year, latest_year, getYear(state.getFilter()), true);
		return true;
	}
}
