package db.feature;

import java.time.LocalDate;

import app.Person;
import app.Request;
import db.Select;
import db.View;
import util.Time;

public class MonthFilter extends QuickFilter {
	private String			m_column = "date";
	private final String	m_table;
	private boolean			m_use_data_view;

	// --------------------------------------------------------------------------

	public
	MonthFilter(String table, Location location) {
		super(location);
		m_table = table;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
		if (!isEnabled(v, r))
			return;
		LocalDate max = r.db.lookupDate(new Select("max(" + m_column + ")").from(m_table).where(v.getState().getBaseFilter()));
		if (max != null) {
			if (s.length() > 0)
				s.append(" AND ");
			s.append(getFilter()).append("='").append(max.getMonthValue()).append("'");
		}
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return "extract(month from " + m_column + ")";
	}

	// --------------------------------------------------------------------------

	private boolean
	isEnabled(View v, Request r) {
		if (!m_use_data_view)
			return true;
		Person user = r.getUser();
		if (user == null)
			return true;
		String view = user.getDataString("qf view " + v.getViewDef().getName());
		return view == null || "month".equals(view);
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		return getFilter() + "='" + nvp.getDate(m_column).getMonthValue() + "'";
//	}

	// --------------------------------------------------------------------------

	public final MonthFilter
	setColumn(String column) {
		m_column = column;
		return this;
	}

	// --------------------------------------------------------------------------

	public final MonthFilter
	setUseDataView(boolean use_data_view) {
		m_use_data_view = use_data_view;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		if (!isEnabled(v, r))
			return false;
		if (v.getState().getQuicksearch() != null)
			return false;
		int month = Time.newDate().getMonthValue();
		String filter = v.getState().getFilter();
		if (filter != null) {
			int start = filter.indexOf("extract(month from " + m_column + ")='");
			if (start != -1) {
				start += 22 + m_column.length();
				int end = filter.indexOf("'", start);
				month = Integer.parseInt(filter.substring(start, end));
			}
		}
		r.w.addStyle("cursor:pointer").ui.icon("arrow-left")
			.setAttribute("data-filter", getFilter())
			.setAttribute("data-type", "month")
			.ui.monthSelect(month)
			.addStyle("cursor:pointer").ui.icon("arrow-right");
		return true;
	}
}
