package db.feature;

import app.Request;
import db.View;
import db.ViewDef;

public abstract class Feature {
	public enum Location { LIST_HEAD, TOP_LEFT, TOP_RIGHT }

	protected final Location m_location;

	// --------------------------------------------------------------------------

	protected
	Feature(Location location) {
		m_location = location;
	}

	// --------------------------------------------------------------------------

	public final Location
	getLocation() {
		return m_location;
	}

	// --------------------------------------------------------------------------

	public Feature
	init(ViewDef view_def) {
		return this;
	}

	// --------------------------------------------------------------------------

	public abstract boolean
	write(Location location, View v, Request r);
}
