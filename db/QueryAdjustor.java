package db;

import app.Request;

public interface QueryAdjustor {
	void adjust(Select query, View v, Request r);
}
