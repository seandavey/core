package db;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.eclipsesource.json.JsonObject;

import app.Person;
import app.Request;
import app.Site;
import app.Stringify;
import db.access.AccessPolicy;
import db.column.ColumnBase;
import db.column.FileColumn;
import db.feature.Feature;
import db.feature.Feature.Location;
import db.jdbc.JDBCColumn;
import db.section.Section;
import jakarta.servlet.http.Part;
import util.Text;
import web.JSONBuilder;
import web.URLBuilder;

@Stringify
public class ViewDef implements Cloneable {
	private AccessPolicy				m_access_policy;
	private Map<String, Action>			m_actions;
	private String						m_add_button_text	= Text.add;
//	private boolean						m_add_before_one_exists;
	String								m_after_form;
	String								m_after_list;
	private boolean						m_allow_quick_edit;
	private boolean						m_allow_sorting		= true;
	private String						m_base_filter;
	private boolean						m_column_heads_can_wrap;
	Map<String, ColumnBase<?>>			m_columns			= new HashMap<>();
	private String[]					m_column_names_all;
	private String[]					m_column_names_form;
	private String[]					m_column_names_form_table;
	private String[]					m_column_names_table;
	String								m_default_order_by;
	private String						m_delete_button_text;
	private List<DeleteHook>			m_delete_hooks;
	private String						m_delete_text;
	private String						m_edit_button_text	= Text.edit;
	private List<Feature>				m_features;
	protected String[]					m_filters;
	ArrayList<FormHook>					m_form_hooks;
	protected String					m_from;
	private List<InsertHook>			m_insert_hooks;
	protected String					m_name;
	private String						m_on_success;
	private Location					m_print_button_location;
	private String						m_print_function;
	protected String					m_record_name;
	private List<RelationshipDef<?>>	m_relationship_defs;
	private Reorderable					m_reorderable;
	private boolean						m_replace_on_quick_edit;
	private int							m_row_window_size;						// = 200;
	private List<Section>				m_sections;
	private Select						m_select;
	private String						m_select_columns	= "*";
	private String						m_select_from;
	private boolean						m_show_add_button_on_one_list;
//	@DBField
//	private boolean						m_show_column_heads	= true;
	private boolean						m_show_done_link;
	private boolean						m_show_grand_totals	= true;
	private boolean						m_show_head			= true;
	private boolean						m_show_num_records	= true;
	private boolean						m_show_print_button_form;
	private boolean						m_show_table_column_picker;
	private boolean						m_timestamp_records;
	private String[]					m_tsvector_columns;
	private List<UpdateHook>			m_update_hooks;
	private List<ValidateHook>			m_validate_hooks;
	private String						m_view_link_column;
	private Class<? extends View>		m_view_class		= View.class;

	// --------------------------------------------------------------------------

	public
	ViewDef(String name) {
		m_name = name;
		m_from = name;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addAction(Action action) {
		if (m_actions == null)
			m_actions = new TreeMap<>();
		m_actions.put(action.getName(), action);
		return this;
	}

	// --------------------------------------------------------------------------

//	public final boolean
//	addBeforeOneExists() {
//		return m_add_before_one_exists;
//	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addDeleteHook(DeleteHook delete_hook) {
		if (m_delete_hooks == null)
			m_delete_hooks = new ArrayList<>();
		m_delete_hooks.add(delete_hook);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addFeature(Feature feature) {
		if (m_features == null)
			m_features = new ArrayList<>();
		m_features.add(feature);
		feature.init(this);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addFormHook(FormHook form_hook) {
		if (m_form_hooks == null)
			m_form_hooks = new ArrayList<>();
		m_form_hooks.add(form_hook);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addInsertHook(InsertHook insert_hook) {
		if (m_insert_hooks == null)
			m_insert_hooks = new ArrayList<>();
		m_insert_hooks.add(insert_hook);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addRelationshipDef(RelationshipDef<?> relationship_def) {
		if (m_relationship_defs == null)
			m_relationship_defs = new ArrayList<>();
		m_relationship_defs.add(relationship_def);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addSection(Section section) {
		if (m_sections == null)
			m_sections = new ArrayList<>();
		m_sections.add(section);
		return this;
	}

	// --------------------------------------------------------------------------

	public void
	addToViewJSON(JSONBuilder json) {
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addUpdateHook(UpdateHook update_hook) {
		if (m_update_hooks == null)
			m_update_hooks = new ArrayList<>();
		m_update_hooks.add(update_hook);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	addValidateHook(ValidateHook validate_hook) {
		if (m_validate_hooks == null)
			m_validate_hooks = new ArrayList<>();
		m_validate_hooks.add(validate_hook);
		return this;
	}

	// --------------------------------------------------------------------------

	protected void
	afterDelete(String where, Request r) {
		for (ColumnBase<?> c : m_columns.values())
			c.afterDelete(this, where, r);
		if (m_delete_hooks != null)
			for (DeleteHook delete_hook : m_delete_hooks)
				delete_hook.afterDelete(where, r);
	}

	// --------------------------------------------------------------------------

	protected void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		if (m_insert_hooks != null)
			for (InsertHook insert_hook : m_insert_hooks)
				insert_hook.afterInsert(id, nvp, r);
		for (String name : nvp.getNames()) {
			ColumnBase<?> column = getColumn(name);
			if (column != null && column.defaultIsPrevious())
				r.setSessionAttribute("default:" + column.getName(), nvp.getString(name));
		}
	}

	// --------------------------------------------------------------------------

	public void
	afterList(View v, Request r) {
		if (m_after_list != null)
			r.w.write(m_after_list);
	}

	// --------------------------------------------------------------------------

	protected void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		if (m_update_hooks != null)
			for (UpdateHook m_update_hook : m_update_hooks)
				m_update_hook.afterUpdate(id, nvp, previous_values, r);
	}

	// --------------------------------------------------------------------------

	public final boolean
	allowQuickEdit() {
		return m_allow_quick_edit;
	}

	// --------------------------------------------------------------------------

	final boolean
	allowSorting() {
		return m_allow_sorting;
	}

	//--------------------------------------------------------------------------

	public void
	appendTokens(int id, NameValuePairs nvp, StringBuilder tsvector, Request r) {
		for (String column_name : m_tsvector_columns) {
			String text = nvp.getString(column_name);
			if (text != null && text.length() > 0)
				tsvector.append(text).append("\n");
		}
		for (FileColumn column : getFileColumns(r))
			column.appendTokens(nvp, tsvector);
	}

	// --------------------------------------------------------------------------

	protected String
	beforeDelete(String where, Request r) {
		for (ColumnBase<?> c : m_columns.values())
			c.beforeDelete(this, where, r);
		if (m_delete_hooks != null)
			for (DeleteHook m_delete_hook : m_delete_hooks) {
				String error = m_delete_hook.beforeDelete(where, r);
				if (error != null)
					return error;
			}
		return null;
	}

	// --------------------------------------------------------------------------

	protected String
	beforeInsert(NameValuePairs nvp, Request r) {
		if (m_insert_hooks != null)
			for (InsertHook m_insert_hook : m_insert_hooks) {
				String error = m_insert_hook.beforeInsert(nvp, r);
				if (error != null)
					return error;
			}
		return null;
	}

	// --------------------------------------------------------------------------

	protected void
	beforeList(View v, Request r) {
	}

	// --------------------------------------------------------------------------

	protected String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		if (m_update_hooks != null)
			for (UpdateHook m_update_hook : m_update_hooks) {
				String error = m_update_hook.beforeUpdate(id, nvp, previous_values, r);
				if (error != null)
					return error;
			}
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public Object
	clone() throws CloneNotSupportedException {
		return super.clone();
	}

	// --------------------------------------------------------------------------

	public final ColumnBase<?>
	cloneColumn(String name) {
		try {
			return (ColumnBase<?>) m_columns.get(name).clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final boolean
	columnHeadsCanWrap() {
		return m_column_heads_can_wrap;
	}

	// --------------------------------------------------------------------------

	public final String
	delete(String table, int id, String column, int id2, String column2, Request r) {
		if (m_access_policy != null && !r.userIsAdmin() && !m_access_policy.canDeleteRow(m_from, id, r)) {
			r.release();
			Person user = r.getUser();
			return user == null ? "user is null when deleting " + m_from + " id " + id : user.getName() + " doesn't have delete rights for " + m_from + " id " + id + ", delete failed";
		}

		String where = column == null ? "id=" + id : column + "=" + id + " AND " + column2 + "=" + id2;
		String error = beforeDelete(where, r);
		if (error != null)
			return error;

		if (r.db.hasColumn(table, "_order_")) {
			String w = "_order_ > " + r.db.lookupString("_order_", m_from, where);
			if (m_sections != null)
				w += w + " AND " + Section.getWhere(m_sections, m_from, where, r.db);
			r.db.update(table, "_order_=_order_-1", w);
		}

		String s = m_name + " - " + table + " - " + where;
		Person user = r.getUser();
		if (user != null) {
			String name = user.getName();
			if (name != null)
				s = name + " - " + s;
		}
		Site.site.log(s, "delete.log");

		r.db.delete(table, where, true);
		afterDelete(where, r);
		return null;
	}

	// --------------------------------------------------------------------------

	public final
	AccessPolicy getAccessPolicy() {
		return m_access_policy;
	}

	// --------------------------------------------------------------------------

	final Map<String, Action>
	getActions() {
		return m_actions;
	}

	// --------------------------------------------------------------------------

	public String
	getAddButtonOnClick(String url) {
		return "_.table.dialog_add(this,'" + m_name + "','" + url + "')";
	}

	// --------------------------------------------------------------------------

	public String
	getAddButtonOnClick(Relationship r, boolean set_owner) {
		return r.getAddButtonOnClick(set_owner);
	}

	// --------------------------------------------------------------------------

	public final String
	getAddButtonText() {
		return m_add_button_text == null ? Text.add : m_add_button_text;
	}

	// --------------------------------------------------------------------------

	public final String
	getBaseFilter() {
		return m_base_filter;
	}

	// --------------------------------------------------------------------------

	public final ColumnBase<?>
	getColumn(String name) {
		return m_columns.get(name);
	}

	// --------------------------------------------------------------------------

	final String[]
	getColumnNamesAll() {
		return m_column_names_all;
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNamesForm() {
		return m_column_names_form;
	}

	// --------------------------------------------------------------------------

	final String[]
	getColumnNamesFormTable() {
		if (m_column_names_form_table != null)
			return m_column_names_form_table;
		return m_column_names_table;
	}

	// --------------------------------------------------------------------------

	public final String[]
	getColumnNamesTable() {
		return m_column_names_table;
	}

	// --------------------------------------------------------------------------

	public final String
	getDefaultOrderBy() {
		return m_default_order_by;
	}

	// --------------------------------------------------------------------------

	final String
	getDeleteButtonText() {
		return m_delete_button_text;
	}

	// --------------------------------------------------------------------------

	public final String
	getDeleteText() {
		return m_delete_text;
	}

	// --------------------------------------------------------------------------

	public String
	getEditButtonText(View v, Request r) {
		return m_edit_button_text == null ? "edit" : m_edit_button_text;
	}

	// --------------------------------------------------------------------------

	final List<Feature>
	getFeatures() {
		return m_features;
	}

	// --------------------------------------------------------------------------

	final List<FileColumn>
	getFileColumns(Request r) {
		ArrayList<FileColumn> c = new ArrayList<>();
		for (ColumnBase<?> column : m_columns.values())
			if (column instanceof FileColumn && !column.isHidden(r))
				c.add((FileColumn)column);
		return c;
	}

	// --------------------------------------------------------------------------

	public final String[]
	getFilters() {
		return m_filters;
	}

	// --------------------------------------------------------------------------

	protected String
	getFormDeleteOnClick(Rows data, Relationship relationship, boolean dialog, Request r) {
		StringBuilder s = new StringBuilder("_.db.delete(this,'");
		if (relationship != null && relationship.def instanceof ManyToMany)
			s.append (m_delete_button_text != null ? m_delete_button_text : "Remove");
		else
			s.append(m_delete_button_text != null ? m_delete_button_text : "Delete");
		s.append("','").append(getName()).append("','");
		if (relationship != null && relationship.def instanceof ManyToMany)
			s.append("table=").append(((ManyToMany)relationship.def).getManyManyTable()).append("&id=").append(relationship.one.data.getInt("id")).append("&column=").append(relationship.one.getFrom()).append("_id&id2=").append(data.getInt("id")).append("&column2=").append(m_from).append("_id");
		else if (data.getColumnIndex("id") == -1) { // many many table, probably in admin
			String[] columns = r.db.getJDBCTable(getFrom()).getColumnNames();
			s.append("id=").append(data.getInt(columns[0])).append("&column=").append(columns[0]).append("&id2=").append(data.getInt(columns[1])).append("&column2=").append(columns[1]);
		} else
			s.append("id=").append(data.getInt("id"));
		return s.append("',").append(dialog ? "true" : "false").append(")").toString();
	}

	// --------------------------------------------------------------------------

	public final String
	getFrom() {
		return m_from;
	}

	// --------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	// --------------------------------------------------------------------------

	public final String
	getOnSuccess(Request r) {
		return m_on_success;
	}

	// --------------------------------------------------------------------------

	public final Location
	getPrintButtonLocation() {
		return m_print_button_location;
	}

	// --------------------------------------------------------------------------

	final String
	getPrintFunction() {
		return m_print_function;
	}

	// --------------------------------------------------------------------------

	public final String
	getRecordName() {
		if (m_record_name != null)
			return m_record_name;
		return Text.singularize(m_from);
	}

	// --------------------------------------------------------------------------

	public final String
	getRecordNamePlural() {
		if (m_record_name != null)
			return Text.pluralize(m_record_name);
		return m_from;
	}

	// --------------------------------------------------------------------------

	final List<RelationshipDef<?>>
	getRelationshipDefs() {
		return m_relationship_defs;
	}

	// --------------------------------------------------------------------------

	public Reorderable
	getReorderable(Request r) {
		return m_reorderable;
	}

	// --------------------------------------------------------------------------

	public final int
	getRowWindowSize() {
		return m_row_window_size;
	}

	// --------------------------------------------------------------------------

	public final List<Section>
	getSections() {
		return m_sections;
	}

	// --------------------------------------------------------------------------

	final Select
	getSelect() {
		return m_select == null ? null : (Select) m_select.clone();
	}

	// --------------------------------------------------------------------------

	final String
	getSelectColumns() {
		return m_select_columns;
	}

	// --------------------------------------------------------------------------

	final String
	getSelectFrom() {
		if (m_select_from != null)
			return m_select_from;
		return m_from;
	}

	// --------------------------------------------------------------------------

	final URLBuilder
	getURL(Request r) {
		return new URLBuilder(Site.context).append("/Views/").append(m_name);
	}

	// --------------------------------------------------------------------------

	final String
	getViewLinkColumn() {
		return m_view_link_column;
	}

	// --------------------------------------------------------------------------

	public final Result
	insert(Request r) {
		String error = validate(0, r);
		if (error != null)
			return new Result(error);

		NameValuePairs nvp = new NameValuePairs();
		Result result = null;

		// check for nvps indicating multiple records to be input
		if (r.getParameter("db_num_rows") != null) {
			String[] columns = r.getParameter("db_columns").split(",");
			int num_rows = r.getInt("db_num_rows", 0);
			String[] row_columns = r.getParameter("db_row_columns").split(",");

			int many_column = -1;
			for (int i = 0; i < num_rows; i++) {
				nvp.clear();
				if (many_column != -1)
					nvp.set(r.getParameter("db_many_column"), many_column);
				nvp.setFromParameters(columns, r);

				for (String row_column : row_columns)
					nvp.set(row_column, r.getParameter("r" + i + row_column));
				insert(nvp, r);
			}
		} else {
			setValues(nvp, 0, r);
			// handle file uploads
			if (r.request.getContentType().startsWith("multipart/form-data")) {
				List<FileColumn> file_columns = getFileColumns(r);
				for (FileColumn file_column : file_columns)
					for (Part p : r.getParts())
						if (p.getName().equals(file_column.getName())) {
							String filename = p.getSubmittedFileName();
							if (filename != null && filename.endsWith(".zip") && r.getBoolean("db_" + file_column.getName() + "_unzip"))
								try {
									ZipEntry entry;
									ZipInputStream zis = new ZipInputStream(p.getInputStream());
									while ((entry = zis.getNextEntry()) != null) {
										result = insert(nvp, r);
										if (result.error != null)
											return result;
										file_column.handleFile(null, zis, entry.getName(), this, true, result.id, r);
									}
									zis.close();
								} catch (IOException e) {
								}
							else {
								// two situations: 1) a single multiple file input so each should get a record inserted, 2) multiple file inputs, all for same record
								if (result == null || file_columns.size() == 1) {
									result = insert(nvp, r);
									if (result.error != null)
										return result;
								}
								file_column.handleFile(p, null, null, this, true, result.id, r);
							}
						}
			}
			if (result == null) {
				result = insert(nvp, r);
				if (result.error != null)
					return result;
			}
			ViewState state = (ViewState) r.getState(ViewState.class, m_name);
			if (state != null && result.id != 0) // state is null for many many tables
				state.setKeyValue(result.id);
			setTSVector(nvp, result.id, r);
		}
		JsonObject o = r.response.publish(m_name, "insert");
		if (result != null) {
			o.add("data", result.id);
			r.response.add("id", result.id);
		}
		r.response.js(getOnSuccess(r));
		return null;
	}

	// --------------------------------------------------------------------------

	private Result
	insert(NameValuePairs nvp, Request r) {
		String error = beforeInsert(nvp, r);
		if (error != null)
			return new Result(error);

		if (r.db.hasColumn(m_from, "_owner_") && !nvp.containsName("_owner_")) {
			Person user = r.getUser();
			if (user != null) {
				int user_id = user.getId();
				if (user_id != -1)
					nvp.set("_owner_", user_id);
			}
		}
		if (r.db.hasColumn(m_from, "_order_"))
			nvp.set("_order_", m_sections == null ? "NEXT" : "NEXT:" + Section.getWhere(m_sections, nvp));

		Result result = r.db.insert(m_from, nvp);
		if (result.error != null) {
			if (result.error.endsWith(" is required"))
				result.error = ColumnBase.getDisplayName(result.error.substring(0, result.error.indexOf(' ')), false);
			return result;
		}
//		if (m_features != null) {
//			StringBuilder filter = new StringBuilder();
//			for (Feature feature : m_features)
//				if (feature instanceof QuickFilter) {
//					String f = ((QuickFilter)feature).set(nvp);
//					if (f != null) {
//						if (filter.length() != 0)
//							filter.append(" AND ");
//						filter.append(f);
//					}
//				}
//			if (filter.length() != 0)
//				((ViewState) r.getOrCreateState(ViewState.class, m_name)).setFilter(filter.toString());
//		}

		afterInsert(result.id, nvp, r);

		return result;
	}

	// --------------------------------------------------------------------------

	public Form
	newForm(int id, View v, Request r) {
		return new Form(id, v, r);
	}

	// --------------------------------------------------------------------------

	public View
	newView(Request r) {
		try {
			return m_view_class.getConstructor(ViewDef.class, Request.class).newInstance(this, r);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	removeSection(Class<? extends Section> section_class) {
		if (m_sections != null) {
			for (int i=0; i<m_sections.size(); i++)
				if (m_sections.get(i).getClass() == section_class) {
					m_sections.remove(i);
					break;
				}
			if (m_sections.size() == 0)
				m_sections = null;
		}
		return this;
	}

	// --------------------------------------------------------------------------

	public final boolean
	replaceOnQuickEdit() {
		return m_replace_on_quick_edit;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAccessPolicy(AccessPolicy access_policy) {
		m_access_policy = access_policy;
		return this;
	}

	// --------------------------------------------------------------------------

//	public final ViewDef
//	setAddBeforeOneExists(boolean add_before_one_exists) {
//		m_add_before_one_exists = add_before_one_exists;
//		return this;
//	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAddButtonText(String add_button_text) {
		m_add_button_text = add_button_text;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAfterForm(String after_form) {
		m_after_form = after_form;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAfterList(String after_list) {
		m_after_list = after_list;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAllowQuickEdit(boolean allow_quick_edit) {
		m_allow_quick_edit = allow_quick_edit;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setAllowSorting(boolean allow_sorting) {
		m_allow_sorting = allow_sorting;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setBaseFilter(String base_filter) {
		m_base_filter = base_filter;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumn(ColumnBase<?> column) {
		m_columns.put(column.getName(), column);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnHeadsCanWrap(boolean column_heads_can_wrap) {
		m_column_heads_can_wrap = column_heads_can_wrap;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesForm(String... column_names_form) {
		return setColumnNamesForm(new ArrayList<>(Arrays.asList(column_names_form)));
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesForm(ArrayList<String> column_names_form) {
		Mods.modColumnNames(m_name, column_names_form, "form");
		m_column_names_form = column_names_form.toArray(new String[column_names_form.size()]);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesFormTable(String... column_names_form_table) {
		m_column_names_form_table = column_names_form_table;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesTable(String... column_names_table) {
		return setColumnNamesTable(new ArrayList<>(Arrays.asList(column_names_table)));
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesTable(ArrayList<String> column_names_table) {
		Mods.modColumnNames(m_name, column_names_table, "table");
		m_column_names_table = column_names_table.toArray(new String[column_names_table.size()]);
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setColumnNamesTableAndForm(String... column_names) {
		setColumnNamesTable(column_names);
		m_column_names_form = column_names;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setDefaultOrderBy(String default_order_by) {
		m_default_order_by = default_order_by;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setDeleteButtonText(String delete_button_text) {
		m_delete_button_text = delete_button_text;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setDeleteText(String delete_text) {
		m_delete_text = delete_text;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setFilters(String[] filters) {
		m_filters = filters;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setFrom(String from) {
		m_from = from;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setName(String name) {
		m_name = name;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setOnSuccess(String on_success) {
		m_on_success = on_success;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setPrintButtonLocation(Location print_button_location) {
		m_print_button_location = print_button_location;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setPrintFunction(String print_function) {
		m_print_function = print_function;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setRecordName(String record_name, boolean capitalize) {
		if (!capitalize || record_name.charAt(0) == '_') {
			m_record_name = record_name;
			return this;
		}
		String name = record_name.replace('_', ' ');
		String[] words = name.split(" ");
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < words.length; i++)
			if (words[i].length() > 0) {
				if (i > 0)
					s.append(' ');
				s.append(words[i].substring(0, 1).toUpperCase() + words[i].substring(1));
			}
		m_record_name = Text.singularize(s.toString());
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setReorderable(Reorderable reorderable) {
		m_reorderable = reorderable;
		m_default_order_by = "_order_";
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setReplaceOnQuickEdit(boolean replace_on_quick_edit) {
		m_replace_on_quick_edit = replace_on_quick_edit;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setRowWindowSize(int row_window_size) {
		m_row_window_size = row_window_size;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setSelect(Select select) {
		m_select = select;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setSelectColumns(String select_columns) {
		m_select_columns = select_columns;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setSelectFrom(String select_from) {
		m_select_from = select_from;
		return this;
	}

	// --------------------------------------------------------------------------

//	public final ViewDef
//	setShowColumnHeads(boolean show_column_heads) {
//		m_show_column_heads = show_column_heads;
//		return this;
//	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowAddButtonOnOneList(boolean show_add_button_on_one_list) {
		m_show_add_button_on_one_list = show_add_button_on_one_list;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowDoneLink(boolean show_done_link) {
		m_show_done_link = show_done_link;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowGrandTotals(boolean show_grand_totals) {
		m_show_grand_totals = show_grand_totals;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowHead(boolean show_head) {
		m_show_head = show_head;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowNumRecords(boolean show_num_records) {
		m_show_num_records = show_num_records;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowPrintLinkForm(boolean show_print_link_form) {
		m_show_print_button_form = show_print_link_form;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setShowTableColumnPicker(boolean show_table_column_picker, String[] column_names_all) {
		m_show_table_column_picker = show_table_column_picker;
		m_column_names_all = column_names_all;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setTimestampRecords(boolean timestamp_records) {
		m_timestamp_records = timestamp_records;
		return this;
	}

	// --------------------------------------------------------------------------

	private void
	setTSVector(NameValuePairs nvp, int id, Request r) {
		if (m_tsvector_columns == null)
			return;
		StringBuilder tsvector = new StringBuilder();
		appendTokens(id, nvp, tsvector, r);
		r.db.update(m_from, new NameValuePairs().set("tsvector", tsvector.length() > 0 ? tsvector.toString() : null), id);
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setTSVectorColumns(String... tsvector_column) {
		m_tsvector_columns = tsvector_column;
		return this;
	}

	// --------------------------------------------------------------------------

	private void
	setValues(NameValuePairs nvp, int id, Request r) {
		Map<String, ColumnBase<?>> columns = new HashMap<>();
		Enumeration<String> params = r.request.getParameterNames();
		while (params.hasMoreElements()) {
			String name = params.nextElement();
			if (name.startsWith("db_"))
				continue;
			ColumnBase<?> column = getColumn(name);
			if (column != null)
				columns.put(name, column);
			else
				nvp.set(name, r.getParameter(name));
		}
		// handle params with Columns after all others have been added to
		// nvp
		for (String name : columns.keySet())
			columns.get(name).setValue(r.getParameter(name), nvp, this, id, r);
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setViewClass(Class<? extends View> view_class) {
		m_view_class = view_class;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ViewDef
	setViewLinkColumn(String view_link_column) {
		m_view_link_column = view_link_column;
		return this;
	}

	// --------------------------------------------------------------------------

//	public final boolean
//	showColumnHeads() {
//		return m_show_column_heads;
//	}

	// --------------------------------------------------------------------------

	public final boolean
	showAddButtonOnOneList() {
		return m_show_add_button_on_one_list;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showDoneLink() {
		return m_show_done_link;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showGrandTotals() {
		return m_show_grand_totals;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showHead() {
		return m_show_head;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showNumRecords() {
		return m_show_num_records;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showPrintLinkForm() {
		return m_show_print_button_form;
	}

	// --------------------------------------------------------------------------

	public final boolean
	showTableColumnPicker() {
		return m_show_table_column_picker;
	}

	// --------------------------------------------------------------------------

	public final boolean
	timestampRecords() {
		return m_timestamp_records;
	}

	// --------------------------------------------------------------------------

	public final String
	update(Request r) {
		int id = r.getInt("db_key_value", 0);
		if (id == 0)
			return "db_key_value not specified";
		String error = validate(id, r);
		if (error != null)
			return error;
		NameValuePairs nvp = new NameValuePairs();
		setValues(nvp, id, r);
		if (r.request.getContentType().startsWith("multipart/form-data")) {
			List<FileColumn> file_columns = getFileColumns(r);
			for (FileColumn file_column : file_columns) {
				Part part = r.getPart(file_column.getName());
				file_column.handleFile(part, null, null, this, false, id, r);
			}
		}

		error = update(id, nvp, r);
		if (error != null)
			return error;
		r.response.publish(m_name, "update");
		r.response.js(getOnSuccess(r));
		return null;
	}

	// --------------------------------------------------------------------------

	public final String
	update(int id, NameValuePairs nvp, Request r) {
		if (m_access_policy != null && !r.userIsAdmin() && !m_access_policy.canUpdateRow(m_from, id, r))
			return "user does not have edit rights for " + m_from + " id " + id;
		Map<String, Object> previous_values = new HashMap<>();
		String error = beforeUpdate(id, nvp, previous_values, r);
		if (error != null)
			return error;

		nvp.remove("id");
		if (nvp.size() == 0)
			r.log("nvp empty, id:" + id, false);
		else {
			r.db.update(m_from, nvp, id);
			setTSVector(nvp, id, r);
			afterUpdate(id, nvp, previous_values, r);
		}
		return null;
	}

	// --------------------------------------------------------------------------

	protected String
	validate(int id, Request r) {
		if (r.getParameter("db_num_rows") != null) {
			String db_columns = r.getParameter("db_columns");
			if (db_columns != null) {
				String[] columns = db_columns.split(",");
				for (String name : columns) {
					String error = validateColumn(name, r.getParameter(name), 0, r);
					if (error != null)
						return error;
				}
			}
			int num_rows = Integer.parseInt(r.getParameter("db_num_rows"));
			String[] row_columns = r.getParameter("db_row_columns").split(",");
			for (int i = 0; i < num_rows; i++)
				for (String row_column : row_columns) {
					String error = validateColumn(row_column, r.getParameter("r" + i + row_column), 0, r);
					if (error != null)
						return error;
				}
		} else {
			Enumeration<String> params = r.request.getParameterNames();
			while (params.hasMoreElements()) {
				String name = params.nextElement();
				if (name.length() > 0 && !name.startsWith("db_")) {
					if (name.equals("id"))
						continue;
					String error = validateColumn(name, r.getParameter(name), id, r);
					if (error != null)
						return error;
				}
			}
		}
		if (m_validate_hooks != null)
			for (ValidateHook h : m_validate_hooks) {
				String error = h.validate(id, r);
				if (error != null)
					return error;
			}
		return null;
	}

	// --------------------------------------------------------------------------

	private String
	validateColumn(String name, String value, int id, Request r) {
		ColumnBase<?> column = getColumn(name);
		if (column != null)
			return column.validate(this, value, id, r);

		if (value == null || value.length() == 0)
			return null;
		JDBCColumn jdbc_column = r.db.getColumn(m_from, name);
		if (jdbc_column == null)
			return null;
		return Validation.validate(jdbc_column, name, value, r);
	}
}