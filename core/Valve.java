package core;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.valves.ValveBase;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpSession;

public class Valve extends ValveBase {
	private Cookie
	getCookie(String name, Request request) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
			for (Cookie cookie : cookies)
				if (name.equals(cookie.getName()))
					return cookie;
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	invoke(final Request request, final Response response) throws IOException, ServletException {
		Cookie cookie = getCookie("uuid", request);
		if (cookie != null) {
			HttpSession session = request.getSession();
			GenericPrincipal user = (GenericPrincipal)session.getAttribute("up");
			if (user == null) {
				user = newUser(cookie.getValue());
				session.setAttribute("up", user);
			}
			request.setUserPrincipal(user);
		}
		getNext().invoke(request, response);
	}

	// --------------------------------------------------------------------------

	private GenericPrincipal
	newUser(String uuid) {
		GenericPrincipal user = null;
		try {
			DataSource data_source = (DataSource)((Context)new InitialContext().lookup("java:/comp/env")).lookup("jdbc/db");
			Connection connection = data_source.getConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT id,user_name,password FROM people WHERE uuid='" + uuid + "'");
			if (!rs.next()) {
				st.close();
				connection.close();
				return null;
			}
			int id = rs.getInt(1);
			String user_name = rs.getString(2);
//			String password = rs.getString(3);
			rs.close();
			final List<String> roles = new ArrayList<>();
			rs = st.executeQuery("SELECT role FROM user_roles WHERE people_id=" + id);
			while (rs.next())
				roles.add(rs.getString(1));
			st.close();
			connection.close();
			user = new GenericPrincipal(user_name, roles);
		} catch (NamingException | SQLException e) {
		}
		return user;
	}
}