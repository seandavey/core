package bootstrap5;

import web.HTMLWriter;

class NavList extends Nav<NavList> implements ui.NavList {
	private boolean	m_first = true;
	private boolean m_pills;
	private boolean m_stacked = true;

	//--------------------------------------------------------------------------

	public
	NavList(HTMLWriter w) {
		super(w);
	}

	//--------------------------------------------------------------------------

	@Override
	public NavList
	a(String text, String href) {
		m_first = false;
		return super.a(text, href);
	}

	//--------------------------------------------------------------------------

	@Override
	public NavList
	aOnClick(String text, String on_click) {
		m_first = false;
		return super.aOnClick(text, on_click);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	heading(String section_head) {
		if (m_mark == -1)
			open();
		m_w.write("<li><h4");
		if (m_first)
			m_w.write(" style=\"margin-top:0;\"");
		m_w.write('>').write(section_head).write("</h4></li>");
		m_first = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavList
	open() {
		addClass("nav");
		if (m_stacked)
			addClass("flex-column");
		if (m_pills)
			addClass("nav-pills");
		addStyle("margin", "0");
		return super.open();
	}

	//--------------------------------------------------------------------------

	@Override
	public NavList
	setPills(boolean pills) {
		m_pills = pills;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavList
	setStacked(boolean stacked) {
		m_stacked = stacked;
		return this;
	}
}