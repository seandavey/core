package bootstrap5;

import ui.Dropdown;
import web.HTMLWriter;

public class InputGroup extends TagBase<InputGroup> implements ui.InputGroup {
	public
	InputGroup(HTMLWriter w) {
		super("div", w);
		addClass("input-group");
		addStyle("margin-bottom", 0);
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	dropdownOpen() {
		return m_w.ui.dropdown(null, false).setDropdownMenuRight(true).open();
	}

	//--------------------------------------------------------------------------

	@Override
	public InputGroup
	prepend(String text) {
		prependOpen();
		m_w.write(text);
		prependClose();
		return this;
	}

	//--------------------------------------------------------------------------

	private InputGroup
	prependClose() {
		m_w.tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public InputGroup
	prependIcon(String icon) {
		prependOpen();
		m_w.ui.icon(icon);
		prependClose();
		return this;
	}

	//--------------------------------------------------------------------------

	private InputGroup
	prependOpen() {
		open();
		m_w.addClass("input-group-text").tagOpen("span");
		return this;
	}
}
