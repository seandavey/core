package bootstrap5;

import java.util.ArrayList;
import java.util.List;

import util.Text;
import web.HTMLWriter;
import web.JS;

class Tabs implements ui.Tabs {
	private final String		m_id;
	private final List<String>	m_labels = new ArrayList<>();
	private int					m_num_panes;
	private boolean				m_show_bottom_line = true;
	private boolean				m_show_close;
	private String				m_start_tab;
	private boolean				m_start_with_last;
	private final HTMLWriter	m_w;

	//--------------------------------------------------------------------------

	public
	Tabs(String id, HTMLWriter w) {
		m_id = id == null ? Text.uuid() : id.replace(' ', '-') + "-tabs";
		m_w = w;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	clearLabels() {
		m_labels.clear();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	close() {
		if (m_num_panes > 0)
			m_w.write("</div>\n");
		m_w.write("</div></div>");
		if (m_labels.size() == 0 && m_num_panes > 0) {
			m_w.js("_.$('#" + m_id + "').querySelector('.tab-pane').style.display='block';");
			return;
		}
		StringBuilder options = new StringBuilder();
		if (m_show_close)
			options.append("show_close:true");
		if (m_start_tab != null) {
			if (options.length() > 0)
				options.append(',');
			options.append("start_tab:").append(JS.string(m_start_tab));
		}
		if (m_start_with_last) {
			if (options.length() > 0)
				options.append(',');
			options.append("start_with_last:true");
		}
		StringBuilder js = new StringBuilder("new Tabs(_.$('#")
			.append(m_id)
			.append("'),")
			.append(JS.array(m_labels));
		if (options.length() > 0)
			js.append(",{").append(options.toString()).append('}');
		js.append(")");
		m_w.js(js.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	public List<String>
	getLabels() {
		return m_labels;
	}

	//--------------------------------------------------------------------------

	@Override
	public Tabs
	open() {
		m_w.write("<div id=\"").write(m_id).write("\"");
		if (!m_show_bottom_line)
			m_w.write(" style=\"border-bottom:none\"");
		m_w.write("><div class=\"tab-content\">");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	pane(String label) {
		if (m_num_panes > 0)
			m_w.write("</div>\n");
		m_labels.add(label);
		m_w.write("<div class=\"tab-pane\">");
		m_num_panes++;
	}

	//--------------------------------------------------------------------------

	@Override
	public Tabs
	setShowBottomLine(boolean show_bottom_line) {
		m_show_bottom_line = show_bottom_line;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Tabs
	setShowClose(boolean show_close) {
		m_show_close = show_close;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Tabs
	setStartTab(String start_tab) {
		m_start_tab = start_tab;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Tabs
	setStartWithLast(boolean start_with_last) {
		m_start_with_last = start_with_last;
		return this;
	}
}