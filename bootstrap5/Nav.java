package bootstrap5;

import java.util.Map;
import java.util.Stack;

import web.HTMLWriter;
import web.JS;

class Nav<T> extends TagBase<T> {
	private String					m_active_item;
	protected Stack<ui.Dropdown>	m_dropdowns;
	private String					m_path;
	protected String				m_target;

	//--------------------------------------------------------------------------

	public
	Nav(HTMLWriter w) {
		super("ul", w);
	}

	//--------------------------------------------------------------------------

	public T
	a(String text, String href) {
		return a(text, href, href.startsWith("http"));
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	a(String text, String href, boolean new_tab) {
		if (m_dropdowns != null) {
			m_dropdowns.peek().a(text, href, new_tab);
			return (T)this;
		}
		liOpen();
		m_w.addClass("nav-link").addStyle("white-space:nowrap");
		if (text != null && text.equals(m_active_item))
			m_w.addClass("active");
		if (new_tab)
			m_w.aBlank(text, href);
		else
			m_w.a(text, href);
		m_w.tagClose();
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	aOnClick(String text, String on_click) {
		if (m_dropdowns != null) {
			m_dropdowns.peek().aOnClick(text, on_click);
			return (T)this;
		}
		liOpen();
		m_w.addClass("nav-link")
			.addStyle("white-space:nowrap")
			.setAttribute("data-text", text);
		if (text != null && text.equals(m_active_item))
			m_w.addClass("active");
		m_w.aOnClick(text, "#", on_click)
			.tagClose();
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	divider() {
		if (m_dropdowns != null) {
			m_dropdowns.peek().divider();
			return (T)this;
		}
		if (m_mark == -1)
			open();
		m_w.write("<li class=\"dropdown-divider\"></li>");
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	dropdownClose() {
		m_dropdowns.pop().close();
		if (m_dropdowns.size() == 0)
			m_dropdowns = null;
		m_w.write("</li>");
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	dropdownOpen(String text, boolean right) {
		if (m_mark == -1)
			open();
		m_w.write("<li class=\"nav-item dropdown\">");
		if (m_dropdowns == null)
			m_dropdowns = new Stack<>();
		m_dropdowns.push(m_w.ui.dropdown(text, false).setDropdownMenuRight(right).setInNavBar(true).open());
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public final T
	item(String text) {
		return item(text, text);
	}

	//--------------------------------------------------------------------------

	public final T
	item(String text, String url) {
		if (url == null)
			url = m_path == null ? text : m_path + "/" + text;
		else if (url.startsWith("/"))
			; // do nothing
		else if (url.startsWith("js:"))
			return aOnClick(text, url.substring(3));
		else if (!url.startsWith("http"))
			url = m_path + "/" + url;
		return aOnClick(text, "_.nav.go(this,'#" + m_target + "'," + JS.string(text) + ",'" + url + "')");
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	items(Map<String,Object> items) {
		for (String text : items.keySet()) {
			Object item = items.get(text);
			if (item == null || item instanceof String)
				item(text, (String)item);
			else {
				dropdownOpen(text, false);
				if (item instanceof Map)
					items((Map<String, Object>)item);
				else {
					String[][] a = (String[][])item;
					for (String[] i : a)
						if ("-".equals(i[0]))
							divider();
						else
							item(i[0], i[1]);
				}
				dropdownClose();
			}
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	private int
	liOpen() {
		if (m_mark == -1)
			open();
		m_w.addClass("nav-item");
		return m_w.tagOpen("li");
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setActiveItem(String active_item) {
		m_active_item = active_item;
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setPath(String path) {
		m_path = path;
		return (T)this;
	}
}
