package bootstrap5;

import java.io.Closeable;
import java.util.ArrayList;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import app.Request;
import web.JS;

public class ButtonNav extends TagBase<ButtonNav>  implements Closeable {
	public static class
	Panel {
		public String	text;
		public String	url;
		public String	icon;
		public boolean	hidden;
		public
		Panel(String text, String icon) {
			this(text, icon, null);
		}
		public
		Panel(String text, String icon, String url) {
			this.text = text;
			this.icon = icon;
			this.url = url;
		}
	}
	private boolean				m_one_div;
	private boolean				m_outline;
	private String				m_panel_display;
	private int					m_pane_mark = -1;
	private ArrayList<Panel>	m_panels = new ArrayList<>();
	private Request				m_r;
	private int					m_segment;
	private String				m_toggle_dialog_title;
	private String				m_toggle_url;

	//--------------------------------------------------------------------------

	public
	ButtonNav(String id, int segment, Request r) {
		super("nav", r.w);
		setId(id);
		m_segment = segment;
		m_r = r;
	}

	//--------------------------------------------------------------------------

	public final ButtonNav
	add(Panel panel) {
		if (m_mark == -1)
			open();
		if (m_pane_mark != -1) {
			m_w.tagsCloseTo(m_pane_mark);
			m_w.addStyle("display:none");
		}
		if (!m_one_div) {
			m_w.setId(m_id + "_" + m_panels.size());
			m_pane_mark = m_w.tagOpen("div");
		}
		m_panels.add(panel);
		return this;
	}

	//--------------------------------------------------------------------------

	public final ButtonNav
	allowToggling(String title, String url) {
		m_toggle_dialog_title = title;
		m_toggle_url = url;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	close() {
		if (m_pane_mark != -1)
			m_w.tagsCloseTo(m_pane_mark);
		m_w.write("<script>_.$('#").write(m_id).write("').button_nav=new ButtonNav('#").write(m_id).write("',[");
		for (int i=0; i<m_panels.size(); i++) {
			Panel panel = m_panels.get(i);
			if (i > 0)
				m_w.write(',');
			m_w.write("{");
			if (!m_one_div)
				m_w.write("div:_.$('#")
					.write(m_id)
					.write('_')
					.write(i)
					.write("'),");
			m_w.write("text:")
				.jsString(panel.text);
			if (panel.icon != null)
				m_w.write(",icon:").jsString(panel.icon);
			if (panel.url != null)
				m_w.write(",url:").jsString(panel.url);
			if (panel.hidden)
				m_w.write(",hidden:true");
			if (m_panel_display != null)
				m_w.write(",display:").jsString(m_panel_display);
			m_w.write("}");
		}
		m_w.write("]");
		if (m_one_div || m_outline || m_toggle_dialog_title != null) {
			JsonObject o = Json.object();
			if (m_one_div)
				o.add("one_div", true);
			if (m_outline)
				o.add("outline", true);
			if (m_toggle_dialog_title != null) {
				o.add("title", m_toggle_dialog_title);
				o.add("url", m_toggle_url);
			}
			m_w.comma().write(o.toString());
		}
		m_w.write(")</script>");
		super.close();
		String item = m_r.getPathSegment(m_segment);
		if (item != null)
			m_w.js("_.$('#" + m_id + "').nav.click(" + JS.string(item) + ")");
	}

	//--------------------------------------------------------------------------

	public final ButtonNav
	setOneDiv(boolean one_div) {
		m_one_div = one_div;
		return this;
	}

	//--------------------------------------------------------------------------

	public final ButtonNav
	setOutline(boolean outline) {
		m_outline = outline;
		return this;
	}

	//--------------------------------------------------------------------------

	public final ButtonNav
	setPanelDisplay(String display) {
		m_panel_display = display;
		return this;
	}
}
