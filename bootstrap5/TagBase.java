package bootstrap5;

import web.HTMLWriter;

class TagBase<T> {
	protected StringBuilder 	m_attributes;
	protected StringBuilder 	m_class;
	protected String			m_id;
	protected int				m_mark = -1;
	protected String			m_on_click;
	protected StringBuilder 	m_style;
	protected final String		m_tag;
	protected String			m_text;
	protected final HTMLWriter	m_w;

	//--------------------------------------------------------------------

	public
	TagBase(String tag, HTMLWriter w) {
		m_tag = tag;
		m_w = w;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addAttribute(String name, String value) {
		if (m_attributes == null)
			m_attributes = new StringBuilder();
		m_attributes.append(' ').append(name).append("=\"").append(value.replaceAll("\\\"", "&quot;")).append('"');
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addAttribute(String name, int value) {
		if (m_attributes == null)
			m_attributes = new StringBuilder();
		m_attributes.append(' ').append(name).append("=\"").append(value).append('"');
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addClass(String name) {
		if (m_class == null)
			m_class = new StringBuilder();
		else if (m_class.length() > 1)
			m_class.append(' ');
		m_class.append(name);
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addStyle(String name, String value) {
		if (m_style == null)
			m_style = new StringBuilder();
		m_style.append(name).append(':').append(value).append(';');
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addStyle(String name, int value) {
		if (m_style == null)
			m_style = new StringBuilder();
		m_style.append(name).append(':').append(value);
		if (value != 0)
			m_style.append("px");
		m_style.append(';');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public void
	close() {
		if (m_mark != -1)
			m_w.tagsCloseTo(m_mark);
	}

	//--------------------------------------------------------------------------

	public int
	getMark() {
		return m_mark;
	}

	//--------------------------------------------------------------------------

	public String
	getText() {
		return m_text;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	open() {
		m_w.write('<');
		m_w.write(m_tag);
		if (m_id != null)
			m_w.write(" id=\"").write(m_id).write('"');
		if (m_attributes != null)
			m_w.write(m_attributes.toString());
		if (m_class != null)
			m_w.write(" class=\"").write(m_class.toString()).write('"');
		if (m_style != null)
			m_w.write(" style=\"").write(m_style.toString()).write('"');
		if (m_on_click != null)
			m_w.write(" onclick=\"").write(m_on_click).write('"');
		m_w.write('>');
		m_mark = m_w.tagPush(m_tag);
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setId(String id) {
		m_id = id;
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setOnClick(String on_click) {
		m_on_click = on_click;
		return (T)this;
	}

	//--------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setText(String text) {
		m_text = text;
		return (T)this;
	}

	//--------------------------------------------------------------------

	@Override
	public String
	toString() {
		m_w.captureStart();
		write();
		return m_w.captureEnd();
	}

	//--------------------------------------------------------------------

	public void
	write() {
		open();
		if (m_text != null)
			m_w.write(m_text);
		close();
	}

	//--------------------------------------------------------------------

	public final void
	write(String text) {
		open();
		m_w.write(text);
		close();
	}
}
