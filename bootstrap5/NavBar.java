package bootstrap5;

import app.Request;
import web.HTMLWriter;
import web.JS;

class NavBar extends Nav<NavBar> implements ui.NavBar {
	private String	m_brand;
	private String	m_brand_href;
	private boolean	m_dark;
	private boolean	m_fixed_top;
	private int		m_segment;

	//--------------------------------------------------------------------------

	NavBar(String id, String target, int segment, HTMLWriter w) {
		super(w);
		m_id = id;
		m_target = target;
		m_segment = segment;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	checkClick(Request r) {
		String item = r.getPathSegment(m_segment);
		if (item != null) {
			m_w.js("_.nav.select_by_name('#" + m_id + "'," + JS.string(item) + ")");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	close() {
		super.close();
		m_w.write("</div></div>")
			.tagClose();
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	divider() {
		if (m_dropdowns != null)
			super.divider();
		else
			m_w.write("<li class=\"navbar-divider\"></li>");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	open() {
		m_w.setId(m_id)
			.addClass("navbar navbar-expand-lg navbar-dark")
			.addClass(m_dark ? "bg-dark" : "bg-primary");
		if (m_fixed_top)
			m_w.addClass("fixed-top");
		m_w.setAttribute("data-target", m_target)
			.setAttribute("data-segment", m_segment)
			.tagOpen("nav");
		m_w.write("<div class=\"container-xxl bd-gutter flex-wrap flex-lg-nowrap\">");
		if (m_brand != null)
			if (m_brand_href != null) {
				String href = m_brand_href;
				if (href.charAt(0) == '/')
					href = href.substring(1);
				m_w.addClass("navbar-brand").aOnClick(m_brand, "app.go('" + href + "')");
			} else
				m_w.write("<span class=\"navbar-brand\">").write(m_brand).write("</span>");
		m_w.write("<button type=\"button\" class=\"navbar-toggler\" onclick=\"var d=this.nextElementSibling;d.style.display=window.getComputedStyle(d).display=='none'?'block':'none'\"><span class=\"navbar-toggler-icon\"></span></button>" +
			"<div class=\"collapse navbar-collapse\">");
		return ulOpen(false);
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	formOpen(boolean right) {
		super.close();
		m_w.addClass("navbar-f navbar-" + (right ? "right" : "left"));
		m_mark = m_w.tagOpen("form");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	header(String header) {
		if (m_mark == -1)
			open();
		m_w.write("<li class=\"dropdown-header\">").write(header).write("</li>");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	setBrand(String brand, String href) {
		m_brand = brand;
		m_brand_href = href;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	setDark(boolean dark) {
		m_dark = dark;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	setFixedTop(boolean fixed_top) {
		m_fixed_top = fixed_top;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public final String
	target() {
		return m_target;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	ulOpen(boolean right) {
		super.close();
		m_class = null;
		addClass("navbar-nav");
		if (right)
			addClass("ms-auto");
		return super.open();
	}
}
