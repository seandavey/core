package bootstrap5;

import web.HTMLWriter;

class Card extends TagBase<Card> implements ui.Card {
	private int m_body_mark = -1;
	private int m_header_mark = -1;

	//--------------------------------------------------------------------------

	public
	Card(HTMLWriter w) {
		super("div", w);
		addClass("card mb-3");
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	bodyClose() {
		if (m_body_mark != -1) {
			m_w.tagsCloseTo(m_body_mark);
			m_body_mark = -1;
		}
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	bodyOpen() {
		if (m_mark == -1)
			open();
		else if (m_header_mark != -1)
			headerClose();
		m_body_mark = m_w.addClass("card-body").tagOpen("div");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	footer(String text) {
		bodyClose();
		m_w.addClass("card-footer text-body-secondary").tag("div", text);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	header(String text) {
		headerOpen();
		m_w.write(text);
		headerClose();
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	headerClose() {
		m_w.tagsCloseTo(m_header_mark);
		m_header_mark = -1;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	headerOpen() {
		if (m_mark == -1)
			open();
		m_header_mark = m_w.addClass("card-header").tagOpen("div");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	subtitle(String title) {
		if (m_body_mark == -1)
			bodyOpen();
		m_w.addClass("card-subtitle mb-2 text-body-secondary").h6(title);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	text(String text) {
		if (m_body_mark == -1)
			bodyOpen();
		m_w.addClass("card-text").p(text);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	textOpen() {
		if (m_body_mark == -1)
			bodyOpen();
		m_w.addClass("card-text").tagOpen("p");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Card
	title(String title) {
		if (m_body_mark == -1)
			bodyOpen();
		m_w.addClass("card-title").h5(title);
		return this;
	}
}
