package bootstrap5;

import util.Text;
import web.HTMLWriter;

public class InlineForm implements ui.InlineForm {
	private final String		m_action;
	private int					m_form_group_mark = -1;
	private int					m_form_mark = -1;
	private String				m_method;
	private final HTMLWriter	m_w;

	//--------------------------------------------------------------------------

	public
	InlineForm(String action, HTMLWriter w) {
		m_action = action;
		m_w = w;
	}

	//--------------------------------------------------------------------------

	@Override
	public InlineForm
	close() {
		m_w.tagsCloseTo(m_form_mark);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public InlineForm
	formGroup() {
		if (m_form_mark == -1)
			open();
		else if (m_form_group_mark != -1)
			m_w.tagsCloseTo(m_form_group_mark);
		m_form_group_mark = m_w.tagOpen("div");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public InlineForm
	label(String text) {
		formGroup();
		m_w.write(text);

		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public InlineForm
	open() {
		if (m_action != null)
			m_w.setAttribute("action", m_action);
		if (m_method != null)
			m_w.setAttribute("method", m_method);
		m_w.addClass("row row-cols-lg-auto gx-3 align-items-center");
		m_form_mark = m_w.tagOpen("form");
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public InlineForm
	setMethod(String method) {
		m_method = method;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeSubmit(String text) {
		formGroup();
		m_w.setId(Text.uuid())
			.ui.buttonOnClick(text, "_.form.submit(this,{validate:true})");
	}
}
