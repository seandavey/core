package bootstrap5;

import web.HTMLWriter;

class Td extends TagBase<Td> implements ui.Td {
	public
	Td(HTMLWriter w) {
		super("td", w);
	}

	//--------------------------------------------------------------------------

	@Override
	public Td
	colspan(int colspan) {
		return addAttribute("colspan", colspan);
	}

	//--------------------------------------------------------------------------

	@Override
	public Td
	verticalAlign(String vertical_align) {
		return addStyle("vertical-align", vertical_align);
	}

	//--------------------------------------------------------------------------

	@Override
	public Td
	width(String width) {
		return addStyle("width", width);
	}
}
