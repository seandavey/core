package web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import util.Text;
import util.Time;

public class WriterBase<T> {
	protected final List<String>	m_attribute_names = new ArrayList<>();
	protected final List<String>	m_attribute_values = new ArrayList<>();
	protected final Stack<String>	m_tag_stack = new Stack<>();
	protected Appendable			m_w;
	private Stack<Appendable>		m_writers;

	//--------------------------------------------------------------------------

	public
	WriterBase(Appendable writer) {
		m_w = writer;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addToAttribute(String name, String value) {
		for (int i=0,n=m_attribute_names.size(); i<n; i++)
			if (name.equals(m_attribute_names.get(i))) {
				StringBuilder s = new StringBuilder();
				String v = m_attribute_values.get(i);
				s.append(v);
				if (name.startsWith("on") && !v.endsWith(";"))
					s.append(";");
				s.append(value);
				m_attribute_values.set(i, s.toString());
				return (T)this;
			}
		m_attribute_names.add(name);
		m_attribute_values.add(value);
		return (T)this;
	}

	// --------------------------------------------------------------------------

	public final String
	captureEnd() {
		String s = m_w.toString();
		m_w = m_writers.pop();
		return s;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	captureStart() {
		if (m_writers == null)
			m_writers = new Stack<>();
		m_writers.push(m_w);
		m_w = new StringBuilder();
		return (T)this;
	}

	// --------------------------------------------------------------------------

//	public void
//	close() {
//		if (!m_tag_stack.empty())
//			throw new RuntimeException("tag " + m_tag_stack.peek() + " was not closed");
//		try {
//			if (m_w instanceof Writer)
//				((Writer)m_w).close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	comma() {
		write(',');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public static String
	encode(String s) {
		StringBuilder sb = null;

		if (s != null) {
			String[] encodings = { "&amp;", "&lt;", "&gt;" };

			for (int i=0,j=0,n=s.length(); i<n; i++,j++) {
				int index = "&<>".indexOf(s.charAt(i));
				if (index != -1) {
					if (sb == null)
						sb = new StringBuilder(s);
					sb.replace(j, j + 1, encodings[index]);
					j += encodings[index].length() - 1;
				}
			}
		}

		if (sb == null)
			return s;

		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public Appendable
	getWriter() {
		return m_w;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setAttribute(String name, String value) {
		m_attribute_names.add(name);
		m_attribute_values.add(value);
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setAttribute(String name, int value) {
		m_attribute_names.add(name);
		m_attribute_values.add(Integer.toString(value));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public final T
	setId(String id) {
		return setAttribute("id", id);
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	space() {
		write(' ');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	tag(String tag) {
		write('<');
		write(tag);
		writeAttributes();
		write(" />");
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	tag(String tag, String contents) {
		write('<');
		write(tag);
		writeAttributes();
		write('>');
		if (contents != null)
			write(contents);
		write("</");
		write(tag);
		write('>');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	tagClose() {
		String tag = m_tag_stack.pop();
		write("</");
		write(tag);
		write('>');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public final void
	tagsCloseTo(int mark) {
		if (m_tag_stack.size() <= mark)
			throw new RuntimeException("unable to close tag");
		while (m_tag_stack.size() > mark)
			tagClose();
	}

	//--------------------------------------------------------------------------

	public void
	tagsCloseAll() {
		while (!m_tag_stack.empty())
			tagClose();
	}

	//--------------------------------------------------------------------------

	public final int
	tagOpen(String tag) {
		write('<');
		write(tag);
		writeAttributes();
		write('>');
		m_tag_stack.push(tag);
		return m_tag_stack.size() - 1;
	}

	//--------------------------------------------------------------------------

	public final int
	tagPush(String tag) {
		m_tag_stack.push(tag);
		return m_tag_stack.size() - 1;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(char c) {
		try {
			m_w.append(c);
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(double d) {
		write(Double.toString(d));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(float f) {
		write(Float.toString(f));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(int i) {
		try {
			m_w.append(Integer.toString(i));
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(File file, boolean escape) {
		try {
			BufferedReader r = new BufferedReader(new FileReader(file));
			String l = r.readLine();
			while (l != null) {
				if (escape)
					l = encode(l);
				m_w.append(l);
				m_w.append("\n");
				l = r.readLine();
			}
			r.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(Object o) {
		write(o.toString());
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	write(String s) {
		if (s != null)
			try {
				m_w.append(s);
			} catch (IOException e) {
				System.out.println(e.toString());
			}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	protected final void
	writeAttribute(String name, String value) {
		write(' ');
		write(name);
		if (value != null) {
			write("=\"");
			for (int i=0,n=value.length(); i<n; i++) {
				char c = value.charAt(i);
				if (c == '"')
					write("&#34;");
				else
					write(c);
			}
			write('"');
		}
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	protected final T
	writeAttribute(String name, int value) {
		write(' ');
		write(name);
		write("=\"");
		write(value);
		write('"');
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	protected T
	writeAttributes() {
		if (m_attribute_names != null) {
			for (int i=0,n=m_attribute_names.size(); i<n; i++)
				writeAttribute(m_attribute_names.get(i), m_attribute_values.get(i));
			m_attribute_names.clear();
			m_attribute_values.clear();
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writeCurrency(double d) {
		if (Math.abs(d) < 0.01)
			d = 0;
		write(Text.currency.format(d));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writeCurrency(float f) {
		write(Text.currency.format(f));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writeDate(LocalDate d) {
		if (d != null)
			write(Time.formatter.getDateShort(d));
		return (T)this;
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writeList(Set<String> list, String delimeter) {
		boolean first = true;
		for (String item : list) {
			if (first)
				first = false;
			else
				write(delimeter);
			write(item);
		}
		return (T)this;
	}

	//--------------------------------------------------------------------------

	public final T
	writePercent(double d) {
		return writePercent(d, 0);
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writePercent(double d, int max_digits) {
		NumberFormat nf = NumberFormat.getPercentInstance();
		if (max_digits != 0)
			nf.setMaximumFractionDigits(max_digits);
		if (d < 0 && d > -0.01)
			d = 0;
		write(nf.format(d));
		return (T)this;
	}

	//--------------------------------------------------------------------------

//	public T
//	writePercent(float f) {
//		return writePercent(f, 0);
//	}

	//--------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public T
//	writePercent(float f, int max_digits) {
//		NumberFormat nf = NumberFormat.getPercentInstance();
//		if (max_digits != 0)
//			nf.setMaximumFractionDigits(max_digits);
//		if (f < 0 && f > -0.01)
//			f = 0;
//		write(nf.format(f));
//		return (T)this;
//	}

	//--------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public T
//	writePercent(int i) {
//		NumberFormat nf = NumberFormat.getPercentInstance();
//		write(nf.format(i));
//		return (T)this;
//	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	writeTime(LocalTime t) {
		if (t != null)
			write(Time.formatter.getTime(t, true));
		return (T)this;
	}
}
