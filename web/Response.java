package web;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jakarta.servlet.http.HttpServletResponse;

public class Response {
	public final HttpServletResponse	response;

	private JsonObject					m_json = new JsonObject();

	//--------------------------------------------------------------------------

	public
	Response(HttpServletResponse response) {
		this.response = response;
	}

	//--------------------------------------------------------------------------

	public final Response
	add(String name, JsonValue value) {
		m_json.add(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	add(String name, String value) {
		m_json.add(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	add(String name, boolean value) {
		m_json.add(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	add(String name, int value) {
		m_json.add(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	add(String name, double value) {
		m_json.add(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	addAlert(String title, String body) {
		m_json.add("alert", new JsonObject().add("title", title).add("body", body));
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	addError(String error) {
		JsonValue v = m_json.get("error");
		if (v != null)
			error = v.asString() + "<br>" + error;
		m_json.set("error", error);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Response
	addMessage(String message) {
		JsonValue v = m_json.get("message");
		if (v != null)
			message = v.asString() + "<br>" + message;
		m_json.set("message", message);
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	js(String js) {
		if (js != null)
			m_json.add("js", js);
	}

	//--------------------------------------------------------------------------

	public final void
	ok() {
		if (m_json.get("error") == null)
			add("ok", true);
	}

	//--------------------------------------------------------------------------

	public final JsonObject
	publish(String name, String message) {
		JsonObject object = new JsonObject().add("object", name).add("message", message);
		m_json.add("publish", object);
		return object;
	}

	//--------------------------------------------------------------------------

	public final void
	release(HTMLWriter w) {
		if (m_json != null && !m_json.isEmpty()) {
			w.write(m_json.toString());
			m_json = null;
		}
	}

	//--------------------------------------------------------------------------

	public final void
	setContentCSV(String filename) {
		response.setContentType("text/csv;");
		response.setHeader("Content-disposition", "attachment; filename=\"" + filename + ".csv\"");
	}
}
