package web;

import java.util.HashMap;
import java.util.Map;

public class URLBuilder {
	private final Map<String,String>	m_params = new HashMap<>();
	protected final StringBuilder		m_url = new StringBuilder();

	//--------------------------------------------------------------------------

	public
	URLBuilder(String base_url) {
		if (base_url != null)
			m_url.append(base_url);
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	append(String s) {
		m_url.append(s);
		return this;
	}

	//--------------------------------------------------------------------------

	public static String
	encode(String s) {
		StringBuilder sb = null;

		if (s != null) {
			String[] encodings = { "%20", "%23", "%25", "%26", "%27", "%3D", "%3F", "%2C" };

			for (int i=0,j=0,n=s.length(); i<n; i++,j++) {
				char c = s.charAt(i);
				int index = " #%&'=?,".indexOf(c);

				if (index != -1) {
					if (sb == null)
						sb = new StringBuilder(s);
					sb.replace(j, j + 1, encodings[index]);
					j += 2;
				}
			}
		}

		if (sb == null)
			return s;

		return sb.toString();
	}

	//--------------------------------------------------------------------------

//	public URLBuilder
//	ensureProtocol(HttpServletRequest request) {
//		if (m_url.indexOf("://") == -1)
//			if (m_url.charAt(0) == '/') {
//				String url = request.getRequestURL().toString();
//				m_url.insert(0, url.substring(0, url.indexOf('/', 7)));
//			} else
//				m_url.insert(0, "https://");
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	segment(String s) {
		m_url.append("/").append(encode(s).replaceAll(" ", "+"));
		return this;
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	set(String name, String value) {
		m_params.put(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	set(String name, int value) {
		return set(name, Integer.toString(value));
	}

	//--------------------------------------------------------------------------

	public final <E extends Enum<E>> URLBuilder
	set(String name, Enum<E> e) {
		return set(name, e.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	toString() {
		StringBuilder s = new StringBuilder(m_url);
		if (m_params.size() > 0) {
			char delim = s.indexOf("?") != -1 ? '&' : '?';
			for (String name : m_params.keySet()) {
				s.append(delim).append(name).append('=').append(encode(m_params.get(name)));
				delim = '&';
			}
		}
		return s.toString();
	}
}