package web;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import app.Site;
import bootstrap5.Bootstrap5;
import util.Text;

public class HTMLWriter extends WriterBase<HTMLWriter> {
	private static final String[]	s_hours = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
	private static final String[]	s_meridian = { "AM", "PM" };
	private static final String[]	s_minutes_5 = { "00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55" };
	private static final String[]	s_minutes_15 = { "00", "15", "30", "45" };
	private static final String[]	s_minutes_30 = { "00", "30" };
	private static final String[]	s_minutes_60 = { "00" };

	private List<String>		m_classes;
	private String				m_footer;
	private int					m_id_count;
	private List<String>		m_styles;
	public final Bootstrap5		ui = new Bootstrap5(this);

	//--------------------------------------------------------------------------

	public
	HTMLWriter(Appendable writer) {
		super(writer);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	a(String url) {
		setAttribute("href", url);
		return tag("a", url);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	a(String text, String href) {
		if (href != null)
			setAttribute("href", href);
		return tag("a", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aBlank(String text, String href) {
		setAttribute("href", href);
		setAttribute("target", "_blank");
		setAttribute("rel", "noreferrer");
		return tag("a", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	addClass(String class_name) {
		if (m_classes == null) {
			m_classes = new ArrayList<String>();
			m_classes.add(class_name);
			return this;
		}
		int index = m_classes.indexOf(class_name);
		if (index == -1)
			m_classes.add(class_name);
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	addStyle(String style) {
		if (m_styles == null) {
			m_styles = new ArrayList<String>();
			m_styles.add(style);
			return this;
		}
		int index = m_styles.indexOf(style);
		if (index == -1)
			m_styles.add(style);
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	alert(String type, String text) {
		write("<div class=\"alert alert-" + type + "\" role=\"alert\">").write(encode(text)).write("</div>");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aOnClick(String text, String on_click) {
		setOnClick(on_click);
		return a(text, "#");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aOnClick(String text, String href, String on_click) {
		setOnClick(on_click);
		return a(text, href);
	}

	//--------------------------------------------------------------------------

	public int
	aOnClickOpen(String on_click) {
		setOnClick(on_click);
		return aOpen("#");
	}

	//--------------------------------------------------------------------------

	public int
	aOpen(String href) {
		if (href != null)
			setAttribute("href", href);
		return tagOpen("a");
	}

	//--------------------------------------------------------------------------

	public int
	aShowHideOpen(String show_text, String hide_text, String tag, String id) {
		StringBuilder on_click = new StringBuilder();
		if (hide_text != null) {
			on_click.append("if(this.nextSibling.style.display=='none'){" +
				"this.firstChild.nodeValue='");
			on_click.append(hide_text);
			on_click.append("';");
		}
		on_click.append("this.nextSibling.style.display='';");
		if (hide_text != null) {
			on_click.append("}else{" +
				"this.firstChild.nodeValue='");
			on_click.append(show_text);
			on_click.append("';");
			on_click.append("this.nextSibling.style.display='none';");
			on_click.append("}");
		} else
			on_click.append("this.style.display='none'");
		aOnClick(show_text, on_click.toString());
		if (id != null)
			setId(id);
		addStyle("display:none");
		return tagOpen(tag);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	br() {
		return write("<br />");
	}

	//--------------------------------------------------------------------------

	public static String
	breakNewlines(String s) {
		if (s != null)
			s = s.replaceAll("\n", "<br/>").replaceAll("\r", "");
		return s;
	}

	//--------------------------------------------------------------------------

	public void
	colorInput(String name, String value) {
		colorInput(name, value, null, false);
	}

	//--------------------------------------------------------------------------

	public void
	colorInput(String name, String value, String on_change, boolean required) {
		write("<div style=\"display:flex\">");
		setAttribute("onchange", "this.nextElementSibling.nextElementSibling.style.display=''");
		if (value != null && value.length() == 0)
			value = null;
		hiddenInput(name, value);

		String button_id = Text.uuid();
		setId(button_id);
		addClass("form-control");
		addStyle("flex-grow:1");
		setAttribute("data-jscolor", value != null ? "{value:'" + value + "'}" : "");
		setAttribute("onclick", "this.jscolor.option({hash:true,onChange:function(){if(this.nextElementSibling)this.nextElementSibling.style.display=''" + (on_change != null ? ";" + on_change : "") + "}.bind(this),valueElement:this.previousElementSibling})");
		tag("button", "&nbsp;");

		if (!required) {
			addStyle("margin-left:10px");
			if (value == null)
				addStyle("display:none");
			ui.buttonOnClick("No&nbsp;Color", "this.previousElementSibling.jscolor.fromString('#ffffff');this.previousElementSibling.previousElementSibling.value='';this.style.display='none';");
		}

		js("""
			JS.get('jscolor.min',function(first){
				jscolor.presets.default={
					palette:['#001f3f','#0074D9','#7FDBFF','#39CCCC','#B10DC9','#F012BE','#85144b','#FF4136','#FF851B','#FFDC00','#3D9970','#2ECC40','#01FF70','#111111','#AAAAAA','#DDDDDD','#FFFFFF'],
					paletteCols:9,
					hideOnPaletteClick:true}
				if(first)
					jscolor.init()
				else
					jscolor.install()
			})""");
		write("</div>");
	}

	//--------------------------------------------------------------------------

	public String
	componentOpen(String url) {
		String id = "c_" + Text.uuid();
		setId(id);
		tagOpen("div");
		js("app.component('#" + id + "'," + (url == null ? "null" : JS.string(url)) + ")");
		return id;
	}

    //--------------------------------------------------------------------------

	public HTMLWriter
	dateInput(String name, LocalDate value) {
		return dateInput(name, value, null, false, false);
	}

    //--------------------------------------------------------------------------

	public HTMLWriter
	dateInput(String name, LocalDate value, String on_change, boolean check_not_same, boolean short_display) {
		write("<div>");
		String uuid = Text.uuid();
		setId(uuid);
		addStyle("min-width:220px");
		textInput(name, 0, value == null ? null : value.toString());
		js("_.form.new_date_input('#" + uuid + "'," +
			(on_change != null ? on_change : "null") +
			(check_not_same ? ",true" : ",false") +
			(short_display ? ",true" : ",false") +
		")");
		write("</div>");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	emailInput(String name, String value) {
		setAttribute("type", "email");
		if (name != null)
			setAttribute("name", name);
		if (value != null)
			setAttribute("value", value);
		addClass("form-control");
		tag("input");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	fileInput(String name, String accept, boolean multiple) {
		setAttribute("type", "file");
		if (name != null)
			setAttribute("name", name);
		addClass("form-control");
		addStyle("font-size:inherit;height:inherit");
		if (accept != null)
			setAttribute("accept", accept);
		if (multiple)
			setAttribute("multiple", null);
		tag("input");
		return this;
	}

	//--------------------------------------------------------------------------

	public int
	formOpen(String method, String action) {
		setAttribute("method", method);
		if (action != null)
			setAttribute("action", action);
		return tagOpen("form");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	hiddenInput(String name, String value) {
		write("<input type=\"hidden\" name=\"").write(name).write('"');
		if (value != null)
			writeAttribute("value", value);
		writeAttributes();
		write(" />");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	hiddenInput(String name, int value) {
		write("<input type=\"hidden\" name=\"").write(name).write('"');
		writeAttribute("value", value);
		writeAttributes();
		write(" />");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	hr() {
		return tag("hr");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h1(String text) {
		return tag("h1", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h2(String text) {
		return tag("h2", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h3(String text) {
		return tag("h3", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h4(String text) {
		return tag("h4", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h5(String text) {
		return tag("h5", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	h6(String text) {
		return tag("h6", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	img(String src) {
		write("<img src=\"");
		write(src);
		write("\" alt=\"\" border=\"0\" loading=\"lazy\"");
		writeAttributes();
		write(" />");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	img(String... src) {
		write("<img src=\"");
		write(Site.context);
		for (String s : src)
			write("/").write(s);
		write("\" alt=\"\" border=\"0\" loading=\"lazy\"");
		writeAttributes();
		write(" />");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	js(String code) {
		write("<script>");
		write(code);
		write("</script>");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	jsArray(int[] array) {
		write('[');
		for (int i=0; i<array.length; i++) {
			if (i > 0)
				write(',');
			write(Integer.toString(array[i]));
		}
		write(']');
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	jsArray(Object[] array) {
		write('[');
		for (int i=0; i<array.length; i++) {
			if (i > 0)
				write(',');
			if (array[i].getClass().isArray())
				jsArray((Object[])array[i]);
			else
				jsString((String)array[i]);
		}
		write(']');
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	jsArray(Collection<? extends Object> values) {
		write('[');
		int size = 0;
		for (Object value : values)
			if (value != null) {
				if (size > 0)
					write(',');
				size++;
				jsString(value.toString());
			}
		write(']');
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	jsString(String s) {
		return write('\'').write(JS.escape(s)).write('\'');
	}

	//--------------------------------------------------------------------------

	public int
	mainOpen() {
		addClass("container");
		return tagOpen("main");
	}

	//--------------------------------------------------------------------------

	public static String
	makeLinks(String s) {
		if (s != null)
			//			s = s.replaceAll("&lt;", "<");
//			s = s.replaceAll("&gt;", ">");
			s = s.replaceAll("(?<!(href|src|url|data-oembed-url)=\")(https?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;\\*$]*[-a-zA-Z0-9+&@#/%=~_|])", "<a href=\"$2\" rel=\"noreferrer\" target=\"_blank\">$2</a>");
		return s;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	nbsp() {
		return write("&nbsp;");
	}

	//--------------------------------------------------------------------------

	public int
	newID() {
		return ++m_id_count;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	numberInput(String name, String min, String max, String step, String value, boolean inline) {
		setAttribute("type", "number");
		if (name != null)
			setAttribute("name", name);
		if (min != null)
			setAttribute("min", min);
		if (max != null)
			setAttribute("max", max);
		if (step != null)
			setAttribute("step", step);
		if (value != null)
			setAttribute("value", value);
		setAttribute("autocomplete", "off");
		setAttribute("onwheel", "event.preventDefault()");
		addClass("form-control");
		if (inline)
			addStyle("display:inline;width:inherit");
		tag("input");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	option(String text, String value, boolean selected) {
		write("<option");
		if (selected)
			write(" selected=\"selected\"");
		if (value != null)
			writeAttribute("value", value);
		writeAttributes();
		write('>');
		if (text == null || text.length() == 0)
			space();
		else
			write(text);
		write("</option>");
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	p(String ...contents) {
		tagOpen("p");
		for (String s : contents)
			write(s);
		tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	passwordInput(String name, String value, boolean new_password) {
		setAttribute("autocomplete", new_password ? "new-password" : "current-password");
		setAttribute("name", name);
		setAttribute("required", "required");
		setAttribute("spellcheck", "false");
		setAttribute("type", "password");
		if (value != null)
			setAttribute("value", value);
		addClass("form-control");
		tag("input");
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	script(String url, boolean version, String function) {
		write("<script>JS.get('");
		if (url.indexOf('/') == -1) {
			write("/js/");
			if (version)
				url = Site.site.getResources().get("js", url);
		}
		write(url)
			.write("'");
		if (function != null)
			write(",function(){").write(function).write("}");
		write(")</script>");
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	setFooter(String footer) {
		m_footer = footer;
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	setOnClick(String on_click) {
		if (on_click != null) {
			if (!on_click.startsWith("return "))
				on_click += on_click.charAt(on_click.length() - 1) == ';' ? "return false" : ";return false";
			setAttribute("onclick", on_click);
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	style(String style) {
		tag("style", style);
		return this;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	styleSheet(String style_sheet) {
		return styleSheet(style_sheet, true);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	styleSheet(String style_sheet, boolean version) {
		write("<link rel=\"stylesheet\" href=\"")
			.write(Site.context);
		if (style_sheet.charAt(0) != '/') {
			write("/css/");
			if (version)
				style_sheet = Site.site.getResources().get("css", style_sheet);
		}
		write(style_sheet)
			.write(".css\" type=\"text/css\" />");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	tagsCloseAll() {
		if (m_tag_stack.isEmpty())
			return;
		while(m_tag_stack.size() > 1)
			tagClose();
		if (m_footer != null)
			write(m_footer);
		tagClose();
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	textArea(String name, String value) {
		textAreaOpen(name, null, null);
		if (value != null)
			write(encode(value));
		tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	textAreaExpanding(String name, String style, String value) {
		String id = Text.uuid();
		setId(id);
		addClass("form-control");
		addStyle(style != null ? "overflow:hidden;" + style : "overflow:hidden");
		setAttribute("onkeyup", "_.form.resize_textarea(this)");
		textAreaOpen(name, "1", null);
		if (value != null)
			write(encode(value));
		tagClose();
		js("setTimeout(function(){_.form.resize_textarea(_.$('#" + id + "'));},100);");
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	textAreaOpen(String name, String rows, String cols) {
		if (name != null) {
			setAttribute("name", name);
			setId(name);
		}
		if (rows != null)
			setAttribute("rows", rows);
		if (cols != null)
			setAttribute("cols", cols);
		setAttribute("autocomplete", "off");
		addStyle(rows == null && cols == null ? "font-size:inherit;width:100%;" : "font-size:inherit");
		tagOpen("textarea");
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	textInput(String name, int size, String value) {
		setAttribute("type", "text");
		if (name != null)
			setAttribute("name", name);
		if (size > 0) {
			setAttribute("size", size);
			setAttribute("maxlength", size);
			addStyle("width:inherit");
		}
		if (value != null)
			setAttribute("value", value);
		setAttribute("autocomplete", "off");
		addClass("form-control");
		tag("input");
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	timeInput(String name, LocalTime time, int resolution, boolean check_order, boolean adjust_meridian, boolean inline) {
		String h = null;
		String m = null;
		String ap = null;
		if (time != null) {
			int hour = time.getHour() % 12;
			if (hour == 0)
				hour = 12;
			h = Integer.toString(hour);
			int minutes = time.getMinute();
			m = minutes == 0 ? "00" : minutes == 5 ? "05" : Integer.toString(minutes);
			ap = time.get(ChronoField.AMPM_OF_DAY) == 1 ? "PM" : "AM";
		}
		setAttribute("data-type", "time");
		if (check_order)
			setAttribute("data-check_order", "true");
		hiddenInput(name, time == null ? null : h + ':' + m + ' ' + ap);
		if (inline)
			addStyle("display:inline-block");
		tagOpen("div");
		write("<span style=\"vertical-align:middle;width:auto;\">");
		setId(name + "1");
		ui.select(null, s_hours, h, null, true);
		write(':');
		setId(name + "2");
		ui.select(null, resolution == 60 ? s_minutes_60 : resolution == 30 ? s_minutes_30 : resolution == 15 ? s_minutes_15 : s_minutes_5, m, null, true);
		write(' ');
		setId(name + "3");
		if (adjust_meridian)
			setAttribute("onchange", "_.form.adjust_meridian(this)");
		ui.select(null, s_meridian, ap, null, true);
		write("</span>");
		tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	public static String
	URIEncode(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8")
				.replaceAll("\\+", "%20")
				.replaceAll("\\%21", "!")
                .replaceAll("\\%27", "'")
                .replaceAll("\\%28", "(")
                .replaceAll("\\%29", ")")
                .replaceAll("\\%2F", "-slash-")
                .replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public static String
	URLEncode(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public HTMLWriter
	writeAttributes() {
		if (m_classes != null) {
			writeAttribute("class", Text.join(" ", m_classes));
			m_classes = null;
		}
		if (m_styles != null) {
			writeAttribute("style", Text.join(";", m_styles));
			m_styles = null;
		}
		super.writeAttributes();
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	writeFile(Path path, boolean use_brs) {
		List<String> a = app.Files.read(path);
		for (int i=0; i<a.size(); i++) {
			if (i > 0)
				if (use_brs)
					br();
				else
					write('\n');
			write(WriterBase.encode(a.get(i)));
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final HTMLWriter
	writeWithLinks(String s) {
		if (s != null)
			write(makeLinks(breakNewlines(s)));
		return this;
	}
}