package web;

import java.io.IOException;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class JSON {
	public static Appendable
	append(String s, Appendable a) {
		try {
			a.append('"');
			for (int i=0; i<s.length(); i++) {
				char c = s.charAt(i);
				if (c == '"')
					a.append("\\\"");
				else if (c == '\\')
					a.append("\\\\");
				else if (c == '\b')
					a.append("\\b");
				else if (c == '\f')
					a.append("\\f");
				else if (c == '\n')
					a.append("\\n");
				else if (c == '\r')
					a.append("\\r");
				else if (c == '\t')
					a.append("\\t");
				else if (c < ' ' || c >= '\u0080' && c < '\u00a0' || c >= '\u2000' && c < '\u2100') {
				    String hex = "000" + Integer.toHexString(c);
				    a.append("\\u" + hex.substring(hex.length() - 4));
				} else
					a.append(c);
			}
			a.append('"');
		} catch (IOException e) {
		}
		return a;
	}

	//--------------------------------------------------------------------------

	public static JsonArray
	getArray(JsonObject o, String name) {
		JsonValue v = o.get(name);
		JsonArray a;
		if (v != null)
			a = v.asArray();
		else {
			a = new JsonArray();
			o.add(name, a);
		}
		return a;
	}

	//--------------------------------------------------------------------------

	public static JsonObject
	getObject(JsonObject o, String ...names) {
		for (String name : names) {
			JsonValue v = o.get(name);
			if (v == null || v.isNull()) {
				JsonObject object = Json.object();
				o.add(name, object);
				o = object;
			} else
				o = v.asObject();
		}
		return o;
	}

	//--------------------------------------------------------------------------

	public static String
	string(String s) {
		StringBuilder sb = new StringBuilder();
		append(s, sb);
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String[]
	toArray(JsonArray json_array) {
		String[] a = new String[json_array.size()];
		for (int i=0; i<a.length; i++)
			if (json_array.get(i).isObject()) // tagify
				a[i] = json_array.get(i).asObject().get("value").asString();
			else
				a[i] = json_array.get(i).asString();
		return a;
	}
}
