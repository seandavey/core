package web;

import java.util.Collection;

public class JS {
	public static String
	array(Collection<? extends Object> objects) {
		StringBuilder sb = new StringBuilder("[");
		int size = 0;
		for (Object o : objects)
			if (o != null) {
				if (size > 0)
					sb.append(',');
				size++;
				sb.append(string(o.toString()));
			}
		sb.append("]");
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	array(String[] strings) {
		StringBuilder sb = new StringBuilder("[");
		int size = 0;
		for (String s : strings)
			if (s != null) {
				if (size > 0)
					sb.append(',');
				size++;
				sb.append(string(s.toString()));
			}
		sb.append("]");
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	array(int[] ints) {
		StringBuilder sb = new StringBuilder("[");
		int size = 0;
		for (int i : ints) {
			if (size > 0)
				sb.append(',');
			size++;
			sb.append(i);
		}
		sb.append("]");
		return sb.toString();
	}

	//--------------------------------------------------------------------------
	public static String
	escape(String s) {
		if (s == null)
			return null;

		StringBuilder sb = new StringBuilder(s);
		for (int i=0; i<sb.length(); i++) {
			char c = sb.charAt(i);
			if (c == '\'' || c == '"' || c == '\\') {
				sb.insert(i, '\\');
				i++;
			} else if (c == '\n' || c == '\r') {
				sb.delete(i, i+1);
				i--;
			}
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public static String
	string(String s) {
		return '\'' + escape(s) + '\'';
	}
}
