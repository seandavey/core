package web;

import java.io.IOException;
import java.util.Stack;

public class JSONBuilder {
	private final Stack<Boolean>	m_firsts = new Stack<>();
	private final Appendable		m_appendable;

	//--------------------------------------------------------------------------

	public
	JSONBuilder() {
		m_appendable = new StringBuilder();
	}

	//--------------------------------------------------------------------------

	public
	JSONBuilder(Appendable appendable) {
		m_appendable = appendable;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	array(String[] strings) {
		comma();
		try {
			m_appendable.append('[');
			for (int i=0; i<strings.length; i++) {
				if (i > 0)
					m_appendable.append(',');
				JSON.append(strings[i], m_appendable);
			}
			m_appendable.append(']');
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	array(String name, String[] strings) {
		name(name);
		try {
			m_appendable.append('[');
			for (int i=0; i<strings.length; i++) {
				if (i > 0)
					m_appendable.append(',');
				JSON.append(strings[i], m_appendable);
			}
			m_appendable.append(']');
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	bool(String name, boolean value) {
		name(name);
		try {
			m_appendable.append(value ? "true" : "false");
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	comma() {
		if (m_firsts.isEmpty())
			return;
		if (m_firsts.peek()) {
			m_firsts.pop();
			m_firsts.push(false);
		} else
			try {
				m_appendable.append(',');
			} catch (IOException e) {
			}
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	endArray() {
		try {
			m_appendable.append(']');
		} catch (IOException e) {
		}
		m_firsts.pop();
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	endObject() {
		try {
			m_appendable.append('}');
		} catch (IOException e) {
		}
		m_firsts.pop();
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	json(String json) {
		try {
			m_appendable.append(json);
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	name(String name) {
		comma();
		try {
			JSON.append(name, m_appendable);
			m_appendable.append(':');
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	number(String name, int number) {
		name(name);
		try {
			m_appendable.append(Integer.toString(number));
		} catch (IOException e) {
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	startArray() {
		comma();
		try {
			m_appendable.append('[');
		} catch (IOException e) {
		}
		m_firsts.push(true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	startArray(String name) {
		name(name);
		try {
			m_appendable.append('[');
		} catch (IOException e) {
		}
		m_firsts.push(true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	startObject() {
		comma();
		try {
			m_appendable.append('{');
		} catch (IOException e) {
		}
		m_firsts.push(true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	startObject(String name) {
		name(name);
		try {
			m_appendable.append('{');
		} catch (IOException e) {
		}
		m_firsts.push(true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	string(String string) {
		comma();
		JSON.append(string, m_appendable);
		return this;
	}

	//--------------------------------------------------------------------------

	public final JSONBuilder
	string(String name, String string) {
		name(name);
		JSON.append(string, m_appendable);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	toString() {
		return m_appendable.toString();
	}
}
