package ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import web.HTMLWriter;

public class Select {
	public enum Type { SELECT, RADIO, DROPDOWN }

	private boolean									m_allow_no_selection;
	private Dropdown								m_dropdown;
	private List<SelectOption>						m_first_options;
	private final boolean							m_has_values;
	private boolean									m_inline;
	private boolean									m_is_required;
	private List<SelectOption>						m_last_options;
	protected final String							m_name;
	protected String								m_on_change;
	protected int									m_option_index;
	protected Collection<? extends SelectOption>	m_options;
	private SelectOption							m_selected_option;
	private boolean									m_text_is_value;
	private Type									m_type = Type.SELECT;

	//--------------------------------------------------------------------------

	public
	Select(String name, Collection<? extends SelectOption> options) {
		m_name = name;
		m_options = options;
		m_has_values = true;
	}

	//--------------------------------------------------------------------------

	public
	Select(String name, String[] options) {
		m_name = name;
		ArrayList<SelectOption> list = new ArrayList<>(options.length);
		for (String option : options)
			list.add(new Option(option));
		m_options = list;
		m_has_values = false;
	}

	//--------------------------------------------------------------------------

	public
	Select(String name, List<String> options) {
		m_name = name;
		ArrayList<SelectOption> list = new ArrayList<>(options.size());
		for (String option : options)
			list.add(new Option(option));
		m_options = list;
		m_has_values = false;
	}

	//--------------------------------------------------------------------------

	public
	Select(String name, Set<String> options) {
		m_name = name;
		ArrayList<SelectOption> list = new ArrayList<>(options.size());
		for (String option : options)
			list.add(new Option(option));
		list.sort(new Comparator<SelectOption>() {
			@Override
			public int
			compare(SelectOption o1, SelectOption o2) {
				return String.CASE_INSENSITIVE_ORDER.compare(o1.getText(), o2.getText());
			}
		});
		m_options = list;
		m_has_values = false;
	}

	//--------------------------------------------------------------------------

	public
	Select(String name, String[] options, String[] option_values) {
		m_name = name;
		ArrayList<SelectOption> list = new ArrayList<>(options.length);
		if (option_values == null) {
			for (String option : options)
				list.add(new Option(option));
			m_has_values = false;
		} else {
			for (int i=0; i<options.length; i++)
				list.add(new Option(options[i], option_values[i]));
			m_has_values = true;
		}
		m_options = list;
	}

	//--------------------------------------------------------------------------

	public
	Select(String name, Class<?> enum_class) {
		m_name = name;
		ArrayList<SelectOption> list = new ArrayList<>();
		Object[] enum_constants = enum_class.getEnumConstants();
		for (Object constant : enum_constants)
			list.add(new Option(constant.toString()));
		m_options = list;
		m_has_values = false;
	}

	//--------------------------------------------------------------------------

	public final Select
	addFirstOption(Option o) {
		if (m_first_options == null)
			m_first_options = new ArrayList<>();
		m_first_options.add(o);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	addLastOption(Option o) {
		if (m_last_options == null)
			m_last_options = new ArrayList<>();
		m_last_options.add(o);
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	close(HTMLWriter w) {
		if (m_type == Type.SELECT)
			w.tagClose();
		else if (m_type == Type.DROPDOWN)
			m_dropdown.close();
//		if (!m_inline)
//			writer.tagClose();
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final Iterator<? extends SelectOption>
	iterator() {
		return m_options.iterator();
	}

	//--------------------------------------------------------------------------

	public Select
	open(HTMLWriter w) {
//		if (!m_inline)
//			writer.tagOpen("div");
		if (m_name != null) {
			w.setAttribute("name", m_name);
			w.setId(m_name);
		}
		if (m_is_required)
			w.setAttribute("required", "yes");
		if (m_type == Type.SELECT) {
			if (m_on_change != null)
				w.setAttribute("onchange", m_on_change);
			w.ui.selectOpen(null, m_inline);
		} else if (m_type == Type.DROPDOWN) {
			if (m_selected_option == null)
				m_dropdown = w.ui.dropdown(m_options.iterator().next().getText(), true);
			else {
				String text = m_selected_option.getText();
				if (text == null) {
					if (m_first_options != null)
						for (SelectOption o : m_first_options)
							if (m_selected_option.getValue().equals(o.getValue())) {
								text = o.getText();
								break;
							}
					if (text == null)
						for (SelectOption o : m_options)
							if (m_selected_option.getValue().equals(o.getValue())) {
								text = o.getText();
								break;
							}
					if (text == null && m_last_options != null)
						for (SelectOption o : m_last_options)
							if (m_selected_option.getValue().equals(o.getValue())) {
								text = o.getText();
								break;
							}
				}
				m_dropdown = w.ui.dropdown(text, true);
			}
			if (m_inline)
				m_dropdown.setDivClass("d-inline-block");
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setAllowNoSelection(boolean allow_no_selection) {
		m_allow_no_selection = allow_no_selection;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setInline(boolean inline) {
		m_inline = inline;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setIsRequired(boolean is_required) {
		m_is_required = is_required;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setOnChange(String on_change) {
		m_on_change = on_change;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setSelectedOption(String text, String value) {
		if (text != null && text.length() > 0 || value != null && value.length() > 0)
			m_selected_option = new Option(text, value);
		else
			m_selected_option = null;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setTextIsValue(boolean text_is_value) {
		m_text_is_value = text_is_value;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Select
	setType(Type type) {
		m_type = type;
		return this;
	}

	//--------------------------------------------------------------------------

	public int
	size() {
		return m_options != null ? m_options.size() : 0;
	}

	//--------------------------------------------------------------------------

	public void
	write(HTMLWriter w) {
		if (m_options == null && m_selected_option == null && !m_is_required)
			return;
		m_option_index = 0;
		open(w);
		boolean option_in_select = false;
		if (m_allow_no_selection)
			option_in_select = writeOption(new Option("", ""), w);
		if (m_first_options != null)
			for (SelectOption option : m_first_options)
				option_in_select |= writeOption(option, w);
		if (m_options != null)
			for (SelectOption option : m_options) {
				option_in_select |= writeOption(option, w);
				if (m_type == Type.RADIO)
					w.nbsp();
			}
		if (m_last_options != null) {
			w.write("<option disabled></option>");
			for (SelectOption option : m_last_options)
				option_in_select |= writeOption(option, w);
		}
		if (!option_in_select && m_selected_option != null)
			writeOption(m_selected_option, w);
		close(w);
	}

	//--------------------------------------------------------------------------

	private boolean
	writeOption(SelectOption option, HTMLWriter w) {
		String text = option.getText();
		if (text == null && option.getValue() == null)
			return false;

		boolean option_selected = false;
		if (m_selected_option != null)
			if (m_has_values) {
				if (text != null)
					option_selected = text.equals(m_selected_option.getText());
				if (!option_selected)
					option_selected = (option.getValue() == null ? text : option.getValue()).equals(m_selected_option.getValue());
			} else if (text != null)
				option_selected = text.equals(m_selected_option.getText());

		switch(m_type) {
		case DROPDOWN:
			if ("-".equals(text))
				m_dropdown.divider();
			else
				m_dropdown.aOnClick(text, null);
			break;
		case RADIO:
			if (m_on_change != null)
				w.setAttribute("onclick", m_on_change);
			w.ui.radioButtonInline(m_name, null, text, m_text_is_value ? null : option.getValue() == null ? text : option.getValue(), option_selected, null);
			break;
		case SELECT:
			if ("-".equals(text)) {
				w.write("<option disabled>──────────</option>");
				return false;
			}
			w.option(text, m_text_is_value ? null : option.getValue(), option_selected);
			m_option_index++;
			break;
		}
		return option_selected;
	}
}