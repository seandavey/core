package ui;

import java.util.Map;

interface Nav<T> extends TagBase<T> {

	T a(String text, String href);

	T a(String text, String href, boolean new_tab);

	T aOnClick(String text, String on_click);

	T divider();

	T dropdownClose();

	T dropdownOpen(String text, boolean right);

	T item(String text);

	T item(String text, String url);

	T items(Map<String,Object> items);

	T setActiveItem(String active_item);

	T setPath(String path);

}