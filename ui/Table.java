package ui;

public interface Table extends TagBase<Table> {

	Table addDefaultClasses();

	Table borderSpacing(int border_spacing);

//	Table cellPadding(int cell_padding);

	Table heads(String... heads);

	int numColumns();

//	Table rowOpen(String label);

//	Table setLabelTd(Td label_td);

	Table td();

	Table td(Td td);

	Table td(int colspan, String text);

	Table td(String text);

//	Table tdCenter();

	Table tdCurrency(double d);

	Table tdRight();

	Table tfoot();

	Table th();

	Table th(String text);

	Table thCenter();

	Table thCenter(String text);

	Table thead();

	Table tr();


}