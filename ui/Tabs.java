package ui;

import java.util.List;

public interface Tabs {

	void clearLabels();

	void close();

	List<String> getLabels();

	Tabs open();

	void pane(String label);

	Tabs setShowBottomLine(boolean show_bottom_line);

	Tabs setShowClose(boolean show_close);

	Tabs setStartTab(String start_tab);

	Tabs setStartWithLast(boolean start_with_last);

}