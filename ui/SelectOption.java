package ui;

public interface SelectOption {
	public String getText();

	public String getValue();
}
