package ui;

public interface Dropdown extends TagBase<Dropdown> {
	Dropdown a(String text, String href, boolean new_tab);

	Dropdown aOnClick(String text, String on_click);

	Dropdown divider();

	String getMenuJS();

	Dropdown setDivClass(String div_class);

	Dropdown setDropdownMenuRight(boolean dropdown_menu_right);

	Dropdown setInNavBar(boolean in_nav_bar);

}