package ui;

public interface InlineForm {

	InlineForm close();

	InlineForm formGroup();

	InlineForm label(String text);

	InlineForm open();

	InlineForm setMethod(String method);

	void writeSubmit(String text);

}
