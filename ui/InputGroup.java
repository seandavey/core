package ui;

public interface InputGroup extends TagBase<InputGroup> {
	Dropdown dropdownOpen();

	InputGroup prepend(String text);

	InputGroup prependIcon(String icon);
}
