package ui;

import java.util.ArrayList;

import util.Text;
import web.HTMLWriter;

public class FormBase<T> {
	public enum Location { BOTTOM, HEAD, NONE }

	protected String			m_action;
	protected ArrayList<Button>	m_buttons;
	protected Location			m_buttons_location = Location.BOTTOM;
	protected int				m_form_mark = -1;
	private boolean				m_keep_submit;
	private String				m_method = "post";
	protected boolean			m_multipart;
	protected final String		m_name;
	private boolean				m_no_submit;
	private String				m_on_click;
//	private String				m_on_complete;
	private String				m_on_submit;
	protected int				m_row_mark = -1;
	protected String			m_submit_text = "Save";
	private int					m_subrows;
	protected Table				m_table;
	private boolean				m_use_submit;
	protected final HTMLWriter	m_w;

	//--------------------------------------------------------------------------

	public
	FormBase(String name, String action, HTMLWriter w) {
		m_action = action;
		m_name = name;
		m_w = w;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	addButton(Button button) {
		if (m_buttons == null)
			m_buttons = new ArrayList<>();
		m_buttons.add(button);
		return (T)this;
	}

	// --------------------------------------------------------------------------

	public void
	close() {
		if (m_row_mark != -1)
			rowClose();
		if (m_table != null)
			m_table.close();
		if (m_buttons_location == Location.BOTTOM) {
			m_w.write("<div style=\"text-align:center\">");
			writeButtons();
			m_w.write("</div>");
		}
		if (m_form_mark != -1)
			m_w.tagClose();
	}

	// --------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	open() {
		if (m_action != null)
			m_w.setAttribute("action", m_action);
		m_w.setAttribute("method", m_method);
		if (m_name != null)
			m_w.setId(m_name)
				.setAttribute("name", m_name);
		if (m_multipart)
			m_w.setAttribute("enctype", "multipart/form-data");
		if (m_no_submit)
			m_w.setAttribute("onsubmit", "return false;");
		m_form_mark = m_w.tagOpen("form");
		m_w.hiddenInput("db_submit_id", null);
		if (m_table != null)
			m_table.open();
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	rowClose() {
		if (m_subrows != 0)
			m_w.tagClose();
		else if (m_row_mark != -1) {
			m_w.tagsCloseTo(m_row_mark);
			m_row_mark = -1;
		}
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	rowOpen(String label) {
		if (m_form_mark == -1)
			open();
		if (m_row_mark != -1 && m_subrows == 0)
			rowClose();
		if (m_table != null)
			m_table.tr().td();
		else {
			if (label != null)
				m_w.setId(label + "_row");
			if (m_table == null) {
				int m = m_w.addClass("mb-3").addClass("position-relative").tagOpen("div");
				if (m_subrows == 0)
					m_row_mark = m;
			}
		}
		writeLabel(label);
		if (m_table != null)
			m_table.td();
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setButtonsLocation(Location buttons_location) {
		m_buttons_location = buttons_location;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setKeepSubmit(boolean keep_submit) {
		m_keep_submit = keep_submit;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setMethod(String method) {
		m_method = method;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setMultipart(boolean multipart) {
		m_multipart = multipart;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setNoSubmit(boolean no_submit) {
		m_no_submit = no_submit;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setOnClick(String on_click) {
		m_on_click = on_click;
		return (T)this;
	}

	// --------------------------------------------------------------------------

//	@SuppressWarnings("unchecked")
//	public T
//	setOnComplete(String on_complete) {
//		m_on_complete = on_complete;
//		return (T)this;
//	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public T
	setOnSubmit(String on_submit) {
		m_on_submit = on_submit;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setSubmitText(String submit_text) {
		m_submit_text = submit_text;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public final T
	setUseSubmit(boolean use_submit) {
		m_use_submit = use_submit;
		return (T)this;
	}

	// --------------------------------------------------------------------------

	public final void
	subrowsEnd() {
		m_subrows--;
	}

	// --------------------------------------------------------------------------

	public final void
	subrowsStart() {
		m_subrows++;
	}

	// --------------------------------------------------------------------------

	protected final void
	writeButtons() {
		if (m_buttons != null)
			for (Button button : m_buttons) {
				button.write();
				m_w.nbsp();
			}
		writeSubmit();
	}

	// --------------------------------------------------------------------------

	protected void
	writeLabel(String label) {
		if (label != null)
			m_w.ui.formLabel(label.replace('_', ' '));
	}

	// --------------------------------------------------------------------------

	public void
	writeSubmit() {
		if (m_use_submit) {
			m_w.ui.submitButton(m_submit_text);
			return;
		}
		String on_click = m_on_click;
		if (on_click == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("_.form.submit(this,{validate:true");
			if (m_keep_submit)
				sb.append(",keep_submit:true");
			if (m_on_submit != null)
				sb.append(",on_submit:" + m_on_submit);
//			if (m_on_complete != null)
//				sb.append(",on_complete:function(t){").append(m_on_complete).append('}');
			sb.append("})");
			on_click = sb.toString();
		}
		m_w.setId(Text.uuid())
			.ui.buttonOnClick(m_submit_text, on_click);
	}
}
