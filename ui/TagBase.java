package ui;

import java.io.Closeable;

interface TagBase<T> extends Closeable {
	T addAttribute(String name, String value);

	T addAttribute(String name, int value);

	T addClass(String name);

	T addStyle(String name, String value);

	T addStyle(String name, int value);

	@Override
	void close();

	int getMark();

	String getText();

	T open();

	T setId(String id);

	T setOnClick(String on_click);

	T setText(String text);

	@Override
	String toString();

	void write();

	void write(String text);
}