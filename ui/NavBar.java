package ui;

import app.Request;

public interface NavBar extends Nav<NavBar> {

	boolean	checkClick(Request r);

	NavBar	formOpen(boolean right);

	NavBar	header(String header);

	NavBar	setBrand(String brand, String href);

	NavBar	setDark(boolean dark);

	NavBar	setFixedTop(boolean fixed_top);
	
	String	target();

	NavBar	ulOpen(boolean right);

}