package ui;

public interface Button extends TagBase<Button> {

	Button setIcon(String icon);

}