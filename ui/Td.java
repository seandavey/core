package ui;

public interface Td extends TagBase<Td> {

	Td colspan(int colspan);

	Td verticalAlign(String vertical_align);

	Td width(String width);

}