package ui;

import web.HTMLWriter;
import web.WriterBase;

public class RichText {
	private boolean	m_encode;
	private String	m_label;
	private String	m_on_click;
	private String	m_on_save;
	private boolean	m_one_at_a_time;

	//--------------------------------------------------------------------------

	public final RichText
	setEncode(boolean encode) {
		m_encode = encode;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RichText
	setLabel(String label) {
		m_label = label;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RichText
	setOnClick(String on_click) {
		m_on_click = on_click;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RichText
	setOnSave(String on_save) {
		m_on_save = on_save;
		return this;
	}

	//--------------------------------------------------------------------------

	public final RichText
	setOneAtATime(boolean is_text_input) {
		m_one_at_a_time = is_text_input;
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	write(String name, String value, String tag, boolean create, String toolbar, HTMLWriter w) {
		String on_click = m_on_click;
		if (on_click == null)
			on_click = "";
		on_click = "_.rich_text.create(" + (create ? "'#" + name + "'" : "this") +
			",{toolbar:" + (toolbar != null ? "'" + toolbar + "'" : "null") +
			",on_save:" + (m_on_save != null ? m_on_save : "null") +
			",one_at_a_time:" + (m_one_at_a_time ? "true" : "false") +
			",focus:" + (create ? "false" : "true") + "});" +
			on_click;
		w.setId(name)
			.setAttribute("name", name);
		if (create)
			w.addStyle("display:none;height:200px;max-width:1024px");
		else
			w.addStyle("cursor:text;min-height:1.5em;min-width:2em;width:100%")
				.setAttribute("onclick", "event.stopPropagation();" + on_click)
				.setAttribute("onmouseleave", "this.style.outline=''")
				.setAttribute("onmouseenter", "this.style.outline='solid 1px'")
				.setAttribute("title", (m_label != null ? m_label : name) + " - click to edit");
		w.tagOpen(tag);
		if (value != null)
			w.write(m_encode ? WriterBase.encode(value) : value);
		w.tagClose();
		if (create)
			w.js(on_click);
	}
}
