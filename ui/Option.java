package ui;

public class Option implements Comparable<Option>, SelectOption {
	private String m_text;
	private String m_value;

	//--------------------------------------------------------------------------

	public
	Option(String text) {
		m_text = text;
	}

	//--------------------------------------------------------------------------

	public
	Option(String text, String value) {
		m_text = text;
		m_value = value;
//		if (value != null)
//			m_value = URLBuilder.encode(value);
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(Option o) {
		return m_text.compareTo(o.m_text);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getText() {
		return m_text;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getValue() {
		return m_value;
	}
}
