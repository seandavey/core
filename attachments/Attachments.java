package attachments;

import app.Site;
import db.DBConnection;
import db.DeleteHook;
import db.InsertHook;
import db.Relationship;
import db.Rows;
import db.Select;
import db.UpdateHook;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.FileColumn;
import web.HTMLWriter;

public class Attachments implements InsertHook, UpdateHook, DeleteHook {
	private final FileColumn	m_column;
	private final String		m_table;
	private ViewDef				m_view_def;

	//--------------------------------------------------------------------------

	public
	Attachments(String table) {
		m_table = table;
		m_column = new FileColumn("filename", m_table + "_attachments", m_table + "/attachments").setDirColumn(m_table + "_id").setIsRequired(true).setTitleColumn("filename");
	}

	//--------------------------------------------------------------------------

	public final void
	appendTokens(int one_id, StringBuilder tsvector, DBConnection db) {
		Rows rows = new Rows(new Select("filename").from(m_table + "_attachments").whereEquals(m_table + "_id", one_id).andWhere("filename IS NOT NULL"), db);
		while (rows.next()) {
			String t = Site.site.loadTokens(Site.site.getPath(m_table, "attachments", Integer.toString(one_id), rows.getString(1)));
			if (t != null && t.length() > 0)
				tsvector.append(t).append("\n");
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	public void
	init(DBConnection db) {
		db.createManyTable(m_table, m_table + "_attachments", "filename VARCHAR");
	}

	//--------------------------------------------------------------------------

	public ViewDef
	_newViewDef(String name) {
		if (m_view_def == null)
			m_view_def = new ViewDef(m_table + "_attachments") {
				@Override
				public String
				getAddButtonOnClick(Relationship r, boolean set_owner) {
					return "_.table.add_attachment(this.parentNode.querySelector('table'))";
				}
			}
			.setAccessPolicy(new AccessPolicy().add().delete())
			.setRecordName("Attachment", true)
			.setColumn(m_column);
		return m_view_def;
	}

	//--------------------------------------------------------------------------

	public void
	write(int id, HTMLWriter w, DBConnection db) {
		Rows rows = new Rows(new Select("*").from(m_table + "_attachments").whereEquals(m_table + "_id", id).andWhere("filename IS NOT NULL"), db);
		while (rows.next()) {
			w.br();
			m_column.writeValue(rows, w);
		}
		rows.close();
	}
}
