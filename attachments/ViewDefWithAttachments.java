package attachments;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import app.Files;
import app.Request;
import app.Site;
import app.Stringify;
import db.NameValuePairs;
import db.ViewDef;
import jakarta.servlet.http.Part;

@Stringify
public class ViewDefWithAttachments extends ViewDef {
	private Attachments m_attachments;

	//--------------------------------------------------------------------------

	public
	ViewDefWithAttachments(String name, Attachments attachments) {
		super(name);
		m_attachments = attachments;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterDelete(String where, Request r) {
		Files.deleteDirectory(m_from, "attachments", where.substring(3));
		super.afterDelete(where, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		handleFiles(id, r);
		super.afterInsert(id, nvp, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		handleFiles(id, r);
		super.afterUpdate(id, nvp, previous_values, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	appendTokens(int id, NameValuePairs nvp, StringBuilder tsvector, Request r) {
		super.appendTokens(id, nvp, tsvector, r);
		if (m_attachments != null)
			m_attachments.appendTokens(id, tsvector, r.db);
	}

	//--------------------------------------------------------------------------

	private void
	handleFiles(int one_id, Request r) {
		for (Part p : r.getParts())
			if (p.getName().startsWith("db_attachment")) {
				Path dir = Site.site.getPath(m_from, "attachments", Integer.toString(one_id));
				dir.toFile().mkdirs();
				String filename = p.getSubmittedFileName();
				filename = Files.getNonexistantFilename(Files.getSafeFilename(filename), dir);
				try {
					p.write(dir.resolve(filename).toString());
				} catch (IOException e) {
					r.abort(e);
				}
				NameValuePairs nvp = new NameValuePairs();
				nvp.set(m_from + "_id", one_id);
				nvp.set("filename", filename);
				r.db.insert(m_from + "_attachments", nvp);
			}
	}
}
