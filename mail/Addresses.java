package mail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import app.Site;
import db.DBConnection;
import db.Select;
import jakarta.mail.Address;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import util.Text;

public class Addresses implements Iterable<String> {
	private final Set<String> m_email_addresses = new HashSet<>();

	//--------------------------------------------------------------------------

	public final Addresses
	add(String email_address) {
		if (email_address == null)
			return this;
		String[] addresses = email_address.strip().split("(,|;|\\s)+");
		for (String a : addresses) {
			if (a.length() == 0)
				continue;
			if (a.charAt(0) == '@')
				Site.site.log("missing local name in email address", true);
			if (a.indexOf('@') == -1)
				a = a + "@" + Site.site.getDomain();
			m_email_addresses.add(a);
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final Addresses
	add(String[] email_addresses) {
		if (email_addresses != null)
			for (String s : email_addresses)
				add(s);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Addresses
	add(Collection<String> email_addresses) {
		if (email_addresses != null)
			for (String s : email_addresses)
				add(s);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Iterator<String>
	iterator() {
		return m_email_addresses.iterator();
	}

	//--------------------------------------------------------------------------

	public final Addresses
	remove(Address address, DBConnection db) {
		String a = ((InternetAddress)address).getAddress();
		if (m_email_addresses.contains(a))
			m_email_addresses.remove(a);
		else {
			int people_id = db.lookupInt(new Select("people_id").from("additional_emails").where("email='" + a + "'"), 0);
			if (people_id != 0) {
				String email = db.lookupString(new Select("email").from("people").whereIdEquals(people_id));
				if (email != null)
					m_email_addresses.remove(email);
			}
		}
		return this;
	}

	//--------------------------------------------------------------------------

	public final Addresses
	remove(Address[] addresses, DBConnection db) {
		if (addresses != null)
			for (Address address : addresses)
				remove(address, db);
		return this;
	}

	//--------------------------------------------------------------------------

	public final int
	size() {
		return m_email_addresses.size();
	}

	//--------------------------------------------------------------------------

	public final InternetAddress
	toAddress() {
		for (String a : m_email_addresses)
			try {
				return new InternetAddress(a);
			} catch (AddressException e) {
				Site.site.log(e);
			}
		return null;
	}

	//--------------------------------------------------------------------------

	public final InternetAddress[]
	toArray() {
		ArrayList<InternetAddress> l = new ArrayList<>();
		for (String a : m_email_addresses)
			try {
				l.add(new InternetAddress(a));
			} catch (AddressException e) {
				Site.site.log(e);
			}
		return l.toArray(new InternetAddress[l.size()]);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	toString() {
		return Text.join(", ", m_email_addresses);
	}
}
