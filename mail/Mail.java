package mail;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import com.sun.mail.util.QPDecoderStream;

import app.MailTemplate;
import app.Site;
import jakarta.mail.Message;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Part;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import web.HTMLWriter;
import web.WriterBase;

public class Mail {
	private String			m_content;
	private String			m_from;
	private String			m_reply_to;
	private String			m_subject;
	private Addresses		m_to;

	//--------------------------------------------------------------------------

	public
	Mail(String subject, String content) {
		m_subject = subject;
		m_content = content;
	}

	//--------------------------------------------------------------------------

	public static void
	addFooter(MimeMessage message, String footer) {
		addFooter(message, footer, 0);
		try {
			message.saveChanges();
		} catch (MessagingException e) {
			Site.site.log(e);
		}
	}

	private static int
	addFooter(Part part, String footer, int type_flags) {
		try {
			String content_type = part.getContentType();
			if (content_type.startsWith("text/plain")) {
				if ((type_flags & 1) == 0)
					part.setText(part.getContent() + "\n----------\n" + footer);
				return 1;
			} else if (content_type.startsWith("text/html")) {
				if ((type_flags & 2) == 0) {
					String html = (String)part.getContent();
					String div = "<div style=\"background-color:#eee;border:solid 1px #ccc;margin-bottom:10px;margin-top:40px;padding:5px;width:100%;\">" + WriterBase.encode(footer) + "</div>";
					int i = html.indexOf("</body");
					if (i != -1)
						html = html.substring(0, i) + "<hr>" + HTMLWriter.breakNewlines(div) + html.substring(i);
					else
						html += "<hr>" + HTMLWriter.breakNewlines(div);
					part.setContent(html, "text/html; charset=utf-8");
				}
				return 2;
			} else if (content_type.startsWith("multipart/")) {
				Multipart content = (Multipart)part.getContent();
				for (int i=0; i<content.getCount() && type_flags != 3; i++)
					type_flags |= addFooter(content.getBodyPart(i), footer, type_flags);
				return type_flags;
			}
		} catch (IOException | MessagingException e) {
			Site.site.log(e);
		}
		return 0;
	}

	//--------------------------------------------------------------------------

	public final Mail
	from(String from) {
		m_from = from;
		return this;
	}

	//--------------------------------------------------------------------------

	public final MimeMessage
	getMessage(boolean use_template) {
		if (m_content == null) {
			System.out.println("no content in Mail.getMessage()");
			return null;
		}
		MimeMessage m = new MimeMessage(Site.site.getMailSession());
		try {
			if (m_from != null && m_from.length() > 0)
				m.setFrom(new Addresses().add(m_from).toAddress());
			else
				m.setFrom(Site.site.getNoreplyAddress());
			if (m_reply_to != null && m_reply_to.length() > 0)
				m.setReplyTo(new Addresses().add(m_reply_to).toArray());
			m.setSubject(m_subject);
			if (use_template) {
				MailTemplate t = Site.site.newMailTemplate();
				if (t != null) {
					m.setContent(t
						.header(m_subject.charAt(0) == '[' ? m_subject.substring(m_subject.indexOf(']') + 2): m_subject)
						.append(m_content)
						.toString(), "text/html;charset=UTF-8");
					m.saveChanges();
					return m;
				}
			}
			m.setContent(m_content.startsWith("<html") ? m_content : "<html><body>" + m_content + "</body></html>", "text/html;charset=UTF-8");
			m.saveChanges();
		} catch (MessagingException e) {
			handleError(m, e);
			throw new RuntimeException(e);
		}
		return m;
	}

	//--------------------------------------------------------------------------

	private void
	handleError(Message message, Exception e) {
		Site.site.log(e);
		try {
			message.writeTo(System.out);
			System.out.println();
		} catch (IOException e1) {
			System.out.println(e1);
		} catch (MessagingException e1) {
			System.out.println(e1);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	insertHeader(MimeMessage message, String text) {
		insertHeader(message, text, 0);
		try {
			message.saveChanges();
		} catch (MessagingException e) {
			Site.site.log(e);
		}
	}

	private static int
	insertHeader(Part part, String text, int type_flags) {
		try {
			String content_type = part.getContentType();
			if (content_type.startsWith("text/plain")) {
				if ((type_flags & 1) == 0)
					part.setText(text + "\n-----------\n" + part.getContent());
				return 1;
			} else if (content_type.startsWith("text/html")) {
				if ((type_flags & 2) == 0) {
					Object content = part.getContent();
					String html;
					if (content instanceof QPDecoderStream) {
				   		BufferedInputStream bis = new BufferedInputStream((InputStream)content);
				   		ByteArrayOutputStream baos = new ByteArrayOutputStream();
				   		while (true) {
				   			int c = bis.read();
				   			if (c == -1)
								break;
				   			baos.write(c);
				   		}
				   		html = new String(baos.toByteArray());
				   	} else
				   		html = (String)content;
					String div = "<div style=\"background-color:#eee;border:solid 1px #ccc;margin-bottom:10px;padding:5px;width:100%;\">" + WriterBase.encode(text) + "</div>";
					int i = html.indexOf("<body");
					if (i != -1) {
						i = html.indexOf(">", i + 5) + 1;
						html = html.substring(0, i) + div + html.substring(i);
					} else
						html = div + html;
					part.setContent(html, "text/html; charset=utf-8");
				}
				return 2;
			} else if (content_type.startsWith("multipart/")) {
				Multipart content = (Multipart)part.getContent();
				for (int i=0; i<content.getCount() && type_flags != 3; i++)
					type_flags |= insertHeader(content.getBodyPart(i), text, type_flags);
				return type_flags;
			}
		} catch (IOException | MessagingException e) {
			Site.site.log(e);
		}
		return 0;
	}

	//--------------------------------------------------------------------------

	public final Mail
	replyTo(String reply_to) {
		m_reply_to = reply_to;
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	send() {
		send(true);
	}

	//--------------------------------------------------------------------------

	public final void
	send(boolean use_template) {
		if (Site.site.isDevMode())
			System.out.println(m_to.toString());
		MimeMessage message = sendCheck(use_template);
		if (message == null)
			return;
		if (m_to == null || m_to.size() == 0)
			return;
		Site.site.log(m_to.toString(), "send.log");
		send(message, null, m_to.toArray());
	}

	//--------------------------------------------------------------------------

	private void
	send(MimeMessage message, InternetAddress to, InternetAddress[] tos) {
		try {
			if (to != null)
				message.setRecipient(RecipientType.TO, to);
			else
				message.setRecipients(RecipientType.TO, tos);
			String smtp_username = Site.settings.getString("smtp username");
			String smtp_password = Site.settings.getString("smtp password");
			if (smtp_username != null && smtp_username.length() > 0 && smtp_password != null && smtp_password.length() > 0)
				Transport.send(message, smtp_username, smtp_password);
			else
				Transport.send(message);
		} catch (MessagingException e) {
			handleError(message, e);
			if (e.getMessage().indexOf("Recipient address rejected") != -1 && tos != null)
				for (InternetAddress a : tos)
					send(message, a, null);
		}
	}

	//--------------------------------------------------------------------------

	private MimeMessage
	sendCheck(boolean use_template) {
		MimeMessage message = getMessage(use_template);
		if (message == null)
			return null;
		if (Site.site.isDevMode()) {
			System.out.println(m_subject);
			try {
				System.out.println(message.getContent());
			} catch (IOException | MessagingException e) {
				Site.site.log(e);
			}
			return null;
		}
		String smtp = Site.settings.getString("smtp");
		if (smtp == null || smtp.length() == 0) {
			Site.site.log("unable to send mail, smtp setting null or empty", false);
			return null;
		}
		Site.site.log(m_subject, "send.log");
		try {
			message.setHeader("X-from-site", "true");
		} catch (MessagingException e) {
			handleError(message, e);
		}
		if (m_from == null || m_from.length() == 0 || m_from.startsWith("noreply@"))
			insertHeader(message, "This email is from a \"noreply\" account. Do not reply to this email. Replies will be discarded.");
		return message;
	}

	//--------------------------------------------------------------------------

	public final Mail
	to(Addresses addresses) {
		m_to = addresses;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Mail
	to(String ...to) {
		m_to = new Addresses()
			.add(to);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Mail
	to(Collection<String> to) {
		m_to = new Addresses()
			.add(to);
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	write(HTMLWriter w) {
		MimeMessage message = getMessage(false);
		if (message == null)
			return;
		try {
			w.write(message.getSubject())
				.br()
				.write(message.getContent());
		} catch (MessagingException  | IOException e) {
			Site.site.log(e);
		}
	}
}
