export const Img = {
	edit_menu(img, label) {
		let d = new Dropdown()
		if (img.src) {
			d.add('Delete', function(){Dialog.confirm(null, 'Delete ' + (label ? label : 'this image') + '?', _.text.delete, function(){
				net.delete(context+'/People/picture', function(){Img.replace(img.src, '')})
			})})
			d.add('Replace', Img.upload.bind(img))
		} else
			d.add('Upload', Img.upload.bind(img))
		d.show(img)
	},
	replace(from_src, to_src) {
		if (from_src.startsWith('http'))
			from_src = new URL(from_src).pathname
		var images = _.$$('img[src="' + from_src + '"]')
		images.forEach(function(img) {
			img.src = to_src;
			img.parentNode.replaceChild(img, img)
		})
	},
	rotate(btn, direction, table, column, id, thumb_size) {
		var img = btn.parentNode.firstChild
		if (img.tagName === 'A')
			img = img.firstChild
		var src = img.src
		var path = src
		var index = path.indexOf('://')
		if (index != -1)
			path = path.substring(index + 3)
		path = path.substring(path.indexOf('/', path.indexOf(context) + 1) + 1)
		img.src = 'images/loading-bubbles.svg'
		var bg = img.style.backgroundColor
		img.style.backgroundColor = 'lightgrey'
		net.post(context + '/ImageMagick/' + path + '/rotate ' + direction, "thumb_size=" + thumb_size + "&table=" + table + "&column=" + column + "&id=" + id, function(t) {
			img.src = context + '/' + t
			img.style.backgroundColor = bg
			Img.replace(context + '/' + path, img.src)
			app.publish('pictures', 'img src change', {src:src, new_src:img.src})
		})
	},
	show(img) {
		let z_index = app.z_index++
		var o = $div({
				classes: ['modal-backdrop'],
				events: {click:function(){o.remove();i.remove(); app.z_index--}},
				parent: document.body,
				styles: {bottom:0, cursor:'pointer', opacity:0.6, position:'fixed', zIndex:z_index}
			}
		)
		var i = $img({
				events: {click:function(){o.remove();i.remove(); app.z_index--}},
				parent: document.body,
				src: img.src,
				styles: {cursor:'pointer', position:'fixed', left:'50%', maxHeight:'100%', maxWidth:'100%', top:'50%', transform:'translate(-50%,-50%)', zIndex:z_index}
			}
		)
	},
	upload() {
		let img = this
		let f = $input({before:img, events:{change:function(){
			let d = new FormData()
			d.append('file', f.files[0], f.files[0].name)
			net.post(context+'/People/picture', d, function(o) {
				if (o) {
					if (img.src)
						Img.replace(img.src, o.path)
					else
						img.src = o.path
					f.remove()
				}
			})
		}}, styles:{display:'none'}, type:'file'})
		f.click()
	}
}