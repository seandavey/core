const _ = {
	$(selector, el) {
		
		if (typeof selector === 'string')
			return (el || document).querySelector(selector)
		return selector
	},
	$$(selector, el) {
		return (el || document).querySelectorAll(selector)
	},
	c(el) {
		el = _.$(el)
		while (el && el.getAttribute) {
			let id = el.getAttribute('id');
			if (id && typeof id === 'string' && id.substring(0,2) === 'c_')
				return el;
			el = el.parentNode;
		}
		return null;
	},
	text: {
		add: 'Add',
		delete: 'Delete',
		ok: 'OK'
	}
}

import { app } from "./app.js"
import { date } from "./date.js"
import { db } from "./db.js"
import { ButtonNav } from "./button_nav.js"
import { comments } from "./comments.js"
import { Dialog } from "./dialog.js"
import { dom } from "./dom.js"
import { Dropdown} from "./dropdown.js"
import { file_column, form } from "./form.js"
import { Img } from "./image.js"
import { nav, Nav, NavList } from "./nav.js"
import { net } from "./net.js"
import { open_print_window } from "./print.js"
import { rich_text } from "./rich_text.js"
import { FiltersDialog, qe, QuickFilters, sorter, table, view } from "./table.js"
import { Tabs } from "./tabs.js"
import { ui } from "./ui.js"

_.comments = comments
_.date = date
_.db = db
_.dom = dom
_.file_column = file_column
_.form = form
_.nav = nav
_.Nav = Nav
_.NavList = NavList
_.open_print_window = open_print_window
_.qe = qe
_.rich_text = rich_text
_.sorter = sorter
_.table = table
_.ui = ui
_.view = view

window._ = _
window.app = app
window.ButtonNav = ButtonNav
window.Dialog = Dialog
window.Dropdown = Dropdown
window.Img = Img
window.net = net
window.FiltersDialog = FiltersDialog
window.QuickFilters = QuickFilters
window.Tabs = Tabs

const JS = {
	f: null,
	files: {},
	get(file, f) {
		if (JS.loading) {
			setTimeout(function(){JS.get(file,f);}, 500)
			return
		}
		JS.loading = true
		let src = file
		if (!src.startsWith('http')) {
			src = context
			if (file.charAt(0) !== '/')
				src += '/js/'
			src += file + '.js'
		}
		if (!JS.files[src]) {
			JS.files[src] = true
			let s = document.createElement('script')
			s.type = 'text/javascript'
			s.src = src
			document.getElementsByTagName('head').item(0).appendChild(s)
			JS.s = s
			JS.f = f
			s.onload = JS.onload
			s.onreadystatechange = JS.onreadystatechange
			JS.onreadystatechange()
		} else {
			if (f) 
				f()
			JS.loading = false
		}
	},
	loading: false,
	onload() {
		JS.s.onload = null
		JS.s.onreadystatechange = null
		if (JS.f)
			JS.f(true)
		JS.loading = false
	},
	onreadystatechange(){
		if (/loaded|complete/i.test(JS.s.readyState))
			JS.onload()
	},
	s: null,
	with(v, f) {
		if (window.hasOwnProperty(v))
			f()
		else
			setTimeout(JS.with, 100, v, f)
	}
}
window.JS = JS
const Person = {
	popup(id) {
		new Dialog({id:id, url:context+'/api/People/profile/'+id})
	}
}
window.Person = Person
document.addEventListener('mouseup', function(){app.hide_popups()})
window.addEventListener('load',function() {
	window.onpopstate = app.pop_state
})

export { _ }