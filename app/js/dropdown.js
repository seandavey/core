export class Dropdown {
	constructor() {
		this.ul = $ul({classes:['dropdown-menu'], dropdown:true, styles:{left:'auto', zIndex:app.z_index++}})
	}
	add(t, f) {
		let li = $li({parent:this.ul})
		if (f) {
			let dd = this
			$button({classes:['dropdown-item'], events:{mousedown:function(){dd.clicked(this, f)}}, parent:li}, t)
		} else
			$h6({classes:['dropdown-header'], parent:li}, t)
		return this
	}
	clicked(a, f) {
		if (this.button)
			this.button.innerHTML = a.innerHTML
		if (f)
			f(a, this.button)
	}
	select(a) {
		if (this.active)
			this.active.classList.remove('active')
		this.active = a
		a.classList.add('active')
	}
	show(e, left) {
		if (e.tagName === 'BUTTON') {
			this.button = e
			for (let i=0; i<this.ul.children.length; i++) {
				let a = this.ul.children[i].firstChild
				if (e.innerHTML === a.innerHTML) {
					this.select(a)
					break
				}
			}
		}
		_.dom.popup(this.ul, e, left)
	}
}
