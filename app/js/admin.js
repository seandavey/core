const admin = {
	api_url() {
		return app.api_url('Admin', ...arguments)
	},
	column_drop(table,column,f) {
		Dialog.confirm(null, 'Drop column '+column+'?', 'drop', function(){
			net.post(admin.api_url('column/drop'), {table:table,column:column}, f)})
	},
	column_rename(table,column,f) {
		var new_name=prompt('enter new name', column)
		if(new_name)
			net.post(admin.api_url('column', 'rename'), {table:table,column:column,newname:new_name}, f)
	},
	create_many_many() {
		new Dialog({
			cancel: true,
			modal: true,
			ok: {
				click: function() {
					Form.send(Dialog.top().get_form(), function(){
						Dialog.top().close()
						app.refresh_page_menu()
						Dialog.alert('', 'table created')
					})
				},
				text: 'create'
			},
			title: 'Create Many To Many Link Table',
			url: admin.api_url('many_to_many_link_table_create_form')
		})
	},
	create_table() {
		new Dialog({
			cancel: true,
			modal: true,
			ok: {
				click: function() {
					Form.send(Dialog.top().get_form(), function() {
						Dialog.top().close()
						Dialog.alert('', 'table created')
						app.refresh_page_menu()
					})
				},
				text:'create'
			},
			title: 'Create Table',
			url: admin.api_url('table_create_form')
		})	
	},
	execute(b) {
		let f = b.closest('form')
		net.get_text(admin.api_url('sql?' + new URLSearchParams(new FormData(f)).toString()), function(t) {
			_.dom.set_html(b.closest('.tab-pane').querySelector('#results'), t)
		})
	},
	form_menu(event, view_def, mode) {
		let view = app.get('view:' + view_def)
		new Dropdown()
			.add(view['record_name'] + ' id: ' + view.id)
			.add(mode == 'EDIT_FORM' ? 'Read Only Form' : 'Edit Form', function(){})
			.show(event.target)
	},
	load_pane(e, action) {
		net.replace(e.closest('.tab-pane'),action)
	},
	load_in_pane(e, action) {
		net.replace(e.closest('.tab-pane').lastElementChild,action)
	},
	new_tab(label, action) {
		var tabs = _.$('#admin-tabs').tabs
		var tab = tabs.addTab(admin.num_tabs++ + ':' + label)
		net.replace(tab.pane, admin.api_url(action), {on_complete:tabs.show.bind(tabs, tab)})
	},
	num_tabs: 0,
	table_delete(table) {
		var where = prompt('delete where')
		if (where)
			net.post(admin.api_url('table', 'delete'), {table:table,where:where},function(){
				Dialog.alert('','done')
				app.refresh_page_menu()
			})
	},
	table_drop(table) {
		Dialog.confirm(null,'Drop table '+table+'?','drop', function(){net.post(admin.api_url('table', 'drop'), {table:table}, function(){
			_.$('#admin-tabs').tabs.closeTab()
			app.refresh_page_menu()
		})})
	},
	table_menu(event, view_def) {
		let t = event.target.tagName
		if (t != 'TD' && t != 'TR')
			return
		let row = event.target
		if (t == 'TD')
			row = row.parentNode
		let view = app.get('view:' + view_def)
		new Dropdown()
			.add(view['record_name'] + ' id: ' + row.dataset.id)
			.add('Add ' + view.record_name, function(){table.dialog_add(row, view_def, admin.url(view_def, 'ADD_FORM'))})
			.add('Delete ' + view.record_name, function(){delete_record(row, 'Delete', view_def, 'id='+row.dataset.id, Dialog.top())})
			.add('Duplicate ' + view.record_name, function(){net.post(context+'/Views/'+view_def+'/duplicate/'+row.dataset.id, null, function(){net.replace(C._(row))})})
			.add('Edit ' + view.record_name, function(){table.dialog_edit(row, view_def, admin.url(view_def, 'EDIT_FORM', row.dataset.id))})
			.add('Reload Table', function(){app.replace(view_def)})
			.show(event.target)
	},
	table_rename(table) {
		var new_name = prompt('enter new name', table)
		if (new_name)
			net.post(admin.api_url('table', 'rename'), {table:table,newname:new_name}, function(){
				Dialog.alert('','table renamed')
				app.refresh_page_menu()})
	},
	truncate_table(table) {
		Dialog.confirm(null,'Delete all rows from table '+table+'?','delete', function(){
			net.post(admin.api_url('table', 'truncate'), {table:table}, function(){Dialog.alert('','done')})})
	},
	url(view_def, mode, id) {
		let url = new URL(context + '/Views/' + view_def + '/component', document.URL)
		url.searchParams.append('db_mode', mode)
		if (id)
			url.searchParams.append('db_key_value', id)
		return url
	}
}