export const rich_text = {
	create(el, options) {
		options || {}
		el = _.$(el)
		el.classList.add('rt')
		el.rt = {}
		el.rt.options = options
		if (options.one_at_a_time)
			_.rich_text.remove(document)
		let s = _.$(':root').style
		s.setProperty('--ck-z-modal',2000)
		s.setProperty('--ck-color-base-background','transparent')
		s.setProperty('--ck-color-dropdown-panel-background','white')
		s.setProperty('--ck-color-panel-background','white')
		s.setProperty('--ck-color-tooltip-text','white')
		JS.get('ckeditor', function(){
			if (options.on_save)
				_.ui.ok_cancel(el.parentNode, _.rich_text.save.bind(this, el), _.rich_text.destroy.bind(this, el))
			let config = {
				link: {addTargetToExternalLinks: true},
				mediaEmbed: {previewsInData: true},
				removePlugins: ['Title']
			}
			if (options.toolbar === null)
				config.toolbar = null
			else if (options.toolbar === "small")
				config.toolbar = _.rich_text.toolbar_small
			else if (options.toolbar === "full" || options.toolbar === "admin") {
				config.toolbar = _.rich_text.toolbar_full
				if (options.toolbar === "admin") {
					config.toolbar = config.toolbar.slice()
					config.toolbar.push('sourceEditing')
					config.toolbar.push('htmlEmbed')
				}
				config.image = { toolbar: [ 'imageStyle:inline', 'imageStyle:wrapText', 'imageStyle:breakText', '|', 'toggleImageCaption', 'imageTextAlternative' ] }
				config.simpleUpload = { uploadUrl: context + '/ImageUploads' }
				config.table = { contentToolbar: ['toggleTableCaption', 'tableColumn', 'tableRow', 'mergeTableCells', 'tableProperties', 'tableCellProperties'] }
			}
			config.autosave = { save: function(){_.rich_text.update_source_element(el);fetch(context+'/ping')} } // so that the textarea gets updated on blur
			ClassicEditor.create(el, config)
				.then(editor => {
					el.rt.editor = editor
					if (options.focus)
						editor.focus()
				})
				.catch( error => {
	                console.error( error );
	            })
		})
		return el
	},
	destroy(el) {
		if (el && el.rt.editor) {
			let h = el.innerHTML
			el.rt.editor.destroy()
			el.rt.editor = null
			el.innerHTML = h
		}
		_.ui.ok_cancel_remove()
	},
	get_data(el) {
		return el.rt.editor.getData()
	},
	remove(el) {
		el.querySelectorAll('.rt').forEach(e => {
			_.rich_text.destroy(e)
		})
	},
	save(el) {
		_.rich_text.update_source_element(el)
		_.rich_text.destroy(el)
		el.rt.options.on_save(el)
	},
	toolbar_full: [
		'heading',
		'|',
		'bold',
		'italic',
		'fontFamily',
		'fontSize',
		'|',
		'link',
		'|',
		'alignment',
		'bulletedList',
		'numberedList',
		'indent',
		'outdent',
		'|',
		'blockQuote',
		'insertTable',
		'insertImage',
		'mediaEmbed',
		'|',
		'specialCharacters',
		'horizontalLine',
		'|',
		'undo',
		'redo'
	],
	toolbar_small: [
		'bold',
		'italic',
		'|',
		'link',
		'|',
		'bulletedList',
		'numberedList',
		'|',
		'specialCharacters',
		'|',
		'undo',
		'redo'
	],
	update_source_element(el) {
		let data = el.rt.editor.getData()
		if ((data.match(/<p>/g) || []).length === 1)
			data = data.substring(3, data.length - 4) // remove surrounding <p></p>
		el.innerHTML = data
		if (el.rt.options.toolbar === "small") {
			let change = true
			while (change) {
				change = false
				let els = el.querySelectorAll("span, p, strong")
				for (let i=0; i<els.length; i++) {
					let e = els[i]
				    if ((e.textContent === null || e.textContent.trim() === "") && e.childElementCount === 0) {
						e.parentNode.removeChild(e)
						change = true
					}
				}
			}
		}
	}
}
