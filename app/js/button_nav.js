export class ButtonNav {
	constructor(el, buttons, config) {
		this.el = _.$(el)
		this.config = config || {}
		this.el.nav = this
		if (this.config.one_div)
			$div({id:this.el.id+'_div', parentFirst:this.el, styles:{'margin-top':20}})
		this.buttons = $ul({parentFirst:this.el, styles:{padding:0}})
		for (const b of buttons)
			this._add(b)
		if (this.config && this.config.title)
			$a({events:{click:this._manage.bind(this)}, parent:this.buttons}, '...')
	}
	_add(b) {
		let o = this
		let a = $a(
			{classes:['btn', this.config && this.config.outline ? 'btn-outline-primary' : 'btn-primary'],
				'data-text': b.text,
				'data-url': b.url,
				def:b,
				events: {click: function(){o.click(this)}},
				styles:{'align-items':'center', display:'inline-flex', margin:'5px 15px 0 0'}},
			b.icon ? $span({styles:{marginLeft:10}}, b.text) : b.text)
		if (b.icon)
			a.prepend(_.ui.icon(b.icon))
		if (b.hidden)
			a.style.display = 'none'
		if (!this.config.one_div && !b.div)
			b.div = $div({parent:this.el, styles:{display:'none'}})
		this.buttons.appendChild(a)
		if (b.div && b.div.firstChild)
			this._select(a)
		return a
	}
	click(b, f) {
		if (typeof b === 'string') {
			b = this.get(b)
			if (b == null)
				return
		}
		if (b === this.selected)
			return
		this._select(b)
		if (b.def.onclick)
			b.def.onclick()
		else
			app.go_nav('#' + this.el.id, b.dataset.text, b.dataset.url ? b.dataset.url : null, this.config.one_div ? '#'+this.el.id+'_div' : b.def.div, {on_complete:f})
	}
	_click_next(b) {
		let b2 = b.previousSibling
		while (b2) {
			if (b2.style.display != 'none') {
				this.click(b2)
				return
			}
			b2 = b2.previousSibling
		}
		b2 = b.nextSibling
		while (b2 && b2.tagName == 'BUTTON') {
			if (b2.style.display != 'none') {
				this.click(b2)
				return
			}
			b2 = b2.nextSibling
		}
	}
	get(text) {
		for(let i=0; i<this.buttons.children.length; i++)
			if (this.buttons.children[i].textContent === text)
				return this.buttons.children[i]
		return null
	}
	_hide(cb) {
		let text = cb.parentNode.parentNode.previousSibling.innerHTML
		let b = this.get(text)
		b.style.display = 'none'
		if (!this.config.one_div)
			b.def.div.style.display = 'none'
		net.post(context + this.config.url + '/' + text + '/hide')
		if (this.selected == b)
			this._click_next(b)
	}
	_manage() {
		let self = this
		let d = new Dialog({title:this.config.title})
		for (let i=0; i<this.buttons.children.length-1; i++) {
			let b = this.buttons.children[i]
			d.tr(b.innerHTML)
			let div = $div({classes:['form-check','form-switch'], parent:d.body})
			$input({checked:b.style.display!='none', classes:['form-check-input'], events:{change:function(){if(this.checked)self._show(this);else self._hide(this)}}, id:'switch'+i, parent:div, type:'checkbox'})
			$label({classes:['form-check-label'], for:'switch'+i, parent:div})
		}
		d.open()
	}
	refresh(url) {
		if (this.selected) {
			if (url)
				this.selected.def.url = url
			this.click(this.selected)
		}
	}
	_select(b) {
		if (this.selected) {
			this.selected.classList.remove('disabled')
			if (!this.config.one_div) {
				this.selected.def.div.style.display = 'none'
				if (this.selected.def.url)
					_.dom.empty(this.selected.def.div)
			}
		}
		this.selected = b
		b.classList.add('disabled')
		if (!this.config.one_div)
			b.def.div.style.display = b.def.display ? b.def.display : 'block'
	}
	_show(cb) {
		let text = cb.parentNode.parentNode.previousSibling.innerHTML
		let b = this.get(text)
		b.style.display = ''
		this.click(b)
		net.post(context + this.config.url + '/' + text + '/show')
	}
}