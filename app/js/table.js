import { Dialog } from './dialog'

class FilterDropdown {
	constructor(el, column_index) {
		el.fd = this
		app.hide_popups()
		let values = new Set()
		let rows = el.closest('table').tBodies[0].rows
		for (let i=0; i<rows.length; i++)
			values.add(rows[i].cells[column_index].textContent)
		this.ul = $ul({classes:['dropdown-menu'], dropdown:true, styles:{cursor:'pointer', left:'auto', zIndex:app.z_index++}})
		values = Array.from(values)
		values.sort()
		for (let value of values) {
			$li({events:{click:function(){
					el.value = this.textContent
					app.hide_popups()
					_.table.filter(el.closest('table'))
				}},
				parent:this.ul,
				styles:{padding:'0 16px'}},
			value)
		}
		_.dom.popup(this.ul, el, true)
	}
	on_input(el) {
		for (const i of el.fd.ul.children)
			i.style.display = i.textContent.indexOf(el.value) == -1 ? 'none' : ''
	}
}
export class FiltersDialog {
	constructor(view, table, filter_column, filter_op) {
		this.d = new Dialog({modal:true,title:"Filters"})
		this.dd_classes = ['btn','btn-outline-primary','dropdown-toggle']
		this.num_rows = 0
		this.table = table
		this.v = app.get('view:' + view)
		if (this.v.filters && this.v.filters.length)
			for (let i=0; i<this.v.filters.length; i++) {
				let f = this.v.filters[i]
				this.add_filter(f.column.name, f.op, f.value)
			}
		else {
			this.v.and_or = 'And'
			this.add_filter(filter_column ? filter_column : this.v.columns[0].name, filter_op ? filter_op : 'Contains')
		}
		this.d.add_footer()
		this.d.footer.prepend($div({styles:{'flex-grow':2}}))
		this.d.footer.prepend(_.ui.button('add filter', function(){this.add_filter(this.v.columns[0].name, 'Contains')}.bind(this), {classes:['btn','btn-outline-success']}))
	}
	add_filter(column, op, value) {
		this.new_filter(column, op, value)
		this.update_filter()
		return this
	}
	and_or(parent) {
		let fd = this
		_.ui.button(this.v.and_or, function(){new Dropdown().add('And',function(){fd.set_and_or('And')}).add('Or', function(){fd.set_and_or('Or')}).show(this, true)}, {classes:this.dd_classes, parent:parent})
	}
	columns() {
		let d = new Dropdown()
		for (let i=0; i<this.v.columns.length; i++)
			if (this.v.columns[i].filterable)
				d.add(this.v.columns[i].name, this.update_filter.bind(this))
		return d
	}
	filter() {
		let n = 0
		let rows = this.table.tBodies[0].rows
		for (let i=0; i<rows.length; i++) {
			let r = rows[i]
			if (r.cells[0].colSpan > 1 || this.match(r)) {
				r.style.display = ''
				n++
			} else
				r.style.display = 'none'
		}
		_.table.update_num_records(this.v, this.table, n)
	}
	match(row) {
		for (let i=0; i<this.v.filters.length; i++) {
			var m
			let f = this.v.filters[i]
			let v = row.cells[f.column.index].innerText.trim().toLowerCase()
			let fv = f.value.toLowerCase()
			switch (f.op) {
			case 'Is':
				m = v === fv
				break
			case 'Is not':
				m = v !== fv
				break
			case 'Contains':
				m = fv.length == 0 || v.indexOf(fv) != -1
				break
			case 'Does not contain':
				m = v.indexOf(fv) == -1
				break
			case 'Starts with':
				m = v.indexOf(fv) == 0
				break
			case 'Ends with':
				m = v.lastIndexOf(fv) == v.length - fv.length
				break
			case 'Is empty':
				m = v == null || v == ''
				break
			case 'Is not empty':
				m = v.length > 0
			}
			if (m && (this.v.filters.length == 1 || this.v.and_or == 'Or'))
				return true
			if (!m && (this.v.filters.length == 1 || this.v.and_or == 'And'))
				return false
		}
		return this.v.and_or == 'And'
	}
	new_filter(column, op, value) {
		let fd = this
		if (this.num_rows == 0)
			this.d.tr('Where')
		else if (this.num_rows == 1) {
			this.d.tr()
			this.and_or(this.d.body)
			this.d.td()
		} else
			this.d.tr(this.v.and_or)
		if (this.v.columns.length > 1)
			_.ui.button(column, function(){fd.columns().show(this, true)}, {classes:this.dd_classes, parent:this.d.body})
		else
			$span({parent:this.d.body,styles:{'vertical-align':'middle'}}, column)
		this.d.body.append('\u00A0')
		_.ui.button(op, function(){fd.ops().show(this, true)}, {classes:this.dd_classes, parent:this.d.body})
		this.d.body.append('\u00A0')
		let p = {classes:['form-control'], events:{'input':this.update_filter.bind(this)}, styles:{display:'inline', 'vertical-align':'bottom', width:100}}
		if (value)
			p.value = value
		this.d.add_input(null, 'text', p)
		this.d.td()
		_.ui.button(null, function(){fd.remove_filter(this)}, {children:[_.ui.icon('x')], classes:['btn','btn-outline-danger'], parent: this.d.body, styles:{border:'none'}})
		++this.num_rows
	}
	open() {
		this.d.open()
	}
	ops() {
		let fd = this
		let d = new Dropdown();
		['Is', 'Is not', 'Contains', 'Does not contain', 'Starts with', 'Ends with', 'Is empty', 'Is not empty'].forEach(
			s => d.add(s, function(a, b){b.nextElementSibling.style.visibility = a.innerHTML == 'Is empty' || a.innerHTML == 'Is not empty' ? 'hidden' : 'visible';fd.update_filter()}))
		return d
	}
	remove_filter(b) {
		b.closest('tr').remove()
		for (let i=0; i<this.d.tbody.rows.length; i++) {
			let td = this.d.tbody.rows[i].firstChild
			if (i == 0)
				td.innerText = 'Where'
			else if (i == 1) {
				_.dom.empty(td)
				this.and_or(td)
			} else
				td.innerText = this.v.and_or
		}
		this.update_filter()
	}
	set_and_or(and_or) {
		this.v.and_or = and_or
		for (let i=2; i<this.d.tbody.rows.length; i++) {
			let td = this.d.tbody.rows[i].firstChild
			td.innerText = and_or
		}
		this.update_filter()
	}
	update_filter() {
		let f = []
		for (let i=0; i<this.d.tbody.rows.length; i++) {
			let td = this.d.tbody.rows[i].firstChild.nextSibling
			f.push({column:_.view.column(this.v, td.firstChild.innerHTML), op:td.firstChild.nextElementSibling.innerHTML, value:td.lastChild.value})
		}
		this.v.filters = f
		this.v.and_or = f.length <= 1 || this.d.tbody.rows[1].firstChild.firstChild.innerHTML == 'And' ? 'And' : 'Or'
		this.filter()
	}
}
export const qe = {
	columns: [],
	add(t, c) {
		qe.columns[t] = c
	},
	nodeIndex(el) {
		let i = 0
		while (el.previousSibling) {
			i++
			el = el.previousSibling
		}
		return i
	},
	send(vd, c, v, id, c_) {
		net.post(context+'/Views/'+vd+'/update', 'db_key_value='+id+'&'+c+'='+encodeURIComponent(v), c_ ? function(){net.replace(c_);app.publish(vd,'update');} : null)
	},
	send_b(vd, cb, replace) {
		let cols = qe.columns[vd]
		qe.send(vd, cols[qe.nodeIndex(cb.closest('td'))*2], cb.checked, cb.closest('tr').dataset.id, replace ? _.c(cb) : null)
	}
}
export class QuickFilters {
	constructor(view_def_name) {
		let s = _.$('#quickfilter')
		s.removeAttribute('id')
		this.filters = _.c(s).querySelectorAll('select[data-filter]')
		this.view_def_name = view_def_name
		for (let i=0,n=this.filters.length; i<n; i++) {
			let s = this.filters[i]
			s.filter = s.dataset.filter
			delete s.dataset.filter
			s.addEventListener('change', function(){this.send()}.bind(this))
			let t = s.dataset.type
			if (t == 'month')
				this.month_filter = s
			else if (t == 'year') {
				this.year_filter = s
				if (s.nextElementSibling && s.nextElementSibling.tagName === 'svg')
					s.nextElementSibling.addEventListener('click', function(e){this.show_menu(e.target)}.bind(this))
			}
		}
		if (this.month_filter) {
			let m = this.month_filter
			let y = this.year_filter
			if (m.selectedIndex == 11 && (!y || y.selectedIndex == y.options.length-1))
				m.nextSibling.style.display = 'none'
			else
				m.nextSibling.addEventListener('click', this.month_next.bind(this))
			if (m.selectedIndex == 0 && (!y || y.selectedIndex == 0))
				m.previousSibling.style.display = 'none'
			else
				m.previousSibling.addEventListener('click', this.month_prev.bind(this))
		}
	}
	append(w, s) {
		let o = s.options[s.selectedIndex]
		let v = o.value ? o.value : o.text
		if (v !== 'all') {
			if (w)
				w += ' AND '
			w += s.filter + (v === '' || v === 'null' ? ' IS NULL' : '=\'' + v.replaceAll('%27', "''") + '\'')
		}
		return w
	}
	build() {
		let w = '';
		for (let i=0,n=this.filters.length; i<n; i++)
			w = this.append(w, this.filters[i])
		return w
	}
	month_next() {
		let s = this.month_filter
		if (s.selectedIndex == 11) {
			if ('year_filter' in this) {
				let y = this.year_filter
				if (y.selectedIndex < y.options.length - 1) {
					y.selectedIndex++
					s.selectedIndex = 0
				}
			} else
				s.selectedIndex = 0
		} else
			s.selectedIndex++
		this.send()		
	}
	month_prev() {
		let s = this.month_filter
		if (s.selectedIndex == 0) {
			if ('year_filter' in this) {
				let y = this.year_filter
				if (y.selectedIndex > 0) {
					y.selectedIndex--
					s.selectedIndex = 11
				}
			} else
				s.selectedIndex = 11
		} else
			s.selectedIndex--
		this.send()
	}
	replace() {
		net.replace(_.c(this.filters[0]))
	}
	send(w) {
		if (!w)
			w = this.build()
		net.post(context + '/ViewStates/' + this.view_def_name, 'filter=' + w, this.replace.bind(this))
	}
	set_view(v) {
		let w = '';
		for (let i=0,n=this.filters.length; i<n; i++){
			let s = this.filters[i]
			if (s == this.month_filter && v === 'year')
				continue
			w = this.append(w, s)
		}
		if (v === 'month')
			w += ' AND ' + this.year_filter.filter.replace('year', 'month') + '=' + (new Date().getMonth() + 1)
		net.post(context+'/People/data/qf view ' + this.view_def_name + '/' + v, null, function(){this.send(w)}.bind(this))
	}
	show_menu(e) {
		let d = new Dropdown()
		if (!this.month_filter)
			d.add('View by month',function(){this.set_view('month')}.bind(this))
		else
			d.add('View by year',function(){this.set_view('year')}.bind(this))
		d.show(e)
	}
}
export const sorter = {
	click() {
		if (this !== sorter.cur)
			this.innerHTML = '&nbsp;'
		sorter._set(this)
		let t = this.closest('table')
		sorter.sort(t, this, this.innerText === '↓')
		t.parentElement.scrollTop = 0
	},
	_compare(a, b, i, r, s) {
		let x = a.cells[i].textContent.trim()
		let y = b.cells[i].textContent.trim()
		if (x === '')
			return 1
		if (y === '')
			return -1
		if (s === 'date') {
			x = new Date(x).getTime()
			y = new Date(y).getTime()
			let o = x - y
			return r ? -o : o
		}
		x = x.replace(/\$|,|[\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF]/g,'')
		y = y.replace(/\$|,|[\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF]/g,'')
		let t = +x - +y
		let o = isNaN(t) ? sorter._c.compare(x, y) : t
		return r ? -o : o
	},
	init(t, i) {
		t = _.$(t)
		_.$$('th', t).forEach(function(th) {
			let d = $div({html:'&nbsp;', parent:th, styles:{'text-align':'center'}})
			d.addEventListener('click', sorter.click)
			d.addEventListener('mouseout', sorter.mouse_out)
			d.addEventListener('mouseover', sorter.mouse_over)
		})
		sorter._set(_.$('div', _.$('tr', t).cells[i||0]))
	},
	mouse_out() {
		if (this === sorter.cur)
			return
		this.innerHTML = '&nbsp;'
	},
	mouse_over() {
		if (this === sorter.cur)
			return
		this.innerText = '↑'
	},
	_set(div) {
		div.innerText = div.innerHTML === '&nbsp;' ? '↑' : div.innerText === '↑' ? '↓' : '↑'
		if (div !== this.cur) {
			if (this.cur)
				this.cur.innerHTML = '&nbsp;'
			this.cur = div
		}
	},
	sort(table, el, reverse) {
		sorter._c = Intl.Collator(undefined, {ignorePunctuation: true})
		let th = el.closest('th')
		let column_index = th.cellIndex
		let s = th.dataset.sort
		for (let i = 0; i < table.tBodies.length; i++) {
		    let b = table.tBodies[i]
		    let rows = [].slice.call(b.rows, 0)
			let h
			if (rows[0].classList.contains('mosaic-section-head'))
				h = rows.shift()
			rows.sort(function (a, b) {
			    return sorter._compare(a, b, column_index, reverse, s)
			})
			if (h)
				rows.unshift(h)
		    let c = b.cloneNode()
		    c.append.apply(c, rows)
		    table.replaceChild(c, b)
		}
	}
}
export const table = {
	add_attachment(el) {
		let t = _.$(el)
		let tr = _.table.new_row(t)
		$td({parent:tr}, $input({name:'db_attachment' + _.table.num_rows(t), type:'file'}))
		let td = $td({parent:tr})
		_.ui.button(_.text.delete, function(){this.closest('tr').remove()}, {parent:td, styles:{float:'right'}})
	},
	add_filter(view, column, table) {
		new FiltersDialog(view, table, column, 'Contains').open()
	},
	column_index(el, name) {
		let c = el.querySelector('tr').cells
		for (let i=0; i<c.length; i++) {
			if (c[i].dataset.column == name)
				return i
			}
		return -1
	},
	delete_column(el, column) {
		if (typeof column == 'string') {
			column = table.column_index(el, column)
			if (column == -1)
				return
		}
		for (let r of el.rows)
			r.deleteCell(column)
	},
	dialog_add(el, view_def, url, title) {
		if (!title) {
			let view = app.get('view:' + view_def)
			title = view.add_text+' '+view.record_name
		}
		new Dialog({cancel:true, modal:true, ok:{text:_.text.add}, title:title, url:url, owner:_.c(el)})
	},
	dialog_edit(el, view_def, id, url) {
		new Dialog({cancel:true, modal:true, ok:{text:'Save'}, owner:_.c(el), title:'Edit ' + _.db.record_name(view_def, id), url:url})
	},
	dialog_view(el, view_def, url, title) {
		let view = app.get('view:' + view_def)
		new Dialog({cancel:false, max_width:'doc', owner:_.c(el), text:'done', title:title?title:'View '+view.record_name, url:url})
	},
	filter(t) {
		let filters = []
		let heads = t.tHead.rows[0].cells
		for (let i=0; i<heads.length; i++) {
			let input = _.$('input', heads[i])
			if (input && input.value)
				filters.push([i, input.value])
		}
		let rows = t.tBodies[0].rows
		for (const row of rows) {
			if (_.$('input', row.cells[0]).checked)
				continue
			if (filters.length === 0) {
				row.style.display = ''
				continue
			}
			let show = true
			for (const filter of filters)
				if (row.cells[filter[0]].textContent.indexOf(filter[1]) === -1) {
					show = false
					break
				}
			row.style.display = show ? '' : 'none'
		}
	},
	filter_click(el) {
		if (!el.fd)
			new FilterDropdown(el, el.closest('th').cellIndex)
	},
	new_row(t) {
		if (t.firstElementChild)
			return $tr({parent:t.firstElementChild})
		return $tr({parent:$tbody({parent:t})})
	},
	num_rows(t) {
		return t.firstElementChild ? t.firstElementChild.chlidElementLength : 0
	},
	select(t, cell_index, value) {
		let rows = t.tBodies[0].rows
		for (const row of rows)
			if (row.style.display != 'none') {
				let cell = row.cells[cell_index]
				cell.querySelector('input').checked = value
			}
	},
	select_list_click(id, validate) {
		let e = _.$('#select_list_hidden')
		e.value = id
		let d = Dialog.top()
		if (d) { 
			if (d.ok_button)
				d.ok_button.click()
			else
				d.close()
		} else
			_.form.submit(e, {validate:validate})
	},
	toggle_divider(a) {
		let i = a.lastElementChild
		let rotated = _.dom.rotate(i)
		let tr = a.parentNode.parentNode.nextElementSibling
		while (tr != null && !tr.classList.contains('mosaic-section-head')) {
			if (!tr.dataset.depth)
				tr.style.display = rotated ? '' : 'none'
			tr = tr.nextElementSibling
		}
	},
	update_num_records(view, table, n) {
		if (!table.previousSibling)
			return
		let div = table.previousSibling.querySelector('.list_num_records')
		if (div)
			div.innerText = n + ' ' + (n == 1 ? view.record_name : view.record_name_plural)
	},
	write_controls(id, s, p) {
		let from = _.$('#rwc' + id)
		let to = _.$('#rwcs' + id)
		if (from && to) {
			to.innerHTML = from.innerHTML
			from.remove()
			to.removeAttribute('id')
			to.d = {s:s, p:p}
		}
	}
}
export const view = {
	column(v, name) {
		for (let i=0; i<v.columns.length; i++)
			if (v.columns[i].name == name)
				return v.columns[i]
		return null
	}
}
