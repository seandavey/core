export class Nav {
	constructor(items, options) {
		this.options = options || {}
		this.ul = $ul({styles:{'white-space':'nowrap'}})
		if (items)
			for (let i=0; i<items.length; i++)
				this.a(...items[i])
	}
	a(text, on_click, sub_items, div, depth) {
		let li = $li({classes:this.options.no_item_classes?null:['nav-item'], parent:div?div:this.ul})
		let a_styles
		if (sub_items) {
			let i = _.ui.icon('chevron-right', {events:{mouseup:function(e) {
				this.parentNode.parentNode.nextSibling.style.display = _.dom.rotate(this) ? '' : 'none'
				e.stopPropagation()
				return false
			}}, parent:$div({parent:li, styles:{display:'inline-block', width:20}}), styles: {cursor:'pointer', 'font-size':'smaller', marginLeft:depth ? depth*20 : 0, 'padding-bottom':2}})
			if (!this.options.start_collapsed)
				_.dom.rotate(i)
			a_styles = { cursor: 'pointer', display: 'inline-block', padding:0 };
		} else if (this.options.has_subs)
			a_styles = { cursor:'pointer', marginLeft:20, padding:0 }
		else
			a_styles = { cursor: 'pointer', padding:0 }
		$a({classes:[this.options.a_class?this.options.a_class:'nav-link'], events:{click:on_click}, parent:li, styles:a_styles}, text)
		if (sub_items) {
			let styles = {marginLeft:depth ? depth*20 : 20}
			if (this.options.start_collapsed)
				styles.display = 'none'
			let subs_div = $div({parent:div ? div : this.ul, styles:styles})
			for (let i=0; i<sub_items.length; i++)
				this.a(sub_items[i][0], sub_items[i][1], sub_items[i][2], subs_div, depth + 1)
		}
	}
	button(text, on_click) {
		$button({classes:['btn','btn-secondary','btn-sm'], events:{click:on_click}, parent:$li({parent:this.ul})}, text)
	}
	render(div) {
		if (this.options.classes)
			this.ul.classList.add(...this.options.classes)
		if (this.options.styles)
			_.dom.set_styles(this.ul, this.options.styles)
		_.$(div).appendChild(this.ul)
	}
}
export class NavList extends Nav {
	constructor(items, options) {
		super(items, options)
		this.options.classes = ['nav', 'flex-column']
	}
}
export const nav = {
	go(a, target, text, url) {
		_.$$('.navbar-collapse').forEach((e) => {e.style.display='none'})
		let n = a.closest('nav')
		nav._select(n, a)
		app.go_nav('#' + n.id, text, url, target)
	},
	_select(n, a) {
		let e = n.querySelector('.active')
		if (e)
			e.classList.remove('active')
		a.classList.add('active')
	},
	select_by_name(el, name) {
		el = _.$(el)
		let a = el.querySelector('a[data-text="'+name+'"]')
		if (!a) {
			console.log('nav item ' + name + ' not found')
			return
		}
		let n = el.closest('nav')
		let segment = +n.dataset.segment
		if (segment > 0) {
			let s = document.location.pathname.split('/')
			if (name === s[segment].replaceAll('%20', ' ')) {
				nav.go(a, n.dataset.target, name, 'api/' + s.slice(context ? 2 : 1).join('/'))
				return
			}
		}
		a.classList.remove('disabled')
		a.click()
	}
}
