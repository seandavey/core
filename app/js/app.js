export const app = {
	data: {},
	subscribers: {},
	api_url() {
		let url = context + '/api'
		for (var i = 0; i < arguments.length; i++)
			url += '/' + arguments[i]
		return url
	},
	component(el, url) {
		el = _.$(el)
		el.dataset.url = url
		el.classList.add('component')
	},
	//cookie_get(n) {
	//	let value = document.cookie.match('(?:^|;)\\s*'+n+'=([^;]*)')
	//	return value ? unescape(value[1]) : false
	//}
	//cookie_set(n, v, k) {
	//	if (k) {
	//		let d = new Date()
	//		d.setTime(d.getTime() + (365*24*60*60*1000))
	//		document.cookie = n+'='+v+';expires='+d.toGMTString()
	//	} else
	//		document.cookie = n+'='+v
	//},
	dropdown(el) {
		if (window.getComputedStyle(el).display === 'none') {
			app.hide_popups()
			el.style.display = 'block'
		} else
			el.style.display = 'none'
		app.p = el
	},
	get(key) {
		return this.data[key]
	},
	go(url) {
		let a = _.$('#top_menu').querySelector('.active')
		if (a)
			a.classList.remove('active')
		let li = _.$('#top_menu').querySelector('li[data-url="' + url + '"]')
		if (li)
			li.firstChild.classList.add('active')
		_.$$('.navbar-collapse').forEach((e) => {e.style.display='none'})
		net.replace('#page', context + '/-' + url)
		if (!app.onpop)
			history.pushState({url:url}, '', context + '/' + url)
		let i = url.lastIndexOf('/')
		if (i != -1)
			url = url.substring(i+1)
		_menus = [['#top_menu', url]]
		app.subscribers = {}
	},
	go_nav(nav_id, text, url, target, options) {
		let p = document.location.pathname
		if (p.startsWith(url))
			url = p
		net.replace(target, url, options)
		for (let i=0; i<_menus.length; i++)
			if (_menus[i][0] === nav_id) {
				for (let j=_menus.length-1; j>=i; j--)
					_menus.pop()
				break
			}
		url = context + '/' + _menus[0][1]
		for (let i=1; i<_menus.length; i++)
			url += '/' + _menus[i][1]
		if (!app.onpop) {
			let u = url + '/' + text
			history.pushState(nav_id ? {nav:nav_id, text:text} : {url:u}, '', u)
		}
		_menus.push([nav_id, text])
		app.subscribers = {}		
	},
	hide_popups() {
		if (app.tt)
			app.tt.style.visibility = 'hidden'
		if (app.p) {
			if (app.p.dropdown) {
				app.p.remove()
				app.z_index--
			} else {
				app.p.style.display = 'none'
			}
			app.p = null;
		}
	},
	merge(key, data) {
		this.data[key] = this.data[key] && typeof this.data[key] === 'object' ? { ...this.data[key], ...data } : data
	},
	page_menu_select(item) {
		_.nav.select_by_name(_.$('#page_menu'), item)
	},
	pop_state(event) {
		if (Dialog.top())
			Dialog.top().close()
		let s = event.state
		console.log('event state', s)
		if (!s) {
			document.location = document.location
			return
		}
		if (s.dialog && Dialog.top()) {
			Dialog.top().options.on_close = null
			Dialog.top().close()
			return
		}
		app.onpop = true
		if (s.nav)
			_.nav.select_by_name(s.nav, s.text)
		else
			app.go(s.url)
		app.onpop = false
	},
	publish(object, message, data) {
		let a = this.subscribers[object]
		if (a)
			for (let subscriber of a)
				if (subscriber.publish)
					subscriber.publish(message, data)
				else
					subscriber(object, message, data)
	},
	refresh_page_menu(f) {
		let el = _.$('#page_menu')
		if (el)
			net.replace(el.parentElement, null, {on_complete:f})
	},
	refresh_top_menu(f) {
		let el = _.$('#top_menu')
		if (el)
			net.replace(el.parentElement, null, {on_complete:f})
	},
	reload_main() {
		net.replace('#main')
	},
	replace(view_def) {
		const d = _.$$('div[data-view="'+view_def+'"]')
		d.forEach(function(e){net.replace(e)})
	},
	set(key, data) {
		this.data[key] = data
	},
	set_theme(el, t) {
		let ul = el.parentElement.parentElement
		let li = ul.firstChild
		while (li) {
			if (li.id == 'theme_' + t)
				li.classList.add('disabled')
			else
				li.classList.remove('disabled')
			li = li.nextSibling
		}
	    let oldlink = document.getElementsByTagName("link").item(0)
	    let newlink = document.createElement("link")
	    newlink.setAttribute("rel", "stylesheet")
	    newlink.setAttribute("type", "text/css")
	    newlink.setAttribute("href", context+'/themes/'+t+'/'+t+'.css')
	    document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink)
	},
	subscribe(object, subscriber) {
		let a = this.subscribers[object]
		if (!a) {
			a = []
			this.subscribers[object] = a
		}
		if (!a.includes(subscriber))
			a.push(subscriber)
	},
	tooltip(el, html) {
		if (!app.tt) {
			app.tt = $div({styles:{backgroundColor:'lightyellow', border:'1px solid black', color:'black', fontSize:'9pt', padding:1, position:'absolute', visibility:'hidden'}})
			document.body.appendChild(app.tt)
		}
		app.tt.innerHTML = html
		let p = _.dom.pos(el)
		_.dom.set_styles(app.tt, {top:(p.y+el.clientHeight)+'px', left:p.x+'px', visibility:'visible'})
	},
	unsubscribe(object, subscriber) {
		let a = this.subscribers[object]
		if (a) {
			let i = a.indexOf(subscriber)
			if (i != -1)
				a.splice(i, 1)
		}
	},
	z_index: 1100
}

