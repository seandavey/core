['a', 'button', 'div', 'figure', 'figcaption', 'form', 'h5', 'h6', 'img', 'input', 'label', 'li',
	'nav', 'option', 'pre', 'select', 'span', 'table', 'tbody', 'td', 'textarea', 'th', 'tr', 'ul'
].forEach(tag => window['$'+tag] = function(...args) {
	return _.dom.el(tag, typeof args[0] == 'object' && !(args[0] instanceof HTMLElement) ? args.shift() : null, ...args)
})
export const dom = {
//	add_event(el, type, f) {
//		el = _.$(el)
//		if (!el.$events)
//			el.$events = {}
//		el.$events[type] = (el.$events[type] || []).push(f)
//		el.addEventListener(type, f)
//	},
	add_script(js) {
		if (!js)
			return
		let script = document.createElement('script')
		script.setAttribute('type', 'text/javascript')
		script.text = js
		document.head.appendChild(script)
		document.head.removeChild(script)
	},
	at_bottom() {
		return window.scrollY > document.body.clientHeight - window.innerHeight - 10
	},
	children_height(el) {
		let h = 0
		for (var i=0; i<el.children.length; i++)
			h += el.children[i].offsetHeight
		return h
	},
	el(tag, properties, ...children) {
		let el = document.createElement(tag)
		for (const p in properties)
			if (!_.dom.set(el, p, properties))
				el[p] = properties[p]
		for (const c of children)
			if (c)
				el.append(c)
		return el
	},
	empty(el) {
		el = _.$(el)
		while (el.firstChild)
			el.removeChild(el.firstChild)
		return el
	},
	extract_js(html) {
		let js = ''
		html = html.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(_match, code) {
			js += code + '\n'
			return ''
		})
		return {html:html, js:js}
	},
	hide(el) {
		el = _.$(el)
		el.old_display = el.style.display
		el.style.display = 'none'
		if (el.hasAttribute('required')) {
			el.old_required = true
			el.removeAttribute('required')
		}
	},
	popup(el, target, left) {
		let r = target.getBoundingClientRect()
		if (left)
			el.style.left = (r.left + window.scrollX) + 'px'
		else
			el.style.right = (document.body.clientWidth - r.right + window.scrollX) + 'px'
		el.style.display = 'block'
		app.p = el
		if (Dialog.top()) {
			el.style.top = r.bottom + 'px'
			Dialog.top().modal.append(el)
		} else {
			el.style.top = (r.bottom + window.scrollY) + 'px'
			document.body.append(el)
		}
	},
	pos(el) {
		el = _.$(el)
		let r = el.getBoundingClientRect()
		return {x:r.left + window.scrollX, y:r.top + window.scrollY}
	},
	remove_class(el, c) {
		el.classList.remove(c)
		if (el.classList.length == 0)
			el.removeAttribute('class')
	},
//	remove_events(el, type) {
//		el = _.$(el)
//		if (!el.$events)
//			return
//		el.$events[type].forEach(f => el.removeEventListener(type, f))
//		delete el.$events[type]
//	},
	replace(el, html) {
		el = _.$(el)
		let x = _.dom.extract_js(html)
		let is_tr = x.html.startsWith('<tr')
		let d = _.dom.el(is_tr ? 'table' : 'div', {html:x.html})
		el.parentNode.replaceChild(is_tr ? d.querySelector('tr') : d.firstChild, el)
		if (x.js)
			_.dom.add_script(x.js)
	},
	rotate(el, deg) {
		el = _.$(el)
		let rotated = el.style.transform
		el.style.transform = rotated ? '' : 'rotate('+(deg?deg:'90')+'deg)'
		return !rotated
	},
	set(el, p, properties) {
		if (p.startsWith('data-') || p == 'for' || p == 'required') {
			el.setAttribute(p, properties[p])
			return true
		}
		switch(p) {
		case 'before':
			properties.before.parentNode.insertBefore(el, properties.before)
			return true
		case 'children':
			for (var i=0; i<properties.children.length; i++)
				el.appendChild(properties.children[i])
			return true;
		case 'classes':
			if (properties.classes)
				el.classList.add(...properties.classes)
			return true
		case 'events':
			for (const e in properties.events)
				el.addEventListener(e, properties.events[e])
			return true
		case 'html':
			el.innerHTML = properties[p]
			return true
		case 'parent':
			properties.parent.appendChild(el)
			return true
		case 'parentFirst':
			properties.parentFirst.insertBefore(el, properties.parentFirst.firstChild)
			return true
		case 'styles':
			_.dom.set_styles(el, properties.styles)
			return true
		}
		return false
	},
	set_html(el, html) {
		el = _.$(el)
		let x = _.dom.extract_js(html)
		el.innerHTML = x.html
		if (x.js)
			_.dom.add_script(x.js)
		return el
	},
	set_style(el, style, value) {
		el = _.$(el)
		if (typeof value != 'string') {
			if (_.dom.Styles[style] && value) {
				var map = _.dom.Styles[style].split(' ')
				value = (Array.isArray(value) ? value : [value]).map(function(val, i) {
					if (!map[i]) return ''
					return typeof val  == 'number' ? map[i].replace('@', val) : val
				}).join(' ')
			}
		}
		el.style[style] = value
		return el
	},
	set_styles(el, styles) {
		el = _.$(el)
		for (const style in styles)
			_.dom.set_style(el, style, styles[style])
		return el
	},
	show(el) {
		el = _.$(el)
		el.style.display = el.old_display
		if (el.old_required)
			el.setAttribute('required', 'yes')
	},
	size(el) {
		if (el === window)
			return {x: window.innerWidth, y: window.innerHeight}
		if (el === document)
			el = document.body
		else
			el = _.$(el)
		let r = el.getBoundingClientRect()
		return {x: r.width, y: r.height}
	},
	Styles: {
		left: '@px', top: '@px', bottom: '@px', right: '@px', gap: '@px',
		width: '@px', height: '@px', maxWidth: '@px', maxHeight: '@px', minWidth: '@px', minHeight: '@px',
		backgroundColor: 'rgb(@, @, @)', backgroundSize: '@px', backgroundPosition: '@px @px', color: 'rgb(@, @, @)',
		fontSize: '@px', letterSpacing: '@px', lineHeight: '@px', clip: 'rect(@px @px @px @px)',
		margin: '@px @px @px @px', marginBottom: '@px', marginLeft: '@px', marginRight: '@px', marginTop: '@px',
		padding: '@px @px @px @px', paddingBottom: '@px', paddingLeft: '@px', paddingRight: '@px', paddingTop: '@px',
		border: '@px @ rgb(@, @, @) @px @ rgb(@, @, @) @px @ rgb(@, @, @)',
		borderWidth: '@px @px @px @px', borderStyle: '@ @ @ @', borderColor: 'rgb(@, @, @) rgb(@, @, @) rgb(@, @, @) rgb(@, @, @)',
		textIndent: '@px', borderRadius: '@px @px @px @px'
	},
	svg(properties) {
	    var el = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	    for (const p in properties)
			if (!_.dom.set(el, p, properties))
		        el.setAttribute(p, properties[p])  
	    return el;
	}
}