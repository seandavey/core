export function open_print_window(el, f) {
	let src
	for (let s of document.getElementsByTagName('script'))
		if (s.src.indexOf('site-min') != -1) {
			src = s.src
			break
		}
	el = _.$(el)
	let w = window.open('', '_blank')
	w.focus()
	w.context = context
	let d = w.document
	let s = d.createElement('script')
	s.src = src
	d.body.append(s)
	d.head.innerHTML = `
		<style>
			.btn,.db_button_cell,.list_head,.dropdown-menu,.no-print,input,select,label{display:none !important}
			a{color:black;text-decoration:none}
			table.list_table{width:100%}
			table.table{border:1px solid grey;border-collapse:collapse;font-size:9pt;margin:0 auto;width:100%}
			table.table td,table.table th{border:1px solid grey;padding:2px}
			.text-more-link{display:none}
			.text-more-text{display:inline !important}
		</style>`
	d.body.innerHTML = el.outerHTML
	if (f)
		f(w)
}