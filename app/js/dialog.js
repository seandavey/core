export class Dialog {
	constructor(options) {
		this.options = options || {}
		if (!Dialog.stack)
			Dialog.stack = []
		Dialog.stack.push(this)
		this.inputs = {}
		let z_index = app.z_index++
		this.overlay = $div({
			classes: ["modal-backdrop"],
			parent: document.body,
			styles: { bottom: 0, opacity: 0.5, position: "fixed", zIndex: z_index }
		})
		this.modal = $div({
			classes: ["modal"],
			events: {
				click: function(e) {
					app.hide_popups()
					if (!this.options.modal)
						this.cancel()
					e.stopPropagation()
				}.bind(this),
				mouseup: function(e) {
					e.stopPropagation()					
				}
			},
			parent: document.body,
			styles: { "max-width": "none", width: "inherit", zIndex: z_index }
		})
		let dialog = $div({
			classes: options.dialog_class
				? ["modal-dialog", "modal-dialog-centered", options.dialog_class]
				: ["modal-dialog", "modal-dialog-centered"],
			parent: this.modal
		})
		this.content = $div({
			classes: ["modal-content"],
			events: {
				click: function(e) {
					e.stopPropagation()
					if (options.click)
						options.click(e)
				}
			},
			parent: dialog,
			styles: options.styles
		})
		if (options.title || options.print_on_click || !options.hide_close)
			this._add_header()
		this.body = $div({
			classes: ["modal-body"],
			parent: this.content
		})
		if (options.max_width)
			this.body.style["max-width"] = options.max_width === 'doc' ? '1024px' : options.max_width
		if (options.form) {
			this.body = $form({
				action: options.form,
				enctype: "multipart/form-data",
				method: "post",
				parent: this.body
			})
			this.options.ok = true
			this.options.cancel = true
		}
		if (this.options.ok || this.options.cancel || this.options["delete"]) {
			if (options["delete"]) this.add_delete(options["delete"])
			if (this.options.cancel)
				this.add_cancel(
					typeof this.options.cancel === "string"
						? this.options.cancel
						: "Cancel"
				)
			if (this.options.ok) {
				let click
				let text
				if (typeof this.options.ok === "object") {
					text = this.options.ok.text
					if (this.options.ok.click)
						click = this.options.ok.click
				}
				if (!click)
					click = function() {
						let f = this.content.querySelector("form")
						if (f)
							if (this.options.confirm) {
								this.options.confirm.form = f
								this.options.confirm.next = _.form.submit.bind(f, {on_complete:this.ok.bind(this), validate:true})
								this.options.confirm.f(this.options.confirm)
							} else
								_.form.submit(f, {on_complete:this.ok.bind(this), validate:true})
						else
							this.ok()
					}.bind(this)
				this.add_ok(text || _.text.ok, click)
			}
		}
		if (options.component)
			this.body = $div({
				id: "c_",
				parent: this.body,
				url: options.url
			})
		if (options.content)
			this.body.innerHTML = options.content
		this.load()
	}
	add(x) {
		if (typeof x === 'string')
			this.body.insertAdjacentHTML('beforeend', x)
		else
			this.body.appendChild(x)
		return this
	}
	add_cancel(text) {
		this.add_footer()
		$button({
			classes: ["btn", "btn-secondary"],
			events: { click: this.cancel.bind(this) },
			parent: this.footer
		}, text)
		return this
	}
	add_delete(click, text) {
		if (!this.delete) {
			this.add_footer()
			this.delete = $button({
				classes: ["btn", "btn-danger"],
				events: { click: click },
				styles: { marginRight: "auto" }
			}, text || _.text.delete)
			this.footer.prepend(this.delete)
		}
		return this
	}
	add_footer() {
		if (!this.footer)
			this.footer = $div({
				classes: ["modal-footer"],
				parent: this.content
			})
	}
	_add_header() {
		this.header = $div({
			classes: ["modal-header"],
			parentFirst: this.content
		})
		if (this.options.title)
			this.set_title(this.options.title)
		if (this.options.print_on_click)
			_.ui.button(null, this.options.print_on_click, {
				children: [_.ui.icon("printer")],
				classes: ["btn", "btn-outline-primary"],
				parent: this.header
			})
		if (!this.options.hide_close)
			_.ui.button(null, this.cancel.bind(this), {
				classes: ["btn-close"],
				parent: this.header
			})
	}
	add_hidden(name, value) {
		this.add_input(name, "hidden", { value: value })
		return this
	}
	add_input(name, type, properties) {
		properties = properties || {}
		properties.name = name
		properties.parent = this.body
		properties.type = type
		this.inputs[name] = $input(properties)
		return this
	}
	add_ok(text, click) {
		this.add_footer()
		this.ok_button = _.ui.button(text, click.bind(this), {classes: ["btn", "btn-primary"]})
		this.footer.append(this.ok_button)
		if (this.options.ok && typeof this.options.ok === "object" && this.options.ok.hide)
			this.ok_button.style.display = "none"
		return this
	}
	add_select(name, options) {
		let s = _.ui.select(name, options)
		this.body.appendChild(s)
		this.inputs[name] = s
		return this
	}
	static alert(title, text, focus) {
		new Dialog({
			content: text,
			enter_clicks_ok: true,
			focus: focus,
			max_width: '600px',
			ok: true,
			title: title
		}).open()
	}
	cancel() {
		if (this.was_add) {
			let u = this.options.url
			let i = u.indexOf('/component')
			let url = u.substring(0, i == -1 ? u.indexOf('?') : i)
			let view = app.get('view:' + url.substring(url.lastIndexOf('/') + 1).replaceAll('%20',' '))
			net.delete(url + '?id=' + view.id)
		}
		this.close()
	}
	close() {
		if (this.options.on_close)
			this.options.on_close()
		if (this.options.column) {
			let url = context +  '/Views/' + encodeURIComponent(this.options.db_view_def) + '/input?column=' + encodeURIComponent(this.options.column)
			if (this.options.db_key_value)
				url += '&db_key_value=' + this.options.db_key_value
			net.replace(_.$('#' + this.options.column + '_row'), url)
		}
		Dialog.stack.pop()
		if (Dialog.stack.length === 0)
			_.rich_text.remove(this.body)
		this.modal.remove()
		//		this.overlay.addEventListener("transitionend", function(){this.remove()}, false)
		//		this.overlay.style.transition = 'opacity 1s'
		//		this.overlay.style.opacity = 0
		this.overlay.remove()
		app.z_index--
		if (this.options.focus)
			this.options.focus.focus()
	}
	static confirm(title, qestion, ok_text, f) {
		let d = new Dialog({
			cancel: true,
			content: qestion,
			enter_clicks_ok: true,
			hide_close: true,
			max_width: '600px',
			modal: true,
			ok: {
				click: function() {
					f()
					d.close()
				},
				text: ok_text
			},
			title: title
		}).open()
	}
	get_form() {
		return this.body.querySelector('form')
	}
	get_value(name) {
		var e = this.inputs[name]
		if (e.tagName === "SELECT")
			return e.options[e.selectedIndex].value
		return e.value
	}
	load() {
		if (this.options.url)
			net.replace(this.body, this.options.url, {
				to_html: true,
				on_complete: this.open.bind(this)
			})
	}
	ok() {
		if (this.options.reload_page)
			document.location = document.location
		this.close()
		if (this.options.owner)
			net.replace(this.options.owner)
		if (this.options.on_complete)
			this.options.on_complete()
	}
	static on_key_down(e) {
		let d = Dialog.top()
		if (!d) return
		if (e.key === "Enter") {
			if (d.options.enter_clicks_ok) d.ok_button.click()
			return
		}
		if (e.key === "Escape")
			d.cancel()
	}
	open() {
		this.modal.style.display = "block"
		if (this.options.check_for_change) {
			let f = this.modal.querySelector("form")
			if (f)
				f.form_data = new FormData(f)
		}
    //		if (this.body.scrollWidth > this.body.offsetWidth)
	//		this.modal.firstElementChild.style.width = this.body.scrollWidth+20+'px'
		return this
	}
	static progress_end(text) {
		let d = Dialog.top()
		d.body.innerHTML = text
		d.add_ok("ok", d.ok)
	}
	static progress_start(text) {
		new Dialog({ content: text }).open()
	}
	static prompt(text, f, options) {
		options = options || {}
		options.cancel = true
		options.content = text + ' <input/>'
		options.max_width = '600px'
		options.ok = {
			click: function() {
				var t = d.content.querySelector('input').value
				d.close()
				if (t)
					f(t)
			}
		}
		let d = new Dialog(options).open()
	}
	set_title(html, tag) {
		let s = this.options.dont_capitalize ? {} : { "text-transform": "capitalize" }
		if (!this.options.hide_close || this.options.print_on_click)
			s["flex-grow"] = 1
		let t = _.dom.el(tag || "h5", {
			classes: ["modal-title"],
			html: html,
			styles: s
		})
		if (!this.header) {
			this.options.title = null
			this._add_header()
		}
		if (this.title)
			this.title.replace(t)
		else if (this.header.firstChild)
			this.header.insertBefore(t, this.header.firstChild)
		else
			this.header.appendChild(t)
	}
	td() {
		this.body = $td({ parent: this.body.parentNode })
	}
	static top() {
		if (!Dialog.stack || Dialog.stack.length === 0)
			return null
		return Dialog.stack[Dialog.stack.length - 1]
	}
	tr(label) {
		if (!this.tbody)
			this.tbody = $tbody({
				parent: $table({
					classes: ["table", "form_rows"],
					parent: this.body
				})
			})
		let tr = $tr({ parent: this.tbody })
		if (label)
			$td({ parent: tr, html: label })
		this.body = $td({ parent: tr })
		return this
	}
	static upload(a, oc, h) {
		const d = new Dialog({ form: a, on_complete: oc, title: "Upload" })
		d.add_input("file", "file", { required: "true" })
		if (h)
			for (const n in h)
				if (h[n])
					d.add_hidden(n, h[n])
		d.open()
	}
}
document.addEventListener("keydown", Dialog.on_key_down)
