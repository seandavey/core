package app;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import db.DBConnection;
import db.Rows;
import db.SQL;
import db.Select;
import db.ViewDef;

public class Modules {
	public static final Map<String,Module>	modules = new TreeMap<>();

	//--------------------------------------------------------------------------

	public static void
	add(Module module, DBConnection db) {
		module.load(db);
		modules.put(module.getName().toLowerCase(), module);
		module.init(false, db);
	}

	//--------------------------------------------------------------------------

	public static void
	everyFiveMinutes(LocalDateTime now, DBConnection db) {
		for (Module module : modules.values())
			if (module.isActive())
				module.everyFiveMinutes(now, db);
	}

	//--------------------------------------------------------------------------

	public static void
	everyHour(LocalDateTime now, DBConnection db) {
		for (Module module : modules.values())
			if (module.isActive())
				module.everyHour(now, db);
	}

	//--------------------------------------------------------------------------

	public static void
	everyNight(LocalDateTime now, DBConnection db) {
		for (Module module : modules.values())
			if (module.isActive())
				module.everyNight(now, db);
	}

	//--------------------------------------------------------------------------

	public static Module
	get(String name) {
		return modules.get(name.toLowerCase());
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public static <T> Collection<T>
	getByClass(Class<T> module_class) {
		ArrayList<Module> m = new ArrayList<>();
		for (Module module : modules.values())
			if (module_class.isInstance(module) && module.isActive())
				m.add(module);
		return (Collection<T>)m;
	}

	//--------------------------------------------------------------------------

	public static void
	init(DBConnection db) {
		// load modules that aren't added by Site classes but are only in _modules_ table
		try (Rows rows = new Rows(new Select("class,name").from("_modules_"), db)) {
			while (rows.next()) {
				String name = rows.getString(2);
				Module module = modules.get(name.toLowerCase());
				if (module != null)
					continue;
				module = Module.newInstance(name, rows.getString(1));
				if (module != null) {
					module.load(db);
					modules.put(name.toLowerCase(), module);
					module.init(true, db);
				} else {
					Site.site.log("module " + rows.getString(1) + ", " + name + " could not be loaded -- deleted", false);
					db.delete("_modules_", "name=" + SQL.string(name), false);
				}
			}
		}
		for (Module m : modules.values())
			m.init2(db);
// clean _modules_ and reset class names
//		try (Rows rows = new Rows(new Select("name").distinct().from("_modules_"), db)) {
//			while (rows.next()) {
//				String name = rows.getString(1);
//				db.delete("_modules_", "name=" + SQL.string(name), false);
//				Module m = m_modules.get(name.toLowerCase());
//				if (m != null)
//					m.store(db);
//			}
//		}
	}

	//--------------------------------------------------------------------------

	public static boolean
	isActive(String name) {
		Module module = modules.get(name.toLowerCase());
		return module != null && module.isActive();
	}

	//--------------------------------------------------------------------------

	public static ViewDef
	newViewDef(String name) {
		for (Module module : modules.values()) {
			ViewDef view_def = module.newViewDef(name);
			if (view_def != null)
				return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public static void
	remove(Module module) {
		modules.remove(module.getName().toLowerCase());
	}
}
