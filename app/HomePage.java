package app;

import java.lang.reflect.Field;

import com.eclipsesource.json.JsonObject;

import db.DBConnection;
import db.object.JSONField;
import jakarta.servlet.http.Part;
import pages.Page;

@Stringify
public class HomePage extends SiteModule {
	@JSONField(label="null", type="file")
	private String		m_banner = "banner.png";
	@JSONField(fields = {"m_banner"})
	private boolean		m_show_banner;
	@JSONField(fields = {"m_text_box"})
	private boolean		m_show_text_box;
	@JSONField(label="null", type="html")
	private String		m_text_box;

	//--------------------------------------------------------------------------

	public
	HomePage() {
		m_description = "A Home page for the site";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Home", this, false) {
			@Override
			public void
			writeContent(Request r) {
				r.w.h1("Home Page");
			}
		}, db);
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;

		switch(r.getPathSegment(1)) {
		case "Banner":
			try {
				Part part = r.request.getPart("file");
				String filename = part.getSubmittedFileName();
				part.write(Site.site.getPath("images", filename).toString());
				m_banner = filename;
				store(r.db);
				r.response.js("_.$('#banner_img').src='images/" + filename + "'");
			} catch (Exception e) {
				r.log(e);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public final String
	getTextBox() {
		return m_text_box;
	}

	//--------------------------------------------------------------------------

	protected boolean
	writeBlock(JsonObject block, Request r) {
		switch(block.getString("type", null)) {
		case "banner":
			if (m_show_banner && m_banner != null)
				r.w.write("<div style=\"text-align:center\"><img src=\"").write(Site.context).write("/images/" + m_banner + "\" style=\"max-width:100%\"/></div>");
			return true;
		case "html":
			if (m_show_text_box)
				r.w.write("<div class=\"alert alert-secondary ck-content\" style=\"margin:10px auto;\">").write(m_text_box).write("</div></div><div class=\"row\">");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request r) {
		if ("m_banner".equals(field.getName()))
			r.w.setId("banner_img").addStyle("max-height:100px").img("images", m_banner).space().aOnClick("replace", "Dialog.upload(context+'/HomePage/Banner')");
		else
			super.writeSettingsInput(field, r);
	}
}