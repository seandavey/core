package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import db.DBConnection;
import db.OneToMany;
import db.Reorderable;
import db.Rows;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.DBObject;
import db.object.DBObjects;
import db.object.JSONField;
import db.section.Section;
import jakarta.servlet.http.HttpSession;
import pages.Page;
import ui.FormBase.Location;
import ui.NavBar;
import ui.Select;
import ui.Table;
import util.Array;
import util.Text;
import web.HTMLWriter;
import web.JS;

public class Admin extends Module {
	@JSONField
	private String	m_log_path;
	private Page	m_page;

	//--------------------------------------------------------------------------

	private void
	addClassMenuItems(Class<?> c, Map<String,Object> map) {
		try {
			Method[] m = c.getMethods();
			if (m == null)
				return;
			for (Method method : m)
				if (method.isAnnotationPresent(ClassTask.class)) {
					String name = method.getName();
					boolean has_form;
					try {
						c.getMethod(name + "Form", Request.class);
						has_form = true;
					} catch (NoSuchMethodException e) {
						has_form = false;
					}
					if (!has_form)
						for (Class<?> type : method.getParameterTypes())
							if (type == boolean.class || type == int.class || type == String.class) {
								has_form = true;
								break;
							}
					if (has_form)
						map.put(name + "...", "js:admin.new_tab('" + name + "','get_method_form?class=" + c.getSimpleName() + "&method=" + name + "')");
					else
						map.put(name, "js:net.get_html(admin.api_url('action?class=" + c.getSimpleName() + "&method=" + name + "'))");
				}
		} catch (Exception e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addPages(DBConnection db) {
		m_page = new Page("Admin", this, true){
			@Override
			public void
			writeContent(Request r) {
				writePage(r);
			}
		}.addRole("administrator");
		Site.pages.add(m_page, db);
	}

	//--------------------------------------------------------------------------

	static void
	columnAdd(Request r) {
		String column = r.getParameter("column");
		String table = r.getParameter("table");
		String reftable = r.getParameter("reftable");
		r.db.addColumn(table, column, r.getParameter("type"), r.getParameter("size"), reftable == null || reftable.length() == 0 ? null : r.getParameter("reftable"));
		r.releaseViewDef(table);
		if (column.equals("_order_")) {
			ViewDef view_def = Site.site.getViewDef(table, r.db);
			db.Select query = new db.Select("id").from(table);
			List<Section> sections = view_def.getSections();
			Section.addOrderBys(query, sections);
			query.addOrderBy(view_def.getDefaultOrderBy());
			Rows rows = new Rows(query, r.db);
			int order = 1;
			String[] section_values = null;
			if (sections != null)
				section_values = new String[sections.size()];
			while (rows.next()) {
				if (sections != null)
					for (int i=0; i<sections.size(); i++) {
						String value = sections.get(i).getValue(rows, r);
						if (section_values[i] == null || !section_values[i].equals(value)) {
							section_values[i] = value;
							order = 1;
						}
				}
				r.db.update(table, "_order_=" + order++, rows.getInt(1));
			}
			rows.close();
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (!r.userIsAdministrator())
			return false;
		if ("method".equals(path_segments[0])) { // before super.doGet() handles it
			Object object = null;
			String class_name = r.getParameter("class");
			if (class_name.equals("Admin"))
				object = this;
			else if (class_name.equals("Credentials"))
				object = Site.credentials;
			else if (class_name.equals("DBConnection"))
				object = r.db;
			else if (class_name.equals(Site.site.getClass().getSimpleName()))
				object = Site.site;
			MethodForm.doGet(object, path_segments[1], r);
			return true;
		}

		if (super.doAPIGet(path_segments, r))
			return true;

		switch (path_segments[0]) {
		case "column_form": {
			String table = r.getParameter("table");
			String column = r.getParameter("column");
			writeColumnForm(table, column == null ? null : r.db.getJDBCTable(table).getColumn(column), r);
			return true;
		}
		case "edit settings":
			Site.settings.writeComponent(r);
			return true;
		case "edit_table":
			Site.site.newView("admin__" + r.getParameter("table"), r).writeComponent();
			return true;
		case "exportCSV":
			exportCSV(r.getParameter("table"), r.getParameter("filename"), r);
			return true;
		case "generate jdbctable":
			r.w.write("<p><b>JDBCTable</b></p>");
			r.w.write(r.db.getJDBCTable(r.getParameter("table")).getCode());
			return true;
		case "generate sql":
			r.w.write("<p><b>SQL</b></p>");
			r.w.write(r.db.getJDBCTable(r.getParameter("table")).getSQL());
			return true;
		case "get default form": {
			String table = r.getParameter("table");
			writeDefaultForm(table, r.db.getJDBCTable(table).getColumn(r.getParameter("column")), r);
			return true;
		}
		case "get file manager":
			((FileManager)Modules.get("FileManager")).write(r);
			return true;
		case "get log":
			if (m_log_path == null)
				return true;
			r.w.write("<pre>");
			try {
				BufferedReader br = new BufferedReader(new FileReader(m_log_path + "/" + r.getParameter("log")));
				String l = br.readLine();
				while (l != null) {
					r.w.write(l).write("\n");
					l = br.readLine();
				}
				br.close();
			} catch (FileNotFoundException e) {
				r.abort(e);
			} catch (IOException e) {
				r.abort(e);
			}
			r.w.write("</pre>");
			return true;
		case "get logs":
			if (m_log_path == null)
				return true;
			r.w.write("<table border=\"1\"><tr><td style=\"vertical-align: top;\">");
			String[] logs = new File(m_log_path).list();
			if (logs == null || logs.length == 0)
				r.w.write("no logs found in " + m_log_path);
			else {
				for (String log : logs) {
					r.w.aOnClick(log, "net.replace('#log',admin.api_url('get log?log=" + log + "'))");
					r.w.br();
				}
				r.w.write("</td><td id=\"log\" style=\"vertical-align: top;\"></td></tr></table>");
			}
			return true;
		case "get_method_form":
			String class_name = r.getParameter("class");
			String site_class = Site.site.getClass().getName();
			if (class_name == null)
				new MethodForm(Modules.get(r.getParameter("module")), false).write(r.getParameter("method"), r);
			else if (class_name.equals("Admin"))
				new MethodForm(Admin.class).write(r.getParameter("method"), r);
			else if (class_name.equals(site_class.substring(site_class.indexOf('.') + 1)))
				new MethodForm(Site.site.getClass()).write(r.getParameter("method"), r);
			else {
				Class<?>[] classes = Site.site.getClasses();
				for (Class<?> c : classes)
					if (c.getSimpleName().equals(class_name)) {
						new MethodForm(c).write(r.getParameter("method"), r);
						break;
					}
			}
			return true;
		case "get module":
			String module_name = r.getParameter("module");
			Module module = Modules.get(module_name);
			module.writeAdminPane(null, true, true, r);
			return true;
		case "get_object":
			Site.site.newView(r.getParameter("object"), r).writeComponent();
			return true;
		case "get_session_attributes":
			writeSessionAttributes(r);
			return true;
		case "get sql form":
			writeSQLForm(r);
			return true;
		case "get_table":
			writeTablePane(r.getParameter("table"), r);
			return true;
		case "get_view_def_cache":
			writeViewDefCache(r);
			return true;
		case "many_to_many_link_table_create_form":
			writeManyToManyLinkTableCreateForm(r);
			return true;
		case "search":
			writeSearchPane(r);
			return true;
		case "session attributes":
			componentOpen(null, r);
			writeSessionAttributes(r);
			r.w.tagClose();
			return true;
		case "sql": {
			String sql = r.getParameter("sql");
			if (sql != null) {
				r.setSessionAttribute("sql", sql);
				executeSQL(sql, r.db, r.w);
				return true;
			}
			List<String> tables = r.db.getTables(r.getParameter("tables"));
			if (tables.size() == 0)
				r.w.write("no tables match '" + r.getParameter("tables") + "'");
			else
				for (String table : tables) {
					r.w.h4(table);
					executeSQL(new db.Select(r.getParameter("columns")).from(table).where(r.getParameter("where")).orderBy(r.getParameter("order_by")).toString(), r.db, r.w);
				}
			return true;
		}
		case "table_create_form":
			writeTableCreateForm(r);
			return true;
		case "tables with column":
			String column = r.getParameter("column");
			String where = r.getParameter("where");
			List<String> tables = r.db.findTablesWithColumn(column);
			if (where.length() > 0) {
				r.w.h4("Tables with column " + column + " where " + where);
				for (String table : tables) {
					int num_rows = r.db.countRows(table, column + where);
					if (num_rows > 0)
						r.w.write(table).write("(").write(num_rows).write(")").br();
				}
			} else {
				r.w.h4("Tables with column " + column);
				for (String table : tables)
					r.w.write(table).br();
			}
			return true;
//		case "threads":
//			writeThreads(r);
//			return true;
//		case "tomcat state":
//			writeTomcatState(r);
//			return true;
		case "view_table":
			Site.site.newView("admin__" + r.getParameter("table"), r).setMode(Mode.READ_ONLY_LIST).writeComponent();
			return true;
		case "viewdef cache":
			componentOpen(null, r);
			writeViewDefCache(r);
			r.w.tagClose();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIPost(String[] path_segments, Request r) {
		if (!r.userIsAdministrator())
			return false;
		if (super.doAPIPost(path_segments, r))
			return true;

		switch (path_segments[0]) {
		case "clear viewdef cache":
			Site.site.getViewDefs().clear();
			return true;
		case "column":
			switch (path_segments[1]) {
			case "add":
				columnAdd(r);
				return true;
			case "drop":
				r.db.dropColumn(r.getParameter("table"), r.getParameter("column"));
				r.releaseViewDef(r.getParameter("table"));
				return true;
			case "drop not null":
				r.db.setNotNull(r.getParameter("table"), r.getParameter("column"), false);
				return true;
			case "index":
				r.db.createIndex(r.getParameter("table"), r.getParameter("column"));
				return true;
			case "rename":
				r.db.renameColumn(r.getParameter("table"), r.getParameter("column"), r.getParameter("newname"));
				r.releaseViewDef(r.getParameter("table"));
				return true;
			case "set default":
				r.db.setColumnDefault(r.getParameter("table"), r.getParameter("column"), r.getParameter("default"));
				return true;
			case "set not null":
				r.db.setNotNull(r.getParameter("table"), r.getParameter("column"), true);
				return true;
			case "set type":
				r.db.alterColumnType(r.getParameter("table"), r.getParameter("column"), r.getParameter("type"), r.getParameter("size"));
				return true;
			}
			return false;
		case "drop_index":
			r.db.dropIndex(r.getParameter("index"));
			return true;
		case "many_to_many_link_table_create":
			r.db.createManyToManyLinkTable(r.getParameter("table1"), r.getParameter("table2"));
			return true;
		case "set foreign key":
			r.db.setForeignKey(r.getParameter("table"), r.getParameter("column"), r.getParameter("reftable"), r.getParameter("refcolumn"), "cascade".equals(r.getParameter("on_delete")));
			return true;
		case "table":
			switch (path_segments[1]) {
			case "create":
				Exception e = r.db.createTable(r.getParameter("table"), r.getParameter("columns"));
				if (e != null) {
					r.handleException(e);
					return false;
				}
				return true;
			case "delete":
				r.db.delete(r.getParameter("table"), r.getParameter("where"), true);
				return true;
			case "drop":
				String table = r.getParameter("table");
				r.db.dropTable(table);
				r.releaseViewDef(table);
				return true;
			case "rename":
				r.db.renameTable(r.getParameter("table"), r.getParameter("newname"));
				r.w.write(r.getParameter("newname"));
				return true;
			case "truncate":
				r.db.truncateTable(r.getParameter("table"));
				return true;
			}
			return false;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	doDelete(Request r) {
		if (!r.userIsAdministrator())
			return;
		String segment_one = r.getPathSegment(1);
		if (segment_one != null)
			switch (segment_one) {
			case "file_delete":
				new File(r.getParameter("file")).delete();
				return;
			}
	}

	//--------------------------------------------------------------------------

	private void
	executeSQL(String sql, DBConnection db, HTMLWriter w) {
		ResultSet rs = db.select(sql);
		try {
			if (rs == null || !rs.isBeforeFirst())
				return;
			Table table = w.ui.table().addDefaultClasses();
			ResultSetMetaData metadata = rs.getMetaData();
			int num_columns = metadata.getColumnCount();
			for (int i=1; i<=num_columns; i++)
				table.th(metadata.getColumnLabel(i));
			while (rs.next()) {
				table.tr();
				for (int i=1; i<=num_columns; i++)
					table.td(rs.getString(i));
			}
			table.close();
			rs.getStatement().close();
		} catch (SQLException e) {
			w.write(e.toString());
		}
	}

	//--------------------------------------------------------------------------

	private static void
	exportCSV(String table, String filename, Request r) {
		try {
			FileWriter fw = new FileWriter(Site.site.newFile(filename));
			String[] columns = r.db.getJDBCTable(table).getColumnNames();
			for (int i=0; i<columns.length; i++) {
				if (i > 0)
					fw.write(',');
				fw.write(columns[i]);
			}
			fw.write('\n');
			Rows rows = new Rows(new db.Select("*").from(table), r.db);
			while (rows.next()) {
				for (int i=0; i<columns.length; i++) {
					boolean is_numeric = r.db.getJDBCTable(table).getColumn(columns[i]).isNumber();
					if (i > 0)
						fw.write(',');
					String value = rows.getString(columns[i]);
					if (value != null)
						if (!is_numeric) {
							fw.write('"');
							fw.write(Text.escapeDoubleQuotes(value));
							fw.write('"');
						} else
							fw.write(value);
				}
				fw.write('\n');
			}
			rows.close();
			fw.close();
		} catch (IOException e) {
			r.abort(e);
		}
		r.w.write("export done");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Modules.add(new FileManager(), db);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"table","column","map"})
	public void
	mapTable(String table, String column, String map, DBConnection db) {
		StringBuilder s = new StringBuilder();
		String[] ids = map.split(",");
		for (String id : ids) {
			int i = id.indexOf("-");
			s.append(" WHEN ").append(column).append("=").append(id.substring(0, i)).append(" THEN ").append(id.substring(i + 1));
		}
		db.update(table, column + "=CASE " + s.toString() + " ELSE NULL END", null);
	}

	//--------------------------------------------------------------------------

//	public void
//	mapTablesForm(Request r) {
//		Table table = r.w.ui.table().addDefaultClasses();
//		table.tr().td("from table").td();
//		r.w.ui.select(null, r.db.getTableNames(false), null, null, false);
//		table.tr().td("to table").td();
//		r.w.ui.select(null, r.db.getTableNames(false), null, null, false);
//
//		Rows rows = new Rows(new db.Select("*").from(from_table).orderBy("text"), r.db);
//		while (rows.next()) {
//			table.tr().td(rows.getString("text")).td();
//		}
//		rows.close();
//		table.close();
//	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.startsWith("admin__")) {
			String table = name.substring(7);
			ViewDef view_def = new ViewDef(name) {
				@Override
				public View
				newView(Request r) {
					if (!r.userIsAdministrator())
						return null;
					return super.newView(r);
				}
			}.setFrom(table)
				.setRecordName(table, true)
				.setRowWindowSize(200)
				.setShowTableColumnPicker(true, null);
			DBConnection db = new DBConnection();
			JDBCTable jdbc_table = db.getJDBCTable(table);
			if (jdbc_table.getColumn("_order_") != null)
				view_def.setReorderable(new Reorderable());
			for (String column_name : jdbc_table.getColumnNamesAll())
				if (column_name.equals("_owner_"))
					view_def.setColumn(new LookupColumn(column_name, "people", "first,last").setAllowNoSelection(true).setShowFormLink(true));
				else if (column_name.endsWith("_id") && jdbc_table.getColumn(column_name).isInteger()) {
					String one_table = column_name.substring(0, column_name.length() - 3);
					if (one_table.equals("people"))
						view_def.setColumn(new LookupColumn(column_name, one_table, "first,last").setAllowNoSelection(true).setShowFormLink(true));
					else if (db.getJDBCTable(one_table) != null) {
						String[] column_names = db.getJDBCTable(one_table).getColumnNames();
						int i = Array.indexOf(column_names, "name");
						if (i == -1)
							i = Array.indexOf(column_names, "title");
						if (i == -1)
							i = Array.indexOf(column_names, "date");
						if (i == -1)
							i = 0;
						if (column_names.length > 0)
							view_def.setColumn(new LookupColumn(column_name, one_table, column_names[i]).setAllowNoSelection(true).setShowFormLink(true));
					}
				}
			for (String[] exported_key : db.getExportedKeys(table))
				if (exported_key[1].equals(table + "_id"))
					view_def.addRelationshipDef(new OneToMany(exported_key[0]));
			db.close();
			return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask({"table","column","map"})
	public void
	splitManyManyColumn(String table, String column, String map, DBConnection db) {
		String[] items = map.split("\\|");
		for (String item : items) {
			int i = item.indexOf("-");
			String from = item.substring(0, i);
			JDBCTable t = db.getJDBCTable(table);
			String[] c = t.getColumnNames();
			String other_column = c[column.equals(c[0]) ? 1 : 0];
			List<String> other_ids = db.readValues(new db.Select(other_column).from(table).whereEquals(column, from));
			db.delete(table, column + "=" + from, false);
			String[] ids = item.substring(i + 1).split(",");
			for (String id : ids)
				for (String other_id : other_ids)
					db.insert(table, other_column + "," + column, other_id + "," + id);
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeColumnForm(String table, JDBCColumn jdbc_column, Request r) {
		HTMLWriter w = r.w;
		ui.Form f = new ui.Form(null, apiURL("column", (jdbc_column == null ? "add" : "set type")), w).setButtonsLocation(ui.Form.Location.NONE).open();
		w.hiddenInput("table", table);
		if (jdbc_column == null) {
			f.rowOpen("name");
			w.write("<input type=\"text\" name=\"column\" />");
		} else
			w.hiddenInput("column", jdbc_column.name);
		f.rowOpen("type");
		Select types = new Select("type", JDBCColumn.types);
		if (jdbc_column != null) {
			String type_name = jdbc_column.getSQLType();
			int index = type_name.indexOf('(');
			if (index != -1)
				type_name = type_name.substring(0, index);
			types.setSelectedOption(type_name, null);
		}
		types.write(r.w);
		f.rowOpen("size");
		if (jdbc_column != null && jdbc_column.isString() && jdbc_column.size > 0 && jdbc_column.size < 1000)
			w.numberInput("size", "-1", null, "1", Integer.toString(jdbc_column.size), false);
		else
			w.numberInput("size", "-1", null, "1", null, false);
		if (jdbc_column == null) {
			f.rowOpen("references");
			new Select("reftable", r.db.getTableNames(false)).setAllowNoSelection(true).write(r.w);
		}
		f.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeColumns(String table, DBConnection db, HTMLWriter w) {
		JDBCTable jdbc_table = db.getJDBCTable(table);
		String[] column_names = jdbc_table.getColumnNamesAll();

		w.h4("Columns");
		w.write("<table class=\"table table-condensed table-bordered\" style=\"width:auto;\"><tr><th>name</th><th>type</th><th>default</th><th>nullable</th><th>references</th><th>actions</th></tr>");
		for (String column : column_names) {
			w.write("<tr><td onclick=\"admin.column_rename('").write(table).write("','").write(column).write("',function(){admin.load_pane(this)}.bind(this))\" style=\"cursor:pointer;\">")
				.write(column)
				.write("</td><td onclick=\"new Dialog({cancel:true, ok:true, owner:this.closest('.tab-pane'), title:'Edit Column', url:admin.api_url('column_form?table=").write(table).write("&column=").write(column).write("')})\" style=\"cursor:pointer;\">");
			JDBCColumn jdbc_column = jdbc_table.getColumn(column);
			w.write(jdbc_column.getSQLType());
			if (column.equals("id"))
				w.write("</td><td>");
			else
				w.write("</td><td onclick=\"new Dialog({url:admin.api_url('get default form?table=").write(table).write("&column=").write(column).write("'),title:'Set Default',owner:this.closest('.tab-pane'),ok:true,cancel:true})\" style=\"cursor:pointer;\">")
					.write(jdbc_column.getDefaultValue());
			w.write("</td><td>")
				.setAttribute("onchange", "net.post(admin.api_url('column',(this.checked?'drop':'set')+' not null'),{table:'" + table + "',column:'" + column + "'},function(){admin.load_pane(this)}.bind(this))")
				.ui.checkbox(null, null, null, jdbc_column.isNullable(), true, false)
				.write("</td><td>");
			if (jdbc_column.getPrimaryTable() != null)
				w.write(jdbc_column.getPrimaryTable());
			else
				w.nbsp();
			w.write("</td><td>")
				.aOnClick("drop", "admin.column_drop('" + table + "','" + column + "',function(){admin.load_pane(this)}.bind(this))")
				.write(",&nbsp;")
				.aOnClick("index", "net.post(admin.api_url('column','index'),{table:'" + table + "',column:'" + column + "'},function(){admin.load_pane(this)}.bind(this))")
				.write("</td></tr>");
		}
		w.write("</table>");
	}

	//--------------------------------------------------------------------------

	private void
	writeDefaultForm(String table, JDBCColumn jdbc_column, Request r) {
		r.w.write("<form action=\"").write(apiURL("column", "set default")).write("\" method=\"POST\">")
			.hiddenInput("table", table)
			.write("<table style=\"width:100%\">")
			.hiddenInput("column", jdbc_column.name)
			.write("<tr><td class=\"db_form_label\">default</td><td>")
			.ui.textInput("default", 0, jdbc_column.getDefaultValue())
			.write("</td></tr>")
			.write("<tr><td>&nbsp;</td></tr></table></form>");
	}

	//--------------------------------------------------------------------------

	private void
	writeExportedKeys(String table, DBConnection db, HTMLWriter w) {
		List<String[]> exported_keys = db.getExportedKeys(table);
		if (exported_keys.size() > 0) {
			w.h4("Exported Keys")
				.write("<table class=\"table table-condensed table-bordered\" style=\"width:auto;\"><tr><th>table</th><th>column</th><th>delete rule</th></tr>");
			for (String[] exported_key : exported_keys)
				w.write("<tr><td>").write(exported_key[0])
					.write("</td><td>").write(exported_key[1])
					.write("</td><td onclick=\"net.post(admin.api_url('set foreign key'),{table:'" + exported_key[0] + "',column:'" + table + "_id',reftable:'" + table + "',refcolumn:'id',on_delete:'" + (exported_key[2].equals("cascade") ? "set null" : "cascade") + "'},function(){admin.load_pane(this)}.bind(this))\" style=\"cursor:pointer;\">").write(exported_key[2]).write("</td>")
					.write("</tr>");
			w.write("</table>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeIndexes(String table, DBConnection db, HTMLWriter w) {
		try {
			ResultSet indexes = db.getIndexes(table);
			if (!indexes.isBeforeFirst()) {
				indexes.close();
				return;
			}
			w.h4("Indexes");
			w.write("<table class=\"table table-condensed table-bordered\" style=\"width:auto;\"><tr><th>name</th><th>column</th><th>unique</th><th>clustered</th><th>actions</th></tr>");
			while (indexes.next()) {
				String index = indexes.getString("INDEX_NAME");
				w.write("<tr><td>");
				w.write(index);
				w.write("</td><td>");
				w.write(indexes.getString("COLUMN_NAME"));
				String asc_or_desc = indexes.getString("ASC_OR_DESC");
				if (asc_or_desc != null) {
					w.write(" (");
					w.write(asc_or_desc);
					w.write(")");
				}
				w.write("</td><td style=\"text-align:center\">");
				if (!indexes.getBoolean("NON_UNIQUE"))
					w.write("X");
				w.write("</td><td style=\"text-align:center\">");
				if (indexes.getShort("TYPE") == DatabaseMetaData.tableIndexClustered)
					w.write("X");
				w.write("</td><td>");
				w.aOnClick("drop", "net.post(admin.api_url('drop_index'),{index:'" + index + "'},function(){admin.load_pane(this)}.bind(this))");
				w.write("</td></tr>");
			}
			w.write("</table>");
			indexes.close();
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeManyToManyLinkTableCreateForm(Request r) {
		HTMLWriter w = r.w;
		ui.Form f = new ui.Form(null, apiURL("many_to_many_link_table_create"), w).setButtonsLocation(Location.NONE).open();
		f.rowOpen("table 1");
		w.ui.textInput("table1", 0, null);
		f.rowOpen("table 2");
		w.ui.textInput("table2", 0, null);
		f.close();
	}

	//--------------------------------------------------------------------------

	private void
	writePage(Request r) {
		r.w.ui.tabs("admin").setShowClose(true).open().close();
		r.w.js("JS.get('admin-min')");
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		NavBar nav_bar = navBar(null, 1, r).open();
		nav_bar.dropdownOpen("Actions", false);
		TreeMap<String,Object> m = new TreeMap<>();
		addClassMenuItems(Site.site.getClass(), m);
		addClassMenuItems(this.getClass(), m);
		Class<?>[] classes = Site.site.getClasses();
		if (classes != null)
			for (Class<?> c : classes)
				addClassMenuItems(c, m);
		nav_bar.items(m);
		nav_bar.dropdownClose();

		nav_bar.aOnClick("Edit Settings", "admin.new_tab('Edit Settings','edit settings')")
			.aOnClick("File Manager", "admin.new_tab('File Manager','get file manager')");
		if (m_log_path != null)
			nav_bar.aOnClick("Logs", "admin.new_tab('Logs','get logs')");

		nav_bar.dropdownOpen("Modules", false);
		for (Module module : Modules.modules.values())
			if (module.showOnAdminPage()) {
				String text = module.getDisplayName(r);
				if (!text.equals(module.getName()))
					text += " (" + module.getName() + ")";
				nav_bar.aOnClick(text, "admin.new_tab(" + JS.string(module.getName()) + ",'get module?module=" + JS.escape(module.getName()) + "')");
			}
		nav_bar.dropdownClose();

		Collection<DBObjects<?>> objects_list = Site.site.getAllObjects();
		if (objects_list.size() > 0) {
			nav_bar.dropdownOpen("Objects", false);
			for (DBObjects<?> objects : objects_list)
				nav_bar.aOnClick(objects.getName(), "admin.new_tab(" + JS.string(objects.getName()) + ",'get_object?object=" + JS.escape(objects.getName()) + "')");
			nav_bar.dropdownClose();
		}
		nav_bar.aOnClick("Search", "admin.new_tab('Search','search')")
			.aOnClick("Settings", "admin.new_tab('Settings','settings')")
			.aOnClick("SQL", "admin.new_tab('SQL','get sql form')");

		nav_bar.dropdownOpen("State", false)
			.aOnClick("Session Attributes", "admin.new_tab('Session Attributes','session attributes')")
//			.aOnClick("Threads", "admin.new_tab('Threads','threads')")
//			.aOnClick("Tomcat State", "admin.new_tab('Tomcat State','tomcat state')")
			.aOnClick("Viewdef Cache", "admin.new_tab('Viewdef Cache','viewdef cache')")
			.dropdownClose();

		nav_bar.dropdownOpen("Tables", false)
			.aOnClick("Create table", "admin.create_table()")
			.aOnClick("Create many to many link table", "admin.create_many_many()");
		List<String> tables = r.db.getTableNames(false);
		if (tables != null) {
			nav_bar.divider();
			for (String table : tables)
				nav_bar.aOnClick(table, "admin.new_tab('" + table + "','get_table?table=" + table + "')");
		}
		nav_bar.dropdownClose()
			.close();
		return nav_bar;
	}

	//--------------------------------------------------------------------------

	private void
	writeSearchPane(Request r) {
		HTMLWriter w = r.w;
		w.write("Column: <input /><br />")
			.write("Where: <input /><br />")
			.ui.buttonOnClick(Text.search, """
				var w=this.previousElementSibling.previousElementSibling
				var c=w.previousElementSibling.previousElementSibling
				net.replace('#tables_with_column','""" + absoluteURL("tables with column") + "?column='+encodeURIComponent(c.value)+'&where='+encodeURIComponent(w.value))")
			.write("<div id=\"tables_with_column\"></div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeSessionAttributes(Request r) {
		String remove = r.getParameter("remove");
		if (remove != null)
			r.removeSessionAttribute(remove);
		HTMLWriter w = r.w;
		w.h3("Session Attributes");
		Table table = w.ui.table().addDefaultClasses().addStyle("width", "auto");
		table.th("name").th("value").th("action");
		HttpSession session = r.request.getSession();
		ArrayList<String> l = Collections.list(session.getAttributeNames());
		Collections.sort(l);
		for (String name : l) {
			Object value = session.getAttribute(name);
			table.tr().td(name).td();
			DBObject.write(value, w);
			table.td();
			w.aOnClick("remove", componentReplaceJS("get_session_attributes", "remove=" + name, r));
		}
		table.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeSQLForm(Request r) {
		r.w.write("""
				<form>
				Select <input id="columns" name="columns" onchange="setItem(this)"/>
				from <input id="tables" name="tables" onchange="setItem(this)"/>
				where <input id="where" name="where" onchange="setItem(this)"/>
				order by <input id="order_by" name="order_by" onchange="setItem(this)"/>""")
			.nbsp()
			.ui.buttonOnClick("Execute", "admin.execute(this)")
			.write("</form>")
			.br().br()
			.setAttribute("onchange", "setItem(this)")
			.textAreaOpen("sql", "5", "80")
			.tagClose()
			.nbsp()
			.ui.buttonOnClick("Execute", """
				var p=this.closest('.tab-pane')
				net.replace(p.querySelector('#results'),admin.api_url('sql?sql='+encodeURIComponent(p.querySelector('textarea').value)))""")
			.write("<div id=\"results\"></div>")
			.js("""
				function getItem(key) {
					_.$('#'+key).value = localStorage.getItem(key)
				}
				function setItem(e) {
					localStorage.setItem(e.id,e.value)
				}
				getItem('sql')
				getItem('columns')
				getItem('tables')
				getItem('where')
				getItem('order_by')""");
	}

	//--------------------------------------------------------------------------

	private void
	writeTableCreateForm(Request r) {
		HTMLWriter w = r.w;
		ui.Form f = new ui.Form(null, apiURL("table", "create"), w).setButtonsLocation(Location.NONE).open();
		f.rowOpen("table");
		w.ui.textInput("table", 0, null);
		f.rowOpen("columns");
		w.ui.textInput("columns", 0, null);
		f.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeTablePane(String table, Request r) {
		int num_rows = r.db.countRows(table, null);
		HTMLWriter w = r.w;

		w.h3(table);
		NavBar nav_bar = w.ui.navBar("table_menu", null, 0, r).open()
			.aOnClick("add column", "new Dialog({url:admin.api_url('column_form?table=" + table + "'),title:'Add Column',owner:this.closest('.tab-pane'),ok:true,cancel:true})")
			.aOnClick("delete where...", "admin.table_delete('" + table + "')")
			.aOnClick("delete all rows", "admin.truncate_table('" + table + "')")
			.aOnClick("drop", "admin.table_drop('" + table + "')");
		if (r.db.hasColumn(table, "id"))
			nav_bar.aOnClick("edit " + num_rows + (num_rows == 1 ? " row" : " rows"), "admin.new_tab('" + table + "','edit_table?table=" + table + "')");
		else
			nav_bar.aOnClick("view " + num_rows + (num_rows == 1 ? " row" : " rows"), "admin.new_tab('" + table + "','view_table?table=" + table + "')");
		nav_bar.aOnClick("rename", "admin.table_rename('" + table + "')")
			.dropdownOpen("generate", false)
				.aOnClick("JDBCTable", "admin.load_in_pane(this, admin.api_url('generate jdbctable?table=" + table + "'))")
				.aOnClick("SQL", "admin.load_in_pane(this, admin.api_url('generate sql?table=" + table + "'))")
			.dropdownClose();
		nav_bar.close();

		writeColumns(table, r.db, w);
		writeIndexes(table, r.db, w);
		writeExportedKeys(table, r.db, w);

		r.w.write("<div></div>");
	}

	//--------------------------------------------------------------------------

//	private void
//	writeThreads(Request r) {
//		r.w.h3("Threads");
//		List<Thread> threads = Site.site.getThreads();
//		Table table = r.w.ui.table().addClass("table table-condensed table-striped").addStyle("width", "auto");
//		for (Thread thread : threads) {
//			table.tr().td(thread.getName()).td(thread.getState().toString());
//			table.td();
//			StackTraceElement[] st = thread.getStackTrace();
//			for (StackTraceElement ste : st) {
//				r.w.write(ste.toString());
//				r.w.br();
//			}
//		}
//		table.close();
//	}

	//--------------------------------------------------------------------------

//	private void
//	writeTomcatState(Request r) {
//		DecimalFormat df = new DecimalFormat();
//		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
//
//		try {
//			r.w.h3("Tomcat");
//			r.w.h4("Memory");
//			Table table = r.w.ui.table().addClass("table table-condensed table-striped").addStyle("width", "auto");
//			Runtime runtime = Runtime.getRuntime();
//			table.tr().td("max").td(df.format(runtime.maxMemory()));
//			table.tr().td("total").td(df.format(runtime.totalMemory()));
//			table.tr().td("free").td(df.format(runtime.freeMemory()));
//			table.tr().td("used").td(df.format(runtime.totalMemory() - runtime.freeMemory()));
//			table.close();
//
//			r.w.h4("Sessions");
//			String context = Site.context;
//			if (context == null || context.length() == 0)
//				context = "/";
//			ObjectName objectName = new ObjectName("Catalina:type=Manager,context=" + context + ",host=localhost");
//			table = r.w.ui.table().addClass("table table-condensed table-striped").addStyle("width", "auto");
//			table.tr().td("count").td(mBeanServer.getAttribute(objectName, "sessionCounter").toString());
//			table.tr().td("active").td(mBeanServer.getAttribute(objectName, "activeSessions").toString());
//			table.tr().td("expired").td(mBeanServer.getAttribute(objectName, "expiredSessions").toString());
//			table.close();
//		} catch (MalformedObjectNameException | AttributeNotFoundException | InstanceNotFoundException | MBeanException | ReflectionException e) {
//			Site.site.log(e);
//		}
//	}

	//--------------------------------------------------------------------------

	public final void
	writeUserDropdownItems(NavBar nav_bar, Request r) {
		m_page.a(nav_bar, r);
		nav_bar.aOnClick("Clear ViewDef Cache", "net.post(admin.api_url('clear viewdef cache'))")
			.aOnClick("Impersonate", "var d=new Dialog({cancel:true,ok:{click:function(){var s=Dialog.top().body.querySelector('select');document.location=context+'/impersonate/'+s.options[s.selectedIndex].value}},title:'Impersonate',url:context+'/people select'})")
			.divider();
	}

	//--------------------------------------------------------------------------

	private void
	writeViewDefCache(Request r) {
		Map<String,ViewDef> view_defs = Site.site.getViewDefs();
		String remove = r.getParameter("remove");
		if ("all".equals(remove))
			view_defs.clear();
		else if (remove != null)
			r.releaseViewDef(remove);
		HTMLWriter w = r.w;
		if (view_defs.size() > 1)
			w.addStyle("float:right")
				.aOnClick(Text.remove_all, componentReplaceJS("get_view_def_cache", "remove=all", r));
		w.h3("ViewDef Cache");
		Table table = w.ui.table().addDefaultClasses().addStyle("width", "auto");
		for (String name : view_defs.keySet()) {
			table.tr().td(name).td();
			DBObject.write(view_defs.get(name), w);
			table.td();
			w.aOnClick(Text.remove, componentReplaceJS("get_view_def_cache", "remove=" + name, r));
		}
		table.close();
	}
}
