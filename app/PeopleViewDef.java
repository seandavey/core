package app;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import db.NameValuePairs;
import db.OneToMany;
import db.SQL;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.EmailColumn;
import db.column.OptionsColumn;
import db.column.PasswordColumn;
import db.column.PictureColumn;
import db.column.UsernameColumn;
import util.Text;

@Stringify
public class PeopleViewDef extends ViewDef {
	public
	PeopleViewDef(String name, AccessPolicy access_policy, int user_pic_size) {
		super(name);
		setAccessPolicy(access_policy);
		setDefaultOrderBy("LOWER(first),LOWER(last)");
		setRecordName("Person", true);
		setColumn(new Column("active").setDefaultValue(true).setViewRole("people"));
		setColumn(new EmailColumn("email"));
		setColumn(new Column("first"));
		setColumn(new Column("json").setIsHidden(true));
		setColumn(new Column("last"));
		setColumn(new Column("last_login").setIsReadOnly(true).setHideOnForms(Mode.ADD_FORM));
		setColumn(new PasswordColumn("password"));
		setColumn(new PictureColumn("picture", "people", "people", 0, 0).setImageSize(user_pic_size).setDirColumn("id"));
		{
			Set<String> zone_ids = ZoneId.getAvailableZoneIds();
			String[] a = zone_ids.toArray(new String[zone_ids.size()]);
			Arrays.sort(a);
			String tz = Site.site.getTimeZone();
			setColumn(new OptionsColumn("timezone", a).setAllowNoSelection(true).setHelpText("only set this if you are in a timezone other than " + (tz == null ? "the one for the site" : tz)));
		}
		setColumn(new UsernameColumn("user_name").setDisplayName("username")); // TODO: change column name to username
		if (!name.equals("person settings"))
			addRelationshipDef(new OneToMany("user_roles").setViewRole("admin"));
		{
			ArrayList<String> a = new ArrayList<>();
			a.add("Current Members|active AND user_name != 'admin' AND password IS NOT NULL");
			a.add("Past Members|NOT active AND user_name != 'admin' AND password IS NOT NULL");
			if (name.equals("people edit") && Site.site.allowAccountRequests())
				a.add("Account Requests|password IS NULL");
			m_filters = a.toArray(new String[a.size()]);
		}
	}

	//--------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		if (Site.site.getDefaultRole() != null)
			r.db.insert("user_roles", "people_id,user_name,role", id + ",'" + nvp.getString("user_name") + "','" + Site.site.getDefaultRole() + "'");
		super.afterInsert(id, nvp, r);
	}

	//--------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		if (nvp.containsName("active")) {
			boolean active = nvp.getBoolean("active");
			if (active != (Boolean)previous_values.get("active"))
				if (!active) {
					String uuid = Text.uuid();
					nvp.set("password", uuid);
					nvp.set("user_name", uuid);
					r.db.delete("user_roles", "people_id", id, false);
				} else
					r.db.insert("user_roles", "people_id,user_name,role", id + ",'" + nvp.getString("user_name") + "','" + Site.site.getDefaultRole() + "'");
		}
		String new_user_name = nvp.getString("user_name");
		String old_user_name = (String)previous_values.get("user_name");
		Person user = r.getUser();
		if (user != null && !user.getUsername().equals(old_user_name)) {
			r.removeSessionAttribute("user");
			r.removeSessionAttribute("avatar");
		}
		if (new_user_name != null && !new_user_name.equals(old_user_name)) {
			r.db.update("user_roles", "user_name=" + SQL.string(new_user_name), "user_name=" + SQL.string(old_user_name));
			if (r.getUser().getUsername().equals(old_user_name))
				r.request.getSession().invalidate();
		}
		Site.site.clearName(id);
		super.afterUpdate(id, nvp, previous_values, r);
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs nvp, Request r) {
		if (r.db.hasColumn("people", "active") && !nvp.containsName("active"))
			nvp.set("active", true);
		nvp.set("password", "md5(" + SQL.string(nvp.getString("password")) + ")");
		return super.beforeInsert(nvp, r);
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		previous_values.put("active", r.db.lookupBoolean(new Select("active").from("people").whereIdEquals(id)));
		previous_values.put("user_name", r.db.lookupString(new Select("user_name").from("people").whereIdEquals(id)));
		return super.beforeUpdate(id, nvp, previous_values, r);
	}
}
