package app;

import java.util.Enumeration;

public class Debug {
	public static void
	dump(Request r) {
		System.out.println("url: " + r.request.getRequestURL().toString());
		System.out.println("query string: " + r.request.getQueryString());
		Enumeration<String> e = r.request.getParameterNames();
		while (e.hasMoreElements()) {
			String name = e.nextElement();
			System.out.println(name + "=" + r.request.getParameter(name));
		}
	}
}
