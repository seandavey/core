package app;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.LocalTime;

import db.DBConnection;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import util.Time;

public class Request extends web.Request {
	public final DBConnection	db;
	public int					one_id; // set when writing many table row

	//--------------------------------------------------------------------------

	public
	Request(HttpServletRequest request, String[] path_segments, HttpServletResponse response) throws IOException {
		this(request, path_segments, response, response.getWriter());
	}

	//--------------------------------------------------------------------------

	public
	Request(HttpServletRequest request, String[] path_segments, HttpServletResponse response, Writer writer) {
		super(request, path_segments, response, writer);
		Site.getInstance(request);
		db = new DBConnection();
		String set_uuid = getCookie("setuuid");
		if ("true".equals(set_uuid)) {
			Person user = getUser();
			if (user != null) {
				response.addHeader("Set-Cookie", "setuuid=; expires=Thu, 01 Jan 1970 00:00:00 GMT");
				response.addHeader("Set-Cookie", "uuid=" + user.getUUID(db) + "; Path=" + Site.context + "; Max-Age=" + 365*24*60*60*1000 + "; SameSite=lax");
			}
		}
	}

	//--------------------------------------------------------------------------

	public void
	abort(Exception e) {
		db.close();
		log(e);
		if (e instanceof RuntimeException)
			throw (RuntimeException)e;
		throw new RuntimeException(e);
	}

	//--------------------------------------------------------------------------

	public void
	abort(String message) {
		db.close();
		log(message, false);
		throw new RuntimeException(message);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	close() {
		super.close();
		db.close();
	}

	//--------------------------------------------------------------------------

	public LocalDate
	getDate(String name) {
		String date = getParameter(name);
		if (date == null || date.length() == 0)
			return null;
		return LocalDate.parse(date);
	}

	//--------------------------------------------------------------------------

	public LocalTime
	getTime(String name) {
		String time = getParameter(name);
		if (time == null || time.length() == 0)
			return null;
		return Time.formatter.parseTime(time);
	}

	//--------------------------------------------------------------------------

	public Object
	getOrCreateState(Class<?> state_class, String name) {
		String class_name = state_class.getName();
		int last_dot = class_name.lastIndexOf('.');
		if (last_dot != -1)
			class_name = class_name.substring(last_dot + 1);
		String key = class_name + ":" + name;
		Object state = getSessionAttribute(key);
		if (state == null) {
			state = Site.site.newState(state_class, name, this);
			setSessionAttribute(key, state);
		}
		return state;
	}

	//--------------------------------------------------------------------------

	public Person
	getUser() {
		if (request.getRemoteUser() == null)
			return null;
		Avatar avatar = (Avatar)getSessionAttribute("avatar");
		if (avatar != null)
			return avatar.getPerson();
		Person user = (Person)getSessionAttribute("user");
		if (user == null) {
			user = Site.site.newPerson(this);
			setSessionAttribute("user", user);
			Site.site.onLogin(user, this);
		}
		return user;
	}

	//--------------------------------------------------------------------------

	public void
	handleException(Exception e) {
		db.close();
		String message = e.getMessage();
		if (message == null)
			message = e.toString();
		log(e);
		response.addError(message.replace('\n', ' '));
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	log() {
		super.log();
		System.out.println(Site.site.getDisplayName());
		Person user = getUser();
		if (user != null)
			System.out.println(user.getUsername());
	}

	//--------------------------------------------------------------------------

	public void
	release() {
		db.close();
		response.release(w);
	}

	//--------------------------------------------------------------------------

	public void
	releaseViewDef(String name) {
		Site.site.removeViewDef(name);
		Site.site.removeViewDef("admin__" + name);
		removeSessionAttribute("ViewState:" + name);
		removeSessionAttribute("ViewState:admin__" + name);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	userHasRole(String role) {
		Avatar avatar = (Avatar)getSessionAttribute("avatar");
		if (avatar != null)
			return avatar.hasRole(role, db);
		return super.userHasRole(role);
	}

	//--------------------------------------------------------------------------

	public boolean
	userIsAdmin() {
		return userHasRole("admin") || userHasRole("administrator");
	}

	//--------------------------------------------------------------------------

	public boolean
	userIsAdministrator() {
		return userHasRole("administrator");
	}

	//--------------------------------------------------------------------------

	public boolean
	userIsGuest() {
		return userHasRole("guest");
	}
}