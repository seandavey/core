package app;

import java.time.ZoneId;
import java.util.Map;
import java.util.TreeMap;

import db.DBConnection;
import db.Validation;
import pages.Page;
import ui.Card;
import ui.NavBar;
import ui.Select;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;

public class EditSite extends Module {
	protected void
	addItems(Map<String,String> items, Request r) {
		String root = Site.settings.getString("file manager root");
		if (root != null)
			items.put("File Manager", null);
		items.put("Modules", null);
		items.put("Site", null);
		for (Module module: Modules.modules.values()) {
			String[][] module_config_tasks = module.getConfigTasks(r);
			if (module_config_tasks != null)
				for (String[] label_path : module_config_tasks)
					items.put(label_path[0], module.apiURL(label_path[1]));
		}
	}

	//--------------------------------------------------------------------------

	protected boolean
	addItemToNavBar(String item, NavBar nav_bar, Request r) {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "File Manager":
			((FileManager)Modules.get("FileManager")).setRoot(Site.settings.getString("file manager root")).write(r);
			return true;
		case "Modules":
			writeModulesPane(r);
			return true;
		case "Site":
			r.w.h2("Site Settings");
			ui.Form f = new ui.Form(m_name, Site.context + "/EditSite/Site", r.w).open();
			f.rowOpen("time zone");
			Select select = new Select("time zone", ZoneId.getAvailableZoneIds()).setOnChange("if(this.nextSibling)this.nextSibling.remove()").setAllowNoSelection(true);
			String time_zone = Site.settings.getString("time zone");
			if (time_zone != null)
				select.setSelectedOption(time_zone, null);
			select.write(r.w);
			r.w.write(" Server Time: ").writeTime(Time.newTime());
			f.rowOpen("title");
			r.w.setAttribute("required", "yes");
			r.w.textInput("title", 0, Site.site.getDisplayName());
			f.close();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		switch (r.getPathSegment(1)) {
		case "Modules":
			String module_name = r.getPathSegment(2);
			SiteModule module = (SiteModule)Modules.get(module_name);
			module.setActive(r.getBoolean("active"), r.db);
			return true;
		case "Site":
			r.response.js("_.form.show_saved('#" + Validation.getValidIdentifier("db_submit_id", r) + "')");
			Site.site.setTimeZone(r.getParameter("time zone"), r.db);
			Site.settings.set(Site.SITE_DISPLAY_NAME, r.getParameter("title"), r.db);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		Site.site.addUserDropdownItem(new Page("Edit Site", this, true).addRole("admin"));
	}

	//--------------------------------------------------------------------------

	private void
	writeModulesPane(Request r) {
		HTMLWriter w = r.w;
		for (Module module : Modules.modules.values()) {
			if (!(module instanceof SiteModule))
				continue;
			Card card = w.ui.card();
			String header = module.getDisplayName(r);
			if (r.userIsAdministrator() && !header.equals(module.getClass().getSimpleName()) && module.getClass().getName().indexOf('$') == -1)
				header += " (" + module.getClass().getName() + ")";
			card.header(header)
				.bodyOpen();
			w.write(((SiteModule)module).getDescription())
				.br()
				.write("<input type=\"checkbox\" onchange=\"net.post(context+'/EditSite/Modules/")
				.write(module.getName())
				.write("','active=' + (this.checked ? 'true' : 'false'),app.refresh_top_menu)\"");
			if (module.isActive())
				w.write(" checked");
			w.write("/> active ")
				.addStyle("margin-left:10px")
				.ui.buttonOnClick(Text.settings, "new Dialog({cancel:true,modal:true,ok:true,title:'" + JS.escape(module.getDisplayName(r)) + " Settings',url:'" + module.apiURL("settings") + "'})");
			card.close();
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		Map<String,String> items = new TreeMap<>();
		NavBar nav_bar = navBar("main", 1, r).open();
		addItems(items, r);
		for (String key : items.keySet())
			if (!addItemToNavBar(key, nav_bar, r)) {
				String value = items.get(key);
				if (value != null && value.endsWith("/settings"))
					nav_bar.aOnClick(key, "new Dialog({cancel:true,modal:true,ok:true,title:'" + JS.escape(key) + " Settings',url:'" + value + "'})");
				else
					nav_bar.item(key, value);
			}
		nav_bar.close();
		return nav_bar;
	}
}
