package app;

import java.io.IOException;
import java.util.Arrays;

import db.View;
import db.ViewState;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pages.Page;

@MultipartConfig
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//--------------------------------------------------------------------

	@Override
	protected void
	doDelete(HttpServletRequest http_request, HttpServletResponse http_response) throws ServletException, IOException {
		if (http_request.getRemoteUser() == null)
			return;
		http_response.setContentType("application/json");
		Request r = new Request(http_request, null, http_response);
		String segment_zero = r.getPathSegment(0);
		if (segment_zero == null)
			return;
		Site.getInstance(http_request); // make sure it's loaded
		switch(segment_zero) {
		case "Views":
			View.doDelete(r);
			r.response.ok();
			break;
		default:
			Module module = Modules.get(segment_zero);
			if (module != null) {
				module.doDelete(r);
				r.response.ok();
			}
		}
		r.release();
	}

	//--------------------------------------------------------------------

	@Override
	protected void
	doGet(HttpServletRequest http_request, HttpServletResponse http_response) throws ServletException, IOException {
		String path = http_request.getServletPath();
		if (path.endsWith("/none") || path.endsWith("/ping"))
			return;
		if (path.startsWith("/images") || path.startsWith("/js") || path.startsWith("/css") || path.startsWith("/themes")) {
			getServletContext().getNamedDispatcher("default").forward(http_request, http_response);
			return;
		}

		Site site = Site.getInstance(http_request);
		if (path.equals("/") || path.indexOf("j_security_check") != -1) {
			http_response.sendRedirect(http_request.getContextPath() + "/" + site.getHomePage());
			return;
		}

		boolean is_user = http_request.getRemoteUser() != null;
		if (path.equals("/login")) {
			if (!is_user && !http_request.authenticate(http_response))
				return;
			http_response.sendRedirect(http_request.getContextPath() + "/" + site.getHomePage());
		}
		if (path.equals("/logout")) {
			http_request.getSession().invalidate();
			http_response.sendRedirect(http_request.getContextPath() + "/" + site.getHomePage());
			return;
		}

		http_request.setCharacterEncoding("UTF-8");
		http_response.setCharacterEncoding("UTF-8");

		String[] path_segments = Request.getPathSegments(http_request);

		if (site.doGet(http_request, path_segments, http_response))
			return;

		if (path_segments.length > 1 && "attachments".equals(path_segments[1])) { // attachment may not have an extension
			getServletContext().getNamedDispatcher("default").forward(http_request, http_response);
			return;
		}

		boolean full_page = path_segments[0].charAt(0) != '-';
		if (!full_page)
			path_segments[0] = path_segments[0].substring(1);

		Module module = Modules.get(path_segments[0]);
		if (module != null)
			if ((is_user || !module.isSecured()) && module.doGet(http_request, path_segments, http_response))
				return;

		if (path.lastIndexOf('.') != -1 && path.charAt(path.length() - 1) != 'Z') { // Z is last char in UTC timestamp which has a . in it
			getServletContext().getNamedDispatcher("default").forward(http_request, http_response);
			return;
		}

		Request r = new Request(http_request, path_segments, http_response);
		http_response.setContentType("text/html"); // don't put before handling of files with extensions
		try {
			if (path_segments.length > 1 && "api".equals(path_segments[0])) {
				module = Modules.get(path_segments[1]);
				if (module != null && path_segments.length > 2)
					module.doAPIGet(Arrays.copyOfRange(path_segments, 2, path_segments.length), r);
				return;
			}
	
			if ("Views".equals(path_segments[0])) {
				if (!is_user && !http_request.authenticate(http_response))
					return;
				View.doGet(r);
				return;
			}

			if (module != null) {
				if (module.isSecured() && !is_user && !http_request.authenticate(http_response))
					return;
				if (module.doGet(full_page, r))
					return;
			}

			Page page = Site.pages.getPageByURL("/" + path_segments[0]);
			if (page == null)
				page = Site.pages.getPageByName(path_segments[0]);
			if (page != null) {
				if (!page.isPublic() && !is_user && !http_request.authenticate(http_response))
					return;
				page.write(full_page, r);
			} else {
				if (full_page)
					site.pageOpen(null, true, r);
				r.w.h1("Page Not Found")
					.write("<p>The page ").write(r.request.getRequestURI()).write(" was not found on this site.</p>")
					.tagOpen("p");
				r.w.a(site.getDisplayName() + " Home Page", Site.context + "/" + site.getHomePage())
					.tagClose();
				r.close();
			}
		} catch (Exception e) {
			r.log(e);
		} finally {
			r.release();
		}
	}

	//--------------------------------------------------------------------

	@Override
	protected void
	doPost(HttpServletRequest http_request, HttpServletResponse http_response) throws ServletException, IOException {
		http_request.setCharacterEncoding("UTF-8");
		http_response.setCharacterEncoding("UTF-8");

		String[] path_segments = Request.getPathSegments(http_request);

		Site site = Site.getInstance(http_request);
		if (site.doPost(http_request, path_segments, http_response)) {
			http_response.getWriter().write("{\"ok\":true}");
			return;
		}

		http_response.setContentType("text/html");
		Request r = new Request(http_request, path_segments, http_response);

		try {
			if (path_segments[0] == null)
				return;
			switch (path_segments[0]) {
			case "api":
				if (path_segments.length > 1 && "api".equals(path_segments[0])) {
					Module module = Modules.get(path_segments[1]);
					if (module != null && path_segments.length > 2) {
						module.doAPIPost(Arrays.copyOfRange(path_segments, 2, path_segments.length), r);
						r.response.ok();
					}
					return;
				}
				return;
			case "Views":
				if (!http_request.authenticate(http_response))
					return;
				View.doPost(r);
				r.response.ok();
				return;
			case "ViewStates":
				if (!http_request.authenticate(http_response))
					return;
				ViewState.doPost(r);
				r.response.ok();
				return;
			default:
				Module module = Modules.get(path_segments[0]);
				if (module != null) {
					if (module.isSecured() && !http_request.authenticate(http_response))
						return;
					if (module.doPost(r))
						r.response.ok();
				}
			}
		} catch (Exception e) {
			r.log(e);
			throw e;
		} finally {
			r.release();
		}
	}
}
