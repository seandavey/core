package app;

public abstract class Block {
	protected Module		m_module;
	protected final String	m_name;

	//--------------------------------------------------------------------------

	public
	Block(String name) {
		m_name = name;
	}

	//--------------------------------------------------------------------------

	public void
	doPost(Request r) {
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public abstract boolean
	handlesContext(String context, Request r);

	//--------------------------------------------------------------------------

	final Block
	setModule(Module module) {
		m_module = module;
		return this;
	}

	//--------------------------------------------------------------------------

	public abstract void
	write(String context, Request r);
}
