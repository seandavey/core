package app;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil;

import db.DBConnection;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.object.JSONField;
import mail.Mail;
import ui.Form;
import ui.FormBase.Location;
import ui.InputGroup;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class Credentials extends SiteModule {
	private static final String	s_special		= "!@#$%^&*()-=+{}[]";
	private static final String s_special_text	= "Passwords must contain at least 1 lower case letter, 1 upper case letter, 1 number, 1 of " + s_special + ", and be at least 8 characters long.";
	@JSONField
	private boolean				m_allow_account_requests;
	@JSONField
	private int					m_max_password_age;
	@JSONField
	private boolean				m_require_2fa;
	@JSONField
	private boolean				m_require_strong_passwords;
	@JSONField
	private boolean				m_show_stay_signed_in = true;

	// --------------------------------------------------------------------------

	public
	Credentials() {
		m_description = "Handling of user logins, account request, password strength, staying logged in, and 2fa.";
		m_is_secured = false;
		m_required = true;
	}

	// --------------------------------------------------------------------------

	public final boolean
	canAccessSite(Request r) {
		Person user = r.getUser();
		if (user == null)
			return false;
		if (user.mustChangeCredentials() || mustChangePassword(user.getId(), r.db)) {
			Site.site.pageOpen(m_name, false, r);
			r.w.js("new Dialog({hide_close:true,modal:true,title:'Enter New Username and Password',url:context+'/Credentials/form'}).open()");	
			return false;
		}
		if (m_require_2fa && !r.testSessionFlag("2fa")) {
			if (!r.testSessionFlag("need 2fa") && user.getUUID(r.db).equals(r.getCookie("uuid")))
				return true;
			r.setSessionFlag("need 2fa", true);
			Site.site.pageOpen(m_name, false, r);
			r.w.js("""
				let d = new Dialog({hide_close:true,modal:true,title:'Two Factor Authentication',url:context+'/Credentials/2fa form'})
				d.add_footer()
				d.footer.append(_.ui.button('Show QR code',function(){net.replace('#qr_code',context+'/Credentials/QR code')},{
					classes:["btn","btn-secondary"],
					styles:{marginRight:"auto"}
				}))
				d.footer.append(_.ui.button('OK',function(){
					let f=Dialog.top().body.querySelector('form')
					if(!f.reportValidity())
						return false
					net.post(context+'/Credentials/2fa code',new FormData(f),function(){
						Dialog.top().close()
						document.location=document.location
					})
				},{classes:["btn","btn-primary"]}))
				d.open()""");	
			return false;
		}
		return true;
	}

	// --------------------------------------------------------------------------

	@ClassTask
	public final void
	check(Request r) {
		Rows rows = new Rows(new Select("id,user_name,password,email").from("people").where("active"), r.db);
		while (rows.next())
			if (validate(rows.getString(2), null, rows.getString(3), rows.getString(4), rows.getInt(1), null) != null)
				r.db.update("people", "must_change_credentials=TRUE", rows.getInt(1));
		rows.close();
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "2fa form":
			write2FAForm(r);
			return true;
		case "form": // user is being forced to update
			writeForm(null, null, r);
			return true;
		case "QR code":
			Person user = r.getUser();
			if (user == null)
				return false;
			String secret = r.db.lookupString(new Select("secret").from("people").whereIdEquals(user.getId()));
			if (secret == null) {
				secret = generateBase32Secret(16);
				r.db.update("people", "secret=" + SQL.string(secret), user.getId());
			}
			QR.writeQRCode(Site.site.getDisplayName() + " - " + user.getName(), secret, r.w);
			return true;
		case "reset_id":
			int people_id = r.getPathSegmentInt(2);
			user = r.getUser();
			if (user != null && people_id == user.getId() || r.userHasRole("people"))
				r.response.add("reset_id", reset(people_id, r.db));
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "2fa code":
			Person user = r.getUser();
			if (user == null)
				return false;
			String code = r.getParameter("code");
			if (code == null)
				return false;
			code = code.replaceAll("\\s+","");
			String secret = r.db.lookupString(new Select("secret").from("people").whereIdEquals(user.getId()));
			try {
				if (!TimeBasedOneTimePasswordUtil.generateCurrentNumberString(secret).equals(code)) {
					r.response.addError("Invalid Code");
					return false;
				}
			} catch (GeneralSecurityException e) {
			}
			r.setSessionFlag("2fa", true);
			r.removeSessionAttribute("need 2fa");
			return true;
		case "reset":
			reset(r.getParameter("user_name"), r);
			return true;
		case "send email":
			sendEmail(r.getInt("id", 0), false, r.db);
			return true;
		case "set": // PasswordColumn "change" button or user forced to update
			if (r.getUser() != null && update(false, r))
				r.response.addMessage("password changed");
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	private static String
	generateBase32Secret(int numDigits) {
		StringBuilder sb = new StringBuilder(numDigits);
		Random random = new SecureRandom();
		for (int i = 0; i < numDigits; i++) {
			int val = random.nextInt(32);
			if (val < 26)
				sb.append((char) ('A' + val));
			else
				sb.append((char) ('2' + (val - 26)));
		}
		return sb.toString();
	}

	// --------------------------------------------------------------------------

	public final int
	getMaxPasswordAge() {
		return m_max_password_age;
	}

	// --------------------------------------------------------------------------

	private boolean
	isStrong(String password) {
		if (password.length() < 8)
			return false;
		boolean lower = false, upper = false, digit = false, special = false;
		for (char c : password.toCharArray()) {
			if (Character.isLowerCase(c))
				lower = true;
			if (Character.isUpperCase(c))
				upper = true;
			if (Character.isDigit(c))
				digit = true;
			if (!special)
				special = s_special.indexOf(c) != -1;
			if (lower && upper && digit && special)
				return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	private boolean
	mustChangePassword(int people_id, DBConnection db) {
		if (m_max_password_age == 0)
			return false;
		LocalDate d = db.lookupDate(new Select("password_date").from("people").whereIdEquals(people_id));
		if (d == null) {
			db.update("people", "password_date='" + Time.newDate().toString() + "'", people_id);
			return false;
		}
		return ChronoUnit.DAYS.between(d, Time.newDate()) > m_max_password_age;
	}

	// --------------------------------------------------------------------------
	// from "reset credentials" link on login page

	private void
	reset(String user_name, Request r) {
		if (user_name == null || "admin".equals(user_name)) {
			r.response.addError("User name not specified.");
			return;
		}
		int id = r.db.lookupInt(new Select("id").from("people").whereEquals("user_name", user_name), -1);
		if (id == -1) {
			r.response.addError("No account with that user name was found.");
			return;
		}
		boolean active = r.db.lookupBoolean(new Select("active").from("people").whereIdEquals(id));
		if (!active) {
			r.response.addError("The account is not active. Please contact the site administrator to reactivate the account.");
			return;
		}
		String email = r.db.lookupString(new Select("email").from("people").whereIdEquals(id));
		if (email == null) {
			r.response.addError("Unable to send email because an email address has not been configured for the user.");
			return;
		}

		sendEmail(id, false, r.db);
		r.response.addAlert("Account Reset", "The credentials for your account have been reset. An email with instructions for entering a new username and password has been sent to you. If you don't see it in your inbox, please check your junk/spam folder.");
	}

	// --------------------------------------------------------------------------

	public final String
	reset(int people_id, DBConnection db) {
		String reset_id = Text.uuid();
		db.update("people", "reset_id='" + reset_id + "'", people_id);
		return reset_id;
	}

	// --------------------------------------------------------------------------

	public final void
	sendEmail(int people_id, boolean new_account, DBConnection db) {
		Person person = new Person(people_id, db);
		String reset_id = reset(people_id, db);
		String site = Site.site.getDisplayName();
		String url = Site.site.absoluteURL("activate_account.jsp").set("r", reset_id).toString();
		StringBuilder message = new StringBuilder("Hello ").append(person.getName()).append(",<br/><br/>");
		if (new_account)
			message.append("an account has been created for you on the ").append(site).append(" site.");
		message.append(" To set your username and password, please go to <a href=\"").append(url).append("\">").append(url).append("</a>.");
		if (new_account) {
			String s = Site.settings.getString("activate letter");
			if (s != null)
				message.append("<p>").append(s).append("</p>");
		}
		new Mail(site + " Website Account", message.toString())
			.to(person.getEmail())
			.send();
	}

	// --------------------------------------------------------------------------

	public final Credentials
	setMaxPasswordAge(int max_password_age) {
		m_max_password_age = max_password_age;
		return this;
	}

	// --------------------------------------------------------------------------

	public final Credentials
	setRequireStrongPasswords(boolean require_strong_passwords) {
		m_require_strong_passwords = require_strong_passwords;
		return this;
	}

	// --------------------------------------------------------------------------
	// write_errors is true for welcome.jsp but not when user forced to update or PasswordColumn "change"

	public final boolean
	update(boolean write_errors, Request r) {
		String reset_id = r.getParameter("r");
		if (reset_id == null) {
			String err = "Invalid request to update credentials.";
			if (write_errors)
				r.w.write(err);
			else
				r.response.addError(err);
			return false;
		}
		int id = r.db.lookupInt(new Select("id").from("people").whereEquals("reset_id", reset_id), 0);
		if (id == 0) {
			String url = Site.site.absoluteURL(Site.site.getHomePage()).toString();
			String err = "Please go to <a href=\"" + url + "\">" + url + "</a> to sign in.";
			if (write_errors)
				r.w.write(err);
			else
				r.response.addError(err);
			return false;
		}
		String username = r.getParameter("username");
		if (username == null)
			username = r.db.lookupString(new Select("user_name").from("people").whereIdEquals(id));
		String password = r.getParameter("password");
		String err = validate(username, password, null, r.db.lookupString(new Select("email").from("people").whereIdEquals(id)), id, r);
		if (err != null) {
			if (write_errors)
				r.w.write(err + " Please go back and try again.");
			else
				r.response.addError(err);
			return false;
		}
		NameValuePairs nvp = new NameValuePairs();
		String old_username = r.db.lookupString(new Select("user_name").from("people").whereIdEquals(id));
		nvp.set("must_change_credentials", false)
			.set("password", "md5(" + SQL.string(password) + ")")
			.set("user_name", username)
			.set("reset_id", null);
		if (m_max_password_age > 0)
			nvp.setDate("password_date", Time.newDate());
		r.db.update("people", nvp, id);
		nvp.clear();
		nvp.set("user_name", username);
		r.db.update("user_roles", nvp, "user_name" + (old_username == null ? " IS NULL" : "=" + SQL.string(old_username)));
		if (!write_errors)
			r.getUser().load(r.db);
		return true;
	}

	// --------------------------------------------------------------------------
	// r is null when called from check()

	private String
	validate(String user_name, String password, String md5, String email, int people_id, Request r) {
		if (user_name == null)
			return "The username must be set.";
		if (password == null && md5 == null)
			return "The password must be set.";
		if (password != null && user_name.equals(password) || md5 != null && Text.md5(user_name).equals(md5))
			return "The username and the password cannot be the same.";
		if (user_name.equals(email))
			return "The username cannot be the same as the email address.";
		if (email != null && (password != null && password.equals(email) || md5 != null && md5.equals(Text.md5(email))))
			return "The password cannot be the same as the email address.";
		if (password != null && password.equals(user_name) || md5 != null && md5.equals(Text.md5(user_name)))
			return "The password cannot be the same as the username.";
		if (r != null && r.db.exists("people", "user_name=" + SQL.string(user_name) + " AND id!=" + people_id))
			return "The username \"" + user_name + "\" is already in use.";
		if (m_require_strong_passwords) {
			if (password != null && !isStrong(password))
				return s_special_text;
			if (r != null) {
				if (md5 == null)
					md5 = Text.md5(password);
				if (md5.equals(r.db.lookupString(new Select("password").from("people").whereIdEquals(people_id))))
					return "The new password cannot be the same as the current password.";
			}
		}
		return null;
	}

	// --------------------------------------------------------------------------

	private final void
	write2FAForm(Request r) {
		Form f = new Form(null, null, r.w).setButtonsLocation(Location.NONE);
		Person user = r.getUser();
		String secret = r.db.lookupString(new Select("secret").from("people").whereIdEquals(user.getId()));
		try {
			System.out.println(TimeBasedOneTimePasswordUtil.generateCurrentNumberString(secret));
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		r.w.write("<div id=\"qr_code\"></div>");
		f.rowOpen("authenticator code");
		r.w.setAttribute("required", "required")
			.ui.textInput("code", 0, null);
		f.close();
	}

	// --------------------------------------------------------------------------
	// action and reset_id are null when the user is being forced to update

	public final void
	writeForm(String action, String reset_id, Request r) {
		int people_id = action == null ? r.getUser().getId() : r.db.lookupInt(new Select("id").from("people").whereEquals("reset_id", reset_id), 0);
		HTMLWriter w = r.w;
		Form f = new Form(null, action, w).setSubmitText(Text.submit);
		if (action == null)
			f.setOnClick("let f=this.closest('FORM');if(!f.reportValidity())return false;net.post(context+'/Credentials/set',new FormData(f),function(){document.location=document.location})");
		else
			f.setUseSubmit(true);
		f.open();
		if (reset_id == null) {
			String[] row = r.db.readRow(new Select("user_name,password,email").from("people").whereIdEquals(people_id));
			String reason = validate(row[0], null, row[1], row[2], people_id, r);
			if (reason == null && mustChangePassword(people_id, r.db))
				reason = "passwords must be changed every " + m_max_password_age + " days";
			w.write("<p>You must choose a new user name and/or password because ").write(reason == null ? "this account has been reset" : reason).write(".</p>");
			reset_id = reset(people_id, r.db);
		}
		w.hiddenInput("r", reset_id);
		f.rowOpen("username");
		w.setAttribute("autocomplete", "username")
			.setAttribute("required", "required")
			.ui.textInput("username", 0, r.db.lookupString(new Select("user_name").from("people").whereEquals("reset_id", reset_id)));
		f.rowOpen("password");
		w.passwordInput("password", null, true);
		if (m_require_strong_passwords)
			w.ui.helpText(s_special_text);
		f.close();
	}

	// --------------------------------------------------------------------------

	public final void
	writeLoginForm(Request r) {
		HTMLWriter w = r.w;
		w.write("""
			<div style="width:100%"><style>.login_form td{padding-bottom:10px;padding-right:10px;}</style>
			<form id="login_form" method="post" action="j_security_check">
			<table class="login_form" style="margin:20px auto;"><tr>
			<td>user name</td><td>""");
		InputGroup ig = w.ui.inputGroup();
		ig.prependIcon("person");
		w.addClass("form-control")
			.setAttribute("autocapitalize", "off")
			.setAttribute("autocomplete", "username")
			.setAttribute("autocorrect", "off")
			.setAttribute("autofocus", null)
			.setAttribute("name", "j_username")
			.setAttribute("required", "required")
			.setAttribute("spellcheck", "false")
			.setAttribute("type", "text")
			.setId("user_name")
			.tag("input");
		ig.close();
		w.write("</td></tr><tr id=\"label\"><td>password</td><td>");
		ig = w.ui.inputGroup();
		ig.prependIcon("lock");
		w.passwordInput("j_password", null, false);
		ig.close();
		w.write("<td></td></tr><tr id=\"login\"><td colspan=\"2\"><input class=\"btn btn-primary btn-sm\" style=\"float:right\" type=\"submit\" value=\"Sign In\" />");
		if (m_show_stay_signed_in) {
			w.setAttribute("onchange", "document.cookie='setuuid='+(this.checked?'true':'; expires=Thu, 01 Jan 1970 00:00:00 GMT');");
			w.ui.checkbox(null, "stay signed in on this device", null, false, true, false);
		}
		w.write("""
			</td></tr>
			<tr id="link"><td><a href="#" onclick="_.$('#label').style.display='none';_.$('#login').style.display='none';_.$('#link').style.display='none';_.$('#send').style.display='';return false" style="font-size: smaller">reset credentials</a></td></tr>
			<tr id="send" style="display: none"><td colspan="2">
			enter your user name and click reset""").addStyle("margin-left:10px").ui.buttonOnClick("Reset", """
			var u=_.$('#user_name').value
			Dialog.confirm('Reset Credentials',
				'Reset the username and/or password for '+u+'? Instructions will be sent via email if the account is active.',
				'ok',
				function f(){
					if(_.$('#user_name').reportValidity()){
						let d=new FormData()
						d.set('user_name',u)
						net.post(context+'/Credentials/reset',d)
					}
				}
			)""")
			.addStyle("margin-left:10px")
			.ui.buttonOnClick("Cancel", "_.$('#link').style.display='';_.$('#label').style.display='';_.$('#login').style.display='';_.$('#send').style.display='none';return false")
			.write("</td></tr>");
		if (m_allow_account_requests)
			w.write("<tr id=\"request\"><td><a class=\"btn btn-primary btn-sm\" href=\"#\" onclick=\"net.replace('#request_form',context+'/get request form',{on_complete:function(){_.$('#login_form').style.display='none';}});return false\" style=\"font-size: smaller\">Request Account</a></td></tr>");
		w.write("</table></form></div>");
		if (m_allow_account_requests)
			w.write("<div id=\"request_form\" style=\"display:table;margin:20px auto;\"></div>");
	}
}
