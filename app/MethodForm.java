package app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Enumeration;

import db.DBConnection;
import db.Result;
import db.RowsSelect;
import db.Select;
import jakarta.servlet.http.HttpServletRequest;
import ui.Form;
import ui.FormBase.Location;
import util.Text;
import util.Time;
import web.HTMLWriter;

class MethodForm {
	private Form			m_form;
	private boolean			m_in_dialog;
	private final Module	m_module;
	private final Class<?>	m_object_class;

	//--------------------------------------------------------------------------

	public
	MethodForm(Class<?> object_class) {
		m_object_class = object_class;
		m_module = null;
	}

	//--------------------------------------------------------------------------

	public
	MethodForm(Module module, boolean in_dialog) {
		m_module = module;
		m_object_class = null;
		m_in_dialog = in_dialog;
	}

	//--------------------------------------------------------------------------

	public final void
	close(HTMLWriter w) {
		if (m_in_dialog) {
			m_form.close();
			return;
		}
		String uuid = Text.uuid();
		w.ui.buttonOnClick("Call", "var d=_.$('#" + uuid + "');d.innerText='running...';_.form.send(this,function(t){_.dom.set_html(d,t)})");
		m_form.close();
		w.write("<div id=\"").write(uuid).write("\"></div>");
	}

	//--------------------------------------------------------------------------

	public static void
	doGet(Object object, String method_name, Request r) {
		Class<?> object_class = object.getClass();
		try {
			Method[] methods = object_class.getMethods();
			if (methods != null)
				for (Method method : methods)
					if (method_name != null && method_name.equals(method.getName()) && method.isAnnotationPresent(ClassTask.class)) {
						Class<?>[] parameter_types = method.getParameterTypes();
						Object[] args = new Object[parameter_types.length];
						for (int i=0; i<parameter_types.length; i++)
							if (parameter_types[i] == LocalDate.class) {
								String d  = r.getParameter("arg" + i);
								args[i] = d == null || d.length() == 0 ? null : LocalDate.parse(d);
							} else if (parameter_types[i] == LocalDateTime.class) {
								String parameter = r.getParameter("arg" + i);
								if (parameter == null || parameter.length() == 0)
									args[i] = Time.newDateTime();
								else
									args[i] = LocalDateTime.parse(parameter);
							} else if (parameter_types[i] == LocalTime.class)
								args[i] = Time.formatter.parseTime(r.getParameter("arg" + i));
							else if (parameter_types[i] == DBConnection.class)
								args[i] = r.db;
							else if (parameter_types[i] == HttpServletRequest.class)
								args[i] = r.request;
							else if (parameter_types[i] == Request.class)
								args[i] = r;
							else if (parameter_types[i] == Site.class)
								args[i] = Site.site;
							else if (parameter_types[i] == boolean.class)
								args[i] = Boolean.valueOf("true".equals(r.getParameter("arg" + i)));
							else if (parameter_types[i] == int.class) {
								String arg_i = r.getParameter("arg" + i);
								if (arg_i != null && arg_i.length() > 0)
									args[i] = Integer.valueOf(arg_i);
								else
									args[i] = 0;
							} else
								args[i] = r.getParameter("arg" + i);
						System.out.println("invoking " + object_class.getName() + "." + method.getName());
						Object return_value = null;
						try {
							return_value = parameter_types.length == 0 ? method.invoke(object) : method.invoke(object, args);
						} catch (Exception e) {
							r.abort(e);
						}
						Class<?> return_type = method.getReturnType();
						if (return_type == Result.class) {
							Result result = (Result)return_value;
							if (result.error != null)
								r.response.addError(result.error);
						}
						if (return_type == String.class)
							r.w.write(return_value).br();
						r.response.addMessage("done");
						System.out.println(object_class.getSimpleName() + "." + method.getName() + " done");
						return;
					}
		} catch (IllegalArgumentException e) {
			r.abort(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		}
		System.out.println("method " + object_class.getSimpleName() + "." + method_name + " not found or not public or not annotated with ClassTask");
	}

	//--------------------------------------------------------------------------

	public final void
	open(String method_name, Request r) {
		HTMLWriter w = r.w;
		m_form = new Form(null, Site.context + "/api" + (m_module != null ? "/" + m_module.getName() + "/method/" + method_name : "/admin/method/" + method_name), w)
			.setButtonsLocation(Location.NONE)
			.setMethod("GET")
			.open();
		if (m_module != null)
			w.hiddenInput("module", m_module.getName());
		else if (m_object_class != null)
			w.hiddenInput("class", m_object_class.getSimpleName());
		Enumeration<String> parameter_names = r.request.getParameterNames();
		while (parameter_names.hasMoreElements()) {
			String parameter_name = parameter_names.nextElement();
			if (!parameter_name.equals("action") && !parameter_name.equals("class") && !parameter_name.equals("method") && !parameter_name.equals("module"))
				r.w.hiddenInput(parameter_name, r.getParameter(parameter_name));
		}
		if (!m_in_dialog && r.userIsAdministrator())
			w.h3((m_object_class != null ? m_object_class.getName() : m_module.getClass().getName()) + '.' + method_name);
	}

	//--------------------------------------------------------------------------
	// see if there's a method method_name+"Form". if so call it, otherwise write generic f

	public final void
	write(String method_name, Request r) {
		Class<?> c = m_object_class != null ? m_object_class : m_module.getClass();
		try {
			Method method = c.getMethod(method_name + "Form", Request.class);
			method.invoke(m_module, r);
		} catch (NoSuchMethodException e) {
			Method[] methods = c.getMethods();
			for (Method method : methods)
				if (method_name.equals(method.getName()) && method.isAnnotationPresent(ClassTask.class)) {
					write(method, r);
					break;
				}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------
	// write generic form based on method parameters

	private void
	write(Method method, Request r) {
		HTMLWriter w = r.w;
		open(method.getName(), r);
		String[] labels = method.getAnnotation(ClassTask.class).value();
		int label_index = 0;
		Class<?>[] parameter_types = method.getParameterTypes();
		for (int i=0; i<parameter_types.length; i++) {
			if (parameter_types[i] == Request.class || parameter_types[i] == Site.class || parameter_types[i] == DBConnection.class)
				continue;
			String label = null;
			if (labels.length > label_index)
				label = labels[label_index++];
			m_form.rowOpen(parameter_types[i] != boolean.class ? label : null);
			if (Site.site.writeMethodFormControl("arg" + i, parameter_types[i], label, r))
				continue;
			if ("_owner_".equals(label) || "people_id".equals(label))
				new RowsSelect("arg" + i, new Select("id,first,last").from("people").orderBy("first,last"), "first,last", "id", r).write(r.w);
			else if (label != null && label.endsWith("table"))
				w.ui.select("arg" + i, r.db.getTableNames(false), null, null, false);
			else if (parameter_types[i] == boolean.class)
				w.ui.checkbox("arg" + i, label, null, false, false, true);
			else if (parameter_types[i] == LocalDate.class)
				w.dateInput("arg" + i, null);
			else if (parameter_types[i] == String.class || parameter_types[i] == int.class || parameter_types[i] == float.class)
				w.textArea("arg" + i , null);
		}
		close(w);
	}
}
