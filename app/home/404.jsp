<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Site" %>
<%@ page isErrorPage="true" %>
<%
String uri = null;
	try {
		uri = pageContext.getErrorData().getRequestURI();
	} catch (Exception e) {
	}

	Request r = new Request(request, null, response, out);
	boolean full_page = !uri.endsWith(".html") && uri.indexOf("/-") == -1;
	if (full_page)
		Site.site.pageOpen(null, true, r);
	r.w.h1("Page Not Found")
		.p("<p>The page ", uri, " was not found on this site.</p>");
	if (full_page) {
		r.w.tagOpen("p");
		r.w.a(Site.site.getDisplayName() + " Home Page", Site.site.absoluteURL(Site.site.getHomePage()).toString())
	.tagClose();
	}
	r.close();
%>