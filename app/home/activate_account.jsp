<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Site,db.Select,web.Head" %>
<%
Request r = new Request(request, null, response, out);
	new Head(Site.site.getDisplayName() + " Account", r).script("site-min").styleSheet(Site.themes.getTheme(r)).close();
	String reset_id = r.getParameter("r");
	if (reset_id == null) {
		r.close();
		return;
	}
	int id = r.db.lookupInt(new Select("id").from("people").whereEquals("reset_id", reset_id), 0);
	if (id == 0) {
		String url = Site.site.absoluteURL(Site.site.getHomePage()).toString();
		r.w.write("The username and password have already been set for this account. Please go to ").a(url).write(" to sign in.");
		r.close();
		return;
	}
	r.w.mainOpen();
	String site_title = Site.site.getDisplayName();
	r.w.h1(site_title != null ? "Welcome to " + site_title : "Welcome")
		.p("To activate your account, please enter the username and password you would like to use for this site.");
	Site.credentials.writeForm("welcome.jsp", reset_id, r);
	r.close();
%>
