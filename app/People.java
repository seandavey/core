
package app;

import java.sql.Types;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import db.DBConnection;
import db.NameValuePairs;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.RolesColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.jdbc.JSONB;
import db.object.JSONField;
import pages.Page;

public class People extends Module {
	@JSONField(admin_only = true)
	protected JsonObject	m_page_template = Json.parse("""
		{"blocks":[
		{"type":"text","column":"first","view":"person settings","edit_only":true,"label":"first name"},
		{"type":"text","column":"last","view":"person settings","edit_only":true,"label":"last name"},
		{"type":"text","icon":"envelope","column":"email","view":"person settings"}
		]}""").asObject();
	@JSONField
	public int				m_user_pic_size = 40;

	//--------------------------------------------------------------------------

	public
	People() {
		m_required = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("people")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("email", Types.VARCHAR))
			.add(new JDBCColumn("first", Types.VARCHAR))
			.add(new JSONB("json"))
			.add(new JDBCColumn("hide_email_on_site", Types.BOOLEAN))
			.add(new JDBCColumn("last", Types.VARCHAR))
			.add(new JDBCColumn("last_login", Types.TIMESTAMP))
			.add(new JDBCColumn("must_change_credentials", Types.BOOLEAN))
			.add(new JDBCColumn("password", Types.VARCHAR))
			.add(new JDBCColumn("picture", Types.VARCHAR))
			.add(new JDBCColumn("pronouns", Types.VARCHAR))
			.add(new JDBCColumn("reset_id", Types.VARCHAR))
			.add(new JDBCColumn("secret", Types.VARCHAR))
			.add(new JDBCColumn("theme", Types.VARCHAR))
			.add(new JDBCColumn("timezone", Types.VARCHAR))
			.add(new JDBCColumn("user_name", Types.VARCHAR).setUnique(true))
			.add(new JDBCColumn("uuid", Types.VARCHAR));
		addJDBCColumns(table_def);
		if (Site.credentials.getMaxPasswordAge() > 0)
			table_def.add(new JDBCColumn("password_date", Types.DATE));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	doDelete(Request r) {
		String segment_one = r.getPathSegment(1);
		switch (segment_one) {
		case "picture":
			r.getUser().deletePicture(r.db);
			break;
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch(path_segments[0]) {
		case "people edit":
			Site.site.newView("people edit", r).writeComponent();
			return true;
		case "person settings":
			Person user = r.getUser();
			if (user != null) {
				ViewState.setKeyValue("person settings", user.getId(), r);
				Site.site.newView("person settings", r).writeEditForm();
			}
			return true;
		case "profile":
			int id = r.getPathSegmentInt(3);
			if (id != 0) {
				Person person = Site.site.newPerson(id, r.db);
				person.writeProfile(r);
			}
			return true;
		case "roles":
			Roles.writeSettingsPane(r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "data":
			r.getUser().setData(r.getPathSegment(2), r.getPathSegment(3), r.db);
			return true;
		case "picture":
			r.getUser().setPicture(r);
			return true;
		case "Roles":
			String role = r.getPathSegment(2);
			if (role != null)
				switch (r.getPathSegment(3)) {
				case "create":
					Roles.create(role, null, r.db);
					return true;
				case "delete":
					Roles.delete(role, r.db);
					return true;
				}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks(Request r) {
		return new String[][] { { getDisplayName(r), "settings" }, { "Roles", "roles" } };
	}

	//--------------------------------------------------------------------------

	public final JsonObject
	getPageTemplate() {
		return m_page_template;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Roles.add("people", "can edit people with this role");
		initEditPage(db);
	}

	//--------------------------------------------------------------------------

	protected void
	initEditPage(DBConnection db) {
		Site.site.addUserDropdownItem(new Page("Edit People", this, false){
			@Override
			public void
			writeContent(Request r) {
				ViewState.setBaseFilter("people edit", "active AND user_name != 'admin' AND password IS NOT NULL", r);
				Site.site.newView("people edit", r).writeComponent();
			}
		}.addRole("people"));
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("people"))
			return new PeopleViewDef(name, new RoleAccessPolicy("people").add().delete().edit(), 40);
		if (name.equals("people edit"))
			return new PeopleViewDef(name, new RoleAccessPolicy("people").add().delete().edit(), 40)
				.setFrom("people")
				.setColumnNamesForm(new String[] { "first", "last", "email", "hide_email_on_site", "last_login", "user_name", "password" })
				.setColumnNamesTable(new String[] { "first", "last", "email" });
		if (name.equals("person settings"))
			return new PeopleViewDef(name, new AccessPolicy().edit(), 40)
				.setFrom("people")
				.setColumnNamesForm(new String[] { "password", "timezone", "user_name" });
		if (name.equals("user_roles"))
			return new ViewDef(name) {
					@Override
					public String
					beforeInsert(NameValuePairs nvp, Request r) {
						nvp.set("user_name", r.db.lookupString("user_name", "people", nvp.getInt("people_id", 0)));
						return null;
					}
				}
				.setAccessPolicy(new RoleAccessPolicy("people").add().delete())
				.setDefaultOrderBy("role")
				.setRecordName("Role", true)
//				.setShowColumnHeads(false)
				.setColumn(new Column("user_name").setIsHidden(true))
				.setColumn(new RolesColumn("role").setAllowNoSelection(false));
		return null;
	}

	//--------------------------------------------------------------------

//	@ClassTask({"add default role"})
//	public static void
//	syncUserRoles(boolean add_default_role, Request r) {
//		r.db.delete("user_roles", "user_name IS NULL OR role IS NULL", false);
//		r.db.update("user_roles", "user_name=(SELECT user_name FROM people WHERE people.id=user_roles.people_id)", "people_id IS NOT NULL");
//		if (add_default_role) {
//			Rows rows = new Rows(new Select("*").from("people").where("active AND NOT user_name IS NULL"), r.db);
//			while (rows.next()) {
//				String person = rows.getString("user_name").trim();
//				if (!r.db.rowExists("user_roles", "user_name='" + person + "' AND role='" + Site.site.getDefaultRole() + "'"))
//					r.db.insert("user_roles", "user_name,role", "'" + person + "','" + Site.site.getDefaultRole() + "'");
//			}
//			rows.close();
//		}
//		r.db.delete("user_roles", "NOT EXISTS (SELECT 1 FROM people WHERE people.user_name=user_roles.user_name)", false);
//	}
}