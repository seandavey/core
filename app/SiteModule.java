package app;

import java.util.Map;
import java.util.TreeMap;

public abstract class SiteModule extends Module {

	protected Map<String,Block>	m_blocks;
	protected String			m_description;

	//--------------------------------------------------------------------------

	public final Module
	addBlock(Block block) {
		block.setModule(this);
		if (m_blocks == null)
			m_blocks = new TreeMap<>();
		m_blocks.put(block.getName(), block);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch(path_segments[0]) {
		case "blocks":
			Block block = m_blocks.get(path_segments[1]);
			if (block == null)
				r.log("block " + path_segments[1] + " not found", false);
			else
				block.write(path_segments.length > 2 ? path_segments[2] : null, r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "blocks":
			if (m_blocks != null) {
				Block block = m_blocks.get(r.getPathSegment(2));
				if (block != null)
					block.doPost(r);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public Map<String,Block>
	getBlocks() {
		return m_blocks;
	}

	//--------------------------------------------------------------------------

	public final String
	getDescription() {
		return m_description;
	}

	//--------------------------------------------------------------------------

	public final SiteModule
	setDescription(String description) {
		m_description = description;
		return this;
	}
}
