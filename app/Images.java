package app;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.color.CMMException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifSubIFDDirectory;

import db.NameValuePairs;
import db.ViewDef;

public class Images {
	private static BufferedImage
	copy(BufferedImage image, int width, int height) {
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = newImage.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2D.drawImage(image, 0, 0, width, height, null);
		return newImage;
	}

	//--------------------------------------------------------------------------

	public static void
	deleteUnneededThumbs(File directory) {
		String[] picture_files = directory.list();
		if (picture_files != null)
			Arrays.sort(picture_files);
		Path thumbs_dir = directory.toPath().resolve("thumbs");
		String[] thumb_files = thumbs_dir.toFile().list();
		if (thumb_files != null)
			for (String thumb_file : thumb_files)
				if (Arrays.binarySearch(picture_files, thumb_file) < 0)
					thumbs_dir.resolve(thumb_file).toFile().delete();
	}

	//--------------------------------------------------------------------------

	public static BufferedImage
	load(String file_path) {
		BufferedImage image;
		try {
			image = ImageIO.read(new File(file_path));
		} catch (CMMException e) {
			System.out.println("exception loading " + file_path + ": " + e.toString());
			return null;
		} catch (IOException e) {
			System.out.println("exception loading " + file_path + ": " + e.toString());
			return null;
		}
		return image;
	}

	//--------------------------------------------------------------------------

	public static void
	makeThumb(BufferedImage image, Path pictures_dir, String filename, int size, boolean size_is_max_side) {
		if (image == null) {
			System.out.println("makeThumb: null image " + filename);
			return;
		}
		Path new_file_path = pictures_dir.resolve("thumbs");
		File thumbs_dir = new_file_path.toFile();
		if (!thumbs_dir.exists())
			thumbs_dir.mkdirs();
		image = size_is_max_side ? resize(image, size) : scaleImage(image, (double)size / image.getWidth());
		write(image, new_file_path.resolve(filename));
	}

	//--------------------------------------------------------------------------

	public static BufferedImage
	resize(BufferedImage image, int size) {
		int image_width = image.getWidth();
		int image_height = image.getHeight();

		if (image_width <= size && image_height <= size)
			return image;

		int new_width;
		int new_height;

		if (image_width == image_height) {
			new_width = size;
			new_height = size;
		} else if (image_width > image_height) {
			new_width = size;
			double ratio = (double)size / (double)image_width;
			new_height = (int)(image_height * ratio);
		} else {
			new_height = size;
			double ratio = (double)size / (double)image_height;
			new_width = (int)(image_width * ratio);
		}

		return copy(image, new_width, new_height);
	}

	//--------------------------------------------------------------------------

//	public static void
//	rotate(String directory, String filename, String new_filename, boolean clockwise, int thumb_size) {
//		File file = new File(directory + File.separatorChar + filename);
//		BufferedImage image;
//		try {
//			image = ImageIO.read(file);
//		} catch (IOException e) {
//			System.out.println(file.toString());
//			System.out.println(e.toString());
//			return;
//		}
//		if (image == null)
//			return;
//		int width = image.getWidth();
//		int height = image.getHeight();
//
//		BufferedImage new_image = new BufferedImage(height, width, image.getType());
//
//		if (clockwise)
//			for(int i=0; i<width; i++)
//				for(int j=0; j<height; j++)
//					new_image.setRGB(height-1-j, i, image.getRGB(i, j));
//		else
//			for(int i=0; i<width; i++)
//				for(int j=0; j<height; j++)
//					new_image.setRGB(j, width-1-i, image.getRGB(i, j));
//
//		write(new_image, directory + File.separatorChar + new_filename);
//		if (thumb_size > 0)
//			makeThumb(new_image, directory, new_filename, thumb_size, true);
//
//		file.delete();
//		file = new File(directory + File.separatorChar + "thumbs" + File.separatorChar + filename);
//		if (file.exists())
//			file.delete();
//	}

	//--------------------------------------------------------------------------

	public static BufferedImage
	scaleImage(BufferedImage image, double scale) {
		return copy(image, (int)(image.getWidth() * scale), (int)(image.getHeight() * scale));
	}

	//--------------------------------------------------------------------------

	public static void
	setTimestampToExifDate(Path path, ViewDef view_def, int id, Request r) {
		if (!r.db.hasColumn(view_def.getFrom(), "_timestamp_"))
			return;
		try {
			File file = path.toFile();
			if (!file.exists()) {
				System.out.println(file.toString() + " does not exist");
				return;
			}
			Metadata metadata = ImageMetadataReader.readMetadata(file);
			Directory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
			if (directory == null)
				//				System.out.println("ExifSubIFDDirectory not found for " + file.toString());
				return;
			Date date = directory.getDate(ExifDirectoryBase.TAG_DATETIME_ORIGINAL);
			if (date == null)
				//				System.out.println("TAG_DATETIME_ORIGINAL not found for " + file.toString());
				return;
			NameValuePairs nvp = new NameValuePairs();
			nvp.set("_timestamp_", new java.sql.Timestamp(date.getTime()).toString());
			view_def.update(id, nvp, r);
		} catch (ImageProcessingException | IOException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	write(BufferedImage image, Path file_path) {
		String filename = file_path.getFileName().toString();
		String format_name = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
		try {
			javax.imageio.ImageIO.write(image, format_name, file_path.toFile());
			file_path.toFile().setReadable(true, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
