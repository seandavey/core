package app;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
	@Override
	public void
	contextDestroyed(ServletContextEvent event) {
		if (Site.site != null)
			Site.site.destroy();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	contextInitialized(ServletContextEvent event) {
	}
}
