package app;

import java.sql.Types;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import db.DBConnection;
import db.Rows;
import db.SQL;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import ui.Card;
import util.Text;
import web.HTMLWriter;

public class Roles {
	private static final TreeSet<String>		s_created_roles = new TreeSet<>();
	private static boolean						s_first = true;
	private static final TreeMap<String,String>	s_roles = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

	//--------------------------------------------------------------------------

	public static void
	add(String role, String description) {
		s_roles.put(role, description);
	}

	//--------------------------------------------------------------------------

	public static void
	create(String role, String description, DBConnection db) {
		if (!db.tableExists("_roles_"))
			db.createTable("_roles_", false, "role VARCHAR,description VARCHAR", null, false);
		add(role, description);
		s_created_roles.add(role);
		db.insert("_roles_", "role,description", SQL.string(role) + ',' + SQL.string(description));
	}

	//--------------------------------------------------------------------------

	public static void
	delete(String role, DBConnection db) {
		s_roles.remove(role);
		s_created_roles.remove(role);
		db.delete("_roles_", "role", role, false);
		db.delete("user_roles", "role", role, false);
	}

	//--------------------------------------------------------------------------

	public static String[]
	getRoleNames() {
		TreeMap<String,String> roles = getRoles();
		String[] a = new String[s_roles.size()];
		int i = 0;
		for (String role : roles.keySet())
			a[i++] = role;
		return a;
	}

	//--------------------------------------------------------------------------

	public static synchronized TreeMap<String,String>
	getRoles() {
		if (s_first) {
			DBConnection db = new DBConnection();
			if (db.tableExists("_roles_")) {
				JDBCTable table_def = new JDBCTable("_roles_")
					.add(new JDBCColumn("role", Types.VARCHAR))
					.add(new JDBCColumn("description", Types.VARCHAR));
				JDBCTable.adjustTable(table_def, true, false, db);
				Rows roles = new Rows(new Select("role,description").from("_roles_"), db);
				while (roles.next()) {
					String role = roles.getString(1);
					if (!s_roles.containsKey(role)) {
						s_created_roles.add(role);
						add(role, roles.getString(2));
					} else
						db.delete("_roles_", "role", role, false);
				}
				roles.close();
			}
			db.close();
			s_first = false;
		}
		return s_roles;
	}

	//--------------------------------------------------------------------------

	public static boolean
	wasCreated(String role) {
		return s_created_roles.contains(role);
	}

	//--------------------------------------------------------------------

	public static void
	writeSettingsPane(Request r) {
		HTMLWriter w = r.w;
		w.h2("Roles")
			.addStyle("margin-bottom:20px")
			.ui.buttonOnClick(Text.add, "Dialog.prompt('Role',function(r){net.post(context+'/People/Roles/'+r+'/create',null,function(){app.reload_main()})},{title:'Add Role'})");
		TreeMap<String, String> roles = Roles.getRoles();
		for (String role : roles.keySet()) {
			Card card = w.ui.card()
				.headerOpen()
				.title(role);
			String description = roles.get(role);
			if (description != null)
				card.subtitle(description);
			if (Roles.wasCreated(role))
				w.addStyle("float:right").ui.buttonOnClick(Text.delete, "Dialog.confirm(null,'Delete " + role + " role?','delete',function(){net.post(context+'/People/Roles/" + role + "/delete',null,function(){app.reload_main()})})");
			List<String> people = r.db.readValues(new Select("first || ' ' || last").from("people").join("people", "user_roles").whereEquals("role", role).andWhere("people.user_name != 'admin'").orderBy("first,last"));
			card.text(Text.join(", ", people));
			card.close();
		}
	}
}
