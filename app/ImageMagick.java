package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Arrays;

public class ImageMagick {
	// return int[width, height]
	public static int[]
	getSize(Path file_path_from) {
		try {
			Process p = new ProcessBuilder("identify", "-format", "%[fx:w]x%[fx:h]", file_path_from.toString() + "[0]").start();
			BufferedReader reader =  new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			if (line != null) {
				int index = line.indexOf('x');
				return new int[] { Integer.parseInt(line.substring(0, index)), Integer.parseInt(line.substring(index + 1)) };
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public static void
	makeThumb(Path pictures_dir, String filename, int size) {
		Path file_path = pictures_dir.resolve(filename);
		Path new_file_path = pictures_dir.resolve("thumbs");
		File thumbs_dir = new_file_path.toFile();
		if (!thumbs_dir.exists())
			thumbs_dir.mkdirs();
		new_file_path = new_file_path.resolve(filename);
		scale(file_path, new_file_path, size);
	}

	// --------------------------------------------------------------------------

	public static void
	makeThumbs(Path pictures_dir, int size, boolean only_missing_thumbs) {
		String[] picture_files = pictures_dir.toFile().list();
		File thumbs_dir = new File(pictures_dir + "/thumbs");
		if (!thumbs_dir.exists())
			thumbs_dir.mkdir();
		String[] thumb_files = thumbs_dir.list();
		if (thumb_files == null)
			return;
		Arrays.sort(thumb_files);
		for (String picture_file : picture_files) {
			if (picture_file.charAt(0) == '.' || picture_file.equals("thumbs") || picture_file.endsWith(".tif") || only_missing_thumbs && Arrays.binarySearch(thumb_files, picture_file) >= 0)
				continue;
			makeThumb(pictures_dir, picture_file, size);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	rotate(Path pictures_dir, String filename, String new_filename, boolean clockwise, int thumb_size) {
		Path file_path_from = pictures_dir.resolve(filename);
		try {
			Process p = new ProcessBuilder("convert", file_path_from.toString(), "-strip", "-rotate", clockwise ? "90" : "270", pictures_dir.resolve(new_filename).toString()).start();
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		if (thumb_size > 0)
			makeThumb(pictures_dir, new_filename, thumb_size);

		file_path_from.toFile().delete();
		File thumb = pictures_dir.resolve("thumbs").resolve(filename).toFile();
		if (thumb.exists())
			thumb.delete();

	}

	//--------------------------------------------------------------------------

	public static void
	scale(Path file_path_from, Path file_path_to, int max_side) {
		try {
			Process p = new ProcessBuilder("convert", file_path_from.toString(), "-scale", max_side + "x" + max_side, file_path_to.toString()).start();
			BufferedReader ir = p.inputReader();
//			p.waitFor();
			String l = ir.readLine();
			while (l != null) {
				System.out.println(l);
				l = ir.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
