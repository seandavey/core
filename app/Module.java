package app;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;

import app.Site.Message;
import db.DBConnection;
import db.NameValuePairs;
import db.SQL;
import db.ViewDef;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.DBObject;
import db.object.JSONField;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ui.Form;
import ui.NavBar;
import util.Array;
import util.Text;
import web.HTMLWriter;
import web.JS;
import web.URLBuilder;

public class Module {
	@JSONField
	private boolean						m_active = true;
	protected boolean					m_can_be_deleted;
	@JSONField(label="name", required=true)
	private String						m_display_name;
	@JSONField(admin_only=true)
	protected boolean					m_is_secured = true;
	private List<JDBCColumn>			m_jdbc_columns;
	protected final String				m_name;
	protected boolean					m_renameable;
	protected boolean					m_required;
	private final ArrayList<ViewDef>	m_view_defs = new ArrayList<>();

	//--------------------------------------------------------------------------

	public
	Module() {
		m_name = this.getClass().getSimpleName().length() == 0 ? this.getClass().getSuperclass().getSimpleName() : this.getClass().getSimpleName();
		m_display_name = m_name;
	}

	//--------------------------------------------------------------------------

	public
	Module(String name) {
		m_display_name = name;
		m_name = name.replaceAll("/", "");
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	absoluteURL(String ...segments) {
		URLBuilder url = Site.site.absoluteURL(m_name);
		for (String segment : segments)
			url.segment(segment);
		return url;
	}

	//--------------------------------------------------------------------------

	public final void
	addColumnNames(ArrayList<String> column_names) {
		if (m_jdbc_columns != null)
			for (JDBCColumn column : m_jdbc_columns)
				column_names.add(column.name);
	}

	//--------------------------------------------------------------------------

	public final Module
	addJDBCColumn(JDBCColumn column) {
		if (m_jdbc_columns == null)
			m_jdbc_columns = new ArrayList<>();
		m_jdbc_columns.add(column);
		return this;
	}

	//--------------------------------------------------------------------------

	protected void
	addJDBCColumns(JDBCTable table_def) {
		if (m_jdbc_columns != null)
			for (JDBCColumn column : m_jdbc_columns)
				table_def.add(column);
	}

	//--------------------------------------------------------------------------

	protected void
	addPages(DBConnection db) {
	}

	//--------------------------------------------------------------------------

	protected void
	adjustTables(DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public final String
	apiURL(Object ...segments) {
		URLBuilder url = new URLBuilder(Site.context).segment("api").segment(URLBuilder.encode(m_name));
		for (Object s: segments)
			url.segment(s.toString());
		return url.toString();
	}

	//--------------------------------------------------------------------------
	// only called by Admin

	protected final void
	componentOpen(String parameters, Request r) {
		String url = Site.context + '/' + m_name;
		if (parameters != null)
			url += "?" + parameters;
		r.w.componentOpen(url);
	}

	//--------------------------------------------------------------------------
	// only called by Admin

	protected final String
	componentReplaceJS(String path, String parameters, Request r) {
		StringBuilder js = new StringBuilder("net.replace(_.c(this),'");
		js.append(Site.context).append('/').append(m_name);
		if (path != null)
			js.append('/').append(path);
		if (parameters != null)
			js.append('?').append(parameters);
		js.append("')");
		return js.toString();
	}

	//--------------------------------------------------------------------------

	public void
	doDelete(Request r) {
	}

	//--------------------------------------------------------------------------

	public boolean
	doAPIGet(String[] path_segments, Request r) {
		switch(path_segments[0]) {
		case "method":
			MethodForm.doGet(this, path_segments[1], r);
			return true;
		case "method form":
			new MethodForm(this, false).write(path_segments[1], r);
			return true;
		case "method form dialog":
			new MethodForm(this, true).write(path_segments[1], r);
			return true;
		case "page_menu":
			writePageMenu(r);
			return true;
		case "settings":
			writeSettingsForm(null, true, true, r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	doAPIPost(String[] path_segments, Request r) {
		switch (path_segments[0]) {
		case "settings":
			if ("save".equals(path_segments[1])) {
				updateSettings(r);
				return true;
			}
			return false;
		case "remove":
			remove(r.db);
			return true;
		}
		return false;

	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	public boolean
	doGet(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	doGet(boolean full_page, Request r) {
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	doPost(Request r) {
		return false;
	}

	//--------------------------------------------------------------------------

	public void
	everyFiveMinutes(LocalDateTime now, DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public void
	everyHour(LocalDateTime now, DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public void
	everyNight(LocalDateTime now, DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public String[][]
	getConfigTasks(Request r) {
		return null;
	}

	//--------------------------------------------------------------------------

	public String
	getDisplayName(Request r) {
		if (m_display_name == null)
			return m_name;
		return m_display_name;
	}

	//--------------------------------------------------------------------------

	protected String[]
	getFieldOptions(String field_name) {
		return null;
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public void
	init(boolean was_added, DBConnection db) {
		m_can_be_deleted = was_added;
		adjustTables(db);
		addPages(db);
	}

	//--------------------------------------------------------------------------
	// run after all modules have added

	public void
	init2(DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public final boolean
	isActive() {
		return m_active || m_required;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isSecured() {
		return m_is_secured;
	}

	//--------------------------------------------------------------------------

	public final void
	load(DBConnection db) {
		String json = db.lookupString("json", "_modules_", "name=" + SQL.string(m_name));
		if (json != null)
			DBObject.fromJSON(this, json);		
	}

	//--------------------------------------------------------------------------

	protected final NavBar
	navBar(String target, int segment, Request r) {
		return r.w.ui.navBar("page_menu", target, segment, r)
			.setDark(true)
			.setPath(apiURL());
	}

	//--------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public static Module
	newInstance(String name, String class_name) {
		Module m = null;
		Class<?> c = null;
		try {
			c = Class.forName(class_name, true, Thread.currentThread().getContextClassLoader());
			if (c != null)
				try { // first look for constructor(name)
					m = ((Constructor<Module>)c.getConstructor(String.class)).newInstance(name);
				} catch (NoSuchMethodException e) {
					try { // then look for constructor()
						m = (Module)c.getConstructor().newInstance();
					} catch (Exception e1) {
						// assume this module will be created elsewhere
					}
				}
		} catch (Exception e) {
			Site.site.log(e);
		}
		return m;
	}

	//--------------------------------------------------------------------------

//	protected final ViewDef
//	newPrivateViewDef(String from) {
//		ViewDef view_def = new ViewDef(Text.uuid()).setFrom(from);
//		Site.site.addViewDef(view_def);
//		return view_def;
//	}

	//--------------------------------------------------------------------------

	public final ViewDef
	newViewDef(String name) {
		ViewDef view_def = _newViewDef(name);
		if (view_def != null)
			m_view_defs.add(view_def);
		return view_def;
	}

	//--------------------------------------------------------------------------

	public ViewDef
	_newViewDef(String name) {
		return null;
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	relativeURL(String ...segments) {
		URLBuilder url = Site.site.relativeURL(m_name);
		for (String segment : segments)
			url.segment(segment);
		return url;
	}

	//--------------------------------------------------------------------------

	public final void
	remove(DBConnection db) {
		for (ViewDef view_def : m_view_defs)
			Site.site.removeViewDef(view_def.getName());
		Modules.remove(this);
		db.delete("_modules_", "name=" + SQL.string(m_name), false);
	}

	//--------------------------------------------------------------------------

	public final Module
	removeJDBCColumn(String name) {
		if (m_jdbc_columns != null)
			m_jdbc_columns.removeIf(new Predicate<JDBCColumn>() {
				@Override
				public boolean test(JDBCColumn column) {
					return column.name.equals(name);
				}
			});
		return this;
	}

	//--------------------------------------------------------------------------

	public final Module
	setActive(boolean active, DBConnection db) {
		m_active = active;
		store(db);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Module
	setCanBeDeleted(boolean can_be_deleted) {
		m_can_be_deleted = can_be_deleted;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Module
	setDisplayName(String display_name) {
		m_display_name = display_name;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Module
	setIsSecured(boolean is_secured) {
		m_is_secured = is_secured;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Module
	setRequired(boolean required) {
		m_required = required;
		return this;
	}

	//--------------------------------------------------------------------------

	private ui.Form
	settingsFormOpen(Request r) {
		if (m_can_be_deleted)
			r.w.js("Dialog.top().add_delete(function(){Dialog.confirm(null,'Delete " + getDisplayName(r) + "?','Delete',function(){net.post('" + apiURL("remove") + "',null,function(){app.refresh_page_menu();Dialog.top().close()})})})");
		String name = URLBuilder.encode(m_name);
		ui.Form f = new ui.Form(name, apiURL("settings", "save"), r.w);
		return f.open();
	}

	//--------------------------------------------------------------------------

	public final boolean
	showOnAdminPage() {
		if (m_name.charAt(0) == '_')
			return false;
		Class<?> c = getClass();
		if (DBObject.getFields(c, JSONField.class).size() > 0)
			return true;
		for (Method method : c.getMethods())
			if (method.isAnnotationPresent(ClassTask.class))
				return true;
			else if (method.getName().equals("writeAdminPane") && method.getDeclaringClass() == c)
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public final void
	store(DBConnection db) {
		if (!db.tableExists("_modules_"))
			db.createTable("_modules_", "class VARCHAR,name VARCHAR,json VARCHAR");
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("class", getClass().getName());
		nvp.set("name", m_name);
		DBObject.store(this, "_modules_", nvp, db);
	}

	//--------------------------------------------------------------------------

	protected void
	updateSettings(Request r) {
		DBObject.set(this, r);
		store(r.db);
		r.response.js("if(!Dialog.top())_.form.show_saved('#" +r.getParameter("db_submit_id") + "')");
		for (ViewDef view_def : m_view_defs) {
			r.removeSessionAttribute("ViewState:" + view_def.getName());
			Site.site.removeViewDef(view_def.getName());
		}
		m_view_defs.clear();
		adjustTables(r.db);
		Site.site.publish(this, Message.UPDATE, null, r.db);
	}

	//--------------------------------------------------------------------------

	public void
	writeAdminPane(String[] column_names, boolean show_tasks, boolean show_object, Request r) {
		HTMLWriter w = r.w;
		w.ui.buttonOnClick(Text.settings, "new Dialog({cancel:true,modal:true,ok:true,title:'" + JS.escape(getDisplayName(r)) + " Settings',url:'" + apiURL("settings") + "'})");
		if (show_tasks) {
			boolean first = true;
			Class<?> c = getClass();
			Map<String,Method> methods = new TreeMap<>();
			try {
				for (Method method : c.getMethods())
					if (method.isAnnotationPresent(ClassTask.class))
						methods.put(method.getName(), method);
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			}
			for (Method method : methods.values()) {
				boolean has_form;
				try {
					c.getMethod(method.getName() + "Form", Request.class);
					has_form = true;
				} catch (NoSuchMethodException e) {
					has_form = false;
				}
				if (!has_form)
					for (Class<?> type : method.getParameterTypes())
						if (type != Request.class && type != Site.class && type != DBConnection.class) {
							has_form = true;
							break;
						}
				if (first) {
					w.h3("Actions");
					w.tagOpen("table");
					w.write("<tr><td style=\"vertical-align:top;\">");
					first = false;
				}
				w.tagOpen("p");
				w.aOnClick(method.getName() + "...", "net.replace(this.closest('td').nextElementSibling,'" + apiURL(has_form ? "method form" : "method", method.getName()) + "')");
				w.tagClose();
			}
			if (!first) {
				w.write("</td><td id=\"form_td\" style=\"vertical-align:top;\"></td></tr>");
				w.tagClose();
			}
		}
		if (show_object) {
			w.h3("Object");
			DBObject.write(this, w);
		}
	}

	//--------------------------------------------------------------------------

	public NavBar
	writePageMenu(Request r) {
		return null;
	}

	//--------------------------------------------------------------------------

	public void
	writeSettingsForm(String[] field_names, boolean in_dialog, boolean hide_active, Request r) {
		Map<String,Field> fields = DBObject.getFields(this.getClass(), JSONField.class);
		if (fields.size() == 0)
			return;
		if (r.userIsAdministrator())
			r.w.h4(this.getClass().getTypeName());
		ui.Form f = settingsFormOpen(r);
		if (in_dialog)
			f.setButtonsLocation(null);
		Map<String,String> names = new TreeMap<>();
		DBObject.gatherNames(this.getClass(), field_names, names, r);
		HTMLWriter w = r.w;
		for (String name : names.values()) {
			if (name.equals("m_active") && (hide_active || m_required) && !r.userIsAdministrator())
				continue;
			if (name.equals("m_display_name") && !m_renameable)
				continue;
			Field field = fields.get(name);
			if (DBObject.isSubsetting(field, fields) && (field_names == null || Array.indexOf(field_names, name) == -1))
				continue;
			if (field.getAnnotation(JSONField.class).admin_only() && !r.userIsAdministrator())
				continue;
			if (field.getAnnotation(JSONField.class).admin_only() || field_names != null && Array.indexOf(field_names, name) == -1 || name.equals("m_active") && (hide_active || m_required))
				w.addClass("border-start ps-1");
			f.rowOpen(field.getType() == boolean.class && field.getAnnotation(JSONField.class).choices().length == 0 ? null : DBObject.getFieldLabel(field));
			String[] subfields = field.getAnnotation(JSONField.class).fields();
			w.write("<div>");
			if (subfields.length > 0 && field.getType() == boolean.class)
				w.setAttribute("onchange", "var d=this.parentNode.parentNode.nextElementSibling;d.style.display=this.checked?'block':'none';if(this.checked){d.querySelectorAll('textarea').forEach(function(i){_.form.resize_textarea(i);});}");
			writeSettingsInput(field, r);
			w.write("</div>");
			if (subfields.length > 0) {
				w.addStyle("padding-left:30px");
				try {
					if (!field.getBoolean(this))
						w.addStyle("display:none");
				} catch (IllegalArgumentException | IllegalAccessException e) {
				}
				w.tagOpen("div");
				for (String subsetting : subfields)
					if (field_names == null || Array.indexOf(field_names, subsetting) == -1)
						writeSubsettingInput(fields.get(subsetting), fields, r);
				w.tagClose();
			}
		}
		writeSettingsFormOtherRows(f, r);
		f.close();
	}

	//--------------------------------------------------------------------------
	/**
	 * @param f
	 * @throws IOException
	 */

	protected void
	writeSettingsFormOtherRows(Form f, Request r) {
	}

	//--------------------------------------------------------------------------

	protected void
	writeSettingsInput(Field field, Request r) {
		if (Site.site.writeSettingsInput(this, field, r)) {
			String after = field.getAnnotation(JSONField.class).after();
			if (after.length() > 0)
				r.w.ui.helpText(after);
			return;
		}
		DBObject.writeInput(this, field, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeSubsettingInput(Field field, Map<String,Field> fields, Request r) {
		HTMLWriter w = r.w;
		w.write("<div style=\"padding:8px 0\"");
		if (field.getAnnotation(JSONField.class).admin_only())
			w.write(" class=\"alert-info\"");
		w.write(">");
		if (field.getType() != boolean.class || field.getAnnotation(JSONField.class).choices().length > 0) {
			String label = DBObject.getFieldLabel(field);
			if (!"null".equals(label))
				w.addStyle("padding-right:1ex;vertical-align:middle")
					.ui.formLabel(label);
		}
		w.write("<div>");
		String[] subfields = field.getAnnotation(JSONField.class).fields();
		if (subfields.length > 0 && field.getType() == boolean.class)
			w.setAttribute("onchange", "var d=this.parentNode.parentNode.nextElementSibling;d.style.display=this.checked?'block':'none';if(this.checked){d.querySelectorAll('textarea').forEach(function(i){_.form.resize_textarea(i);});}");
		writeSettingsInput(field, r);
		w.write("</div>");
		if (subfields.length > 0) {
			w.write("<div style=\"padding-left:20px;");
			try {
				if (!field.getBoolean(this))
					w.write("display:none;");
			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			}
			w.write("\">");
			for (String subsetting : subfields)
				writeSubsettingInput(fields.get(subsetting), fields, r);
			w.write("</div>");
		}
		w.write("</div>");
	}
}
