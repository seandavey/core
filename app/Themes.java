package app;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Field;
import java.util.Arrays;

import db.column.OptionsColumn;
import db.object.JSONField;

public class Themes extends Module {
	@JSONField
	private boolean		m_allow_users_to_set_their_own_theme = true;
	@JSONField
	private String		m_default_theme = "site";
	private String[]	m_themes;

	// --------------------------------------------------------------------------

	public
	Themes() {
		m_required = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String theme = r.getPathSegment(1);
		if ("set".equals(r.getPathSegment(2))) {
			Person user = r.getUser();
			if (user != null) {
				r.db.update("people", "theme='" + theme + "'", user.getId());
				r.removeSessionAttribute("user");
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public final OptionsColumn
	getColumn() {
		return new OptionsColumn("theme", getThemes()).setDefaultValue(m_default_theme);
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks(Request r) {
		return new String[][] { { getDisplayName(r), "settings" } };
	}

	//--------------------------------------------------------------------------

	public final String
	getDefaultTheme() {
		return m_default_theme;
	}

	//--------------------------------------------------------------------------

	public final String
	getTheme(Request r) {
		String theme = null;
		if (m_allow_users_to_set_their_own_theme || r.userIsAdministrator()) {
			Person user = r.getUser();
			if (user != null)
				theme = user.getTheme();
		}
		if (theme == null)
			theme = m_default_theme;
		if ("site".equals(theme))
			return "site";
		return "/themes/" + theme + "/" + theme;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getThemes() {
		if (m_themes != null)
			return m_themes;
		m_themes = Site.site.getPath("themes").toFile().list(new FilenameFilter(){
			@Override
			public boolean
			accept(File dir, String filename) {
				return filename.charAt(0) != '.';
			}
		});
		if (m_themes != null && m_themes.length > 0)
			Arrays.sort(m_themes);
		return m_themes;
	}

	//--------------------------------------------------------------------------

	public final Themes
	setDefaultTheme(String default_theme) {
		m_default_theme = default_theme;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsInput(Field field, Request r) {
		if (field.getName().equals("m_default_theme")) {
			String[] themes = getThemes();
			if (themes != null && themes.length > 0) {
				if (!m_allow_users_to_set_their_own_theme)
					r.w.setAttribute("onchange", "var t=this.options[this.selectedIndex].text;net.post('" + absoluteURL() + "/'+t+'/set',null,function(){app.set_theme(this,t)}.bind(this))");
				r.w.ui.select("default_theme", themes, m_default_theme, null, false);
			}
			return;
		}
		super.writeSettingsInput(field, r);
	}
}
