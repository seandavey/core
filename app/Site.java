package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Collator;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import db.DBConnection;
import db.Mods;
import db.NameValuePairs;
import db.Result;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.object.DBObjects;
import db.object.JSONField;
import jakarta.mail.Session;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mail.Mail;
import pages.Page;
import pages.Pages;
import ui.Form;
import ui.NavBar;
import util.Array;
import util.Text;
import util.Time;
import util.TimeFormatter;
import web.HTMLWriter;
import web.Head;
import web.Resources;
import web.URLBuilder;

public abstract class Site {
	public enum Message { INSERT, DELETE, UPDATE }

	public static final String HOME_PAGE = "home page";
	public static final String SITE_DISPLAY_NAME = "site display name";

	public static Collator		collator = Collator.getInstance();
	public static String		context;
	public static Credentials	credentials = new Credentials();
	public static Pages			pages = new Pages(collator);
	public static Settings		settings = new Settings();
	public static Site			site;
	public static Themes		themes = new Themes();
	private static boolean		loaded;

	protected String						m_account_requests_email;
	protected boolean						m_allow_account_requests;
	protected final String					m_base_path;
	private final DataSource				m_data_source;
	private final String					m_default_role;
	private String							m_domain;
	private Session							m_mail_session;
	protected Class<MailTemplate>			m_mail_template_class;
	private final Map<String,DBObjects<?>>	m_objects = new TreeMap<>();
	private	final Map<Integer,String>		m_people = new HashMap<>();
	private final Resources					m_resources;
	private final ArrayList<Object>			m_subscribers = new ArrayList<>();
	private final List<Thread>				m_threads = new ArrayList<>();
	private final Map<String,Page>			m_user_dropdown_items = new TreeMap<>();
	private final Map<String,ViewDef>		m_view_defs = new TreeMap<>();

	//--------------------------------------------------------------------------

	public
	Site(String base_path, String home_page, String default_role) {
		m_base_path = base_path;
		m_data_source = getDataSource();
		m_default_role = default_role;
		m_resources = new Resources(base_path, isDevMode());
		m_view_defs.put(settings.getViewDef().getName(), settings.getViewDef());
		collator.setStrength(Collator.TERTIARY);
		settings.setDefault(HOME_PAGE, "\"" + home_page + "\"");
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	absoluteURL(String... segments) {
		URLBuilder url = new URLBuilder("https://").append(m_domain).append(context);
		for (String s: segments)
			url.segment(s);
		return url;
	}

	//--------------------------------------------------------------------------

	public void
	addAddresses(ArrayList<String> addresses, DBConnection db) {
	}

	//--------------------------------------------------------------------------

	public final void
	addObjects(DBObjects<?> objects) {
		String name = objects.getName().toLowerCase();
		if (m_objects.containsKey(name)) {
			System.out.println("site.m_objects already contains " + name);
			throw new RuntimeException("site.m_objects already contains " + name);
		}
		m_objects.put(objects.getName().toLowerCase(), objects);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"first","last","email","send activate email","make admin"})
	public Result
	addPerson(String first, String last, String email, boolean send_activate_email, boolean make_admin, Request r) {
		String username = Text.uuid();
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("active", true)
			.set("first", first)
			.set("last", last)
			.set("email", email)
			.set("password", Text.uuid())
			.set("user_name", username)
			.set("theme", themes.getDefaultTheme())
			.set("uuid", Text.uuid());
		if (!send_activate_email)
			nvp.set("reset_id", Text.uuid());
		Result result = r.db.insert("people", nvp);
		if (result.error != null)
			return result;
		if (m_default_role != null)
			r.db.insert("user_roles", "people_id,user_name,role", result.id + ",'" + username + "','" + m_default_role + "'");
		if (make_admin)
			r.db.insert("user_roles", "people_id,user_name,role", result.id + ",'" + username + "','admin'");
		if (send_activate_email)
			credentials.sendEmail(result.id, true, r.db);
		return result;
	}

	//--------------------------------------------------------------------------

	public final void
	addThread(Thread thread) {
		m_threads.add(thread);
	}

	//--------------------------------------------------------------------------

	public final void
	addUserDropdownItem(Page p) {
		p.setShowInOther(false);
		pages.add(p, null);
		m_user_dropdown_items.put(p.getName(), p);
	}

	//--------------------------------------------------------------------------

	public final boolean
	allowAccountRequests() {
		return m_allow_account_requests;
	}

	//--------------------------------------------------------------------------

	public static boolean
	areEqual(Object a, Object b) {
		if (a == null)
			return b == null;
		if (b == null)
			return false;
		return a.equals(b);
	}

	//--------------------------------------------------------------------------

	public final void
	clearName(int id) {
		m_people.remove(id);
	}

	//--------------------------------------------------------------------------

	public final void
	destroy() {
		site = null;
		for (Thread thread : m_threads)
			thread.interrupt();
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	public boolean
	doGet(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		String path = http_request.getServletPath();
		if (path.endsWith("get request form")) {
			http_response.setContentType("text/html");
			HTMLWriter w = new HTMLWriter(http_response.getWriter());
			writeAccountRequestForm(w);
			return true;
		}
		if (path.startsWith("/impersonate/")) {
			Request r = new Request(http_request, path_segments, http_response);
			if (r.userIsAdministrator())
				impersonate(r.getPathSegmentInt(1), r);
			r.release();
			http_response.sendRedirect(context + "/" + getHomePage());
			return true;
		}
		switch(path) {
		case "/people select":
			http_response.setContentType("text/html");
			Request r = new Request(http_request, path_segments, http_response);
			new RowsSelect(null, new Select("id,first,last").from("people").where("active").orderBy("first,last"), "first,last", "id", r).write(r.w);
			r.release();
			return true;
		case "/stop_impersonating":
			r = new Request(http_request, path_segments, http_response);
			Avatar avatar = (Avatar)r.getSessionAttribute("avatar");
			if (avatar != null) {
				avatar.destroy();
				r.removeSessionAttribute("avatar");
			}
			r.release();
			http_response.sendRedirect(context + "/" + getHomePage());
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	public boolean
	doPost(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		String path = http_request.getServletPath();
		switch(path) {
		case "/request account":
			if (m_allow_account_requests) {
				Request r = new Request(http_request, path_segments, http_response);
				handleAccountRequest(r);
				r.release();
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public void
	everyFiveMinutes(LocalDateTime now, DBConnection db) {
		Modules.everyFiveMinutes(now, db);
	}

	//--------------------------------------------------------------------------

	public void
	everyHour(LocalDateTime now, DBConnection db) {
		Modules.everyHour(now, db);
	}

	//--------------------------------------------------------------------------

	public void
	everyNight(LocalDateTime now, DBConnection db) {
		Modules.everyNight(now, db);
	}

	//--------------------------------------------------------------------------

	public final Collection<DBObjects<?>>
	getAllObjects() {
		return m_objects.values();
	}

	//--------------------------------------------------------------------------

	public Class<?>[]
	getClasses() {
		return null;
	}

	//--------------------------------------------------------------------------

	public final String
	getDatabaseName() {
		String p = m_base_path;
		if (p.endsWith("/ROOT"))
			p = p.substring(0, p.length() - 5);
		return p.substring(p.lastIndexOf('/') + 1);
	}

	//--------------------------------------------------------------------------

	private DataSource
	getDataSource() {
		try {
			return (DataSource)((Context)new InitialContext().lookup("java:/comp/env")).lookup("jdbc/db");
		} catch (NamingException e) {
			System.out.println(e.toString());
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public final String
	getDefaultRole() {
		return m_default_role;
	}

	//--------------------------------------------------------------------------

	public final String
	getDisplayName() {
		return settings.getString(SITE_DISPLAY_NAME);
	}

	//--------------------------------------------------------------------------

	public final String
	getDomain() {
		return m_domain;
	}

	//--------------------------------------------------------------------------

	public final String
	getEmailAddress(String name) {
		return name + "@" + m_domain;
	}

	//--------------------------------------------------------------------------

	public String
	getFooter() {
		return null;
	}

	//--------------------------------------------------------------------------

	public final String
	getHomePage() {
		return settings.getString(HOME_PAGE);
	}

	//--------------------------------------------------------------------------

	public static Site
	getInstance(HttpServletRequest request) {
		if (site == null)
			newInstance(request);
		while (!Site.loaded)
		    try {
		        Thread.sleep(100);
		    } catch (InterruptedException e) {
		        System.err.format("InterruptedException : %s%n", e);
		    }
		return site;
	}

	// --------------------------------------------------------------------------

	public final Session
	getMailSession() {
		if (m_mail_session == null) {
			Properties properties = new Properties();
//			properties.put("mail.smtp.debug", "true");
			String smtp = settings.getString("smtp");
			if (smtp != null) {
				properties.put("mail.smtp.host", smtp);
				String port = settings.getString("smtp port");
				if (port != null)
					properties.put("mail.smtp.port", port);
				properties.put("mail.smtp.starttls.enable", "true");
			}
			m_mail_session = Session.getInstance(properties);
//				m_session.setDebug(true);
//				m_session.setDebugOut(System.out);
		}
		return m_mail_session;
	}

	//--------------------------------------------------------------------------

	public final String
	getNoreplyAddress() {
		return "noreply@" + m_domain;
	}

	//--------------------------------------------------------------------------

	public final DBObjects<?>
	getObjects(String name) {
		return m_objects.get(name.toLowerCase());
	}

	//--------------------------------------------------------------------------

	public final Path
	getPath(String ...segments) {
		return Paths.get(m_base_path, segments);
	}

	// --------------------------------------------------------------------------

	public final Resources
	getResources() {
		return m_resources;
	}

	//--------------------------------------------------------------------------

//	public final List<Thread>
//	getThreads() {
//		return m_threads;
//	}

	//--------------------------------------------------------------------------

	public final String
	getTimeZone() {
		return settings.getString("time zone");
	}

	//--------------------------------------------------------------------------

	public final ViewDef
	getViewDef(final String name, DBConnection db) {
		ViewDef view_def = m_view_defs.get(name);
		if (view_def != null)
			return view_def;
		view_def = newViewDef(name);
		if (view_def != null) {
			Mods.mod(view_def);
			m_view_defs.put(name, view_def);
		}
		return view_def;
	}

	//--------------------------------------------------------------------------

	public final Map<String,ViewDef>
	getViewDefs() {
		return m_view_defs;
	}

	//--------------------------------------------------------------------------

	private Result
	handleAccountRequest(Request r) {
		String first = r.getParameter("first");
		String last = r.getParameter("last");
		String email = r.getParameter("email");
		Result result = addPerson(first, last, email, false, false, r);
		if (result != null && result.error != null) {
			r.response.addError(result.error);
			return result;
		}
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("active", false);
		String fields = setAccountRequestFields(result.id, nvp, r);
		r.db.update("people", nvp, result.id);
		if (m_account_requests_email != null) {
			String content = "Hello,\n  a request for an account on " +	getDisplayName() +  " has been submitted with the following details:\nfirst: " + first + "\nlast: " + last + "\nemail: " + email;
			if (fields != null)
				content += fields;
			content += "\n\nTo respond to the request, go to the Edit People page on the site, change the popup filter to \"Account Requests\", and then either delete the person or edit them and click \"reset and send email\" in the Password field. The later will send the person an email welcoming them to the site and including a link to the activate_account.jsp page.";
			new Mail(getDisplayName() + " Account Request", content)
				.to(m_account_requests_email)
				.send();
		}
		r.response.js("Dialog.alert('Request Submitted','Thank you for submitting you request for an account. We will contact you with more details.');_.$('#login_form').style.display='';_.$('#request_form').innerHTML=''");
		return result;
	}

	//--------------------------------------------------------------------------

	private void
	impersonate(int id, Request r) {
		r.setSessionAttribute("avatar", new Avatar(newPerson(id, r.db)));
	}

	//--------------------------------------------------------------------------

	public void
	init(DBConnection db) {
		Modules.init(db);
	}

	//--------------------------------------------------------------------------

	public final boolean
	isDevMode() {
		return m_base_path.startsWith("/Users");
	}

	//--------------------------------------------------------------------------

	public final String
	loadTokens(Path path) {
		File file = path.toFile();
		if (!file.exists() || file.length() == 0)
			return null;
		String ext = path.toString();
		int i = ext.lastIndexOf('.');
		if (i != -1) {
			ext = ext.substring(i + 1).toLowerCase();
			if (Array.indexOf(new String[] { "avi", "gif", "jpg", "jpeg", "mov", "mp3", "mp4", "png", "svg", "webp" }, ext) != -1)
				return null;
		}
		String jar_path = Paths.get(m_base_path, "tika-app.jar").toString();
		try {
			Process p = new ProcessBuilder("java", "-jar", jar_path, "-t", path.toString()).start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String text = br.lines().collect(Collectors.joining("\n"));
			int result = p.waitFor();
			if (result != 0) {
				System.out.println("tika-app returned " + result);
				br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				System.out.println(br.lines().collect(Collectors.joining("\n")));
			}
			return text;
		} catch (IOException e) {
			log(e);
		} catch (InterruptedException e) {
			log(e);
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public final void
	log(String mesesage, boolean print_stack_trace) {
		System.out.println(LocalDateTime.now().toString() + " - " + getDisplayName() + " - " + mesesage);
		if (print_stack_trace)
			Thread.dumpStack();
	}

	//--------------------------------------------------------------------------

	public final void
	log(String mesesage, String file) {
		try {
			FileWriter fw = new FileWriter(newFile(file), true);
			fw.write(LocalDateTime.now().toString() + " - " + mesesage + "\n");
			fw.close();
		} catch (IOException e) {
			log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	log(Exception e) {
		if (e != null) {
			log(e.toString(), false);
			e.printStackTrace(System.out);
		}
	}

	//--------------------------------------------------------------------------

	public final String
	lookupName(int id, DBConnection db) {
		String name = m_people.get(id);
		if (name == null) {
			name = db.lookupString("first,last", "people", id);
			m_people.put(id, name);
		}
		return name;
	}

	//--------------------------------------------------------------------------

	@ClassTask({"to","subject","content"})
	public final void
	mailTest(String to, String subject, String content) {
		new Mail(subject, content)
			.to(to)
			.send();
	}

	//--------------------------------------------------------------------------

	public final void
	makeDirectory(String ...directories) {
		try {
			Files.createDirectories(getPath(directories));
		} catch (IOException e) {
			log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final Connection
	newConnection() {
		try {
			return m_data_source.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	protected EditSite
	newEditSite() {
		return new EditSite();
	}

	//--------------------------------------------------------------------------

	public final File
	newFile(String ...segments) {
		return Paths.get(m_base_path, segments).toFile();
	}

	//--------------------------------------------------------------------------

	public final Head
	newHead(Request r) {
		r.w.setFooter(getFooter());
		return new Head(getDisplayName(), r).script("site-min").styleSheet(themes.getTheme(r));
	}

	//--------------------------------------------------------------------------

	public static synchronized void
	newInstance(HttpServletRequest r) {
		if (loaded)
			return;
		ServletContext servlet_context = r.getSession().getServletContext();
		String base_dir = servlet_context.getRealPath("admin");
		base_dir = base_dir.substring(0, base_dir.length() - 6);
		try {
			String site_class = servlet_context.getInitParameter("site_class");
			site = (Site)Class.forName(site_class,
				true,
				Thread.currentThread().getContextClassLoader()).getConstructor(new Class[] { String.class }).newInstance(new Object[] { base_dir });
			} catch (ClassNotFoundException | SecurityException | NoSuchMethodException | IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
				throw new RuntimeException(e);
			}
		Site.context = r.getContextPath();
		Time.formatter = new TimeFormatter(null);
		DBConnection db = new DBConnection();
		Site.settings.init(db);
		site.m_domain = Site.settings.getString("domain");
		if (site.m_domain == null)
			site.m_domain = r.getServerName();
		String time_zone = Site.settings.getString("time zone");
		Time.zone_id = time_zone != null ? ZoneId.of(time_zone) : ZoneId.systemDefault();
		if (site.m_default_role != null) {
			Roles.add(site.m_default_role, "default role for site");
			Roles.add("guest", "for guests on the site. guests should not have the " + site.m_default_role + " role and regular members should not have this role.");
		}
		Modules.add(credentials, db);
		Modules.add(pages, db);
		Modules.add(themes, db);
		Modules.add(new Admin(), db);
		site.init(db);
		pages.loadPages(db);
		db.close();
		loaded = true;
	}

	// --------------------------------------------------------------------------

	public MailTemplate
	newMailTemplate() {
		return null;
	}

	//--------------------------------------------------------------------------

	public Person
	newPerson(Request r) {
		return new Person(r);
	}

	//--------------------------------------------------------------------------

	public Person
	newPerson(int id, DBConnection db) {
		return new Person(id, db);
	}

	//--------------------------------------------------------------------------

	public final Object
	newState(Class<?> state_class, String name, Request r) {
		if (state_class == ViewState.class)
			return new ViewState(getViewDef(name, r.db), r);
		try {
			return state_class.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public final View
	newView(String name, Request r) {
		ViewDef view_def = getViewDef(name, r.db);
		if (view_def != null)
			return view_def.newView(r);
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * classes that override this method should call super.newViewDef(name)
	 * @param name
	 * @return a new ViewDef object with appropriate settings
	 */

	protected ViewDef
	newViewDef(String name) {
		ViewDef view_def = Modules.newViewDef(name);
		if (view_def != null)
			return view_def;
		DBObjects<?> objects = m_objects.get(name.toLowerCase());
		if (objects != null)
			return objects.newViewDef();
		return new ViewDef(name);
	}

	//--------------------------------------------------------------------------

	public void
	onLogin(Person user, Request r) {
		r.db.update("people", "last_login='" + Time.formatter.getDateTime(Time.newDateTime()) + "'", "user_name=" + SQL.string(user.getUsername()));
	}

	//--------------------------------------------------------------------------

	public final void
	pageOpen(String current_page, boolean open_main, Request r) {
		pageOpen(current_page, newHead(r), open_main, r);
	}

	//--------------------------------------------------------------------------

	public void
	pageOpen(String current_page, Head head, boolean open_main, Request r) {
		if (head != null)
			head.close();
		if (open_main)
			r.w.mainOpen();
	}

	//--------------------------------------------------------------------------

	public final void
	publish(Object object, Message message, Object data, DBConnection db) {
		for (int i=0; i<m_subscribers.size(); i+=2)
			if (m_subscribers.get(i).equals(object))
				((Subscriber)m_subscribers.get(i+1)).publish(object, message, data, db);
	}

	//--------------------------------------------------------------------------

	public final URLBuilder
	relativeURL(String... segments) {
		URLBuilder url = new URLBuilder(context);
		for (String s: segments)
			url.segment(s);
		return url;
	}

	//--------------------------------------------------------------------------

	public final void
	removeObjects(String name) {
		m_objects.remove(name.toLowerCase());
	}

	//--------------------------------------------------------------------------

	public void
	removeViewDef(String name) {
		if (name != null)
			m_view_defs.remove(name);
	}

	//--------------------------------------------------------------------------

	public final void
	runScript(String script, String... arguments) {
		ArrayList<String> cmd = new ArrayList<>();
		script = getPath("scripts", script).toString();
		cmd.add(script);
		if (!new File(script).exists()) {
			log(script + " not found", false);
			return;
		}
		for (String argument : arguments)
			cmd.add(argument);
		try {
			log(Text.join(" ", cmd), false);
			Process p = new ProcessBuilder(cmd).start();
			int result = p.waitFor();
			if (result != 0) {
				System.out.println(script + " returned " + result);
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				System.out.println(br.lines().collect(Collectors.joining("\n")));
			}
		} catch (Exception e) {
			log(e);
		}
	}

	//--------------------------------------------------------------------------

	protected String
	setAccountRequestFields(int people_id, NameValuePairs nvp, Request r) {
		return null;
	}

	//--------------------------------------------------------------------------

	public final Site
	setTimeZone(String time_zone, DBConnection db) {
		Time.zone_id = time_zone != null && time_zone.length() > 0 ? ZoneId.of(time_zone) : ZoneId.systemDefault();
		settings.set("time zone", time_zone, db);
		return this;
	}

	//--------------------------------------------------------------------------

	protected final void
	startFiveMinuteThread() {
		Thread thread = new Thread(new FiveMinuteThread());
		addThread(thread);
		thread.start();
	}

	//--------------------------------------------------------------------------

	public final void
	subscribe(Object object, Subscriber subscriber) {
		synchronized(m_subscribers) {
			for (int i=0; i<m_subscribers.size(); i+=2)
				if (object.equals(m_subscribers.get(i)) && subscriber == m_subscribers.get(i+1))
					return;
			m_subscribers.add(object);
			m_subscribers.add(subscriber);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	unsubscribe(Object object, Subscriber subscriber) {
		synchronized(m_subscribers) {
			for (int i=0; i<m_subscribers.size(); i+=2)
				if (m_subscribers.get(i).equals(object) && m_subscribers.get(i+1).equals(subscriber)) {
					m_subscribers.remove(i);
					m_subscribers.remove(i);
					return;
				}
		}
	}

	//--------------------------------------------------------------------------

	public final void
	writeAccountRequestForm(HTMLWriter w) {
		w.h4("Request Account");
		Form f = new Form(null, null, w);
		f.open();
		f.rowOpen("First Name");
		w.setAttribute("required", "required")
			.ui.textInput("first", 0, null);
		f.rowOpen("Last Name");
		w.setAttribute("required", "required")
			.ui.textInput("last", 0, null);
		f.rowOpen("Email");
		w.setAttribute("required", "required")
			.emailInput("email", null);
		writeAdditionalAccountFields(f, w);
		f.setSubmitText(Text.submit);
		f.setOnClick("var f=this.closest('FORM');if(_.form.validate(f)){net.post(context+'/request account',new FormData(f))}");
		f.addButton(w.ui.button("Cancel").setOnClick("_.$('#login_form').style.display='';_.$('#request_form').innerHTML='';"));
		f.close();
	}

	//--------------------------------------------------------------------------

	protected void
	writeAdditionalAccountFields(Form f, HTMLWriter w) {
	}

	//--------------------------------------------------------------------------

	public boolean
	writeMethodFormControl(String name, Class<?> parameter_type, String label, Request r) {
		return false;
	}

	//--------------------------------------------------------------------------

	protected boolean
	writeSettingsInput(Object object, Field field, Request r) {
		String type = field.getAnnotation(JSONField.class).type();
		if ("role".equals(type)) {
			String name = field.getName();
			if (name.startsWith("m_"))
				name = name.substring(2);
			try {
				new ui.Select(name, Roles.getRoleNames()).setAllowNoSelection(true).setSelectedOption((String)field.get(object), null).write(r.w);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public final void
	writeUserDropdown(NavBar nav_bar, Request r) {
		Person user = r.getUser();
		if (user != null) {
			String name = user.getName();
			String picture = user.getPicture();
			nav_bar.dropdownOpen(picture != null ? "<img src=\"" + picture + "\" alt=\"\" style=\"margin-right:5px;max-height:40px;max-width:40px;\">" + name : name, true);
			writeUserDropdownItems(nav_bar, r);
			nav_bar.dropdownClose();
		} else
			nav_bar.a(m_allow_account_requests ? "Sign In/Request Account" : "Sign In", context + "/login");
	}

	//--------------------------------------------------------------------------

	protected void
	writeUserDropdownItems(NavBar nav_bar, Request r) {
		if (r.userIsAdministrator())
			((Admin)Modules.get("Admin")).writeUserDropdownItems(nav_bar, r);
		if (m_user_dropdown_items.size() > 0) {
			boolean at_least_one = false;
			for (Page page : m_user_dropdown_items.values())
				if (page.canView(r)) {
					page.a(nav_bar, r);
					at_least_one = true;
				}
			if (at_least_one)
				nav_bar.divider();
		}
		if (Modules.get("people") != null) {
			Person user = r.getUser();
			if (user != null)
				nav_bar.aOnClick("Profile", "Person.popup(" + user.getId() + ")");
			nav_bar.aOnClick("Settings", "new Dialog({cancel:true,ok:{text:'Save'},reload_page:true,title:'Settings',url:context+'/api/People/person settings'})");
		}
		nav_bar.aOnClick("Logout", "document.cookie='uuid=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';document.location=context+'/logout'");
		if (r.getSessionAttribute("avatar") != null)
			nav_bar.divider().aOnClick("Stop Impersonating", "document.location=context+'/stop_impersonating'");
	}
}
