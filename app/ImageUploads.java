package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

import jakarta.servlet.http.Part;
import util.Text;

public class ImageUploads extends Module {
	@Override
	public boolean
	doPost(Request r) {
		Part part = r.getPart("upload");
		String filename = part.getSubmittedFileName();
		String extension = filename.substring(filename.lastIndexOf('.')).toLowerCase();
		filename = Text.uuid() + extension;
		Path path = Site.site.getPath("uploads");
		path.toFile().mkdirs();
		path = path.resolve(filename);
		try {
			part.write(path.toString());
		} catch(IOException e) {
			System.out.println(e.toString());
		}
		r.response.add("url", Site.context + "/uploads/" + filename);
		return true;
	}

	//--------------------------------------------------------------------------

	public static void
	deleteFiles(String l) {
		if (l == null)
			return;
		String s = "<img src=\"" + Site.context + "/uploads/";
		int i = l.indexOf(s);
		while (i != -1) {
			i += s.length();
			String filename = l.substring(i, l.indexOf('"', i));
			File f = Site.site.getPath("uploads", filename).toFile();
			if (f.exists())
				f.delete();
			i = l.indexOf(s, i);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	deleteFiles(File file) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String l = br.readLine();
			while (l != null) {
				deleteFiles(l);
				l = br.readLine();
			}
			br.close();
		} catch (IOException e) {
		}
	}
}
