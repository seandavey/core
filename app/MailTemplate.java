package app;

public interface MailTemplate {
	public MailTemplate append(String s);

	public MailTemplate append(int i);

	public MailTemplate header(String title);
}
