package app;

import java.util.Hashtable;
import java.util.Map;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import db.DBConnection;
import db.DeleteHook;
import db.InsertHook;
import db.NameValuePairs;
import db.Rows;
import db.Select;
import db.UpdateHook;
import db.ViewDef;

public class Settings implements DeleteHook, InsertHook, UpdateHook {
	private final Map<String, JsonValue>	m_default_values = new Hashtable<>();
	private final Map<String, JsonValue>	m_values = new Hashtable<>();
	private final ViewDef					m_view_def;

	//--------------------------------------------------------------------------

	public
	Settings() {
		m_view_def = new ViewDef("_settings_")
			.setDefaultOrderBy("name")
			.setRecordName("Setting", true)
			.addDeleteHook(this)
			.addInsertHook(this)
			.addUpdateHook(this);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		String json = nvp.getString("json");
		if (json != null)
			m_values.put(nvp.getString("name"), Json.parse(json));
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		String json = nvp.getString("json");
		if (json != null)
			m_values.put(nvp.getString("name"), Json.parse(json));
	}

	//--------------------------------------------------------------------------

	@Override
	public final String
	beforeDelete(String where, Request r) {
		m_values.remove(r.db.lookupString("name", "_settings_", where));
		return null;
	}

	//--------------------------------------------------------------------------

	private static void
	ensureTableExists(DBConnection db) {
		if (!db.tableExists("_settings_"))
			db.createTable("_settings_", "name VARCHAR,json JSONB");
	}

	//--------------------------------------------------------------------------

	public final JsonValue
	get(String root, String ...names) {
		JsonValue value = m_values.get(root);
		if (value == null || value.isNull())
			return null;
		JsonObject o = value.asObject();
		for (String name : names) {
			JsonValue v = o.get(name);
			if (v == null || v.isNull())
				return null;
			if (!v.isObject())
				return v;
			o = v.asObject();
		}
		return o;
	}

	//--------------------------------------------------------------------------

	public final JsonArray
	getArray(String root, String ...names) {
		JsonValue a = get(root, names);
		if (a == null)
			return null;
		return a.asArray();
	}

	//--------------------------------------------------------------------------

	public final boolean
	getBoolean(String name) {
		String v = getString(name);
		if (v == null)
			return false;
		v = v.toLowerCase();
		return "yes".equals(v) || "y".equals(v) || "true".equals(v) || "t".equals(v) || "1".equals(v);
	}

	//--------------------------------------------------------------------------

//	public final double
//	getDouble(String name) {
//		try {
//			String string = getString(name);
//			if (string != null)
//				return Double.parseDouble(string);
//		} catch (NumberFormatException e) {
//		}
//		return 0;
//	}

	//--------------------------------------------------------------------------

//	public final float
//	getFloat(String name) {
//		try {
//			String string = getString(name);
//			if (string != null)
//				return Float.parseFloat(string);
//		} catch (NumberFormatException e) {
//		}
//		return 0;
//	}

	//--------------------------------------------------------------------------

	public final int
	getInt(String name) {
		JsonValue value = m_values.get(name);
		if (value == null) {
			value = m_default_values.get(name);
			if (value != null)
				return Integer.parseInt(value.asString());
			else
				return 0;
		}
		if (value.isNumber())
			return value.asInt();
		return 0;
	}

	//--------------------------------------------------------------------------

	public final JsonObject
	getObject(String root, String ...names) {
		JsonValue a = get(root, names);
		if (a == null)
			return null;
		return a.asObject();
	}

	//--------------------------------------------------------------------------

	public final String
	getString(String name) {
		JsonValue value = m_values.get(name);
		if (value == null)
			value = m_default_values.get(name);
		return value == null ? null : value.asString();
	}

	//--------------------------------------------------------------------------

	public final ViewDef
	getViewDef() {
		return m_view_def;
	}

	//--------------------------------------------------------------------------

	public final void
	init(DBConnection db) {
		if (db.tableExists("_settings_"))
			try (Rows rows = new Rows(new Select("name,json").from("_settings_"), db)) {
				while (rows.next()) {
					String json = rows.getString(2);
					if (json != null)
						m_values.put(rows.getString(1), Json.parse(json));
				}
			}
	}

	//--------------------------------------------------------------------------

//	public final void
//	set(String name, boolean b, DBConnection db) {
//		set(name, Json.value(b), db);
//	}

	//--------------------------------------------------------------------------

	public final void
	set(String name, int i, DBConnection db) {
		set(name, Json.value(i), db);
	}

	//--------------------------------------------------------------------------

	public final void
	set(String name, String s, DBConnection db) {
		set(name, Json.value(s), db);
	}

	//--------------------------------------------------------------------------

	public final void
	set(String name, JsonValue v, DBConnection db) {
		if (v == null)
			m_values.remove(name);
		else
			m_values.put(name, v);
		ensureTableExists(db);
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("name", name);
		String where = nvp.getWhereString("_settings_", db);
		nvp.set("json", v == null ? null : v.toString());
		db.updateOrInsert("_settings_", nvp, where);
	}

	//--------------------------------------------------------------------------

	public final void
	setDefault(String name, String value) {
		if (value != null)
			m_default_values.put(name, Json.parse(value));
	}

	//--------------------------------------------------------------------------

	public final void
	writeComponent(Request r) {
		ensureTableExists(r.db);
		Site.site.newView("_settings_", r).writeComponent();
	}
}
