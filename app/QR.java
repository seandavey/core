package app;

import java.io.ByteArrayOutputStream;
import java.util.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import web.HTMLWriter;

public class QR {

	// --------------------------------------------------------------------------

	public static void
	writeQRCode(String label, String secret, HTMLWriter w) {
		String url = "otpauth://totp/" + label.replaceAll(" ", "%20") + "?secret=" + secret;
        try {
        	Writer writer = new QRCodeWriter();
            BitMatrix bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, 350, 350);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", os);
            byte[] bytes = os.toByteArray();
//            BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
            w.img("data:image/png;base64," + Base64.getEncoder().encodeToString(bytes));
        } catch (Exception e) {
        	Site.site.log(e);
        }

	}

}
