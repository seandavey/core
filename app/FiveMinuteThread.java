package app;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

import db.DBConnection;
import util.Time;

class FiveMinuteThread implements Runnable {
	private final AtomicBoolean started = new AtomicBoolean();

	@Override
	public void
	run() {
		if (!started.compareAndSet(false, true))
			return;
		int hour = -1;
		try {
			Thread.sleep((long)(300000L + Math.random() * 300000L)); // wait between 5 and 10 minutes to stagger sites
		} catch (InterruptedException e) {
			return;
		}
		while (true) {
			DBConnection db = new DBConnection();
			try {
				LocalDateTime now = Time.newDateTime();
				Site.site.everyFiveMinutes(now, db);
				if (hour != now.getHour()) {
					hour = now.getHour();
					Site.site.everyHour(now, db);
					if (hour == 0 && !Site.site.isDevMode() && now.getDayOfMonth() != Site.settings.getInt("nightly date")) {
						Site.site.everyNight(now, db);
						Site.settings.set("nightly date", now.getDayOfMonth(), db);
					}
				}
			} catch (Exception e) {
				Site.site.log(e);
			}
			db.close();
			try {
				Thread.sleep(300000L); // 5 minutes
			} catch (InterruptedException e) {
				return;
			}
		}
	}
}